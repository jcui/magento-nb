<?php
class TM_SocialSuite_Adminhtml_Model_System_Config_Source_Tcount
{
     public function toOptionArray()
    {
        return array(
            array('value'=>'horizontal', 'label'=>Mage::helper('socialsuite')->__('Horizontal')),
            array('value'=>'vertical', 'label'=>Mage::helper('socialsuite')->__('Vertical')),
            array('value'=>'none', 'label'=>Mage::helper('socialsuite')->__('No Count')),
        );
    }
}
