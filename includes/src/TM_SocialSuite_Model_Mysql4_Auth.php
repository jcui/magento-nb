<?php
/**
 * DO NOT REMOVE OR MODIFY THIS NOTICE
 *
 * AffiliateSuite module for Magento - flexible banner management
 *
 * @author Templates-Master Team <www.templates-master.com>
 */

class TM_SocialSuite_Model_Mysql4_Auth extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('socialsuite/auth', 'id');
    }

    public function checkAuthUser($id, $email)
    {
        $getAuthData = $this->_getReadAdapter()->select()
            ->from(array('ssa' => $this->getTable('socialsuite/auth')), array('id', 'email'))
            ->where('ssa.facebook_id = ?', $id);

        $result = $this->_getReadAdapter()->fetchAll($getAuthData);
        if (count($result) > 0) {
            if ($result[0]['email'] === $email) {
                return true;
            } else {
                //change email address
            $model = Mage::getModel('socialsuite/auth')
                ->load($result[0]['id'])
                ->addData('email', $email);
            try {
                    $model->setId($id)->save();
                } catch (Exception $e){
                    $session->addError(Mage::helper('socialsuite')->__('Error update Email value.'));
                    $this->_redirect('customer/account/login');
            }
            return true;
            }
        }

        return false;
    }

    public function getLoginId($facebookId, $email)
    {
        $getAuthData = $this->_getReadAdapter()->select()
            ->from(array('ssa' => $this->getTable('socialsuite/auth')), array('customer_id'))
            ->where('ssa.facebook_id = ?', $facebookId)
            ->where('ssa.email = ?', $email);
        $result = $this->_getReadAdapter()->fetchAll($getAuthData);
        if (isset($result[0]['customer_id'])) {
            return $result[0]['customer_id'];
        }
        return false;

    }
}