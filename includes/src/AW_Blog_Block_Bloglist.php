<?php 
class AW_Blog_Block_Bloglist extends AW_Blog_Block_Abstract 
{
    /* Dipper */
    public function getPosts()
    {
        $cat_id = $this->getCatId();

        $collection = parent::_prepareCollection(array('addCatFilter' => $cat_id));

        parent::_processCollection($collection);
        return $collection;          

    }
    
	
}

?>