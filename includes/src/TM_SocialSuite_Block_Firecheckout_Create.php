<?php

class TM_SocialSuite_Block_Firecheckout_Create extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getFacebookLoginUrl()
    {
        $url = Mage::getUrl('socialsuite/auth/index');
        $module = $this->getRequest()->getModuleName();
        $controller = $this->getRequest()->getControllerName();
        $action = $this->getRequest()->getActionName();
        if ($module === 'firecheckout'
                && $controller === 'index'
                    && $action === 'index' ) {
            $url .= '?firecheckout=1';
        }
        return $url;
    }

    public function getGoogleLoginUrl()
    {
        $url = Mage::getUrl('socialsuite/auth/google');
        $module = $this->getRequest()->getModuleName();
        $controller = $this->getRequest()->getControllerName();
        $action = $this->getRequest()->getActionName();
        if ($module === 'firecheckout'
                && $controller === 'index'
                    && $action === 'index' ) {
            $url .= '?firecheckout=1';
        }
        return $url;
    }

    public function getGoogleImage()
    {
        $url = Mage::getBaseUrl('media') . 'socialsuite/' . Mage::getStoreConfig('socialsuite/google_login/firecheckout_image');
        return $url;
    }

    public function getFacebookImage()
    {
        $url = Mage::getBaseUrl('media') . 'socialsuite/' . Mage::getStoreConfig('socialsuite/facebook_login/firecheckout_image');
        return $url;
    }
}