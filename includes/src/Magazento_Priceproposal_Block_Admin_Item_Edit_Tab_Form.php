<?php
/*
 *  Created on May 3, 2011
 *  Author Ivan Proskuryakov - volgodark@gmail.com - Magazento.com
 *  Copyright Proskuryakov Ivan. Magazento.com © 2011. All Rights Reserved.
 *  Single Use, Limited Licence and Single Use No Resale Licence ["Single Use"]
 */
?>
<?php

class Magazento_Priceproposal_Block_Admin_Item_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {


    protected function _prepareForm() {
        $model = Mage::registry('priceproposal_item');
        $form = new Varien_Data_Form(array('id' => 'edit_form_item', 'action' => $this->getData('action'), 'method' => 'post'));
        $form->setHtmlIdPrefix('item_');
        $fieldset = $form->addFieldset('base_fieldset', array('legend' => Mage::helper('priceproposal')->__('General Information'), 'class' => 'fieldset-wide'));
        if ($model->getItemId()) {
            $fieldset->addField('item_id', 'hidden', array(
                'name' => 'item_id',
            ));
        }

        $fieldset->addField('title', 'text', array(
            'name' => 'title',
            'label' => Mage::helper('priceproposal')->__('Title'),
            'title' => Mage::helper('priceproposal')->__('Title'),
            'required' => true,
        ));
        $fieldset->addField('url', 'text', array(
            'name' => 'url',
            'label' => Mage::helper('priceproposal')->__('Url'),
            'title' => Mage::helper('priceproposal')->__('Url'),
            'required' => false,
        ));
        $fieldset->addField('image', 'text', array(
            'name' => 'image',
            'label' => Mage::helper('priceproposal')->__('Image'),
            'title' => Mage::helper('priceproposal')->__('Image'),
            'required' => false,
        ));
        
        $fieldset->addField('position', 'select', array(
            'name' => 'position',
            'label' => Mage::helper('priceproposal')->__('Position'),
            'title' => Mage::helper('priceproposal')->__('Position'),
            'required' => true,
            'options' => Mage::helper('priceproposal')->numberArray(20,Mage::helper('priceproposal')->__('')),
        ));
        if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField('store_id', 'multiselect', array(
                'name' => 'stores[]',
                'label' => Mage::helper('priceproposal')->__('Store View'),
                'title' => Mage::helper('priceproposal')->__('Store View'),
                'required' => true,
                'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            'style' => 'height:150px',
            ));
        } else {
            $fieldset->addField('store_id', 'hidden', array(
                'name' => 'stores[]',
                'value' => Mage::app()->getStore(true)->getId()
            ));
            $model->setStoreId(Mage::app()->getStore(true)->getId());
        }


        $fieldset->addField('is_active', 'select', array(
            'label' => Mage::helper('priceproposal')->__('Status'),
            'title' => Mage::helper('priceproposal')->__('Status'),
            'name' => 'is_active',
            'required' => true,
            'options' => array(
                '1' => Mage::helper('priceproposal')->__('Enabled'),
                '0' => Mage::helper('priceproposal')->__('Disabled'),
            ),
        ));

        $dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
        $fieldset->addField('from_time', 'date', array(
            'name' => 'from_time',
            'time' => true,
            'label' => Mage::helper('priceproposal')->__('From Time'),
            'title' => Mage::helper('priceproposal')->__('From Time'),
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => Varien_Date::DATETIME_INTERNAL_FORMAT,
            'format' => $dateFormatIso,
        ));

        $fieldset->addField('to_time', 'date', array(
            'name' => 'to_time',
            'time' => true,
            'label' => Mage::helper('priceproposal')->__('To Time'),
            'title' => Mage::helper('priceproposal')->__('To Time'),
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => Varien_Date::DATETIME_INTERNAL_FORMAT,
            'format' => $dateFormatIso,
        ));

//        print_r($model->getData());
//        exit();
//        $form->setUseContainer(true);
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

}
