<?php
/**
 * @copyright  Copyright (c) 2010 Capacity Web Solutions Pvt. Ltd  (http://www.capacitywebsolutions.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class CapacityWebSolutions_Inquiry_IndexController extends Mage_Core_Controller_Front_Action
{	
    public function indexAction()
    {
		 $this->loadLayout(array('default'));
		 $this->renderLayout();
    }
	
	public function delAction()
	{
		 $getUrl=Mage::getSingleton('adminhtml/url')->getSecretKey("adminhtml_mycontroller","delAction");
		
		$delid = $this->getRequest()->getParam('delid');
		if(!empty($delid))
		{
			$collection = Mage::getModel("inquiry/inquiry")->load($delid);
			
			if($collection->delete())
			{
				Mage::getSingleton('core/session')->addSuccess("Inquire deleted successfully.");
			}
			else
			{
				Mage::getSingleton('core/session')->addError("Sorry inquire is not deleted.");
			}
		}
		
		$this->_redirectReferer();
	}
	
	
	public function thanksAction()
    {
		 $this->loadLayout(array('default'));
		 $this->renderLayout();
		 if($_POST['SUBMIT']=='SUBMIT')
		 {
		 $fname =  $this->getRequest()->getParam("fname");
		 $lname =  $this->getRequest()->getParam("lname");
		 $company =  $this->getRequest()->getParam("company");
		 $taxvat =  $this->getRequest()->getParam("account_taxvat"); 
		 $address =  $this->getRequest()->getParam("address");
		 $city =  $this->getRequest()->getParam("city");
		 $state =  $this->getRequest()->getParam("state_id");
		 $country =  $this->getRequest()->getParam("country");
          $category =  $this->getRequest()->getParam("category");
		 $zip =  $this->getRequest()->getParam("zip");
		 $phone =  $this->getRequest()->getParam("phone");
		 $email =  $this->getRequest()->getParam("email");
		 $website =  $this->getRequest()->getParam("website");
		 $bdesc =  addslashes($this->getRequest()->getParam("bdesc"));
		 $headers = "";
		 $country1 = explode('$$$',$country);
         $category1 = explode('$$$',$category);
	$insertArr = array("firstname"=>$fname,"lastname"=>$lname,"company"=>$company,"address"=>$address,"taxvat"=>$taxvat,"city"=>$city,"state"=>$state,"country"=>$country,"category"=>$category,"zip"=>$zip,"phone"=>$phone,"email"=>$email,"website"=>$website,"desc"=>$bdesc,"iscustcreated"=>0,"status"=>1,"createddt"=>date('Y-m-d H:i:s')); 
				$collection = Mage::getModel("inquiry/inquiry");
			$collection->setData($insertArr); //
			$collection->save();//echo "<pre>";print_r($collection);die;

		
	 	$adminContent = '<table border="1">
							<tr>
								<td>
									<table border="0">
										<tr>
											<Td><label>Hello Administrator,</label></Td>
										</tr>
										<tr>
											<Td><p>Mr/Ms. '.$fname.' '.$lname.' have filled dealer inquiry form and details are below.</p></td>
										</tr>
										<tr>
												<td>
													<table border="0">
														<tr>
													<td><label>First Name:</label></td>
													<td><label>'.$fname.'</label></td>
												</tr>
												<tr>
													<td><label>Last Name:</label></td>
													<td><label>'.$lname.'</label></td>
												</tr>
												<tr>
													<td><label>Company:</label></td>
													<td><label>'.$company.'</label></td>
												</tr>
												<tr>
													<td><label>TAX/VAT Number:</label></td>
													<td><label>'.$taxvat.'</label></td>
												</tr>
												<tr>
													<td><label>Address:</label></td>
													<td><label>'.$address.'</label></td>
												</tr>

												<tr>
													<td><label>City:</label></td>
													<td><label>'.$city.'</label></td>
												</tr>
												<tr>
													<td><label>State/Province:</label></td>
													<td><label>'.$state.'</label></td>
												</tr>
												<tr>
													<td><label>Country:</label></td>
													<td><label>'.$country1[1].'</label></td>
												</tr>
                                                <tr>
													<td><label>Country:</label></td>
													<td><label>'.$category1[1].'</label></td>
												</tr>
												<tr>
													<td><label>ZIP/Postal Code:</label></td>
													<td><label>'.$zip.'</label></td>
												</tr>
												<tr>
													<td><label>Contact Phone Number:</label></td>
													<td><label>'.$phone.'</label></td>
												</tr>
												<tr>
													<td><label>Email:</label></td>
													<td><label>'.$email.'</label></td>
												</tr>
												<tr>
													<td><label>Website:</label></td>
													<td><label>'.$website.'</label></td>
												</tr>
												<tr>
													<td valign="top" width="15%"><label>Business Description:</label></td>
													<td><label>'.$bdesc.'</label></td>
												</tr>
												<tr><td colspan="2">&nbsp;</td></tr>
												<tr>
													<td colspan="2"><label>Thank You.</label></td>
												</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>';
		$adminSubject = "New Dealer Inquiry from dealer";
		$adminName = Mage::getStoreConfig('trans_email/ident_general/name'); //sender name
		$adminEmail = Mage::getStoreConfig('trans_email/ident_general/email');
		$headers  .= 'MIME-Version: 1.0'."\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From:'. $adminName.' <'.$adminEmail.'>';
		///Comment the mail functionality while using in local in below than ver1.4.1
		//beginning og mail functionality.......................
		mail($adminEmail,$adminSubject,$adminContent,$headers);
		
		$customerContent = '<table border="0">
									<tr>
										<td>
											<table border="0">
												<tr>
													<Td>Hello '.$fname.' '.$lname.',</Td>
												</tr>
												<tr>
													<Td><p>Thank you for contacting '.$adminName.'. A company representative will contact you with more information within two business days.</p></Td>
												</tr>
												<tr><td colspan="2">&nbsp;</td></tr>
												<tr>
													<td colspan="2">Thank You.</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>';
		$headers = "";
		$adminName = Mage::getStoreConfig('trans_email/ident_general/name'); //sender name
		$custSubject = "Thank you for contacting ".$adminName."";
		$headers  .= 'MIME-Version: 1.0'."\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From:'. $adminName.' <'.$adminEmail.'>';
		//end mail functionaliy...............
		mail($email,$custSubject,$customerContent,$headers);
    	} 
	}
}	
?>
