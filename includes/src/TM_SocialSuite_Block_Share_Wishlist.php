<?php

class TM_SocialSuite_Block_Share_Wishlist extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getWishlistProductsDataShare()
    {
        $items = Mage::helper('wishlist')->getWishlistItemCollection();
        $result = array();

        foreach ($items as $item) {
            $url = $item->getData('product')->getProductUrl();
            $result[] = array(text => $item->getName(), href => $url);
        }
        return $result;
    }

}