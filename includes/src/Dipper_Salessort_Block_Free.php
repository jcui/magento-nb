<?php 
class Dipper_Salessort_Block_Free extends Mage_Catalog_Block_Product_List
{
    
    public function __construct()
    {
        $this->addData(array(
            'cache_lifetime'=> 7200,
            'cache_tags'    => array(Mage_Core_Model_Store::CACHE_TAG)
        ));
    }
     
     public function _getProductCollection()
     {
        $order = (isset($_REQUEST['order'])) ? $_REQUEST['order'] : "name";
	$dir = (isset($_REQUEST['dir'])) ? $_REQUEST['dir'] : "asc";
    	$pri = (isset($_REQUEST['price'])) ? $_REQUEST['price'] : "price";
        $visibility = array(
                              Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                              Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG
                          );
        $storeId = Mage::app()->getStore()->getId();
        $_productCollection = Mage::getModel('catalog/product')->getCollection()
                                      ->addAttributeToSelect('*')
                                      ->addAttributeToFilter('visibility', $visibility)
                                      ->addAttributeToFilter('freeshipping','1') 
                                       ->addAttributeToSort($order, $dir,$pri)                                    
                                       ->setPageSize($this->get_prod_count())
         ->setCurPage($this->get_cur_page());
        return  $_productCollection;
     }
     
     
     
     function get_prod_count()
   {
      //unset any saved limits
      Mage::getSingleton('catalog/session')->unsLimitPage();
      return (isset($_REQUEST['limit'])) ? intval($_REQUEST['limit']) : 12;
   }// get_prod_count

   function get_cur_page()
   {
      return (isset($_REQUEST['p'])) ? intval($_REQUEST['p']) : 1;
   }
   
   function get_cur_order()
   {
      return (isset($_REQUEST['order'])) ? intval($_REQUEST['order']) : 'position';
   }
      function get_cur_price()
   {
      return (isset($_REQUEST['price'])) ? intval($_REQUEST['price']) : 'price';
   }
   function get_cur_dir()
   {
      return (isset($_REQUEST['dir'])) ? intval($_REQUEST['dir']) : 'desc';
   }
}

?>