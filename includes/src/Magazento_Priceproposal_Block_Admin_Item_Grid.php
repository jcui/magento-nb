<?php

/*
 *  Created on May 3, 2011
 *  Author Ivan Proskuryakov - volgodark@gmail.com - Magazento.com
 *  Copyright Proskuryakov Ivan. Magazento.com © 2011. All Rights Reserved.
 *  Single Use, Limited Licence and Single Use No Resale Licence ["Single Use"]
 */
?>
<?php

class Magazento_Priceproposal_Block_Admin_Item_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('PriceproposalGrid');
        $this->setDefaultSort('item_id');
        $this->setDefaultDir('ASC');
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('priceproposal/item')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $baseUrl = $this->getUrl();
        $this->addColumn('item_id', array(
            'header' => Mage::helper('priceproposal')->__('ID'),
            'align' => 'left',
            'index' => 'item_id',
        ));
//        $this->addColumn('product_id', array(
//            'header' => Mage::helper('priceproposal')->__('Product ID'),
//            'align' => 'left',
//            'index' => 'product_id',
//        ));
        $this->addColumn('product_name', array(
            'header' => Mage::helper('priceproposal')->__('Product'),
            'align' => 'left',
            'index' => 'product_name',
        ));

        $this->addColumn('competitior_name', array(
            'header' => Mage::helper('priceproposal')->__('Name'),
            'align' => 'left',
            'index' => 'competitior_name',
        ));

        $this->addColumn('competitior_price', array(
            'header' => Mage::helper('priceproposal')->__('Price'),
            'align' => 'left',
            'index' => 'competitior_price',
        ));
        $this->addColumn('competitior_link', array(
            'header' => Mage::helper('priceproposal')->__('URL'),
            'align' => 'left',
            'index' => 'competitior_link',
        ));
        $this->addColumn('user_email', array(
            'header' => Mage::helper('priceproposal')->__('Email'),
            'align' => 'left',
            'index' => 'user_email',
        ));
        $this->addColumn('user_phone', array(
            'header' => Mage::helper('priceproposal')->__('Phone'),
            'align' => 'left',
            'index' => 'user_phone',
        ));
        $this->addColumn('user_skype', array(
            'header' => Mage::helper('priceproposal')->__('Skype'),
            'align' => 'left',
            'index' => 'user_skype',
        ));
        $this->addColumn('comment', array(
            'header' => Mage::helper('priceproposal')->__('Comment'),
            'align' => 'left',
            'index' => 'comment',
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('priceproposal')->__('Status'),
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                0 => Mage::helper('priceproposal')->__('New'),
                1 => Mage::helper('priceproposal')->__('Seen'),
            ),
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header' => Mage::helper('priceproposal')->__('Store View'),
                'index' => 'store_id',
                'type' => 'store',
                'store_all' => true,
                'store_view' => true,
                'sortable' => false,
                'filter_condition_callback'
                => array($this, '_filterStoreCondition'),
            ));
        }

        $this->addColumn('from_time', array(
            'header' => Mage::helper('priceproposal')->__('Time'),
            'index' => 'from_time',
            'type' => 'datetime',
        ));

        $this->addColumn('action',
                array(
                    'header' => Mage::helper('priceproposal')->__('Action'),
                    'index' => 'item_id',
                    'sortable' => false,
                    'filter' => false,
                    'no_link' => true,
                    'width' => '100px',
                    'renderer' => 'priceproposal/admin_item_grid_renderer_action'
        ));
        $this->addExportType('*/*/exportCsv', Mage::helper('priceproposal')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('priceproposal')->__('XML'));
        return parent::_prepareColumns();
    }

    protected function _afterLoadCollection() {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    protected function _filterStoreCondition($collection, $column) {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        $this->getCollection()->addStoreFilter($value);
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('item_id');
        $this->getMassactionBlock()->setFormFieldName('massaction');
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('priceproposal')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('priceproposal')->__('Are you sure?')
        ));

        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('priceproposal')->__('Change status'),
            'url' => $this->getUrl('*/*/massStatus', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('priceproposal')->__('Status'),
                    'values' => array(
                        0 => Mage::helper('priceproposal')->__('New'),
                        1 => Mage::helper('priceproposal')->__('Seen'),
                    ),
                )
            )
        ));
        return $this;
    }

//    public function getRowUrl($row) {
//        return $this->getUrl('*/*/edit', array('item_id' => $row->getId()));
//    }

}
