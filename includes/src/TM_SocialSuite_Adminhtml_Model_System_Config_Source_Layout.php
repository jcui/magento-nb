<?php
class TM_SocialSuite_Adminhtml_Model_System_Config_Source_Layout
{
     public function toOptionArray()
    {
        return array(
            array('value'=>'button_count', 'label'=>Mage::helper('socialsuite')->__('button_count')),
            array('value'=>'standard', 'label'=>Mage::helper('socialsuite')->__('standard')),
            array('value'=>'box_count', 'label'=>Mage::helper('socialsuite')->__('box_count'))
        );
    }
}
