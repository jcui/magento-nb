<?php

class TM_SocialSuite_Block_Share_Order extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getOrderId()
    {
        return Mage::getSingleton('checkout/session')->getLastOrderId();
    }

    public function getOrderProductsDataShare()
    {
        $order = Mage::getModel('sales/order')->load($this->getOrderId());
        $_newProduct = Mage::getModel('catalog/product');
        $items = $order->getAllItems();
        $result = array();

        foreach ($items as $itemId => $item) {
            $url = $_newProduct->load($item->getData('product_id'))
                ->getProductUrl();
            $result[] = array('text' => $item->getName(), 'href' => $url);
        }
        return $result;
    }
}
