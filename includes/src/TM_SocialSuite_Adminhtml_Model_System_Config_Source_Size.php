<?php
class TM_SocialSuite_Adminhtml_Model_System_Config_Source_Size
{
     public function toOptionArray()
    {
        return array(
            array('value'=>'standard', 'label'=>Mage::helper('socialsuite')->__('standard')),
            array('value'=>'small', 'label'=>Mage::helper('socialsuite')->__('small')),
            array('value'=>'medium', 'label'=>Mage::helper('socialsuite')->__('medium')),
            array('value'=>'tall', 'label'=>Mage::helper('socialsuite')->__('tall'))
        );
    }
}
