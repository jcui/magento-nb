<?php
/*
 *  Created on May 3, 2011
 *  Author Ivan Proskuryakov - volgodark@gmail.com - Magazento.com
 *  Copyright Proskuryakov Ivan. Magazento.com © 2011. All Rights Reserved.
 *  Single Use, Limited Licence and Single Use No Resale Licence ["Single Use"]
 */
?>
<?php
class Magazento_Priceproposal_Model_Item extends Mage_Core_Model_Abstract
{
    const CACHE_TAG     = 'priceproposal_admin_item';
    protected $_cacheTag= 'priceproposal_admin_item';

    protected function _construct()
    {
        $this->_init('priceproposal/item');
    }

    public function createProposal($data)
    {
         $product_id=$data['product_id'];
         $product_name=Mage::getModel('catalog/product')->load($product_id)->getName(); //getting product object for particular product id
         $competitior_name=$data['competitor_name'];
         $competitior_price=$data['competitor_price'];
         $competitior_link=$data['competitor_url'];
         $user_email=$data['user_email'];
         $user_skype=$data['user_skype'];
         $user_phone=$data['user_phone'];
         $comment=$data['comment'];
         $status=0;
         
         $storeId= Mage::app()->getStore()->getId();
         $model = Mage::getModel('priceproposal/item');
         $model ->setData('store_id', $storeId);
         $model ->setData('product_id', $product_id);
         $model ->setData('product_name', $product_name);
         $model ->setData('competitior_name', $competitior_name);
         $model ->setData('competitior_price', $competitior_price);
         $model ->setData('competitior_link', $competitior_link);
         $model ->setData('user_email', $user_email);
         $model ->setData('user_skype', $user_skype);
         $model ->setData('user_phone', $user_phone);
         $model ->setData('comment', $comment);
         $model ->setData('status', $status);
         $model ->save();
    }

}
