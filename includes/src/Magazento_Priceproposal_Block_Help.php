<?php

class Magazento_Priceproposal_Block_Help extends Mage_Adminhtml_Block_System_Config_Form_Fieldset {

    public function render(Varien_Data_Form_Element_Abstract $element) {
        $content = '<p></p>';
        $content.= '<style>';
        $content.= '.magazento-help {
                        background:#FAFAFA;
                        border: 1px solid #CCCCCC;
                        margin-bottom: 10px;
                        padding: 10px;
                        height:auto;

                    }
                    .magazento-help h3 {
                        color: #EA7601;
                    }
                    .contact-type {
                        color: #EA7601;
                        font-weight:bold;
                    }
                    .magazento-help img {
                        border: 1px solid #CCCCCC;
                        float:left;
                        height:260px;
                    }
                    .magazento-help .info {
                        border: 1px solid #CCCCCC;
                        background:#E7EFEF;
                        padding: 5px 10px 0 5px;
                        margin-left:210px;
                        height:255px;
                    }
                    ';
        $content.= '</style>';


        $content.= '<div class="magazento-help">';
        $content.= '<img src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).'frontend/default/default/magazento/priceproposal/help.png" alt="www.magazento-help.com" />';
            $content.= '<div class="info">';
            $content.= '<h3> Additional Help</h3>';
            $content.= 'We recommend to add additional block near your product price - it will increase percent of customers feedbacks.<br/>';
            $content.= 'In demo I added it to this file <i> /app/design/frontend/default/default/template/catalog/product/view.phtml</i><br/>';
            $content.= '<b> &lt;?php echo $this-&gt;getLayout()-&gt;createBlock(\'core/template\')-&gt;setTemplate(\'magazento/priceproposal/data.phtml\')-&gt;toHtml(); ?&gt; </b>';
            $content.= '<br/>';
            $content.= '<br/>';
            $content.= '<span class="contact-type">Also if you have problems with displaying this extension :</span><br/>';
            $content.= '60% - is your custom magento template, and you could fix this problem with editing this extension <strong>style.css</strong> at <i>skin/frontend/default/default/magazento/priceproposal</i> <br/>';
            $content.= '30% - other extension or JQuery conflict - for soldving this problem you have to disable other custom magento extension and look how it works without them or try don`t include Jquery';
            $content.= '5% - cache <br/>';
            $content.= '4.999% - my bug  ( write me in skype: <i><strong>volgodark</i></strong>)  <br/> ';
            $content.= '0.001% - other stuff <br/> ';

            $content.= '</div>';
        $content.= '</div>';

        return $content;


    }


}
