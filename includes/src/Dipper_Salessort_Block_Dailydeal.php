<?php 
class Dipper_Salessort_Block_Dailydeal extends Mage_Catalog_Block_Product_List
{
    
    public function __construct()
    {
        $this->addData(array(
            'cache_lifetime'=> 7200,
            'cache_tags'    => array(Mage_Core_Model_Store::CACHE_TAG)
        ));
    }
     
     public function _getProductCollection()
     {
        $totalPerPage = ($this->getShowTotal()) ? $this->getShowTotal() : 5;
        
        $visibility = array(
                              Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                              Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG
                          );
        $todayStartOfDayDate  = Mage::app()->getLocale()->date()
                    ->setTime('00:00:00')
                    ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
                    
        $todayEndOfDayDate  = Mage::app()->getLocale()->date()
                    ->setTime('23:59:59')
                    ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
                    

        $storeId = Mage::app()->getStore()->getId();
     

        $_productCollection = Mage::getModel('catalog/product')->getCollection()
                                      ->addAttributeToSelect('*')
                                      ->addAttributeToFilter('visibility', $visibility)
                                      ->addAttributeToFilter('special_from_date', array('or'=> array(
                                            0 => array('date' => true, 'to' => $todayEndOfDayDate),
                                            1 => array('is' => new Zend_Db_Expr('null')))
                                          ), 'left') 
                                          ->addAttributeToFilter('special_to_date', array('or'=> array(
                                              0 => array('date' => true, 'from' => $todayStartOfDayDate),
                                              1 => array('is' => new Zend_Db_Expr('null')))
                                          ), 'left')  
                                          ->addAttributeToFilter(
                                            array(
                                                array('attribute' => 'special_from_date', 'is'=>new Zend_Db_Expr('not null')) 
                                                )
                                          )
                                      ->addUrlRewrite()                                     
                                      ->setPageSize($totalPerPage)->setCurPage(1); 
        return  $_productCollection;
     }
}
?>