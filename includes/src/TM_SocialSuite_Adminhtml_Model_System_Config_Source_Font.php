<?php
class TM_SocialSuite_Adminhtml_Model_System_Config_Source_Font
{
     public function toOptionArray()
    {
        return array(
            array('value'=>'', 'label'=>Mage::helper('socialsuite')->__('')),
            array('value'=>'arial', 'label'=>Mage::helper('socialsuite')->__('arial')),
            array('value'=>'lucida grande', 'label'=>Mage::helper('socialsuite')->__('lucida grande')),
            array('value'=>'segoe ui', 'label'=>Mage::helper('socialsuite')->__('segoe ui')),
            array('value'=>'tahoma', 'label'=>Mage::helper('socialsuite')->__('tahoma')),
            array('value'=>'trebuchet ms', 'label'=>Mage::helper('socialsuite')->__('trebuchet ms')),
            array('value'=>'verdana', 'label'=>Mage::helper('socialsuite')->__('verdana'))
        );
    }
}