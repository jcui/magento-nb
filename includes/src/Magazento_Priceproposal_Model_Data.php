<?php
/*
 *  Created on May 3, 2011
 *  Author Ivan Proskuryakov - volgodark@gmail.com - Magazento.com
 *  Copyright Proskuryakov Ivan. Magazento.com © 2011. All Rights Reserved.
 *  Single Use, Limited Licence and Single Use No Resale Licence ["Single Use"]
 */
?>
<?php
Class Magazento_Priceproposal_Model_Data {

    protected function getItemModel() {
        return Mage::getModel('priceproposal/item');
    }
    
    protected function getItemCollection() {
        $storeId = Mage::app()->getStore()->getId();
        $collection = $this->getItemModel()->getCollection();
        $collection->addFilter('is_active', 1);
        $collection->addStoreFilter($storeId);
        $collection->addOrder('position', 'ASC');

        $ext  = 'priceproposal';
        $configserial = $ext.'/licence/serial';
        $configlicence= $ext.'/licence/email';
        $salt = 'magazento.com';
        $serialmd5 = Mage::getStoreConfig($configserial);
        $email = Mage::getStoreConfig($configlicence);
        $emailmd5 = md5(md5($email.'.'.$ext.'.'.$salt));
        $emailmd5 = mb_substr($emailmd5,2,24);
        if ($emailmd5 != $serialmd5) {
            Mage::getSingleton('core/session')->addError('This '.$ext.' extension copy is unregistered now. For serial key visit <a target="_blank" title="Magento Extension Developers, Magento exensions" href="http://www.magazento.com/">Magazento.com</a> website.');
        }

        return $collection;
    }
    public function getItems() {
        return $this->getItemCollection();
    }
}
?>