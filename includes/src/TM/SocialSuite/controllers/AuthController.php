<?php

class TM_SocialSuite_AuthController extends Mage_Core_Controller_Front_Action
{
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    public function getCheckoutParam()
    {
        if ((int)$this->getRequest()->getParam('checkout') == 1) {
            return true;
        }
        return false;
    }

    public function getFireCheckoutParam()
    {
        if ((int)$this->getRequest()->getParam('firecheckout') == 1) {
            return true;
        }
        return false;
    }

    public function indexAction()
    {
        if ($this->_getSession()->isLoggedIn()) {
            if ($this->getCheckoutParam()) {
                $this->_redirect('checkout/onepage/index');
                return;
            }
            if ($this->getFireCheckoutParam()) {
                $this->_redirect('firecheckout/index/index');
                return;
            }
            $this->_redirect('customer/account/index');
            return;
        }
        $session = $this->_getSession();
        $state = md5(uniqid(rand(), TRUE));
        $session->setData('social_suite_facebook_state', $state);
        $redirectUri = Mage::getUrl('socialsuite/auth/ftunnel');
        if ($this->getCheckoutParam()) {
            $redirectUri = Mage::getUrl('socialsuite/auth/ftunnel') . "?checkout=1";
        }
        if ($this->getFireCheckoutParam()) {
            $redirectUri = Mage::getUrl('socialsuite/auth/ftunnel') . "?firecheckout=1";
        }
        $url = "http://www.facebook.com/dialog/oauth?scope=email&state="
            . $state ."&client_id="
            . Mage::getStoreConfig('socialsuite/general/appid')
            . "&redirect_uri=" . $redirectUri;

        $this->_redirectUrl($url);
    }

    public function ftunnelAction()
    {
        if ($this->_getSession()->isLoggedIn()) {
            if ($this->getCheckoutParam()) {
                $this->_redirect('checkout/onepage/index');
                return;
            }
            if ($this->getFireCheckoutParam()) {
                $this->_redirect('firecheckout/index/index');
                return;
            }
            $this->_redirect('customer/account/index');
            return;
        }

        $session = $this->_getSession();
        $app_id = Mage::getStoreConfig('socialsuite/general/appid');
        $app_secret = Mage::getStoreConfig('socialsuite/general/appsecret');
        $my_url = Mage::getUrl('socialsuite/auth/ftunnel');
        if ($this->getCheckoutParam()) {
            $my_url = Mage::getUrl('socialsuite/auth/ftunnel') . '?checkout=1';
        }
        if ($this->getFireCheckoutParam()) {
            $my_url = Mage::getUrl('socialsuite/auth/ftunnel') . '?firecheckout=1';
        }

        $code = $this->getRequest()->getParam('code');
        $sessionState = $session->getData('social_suite_facebook_state');

        if ($sessionState !== $this->getRequest()->getParam('state')) {
            $session->addError(Mage::helper('socialsuite')->__('Invalid request param "state".'));
            if ($this->getCheckoutParam()) {
                $this->_redirect('checkout/onepage/index');
                return;
            }
            if ($this->getFireCheckoutParam()) {
                $this->_redirect('firecheckout/index/index');
                return;
            }
            $this->_redirect('customer/account/login');
            return;
        }

        if ($this->getRequest()->getParam('error')) {
            $session->addError($this->getRequest()->getParam('error_description'));
            if ($this->getCheckoutParam()) {
                $this->_redirect('checkout/onepage/index');
                return;
            }
            if ($this->getFireCheckoutParam()) {
                $this->_redirect('firecheckout/index/index');
                return;
            }
            $this->_redirect('customer/account/login');
            return;
        }

        if (Mage::app()->getCookie()->get('socialsuite_facebook_access_token')) {
            $accessToken = Mage::app()->getCookie()->get('socialsuite_facebook_access_token');
        } else {
            $token_url = "https://graph.facebook.com/oauth/access_token?"
                . "client_id=" . $app_id . "&redirect_uri=" . urlencode($my_url)
                . "&client_secret=" . $app_secret . "&code=" . $code;

            $response = @file_get_contents($token_url);
            $params = null;
            parse_str($response, $params);

            $accessToken = $params['access_token'];

            Mage::app()->getCookie()->set('socialsuite_facebook_access_token', $accessToken, (int)$params['expires']);
        }

        //new accses
        $graphParams = array(
            'access_token' => $accessToken,
        );

        try {
            $graphUrl = "https://graph.facebook.com/me";
            $graph = new Zend_Http_Client();
            $graph->setUri($graphUrl);
            $graph->setParameterGet($graphParams);
            $response = $graph->request();
            $responseGraphBody = $response->getBody();
        } catch (Exception $e) {
            $session->addError($e->getMessage());
            if ($this->getCheckoutParam()) {
                $this->_redirect('checkout/onepage/index');
                return;
            }
            if ($this->getFireCheckoutParam()) {
                $this->_redirect('firecheckout/index/index');
                return;
            }
            $this->_redirect('customer/account/login');
            return;
        }

        $user = Zend_Json::decode($responseGraphBody);

        //new accses
        $firstName = $user['first_name'];
        $lastName = $user['last_name'];
        $email = $user['email'];
        $password = $this->_createRandomPassword();
        $facebookId = $user['id'];

        if ($email) {
            if (!$customer = Mage::registry('current_customer')) {
                $customer = Mage::getModel('customer/customer')->setId(null);
            }

            $magVersion = Mage::getVersion();
            $last = false;
            if (version_compare($magVersion, '1.5.0.0', '<')) {
                $last = true;
            }

            if ($last) {
                $customerData = array(
                    'firstname'     => $firstName,
                    'lastname'      => $lastName,
                    'email'         => $email,
                    'password'      => $password,
                    'confirmation'  => $password
                );
            } else {
                $customerForm = Mage::getModel('customer/form');
                $customerForm->setFormCode('customer_account_create')
                    ->setEntity($customer);

                $customerData = array(
                    'firstname' => $firstName,
                    'lastname'  => $lastName,
                    'email'     => $email,
                );
            }
            /* @var $customerForm Mage_Customer_Model_Form */

            $customerData['dob']="1990-01-01 20:00:00";
            /**
             * Initialize customer group id
             */

            $customer->getGroupId();

            try {
                
                if ($last) {
                    $customer->setData($customerData);
                    $customerErrors = $customer->validate();
                } else {
                    $customerErrors = $customerForm->validateData($customerData);
                }
                var_dump($customerErrors);
                if ($customerErrors !== true) {
                    $errors = array_merge($customerErrors, $errors);
                } else {
                    if (!$last) {
                        $customerForm->compactData($customerData);
                        $customer->setPassword($password);
                        $customer->setConfirmation($password);
                        $customerErrors = $customer->validate();
                    }

                    if (is_array($customerErrors)) {
                        $errors = array_merge($customerErrors, $errors);
                    }
                }
                $validationResult = count($errors) == 0;

                if (true === $validationResult) {
                    $customer->save();
                    $session->setData('facebook_access_token', $params['access_token']);
                    //add data to facebook table
                    $authModel = Mage::getModel('socialsuite/auth');
                    $authModel
                        ->setCustomerId($customer->getId())
                        ->setFacebookId($facebookId)
                        ->setEmail($email);

                    $authModel->save();

                    if ($customer->isConfirmationRequired()) {
                        $customer->sendNewAccountEmail('confirmation', $session->getBeforeAuthUrl());
                        $session->addSuccess($this->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.', Mage::helper('customer')->getEmailConfirmationUrl($customer->getEmail())));
                        $this->_redirectSuccess(Mage::getUrl('customer/account/index', array('_secure'=>true)));
                        return;
                    } else {
                        $session->setCustomerAsLoggedIn($customer);
                        $url = $this->_welcomeCustomer($customer);
                        if ($this->getCheckoutParam()) {
                            $this->_redirect('checkout/onepage/index');
                            return;
                        }
                        if ($this->getFireCheckoutParam()) {
                            $this->_redirect('firecheckout/index/index');
                            return;
                        }
                        $this->_redirectSuccess($url);
                        return;
                    }
                } else {
                    $session->setCustomerFormData($this->getRequest()->getPost());
                    if (is_array($errors)) {
                        foreach ($errors as $errorMessage) {
                            $session->addError($errorMessage);
                        }
                    } else {
                        Mage::app()->getCookie()->delete('socialsuite_facebook_access_token');
                        $session->addError($this->__('Invalid customer data'));
                    }
                }
            } catch (Mage_Core_Exception $e) {
                $session->setCustomerFormData($this->getRequest()->getPost());
                if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
                    //if facebook email exist

                    $auth = Mage::getResourceModel('socialsuite/auth');

                    $result = $auth->checkAuthUser($facebookId, $email);

                    if ($result) {

                        $userId = $auth->getLoginId($facebookId, $email);
                        if ($userId) {
                            $newCustomer = Mage::getModel('customer/customer')
                                 ->load($userId);

                            $session->setCustomer($newCustomer);
                            if ($this->getCheckoutParam()) {
                                $this->_redirect('checkout/onepage/index');
                                return;
                            }
                            if ($this->getFireCheckoutParam()) {
                                $this->_redirect('firecheckout/index/index');
                                return;
                            }
                            $this->_redirect('customer/account/index');
                            return;
                        }
                    } else {

                        //email not found auth table
                        $regCustomer = Mage::getModel('customer/customer')
                            ->setWebsiteId(Mage::app()->getWebsite()->getId())
                            ->loadByEmail($email);
                        $customerId = $regCustomer->getId();
                        $authModel = Mage::getModel('socialsuite/auth');
                        $authModel
                            ->setCustomerId($customerId)
                            ->setFacebookId($facebookId)
                            ->setEmail($email);

                        $authModel->save();

                        $newCustomer = Mage::getModel('customer/customer')
                             ->load($customerId);
                        $session->setCustomer($newCustomer);
                        if ($this->getCheckoutParam()) {
                            $this->_redirect('checkout/onepage/index');
                            return;
                        }
                        if ($this->getFireCheckoutParam()) {
                            $this->_redirect('firecheckout/index/index');
                            return;
                        }
                        $this->_redirect('customer/account/index');

                        return;
                    }
                } else {
                    Mage::app()->getCookie()->delete('socialsuite_facebook_access_token');
                    $message = $e->getMessage();
                }
                $session->addError($message);
            } catch (Exception $e) {
                Mage::app()->getCookie()->delete('socialsuite_facebook_access_token');
                $session->setCustomerFormData($this->getRequest()->getPost())
                    ->addException($e, $this->__('Cannot save the customer.'));
            }

        } else {
            $session->addError(Mage::helper('socialsuite')->__('Invalid request param "email".'));
            Mage::app()->getCookie()->delete('socialsuite_facebook_access_token');
            if ($this->getCheckoutParam()) {
                $this->_redirect('checkout/onepage/index');
                return;
            }
            if ($this->getFireCheckoutParam()) {
                $this->_redirect('firecheckout/index/index');
                return;
            }
            $this->_redirect('customer/account/login');
            return;
        }
    }

    public function googleAction()
    {
        if ($this->_getSession()->isLoggedIn()) {
            if ($this->getCheckoutParam()) {
                $this->_redirect('checkout/onepage/index');
                return;
            }
            if ($this->getFireCheckoutParam()) {
                $this->_redirect('firecheckout/index/index');
                return;
            }
            $this->_redirect('customer/account/index');
            return;
        }

        $session = $this->_getSession();
        $state = md5(uniqid(rand(), TRUE));
        $session->setData('social_suite_google_state', $state);
        $scope = 'https://www.googleapis.com/auth/userinfo.email+https://www.googleapis.com/auth/userinfo.profile';
        $redirectUri = Mage::getUrl('socialsuite/auth/gtunnel');
        if ($this->getCheckoutParam()) {
            $redirectUri = Mage::getUrl('socialsuite/auth/gtunnel') . "?checkout=1";
        }
        if ($this->getFireCheckoutParam()) {
            $redirectUri = Mage::getUrl('socialsuite/auth/gtunnel') . "?firecheckout=1";
        }
        $url = "https://accounts.google.com/o/oauth2/auth?response_type=code&scope="
        . $scope . "&state="
        . $state ."&client_id="
        . Mage::getStoreConfig('socialsuite/google_login/appid')
        . "&redirect_uri=" . $redirectUri;

        $this->_redirectUrl($url);
    }

    public function gtunnelAction()
    {
        if ($this->_getSession()->isLoggedIn()) {
            if ($this->getCheckoutParam()) {
                $this->_redirect('checkout/onepage/index');
                return;
            }
            if ($this->getFireCheckoutParam()) {
                $this->_redirect('firecheckout/index/index');
                return;
            }
            $this->_redirect('customer/account/index');
            return;
        }

        $session = $this->_getSession();
        $app_id = Mage::getStoreConfig('socialsuite/google_login/appid');
        $app_secret = Mage::getStoreConfig('socialsuite/google_login/appsecret');
        $my_url = Mage::getUrl('socialsuite/auth/gtunnel');
        if ($this->getCheckoutParam()) {
            $my_url = Mage::getUrl('socialsuite/auth/gtunnel') . '?checkout=1';
        }
        if ($this->getFireCheckoutParam()) {
            $my_url = Mage::getUrl('socialsuite/auth/gtunnel') . '?firecheckout=1';
        }

        $code = $this->getRequest()->getParam('code');

        $sessionState = $session->getData('social_suite_google_state');

        if ($sessionState !== $this->getRequest()->getParam('state')) {
            $session->addError(Mage::helper('socialsuite')->__('Request param "state" not found.'));
            if ($this->getCheckoutParam()) {
                $this->_redirect('checkout/onepage/index');
                return;
            }
            if ($this->getFireCheckoutParam()) {
                $this->_redirect('firecheckout/index/index');
                return;
            }
            $this->_redirect('customer/account/login');
            return;
        }

        if ($this->getRequest()->getParam('error')) {
            $session->addError(Mage::helper('socialsuite')->__('Google Access Denied'));
            if ($this->getCheckoutParam()) {
                $this->_redirect('checkout/onepage/index');
                return;
            }
            if ($this->getFireCheckoutParam()) {
                $this->_redirect('firecheckout/index/index');
                return;
            }
            $this->_redirect('customer/account/login');
            return;
        }

        if (Mage::app()->getCookie()->get('socialsuite_google_access_token')) {
            $accessToken = Mage::app()->getCookie()->get('socialsuite_google_access_token');
        } else {
            //get access token
            $tokenParams = array(
                    'client_id'      => $app_id,
                    'client_secret'  => $app_secret,
                    'redirect_uri'   => $my_url,
                    'grant_type'     => 'authorization_code',
                    'code'           => $code,
            );
            $scope = 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile';

            $tokenUrl = 'https://accounts.google.com/o/oauth2/token';
            try {
                $token = new Zend_Http_Client();
                $token->setMethod(Zend_Http_Client::POST);
                $token->setUri($tokenUrl);
                $token->setParameterPost($tokenParams);
                $response = $token->request();
                $responseBody = $response->getBody();
                $params = Zend_Json::decode($responseBody);
                $accessToken = $params['access_token'];
            } catch (Exception $e) {
                $session->addError($e->getMessage());
                if ($this->getCheckoutParam()) {
                    $this->_redirect('checkout/onepage/index');
                    return;
                }
                if ($this->getFireCheckoutParam()) {
                    $this->_redirect('firecheckout/index/index');
                    return;
                }
                $this->_redirect('customer/account/login');
                return;
            }


            Mage::app()->getCookie()->set('socialsuite_google_access_token', $accessToken, (int)$params['expires_in']);
        }

        $graphParams = array(
            'access_token' => $accessToken,
        );
        //get account data
        $graphUrl = "https://www.googleapis.com/oauth2/v1/userinfo";
        $graph = new Zend_Http_Client();
        $graph->setUri($graphUrl);
        $graph->setParameterGet($graphParams);
        $response = $graph->request();
        $responseGraphBody = $response->getBody();

        $user = Zend_Json::decode($responseGraphBody);

        $firstName = $user['given_name'];
        $lastName = $user['family_name'];
        $email = $user['email'];
        $password = $this->_createRandomPassword();
        $facebookId = $user['id'];

        if ($email) {
            if (!$customer = Mage::registry('current_customer')) {
                $customer = Mage::getModel('customer/customer')->setId(null);
            }

            /* @var $customerForm Mage_Customer_Model_Form */

            $magVersion = Mage::getVersion();
            $last = false;
            if (version_compare($magVersion, '1.5.0.0', '<')) {
                $last = true;
            }

            if ($last) {
                $customerData = array(
                    'firstname'     => $firstName,
                    'lastname'      => $lastName,
                    'email'         => $email,
                    'password'      => $password,
                    'confirmation'  => $password
                );
            } else {
                $customerForm = Mage::getModel('customer/form');
                $customerForm->setFormCode('customer_account_create')
                    ->setEntity($customer);

                $customerData = array(
                    'firstname' => $firstName,
                    'lastname'  => $lastName,
                    'email'     => $email
                );
            }

            /**
             * Initialize customer group id
             */
            $customer->getGroupId();

            try {
                if ($last) {
                    $customer->setData($customerData);
                    $customerErrors = $customer->validate();
                } else {
                    $customerErrors = $customerForm->validateData($customerData);
                }

                if ($customerErrors !== true) {
                    $errors = array_merge($customerErrors, $errors);
                } else {
                    if (!$last) {
                        $customerForm->compactData($customerData);
                        $customer->setPassword($password);
                        $customer->setConfirmation($password);
                        $customerErrors = $customer->validate();
                    }
                    if (is_array($customerErrors)) {
                        $errors = array_merge($customerErrors, $errors);
                    }
                }

                $validationResult = count($errors) == 0;

                if (true === $validationResult) {
                    $customer->save();
                    $session->setData('google_access_token', $accessToken);
                    //add data to facebook table
                    $authModel = Mage::getModel('socialsuite/auth');
                    $authModel
                    ->setCustomerId($customer->getId())
                    ->setFacebookId($facebookId)
                    ->setEmail($email);

                    $authModel->save();

                    if ($customer->isConfirmationRequired()) {
                        $customer->sendNewAccountEmail('confirmation', $session->getBeforeAuthUrl());
                        $session->addSuccess($this->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.', Mage::helper('customer')->getEmailConfirmationUrl($customer->getEmail())));
                        if ($this->getCheckoutParam()) {
                            $this->_redirect('checkout/onepage/index');
                            return;
                        }
                        if ($this->getFireCheckoutParam()) {
                            $this->_redirect('firecheckout/index/index');
                            return;
                        }
                        $this->_redirectSuccess(Mage::getUrl('customer/account/index', array('_secure'=>true)));
                        return;
                    } else {
                        $session->setCustomerAsLoggedIn($customer);
                        $url = $this->_welcomeCustomer($customer);
                        if ($this->getCheckoutParam()) {
                            $this->_redirect('checkout/onepage/index');
                            return;
                        }
                        if ($this->getFireCheckoutParam()) {
                            $this->_redirect('firecheckout/index/index');
                            return;
                        }
                        $this->_redirectSuccess($url);
                        return;
                    }
                } else {
                    $session->setCustomerFormData($this->getRequest()->getPost());
                    if (is_array($errors)) {
                        foreach ($errors as $errorMessage) {
                            $session->addError($errorMessage);
                        }
                    } else {
                        $session->addError($this->__('Invalid customer data'));
                    }
                }
            } catch (Mage_Core_Exception $e) {
                $session->setCustomerFormData($this->getRequest()->getPost());
                if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
                    //if facebook email exist

                    $auth = Mage::getResourceModel('socialsuite/auth');

                    $result = $auth->checkAuthUser($facebookId, $email);

                    if ($result) {

                        $userId = $auth->getLoginId($facebookId, $email);
                        if ($userId) {
                            $newCustomer = Mage::getModel('customer/customer')
                                 ->load($userId);

                            $session->setCustomer($newCustomer);
                            if ($this->getCheckoutParam()) {
                                $this->_redirect('checkout/onepage/index');
                                return;
                            }
                            if ($this->getFireCheckoutParam()) {
                                $this->_redirect('firecheckout/index/index');
                                return;
                            }
                            $this->_redirect('customer/account/index');
                            return;
                        }
                    } else {

                        //email not found auth table
                        $regCustomer = Mage::getModel('customer/customer')
                            ->setWebsiteId(Mage::app()->getWebsite()->getId())
                            ->loadByEmail($email);
                        $customerId = $regCustomer->getId();
                        $authModel = Mage::getModel('socialsuite/auth');
                        $authModel
                            ->setCustomerId($customerId)
                            ->setFacebookId($facebookId)
                            ->setEmail($email);

                        $authModel->save();

                        $newCustomer = Mage::getModel('customer/customer')
                             ->load($customerId);
                        $session->setCustomer($newCustomer);
                        if ($this->getCheckoutParam()) {
                            $this->_redirect('checkout/onepage/index');
                            return;
                        }
                        if ($this->getFireCheckoutParam()) {
                            $this->_redirect('firecheckout/index/index');
                            return;
                        }
                        $this->_redirect('customer/account/index');

                        return;
                    }

                } else {
                    $message = $e->getMessage();
                }
                $session->addError($message);
            } catch (Exception $e) {
                Mage::app()->getCookie()->delete('socialsuite_google_access_token');
                $session->setCustomerFormData($this->getRequest()->getPost())
                    ->addException($e, $this->__('Cannot save the customer.'));
            }

        } else {
            $session->addError(Mage::helper('socialsuite')->__('Invalid request param "email".'));
            Mage::app()->getCookie()->delete('socialsuite_google_access_token');
            if ($this->getCheckoutParam()) {
                $this->_redirect('checkout/onepage/index');
                return;
            }
            if ($this->getFireCheckoutParam()) {
                $this->_redirect('firecheckout/index/index');
                return;
            }
            $this->_redirect('customer/account/login');
            return;
        }
    }

    protected function _welcomeCustomer(Mage_Customer_Model_Customer $customer, $isJustConfirmed = false)
    {
        $this->_getSession()->addSuccess(
            $this->__('Thank you for registering with %s.', Mage::app()->getStore()->getFrontendName())
        );

        $customer->sendNewAccountEmail($isJustConfirmed ? 'confirmed' : 'registered');

        $successUrl = Mage::getUrl('customer/account/index', array('_secure'=>true));
        if ($this->_getSession()->getBeforeAuthUrl()) {
            $successUrl = $this->_getSession()->getBeforeAuthUrl(true);
        }
        return $successUrl;
    }

    private function _createRandomPassword()
    {
        $chars = "abcdefghijkmnopqrstuvwxyz023456789";
        srand((double)microtime()*1000000);
        $i = 0;
        $pass = '';
        while ($i <= 7) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }

        return $pass;
    }
}
