<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 31/12/2012
 * Time: 11:13
 */


// this file initiates the session variables needed for the chat session
session_start();

if (isset($_SESSION['isClient']) && $_SESSION['isClient']) {
    header('location: index.php?action=chat');
}
else
{
    require 'includes/class/database.php';
    require 'includes/class/class.phpmailer.php';
    require 'includes/class/client.php';
    require 'includes/class/displayText.php';

    $c = new client();
    if (isset($_GET['isInvited']) && $_GET['isInvited'] == "true") {
	   $convoId = $c->startInvitedConvo($_GET['operatorId'], $_GET['name'], $_GET['email'], $_GET['department'], $_GET['storeDetails']);
    }
    else
    {
	   $convoId = $c->startConvo($_GET['name'], $_GET['email'], $_GET['department'], $_GET['storeDetails']);
    }


    if ($convoId['success']) {
	   $_SESSION['isClient'] = true;
	   $_SESSION['name'] = $_GET['name'];
	   $_SESSION['convoId'] = $convoId['convoId'];
	   $_SESSION['conversationEnded'] = false;
	   header('location: index.php');
    }
    else
    {
	   header('location: index.php?action=start&error=true');
    }
}

?>
