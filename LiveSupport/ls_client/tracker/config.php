<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 18/03/2013
 * Time: 22:10
 */
if (isset($_GET['getInviteSettings'])) {
    require 'class/database.php';
    require 'class/clientTracker.php';

    $tracker = new clientTracker();
    echo $tracker->getInviteSettings();
}


?>
