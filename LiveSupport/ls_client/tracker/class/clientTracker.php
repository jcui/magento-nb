<?php
/**
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 03/01/2013
 * Time: 20:27
 */

class clientTracker extends database
{


    function __construct()
    {
	   $this->connect();
    }


    public function registerUser()
    {
	   $time = time();
	   $rows = "registered";
	   $vals = array($time);
	   $this->insert('client_tracker', $vals, $rows);
	   $this->select('id', 'client_tracker', "registered = {$time}");
	   $res = $this->getResult();

	   return $res['id'];
    }

    public function updateUser($userId, $page)
    {
	   $rows = array("id = ", $userId);
	   $values = array("page" => $page, "lastSeen" => time());
	   $this->update('client_tracker', $values, $rows);


	   $this->select('*', 'client_tracker', "id = {$userId}");
	   $result = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 0) {
		  $return = array();
		  $return['action'] = "recreate";
		  return json_encode($return);
	   }
	   else if ($rows == 1) {
		  if ($result['convoInvite'] == "1" && $result['userInformed'] == "0") {
			 $return = array();
			 $return['action'] = "invite";
			 $return['id'] = $result['id'];
			 $return['operatorName'] = $this->getoperatorById($result['operatorId']);
			 $return['operatorId'] = $result['operatorId'];
			 $return['inviteMessage'] = $result['message'];
			 $return['departmentId'] = $result['departmentId'];
			 $return['departmentName'] = $this->getDepartmentName($result['departmentId']);
			 return json_encode($return);
		  }
	   }
	   return json_encode($result);
    }

    public function userInformed($userId)
    {
	   $rows = array("id = ", $userId);
	   $values = array("userInformed" => "1");
	   $this->update('client_tracker', $values, $rows);
    }

    private function getoperatorById($id)
    {
	   $this->select('username', 'users', "id = {$id}");
	   $res = $this->getResult();
	   return $res['username'];
    }

    private function getDepartmentName($id)
    {
	   $this->select('departmentName', 'departments', "departmentId = {$id}");
	   $res = $this->getResult();
	   return $res['departmentName'];
    }

    public function getInviteSettings()
    {
	   $this->select('*', 'settings', "setting like 'invite_%'");
	   $res = $this->getResult();
	   return json_encode($res);
    }

}
