var liveSupportTrackerCounter = window.setTimeout(function() {
    trackLiveSupportUser();
}, 5000);
var myLsConvoAgentTrackerId;
var userInviteObject;

var lsli_background;
var lsli_padding;
var lsli_border;
var lsli_height;
var lsli_width;
var lsli_x;
var lsli_y;
var lsli_spacing;
var lsli_textcolor;


$('document').ready(function() {
    trackLiveSupportUser();
    getInviteSettings();
});

function getInviteSettings() {
    time = (new Date).getTime();

    $.ajax({
        url: '/LiveSupport/ls_client/tracker/config.php?getInviteSettings=true&nocache=' + time,
        success: function(data) {
            responseObj = JSON.parse(data);
            $.each(responseObj, function(i, item) {
                switch (item.setting) {
                    case "invite_background": {
                        lsli_background = item.value;
                        break;
                    }
                    case "invite_border": {
                        lsli_border = item.value;
                        break;
                    }
                    case "invite_height": {
                        lsli_height = item.value;
                        break;
                    }
                    case "invite_width": {
                        lsli_width = item.value;
                        break;
                    }
                    case "invite_padding": {
                        lsli_padding = item.value;
                        break;
                    }
                    case "invite_spacing": {
                        lsli_spacing = item.value;
                        break;
                    }
                    case "invite_textcolor": {
                        lsli_textcolor = item.value;
                        break;
                    }
                    case "invite_position":
                    {
                        switch (item.value) {
                            case"topleft":
                            {
                                lsli_x = "top";
                                lsli_y = "left";
                                break;
                            }
                            case"topright":
                            {
                                lsli_x = "top";
                                lsli_y = "right";
                                break;
                            }
                            case"bottomleft":
                            {
                                lsli_x = "bottom";
                                lsli_y = "left";
                                break;
                            }
                            case"bottomright":
                            {
                                lsli_x = "bottom";
                                lsli_y = "right";
                                break;
                            }

                        }

                    }
                }
            });
        }
    });
}

function trackLiveSupportUser() {
    var thisCookie = $.cookie("liveSupportChat_tracker");
    var time;
    var url;
    var currPage = encodeURIComponent(window.location);
    if (thisCookie == null) {
        // record new tracking session
        time = (new Date).getTime();
        url = 'trackUser=true&trackAction=register&nocache=' + time + '&page=' + currPage;
        ;
        $.ajax({
            url: '/LiveSupport/ls_client/tracker/tracker.php?' + url,
            success: function(data) {
                responseObj = JSON.parse(data);
                $.cookie("liveSupportChat_tracker", responseObj.id);
            }
        });
    }
    else {
        time = (new Date).getTime();
        url = 'trackUser=true&trackAction=update&nocache=' + time + '&userId=' + thisCookie + '&page=' + currPage;
        $.ajax({
            url: '/LiveSupport/ls_client/tracker/tracker.php?' + url,
            success: function(data) {
                responseObj = JSON.parse(data);
                if (responseObj.action != undefined) {
                    if (responseObj.action == "recreate") {
                        $.removeCookie("liveSupportChat_tracker");
                    }
                    if (responseObj.action == "invite") {
                        myLsConvoAgentTrackerId = responseObj.id;
                        //$('#liveSupportChatHyperLink').click();
                        launchLiveSupport(myLsConvoAgentTrackerId, responseObj);

                        time = (new Date).getTime();
                        url = 'trackUser=true&trackAction=userInformed&nocache=' + time + '&userId=' + responseObj.id;
                        $.ajax({
                            url: '/LiveSupport/ls_client/tracker/tracker.php?' + url
                        });
                    }
                }
            }
        });
    }

    window.clearTimeout(liveSupportTrackerCounter);
    liveSupportTrackerCounter = window.setTimeout(function() {
        trackLiveSupportUser();
    }, 5000);


}

function launchLiveSupport(inviteId, rObject) {
    if (inviteId != undefined) {
        openLiveSupportChatWindow(inviteId, rObject)
    }
    else {
        window.open(
                '/LiveSupport/ls_client/index.php',
                '1356903505368',
                'width=710,height=630,toolbar=0,menubar=0,location=0,status=0,scrollbars=0,resizable=0,left=0,top=0');
    }


    return false;

}

function launchLiveSupportInvited() {
    var actionUrl = "";
    actionUrl += "?action=invited&inviteId=" + userInviteObject.id;
    actionUrl += "&operatorId=" + userInviteObject.operatorId;
    actionUrl += "&departmentId=" + userInviteObject.departmentId;
    actionUrl += "&departmentName=" + userInviteObject.departmentName;

    window.open(
            '/LiveSupport/ls_client/index.php' + actionUrl,
            '1356903505368',
            'width=710,height=630,toolbar=0,menubar=0,location=0,status=0,scrollbars=0,resizable=0,left=0,top=0');


    return false;

}

function openLiveSupportChatWindow(inviteId, rObject) {
    var actionUrl = '';
    if (inviteId != undefined) {
        actionUrl = "?inviteId=" + inviteId
    }

    var chatWindow;
    chatWindow = '<div onclick="launchLiveSupportInvited()" id="lsChatWindowInvite" style="position:fixed; cursor:pointer; padding:' + lsli_padding + '; border:1px solid ' + lsli_border + '; ' + lsli_x + ':' + lsli_spacing + '; ' + lsli_y + ':' + lsli_spacing + 'px; background:' + lsli_background + '; width:' + lsli_width + 'px; min-height:' + lsli_height + 'px; z-index:1000;">';
    chatWindow += '<p>' + rObject.operatorName + '</p>';
    chatWindow += '<p>' + rObject.inviteMessage + '</p>';
    chatWindow += '</div>';
    userInviteObject = rObject;
    $("body").append(chatWindow);
}



