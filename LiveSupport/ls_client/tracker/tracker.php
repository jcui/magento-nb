<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 03/01/2013
 * Time: 20:27
 */

if (isset($_GET['trackUser']) && isset($_GET['trackAction'])) {
    require 'class/database.php';
    require 'class/clientTracker.php';

    $c = new clientTracker();

    switch ($_GET['trackAction'])
    {
	   case "register":
		  {
		  echo $c->updateUser($c->registerUser(), $_GET['page']);
		  break;
		  }
	   case "update":
		  {
		  echo $c->updateUser($_GET['userId'], $_GET['page']);
		  break;
		  }
	   case "userInformed":
		  {
		  $c->userInformed($_GET['userId']);
		  break;
		  }
	   case "isChatting":
		  {
		  $c->userInformed($_GET['userId']);
		  break;
		  }
    }
}


?>