<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 30/12/2012
 * Time: 21:41
 */

// start session tracking
// session structure made up of the following for a customer :
// isClient - Bool
// name - string
// conversationId - int

session_start();

//require class files
require 'includes/class/database.php';
require 'includes/class/conversation.php';
require 'includes/class/conversationFacade.php';
require 'includes/class/class.phpmailer.php';
require 'includes/class/displayText.php';
require 'includes/class/client.php';

// instantiate classes
$con = new conversation();
$d = new displayText();
$c = new client();
$settings = $c->getSettings();

if (!$c->getOnlineOffline()) {
    require 'pages/email.php';
}
else
{
    if ($_SESSION['conversationEnded']) {
	   require 'pages/end.php';
    }
    else if (isset($_GET['convoEnded']) && $_GET['action'] == "feedback") {
	   require 'pages/feedback.php';
    }
    else if ($c->isActiveUser($_SESSION)) {
	   require 'pages/chat.php';
    }
    else
    {
	   // include template based on required action
	   switch ($_GET['action'])
	   {
		  case "invited":
			 {
			 require 'pages/startInvite.php';
			 break;
			 }
		  case "start":
			 {
			 require 'pages/start.php';
			 break;
			 }
		  case "end":
			 {
			 require 'pages/end.php';
			 break;
			 }
		  case "feedback":
			 {
			 require 'pages/feedback.php';
			 break;
			 }
		  default:
			 {
			 require 'pages/start.php';
			 break;
			 }
	   }
    }
}
?>

