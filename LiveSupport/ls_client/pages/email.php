<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 31/12/2012
 * Time: 10:25
 */

$departments = $c->getDepartments();

?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $c->getApplicationTitle(); ?></title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/client.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>

<div style="height:630px; overflow-y:auto;">

    <div class="clientHeader">
	   <img class="chatLogo" src="<?php echo $c->getApplicationLogo(); ?>" alt="logo" title="logo">
    </div>

    <div id="emailErrorMessage" class="alert alert-error" style="width:88%; margin:0 0 20px 20px;">
	   <p><?php echo $d->getText('sendEmailError'); ?></p>
    </div>


    <div id="emailSend">

	   <div class="well" style="width:90%; margin:0 0 0 20px;">
		  <p><?php echo $d->getText('formNoAgentsOnline'); ?></p>
	   </div>


	   <div class="startContainer">
		  <form class="form-horizontal" id="startForm" method="post" action="">
			 <div id="unControlGroup" class="control-group">

				<div class="controls">
				    <div class="input-prepend">
					   <span class="add-on"><i class="icon-user"></i></span>
					   <input class="span2 inputWide" id="prependedInputName" type="text"
							placeholder="<?php echo $d->getText('formEnterName'); ?>">
				    </div>
				    <span id="usernameErrorText" class="help-inline"
						style="display:none;"><?php echo $d->getText('formEnterNameError'); ?></span>
				</div>
			 </div>
			 <div id="emControlGroup" class="control-group">
				<div class="controls">
				    <div class="input-prepend">
					   <span class="add-on"><i class="icon-envelope"></i></span>
					   <input class="span2 inputWide" id="prependedInputEmail" type="text"
							placeholder="<?php echo $d->getText('formEnterEmail'); ?>">
				    </div>
				    <span id="emailErrorText" class="help-inline"
						style="display:none;"><?php echo $d->getText('formEnterEmailError'); ?></span>
				</div>
			 </div>
			 <?php if (count($departments) != 0) { ?>
			 <div id="deptControlGroup" class="control-group">
				<div class="controls">
				    <p><?php echo $d->getText('formPickDepartment'); ?></p>

				    <div class="input-prepend">
					   <div class="btn-group">
						  <button class="btn dropdown-toggle" data-toggle="dropdown">
							 <i class="icon-home"></i>
							 <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu">
							 <?php for ($i = 0; $i < count($departments); $i++) { ?>
							 <li><a href="#"
								   onclick="$('#deptSelect').val('<?php echo $departments[$i]['departmentName']; ?>'); $('#departmentSelect').val('<?php echo $departments[$i]['departmentId']; ?>');"><?php echo $departments[$i]['departmentName']; ?></a>
							 </li>
							 <?php } ?>
						  </ul>
						  <input type="hidden" id="departmentSelect" value="">
					   </div>
					   <input class="span2" style="width:275px;" id="deptSelect" type="text">
				    </div>
				    <span id="departmentErrorText" class="help-inline"
						style="display:none;"><?php echo $d->getText('formPickDepartmentError'); ?></span>
				</div>
			 </div>
			 <?php } ?>


			 <div class="control-group">
				<div class="controls">
				    <p><?php echo $d->getText('formYourMessage'); ?></p>
				    <textarea id="userEmailMessage" rows="5" cols="100" class="inputWideAppendNarrow"></textarea>

				</div>
			 </div>
			 <div class="control-group">
				<div class="controls">
				    <button type="submit"
						  class="btn btn-primary"><?php echo $d->getText('formSendEmail'); ?></button>
				</div>
			 </div>
		  </form>
	   </div>

    </div>
    <div id="emailSent">

	   <div class="well" style="width:90%; margin:0 0 0 20px;">
		  <h3>
			 <legend><?php echo $d->getText('formThanks'); ?></legend>
		  </h3>
		  <p><?php echo $d->getText('formYourMessageHasBeenSent'); ?></p>
	   </div>

    </div>


</div>
</body>

<script type="text/javascript">

    $(document).ready(function() {

	   $('#emailSent').hide();
	   $('#emailErrorMessage').hide();


	   $('#startForm').submit(function() {

		  $('#usernameErrorText').hide();
		  $('#emailErrorText').hide();
		  $('#departmentErrorText').hide();
		  $('#unControlGroup').removeClass('error');
		  $('#emControlGroup').removeClass('error');

		  // validate name, email address is optional
		  var message = $('#userEmailMessage').val();
		  var username = $('#prependedInputName').val();
		  var department = $('#deptSelect').val();
		  var departmentId = $('#departmentSelect').val();
		  var email = $('#prependedInputEmail').val();
		  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		  if (username.length < 2) {
			 $('#usernameErrorText').show();
			 $('#unControlGroup').addClass('error');

		  }
		  else if (!filter.test(email)) {
			 $('#emailErrorText').show();
			 $('#emControlGroup').addClass('error');
		  }
		  else if (department.length == 0) {
			 $('#departmentErrorText').show();
			 $('#deptControlGroup').addClass('error');
		  }
		  else {
			 // Send email
			 var time = (new Date).getTime();
			 var url = '';
			 url += 'action=sendEmail&noCahe=' + time + '&from=' + email + '&department=' + department + '&message=' + message + '&name=' + username;
			 $.ajax({
				url: 'includes/handler.php?' + url,
				success: function(data) {
				    if (data == "1") {
					   $('#emailErrorMessage').hide();
					   $('#emailSent').show();
					   $('#emailSend').hide();
				    }
				    else {
					   $('#emailErrorMessage').show();
				    }

				}
			 });

		  }

		  return false;
	   });
    });
</script>
</html>