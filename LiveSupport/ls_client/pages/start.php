<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 31/12/2012
 * Time: 10:25
 */

$departments = $c->getDepartments();

?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $c->getApplicationTitle(); ?></title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/client.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<div class="row-fluid">

    <div class="clientHeader">
	   <img class="chatLogo" src="<?php echo $c->getApplicationLogo(); ?>" alt="logo" title="logo">
	   <?php if (isset($_GET['error']) && $_GET['error']) { ?>
	   <div class="notificationContainer" style="width:445px; height:75px; position:absolute; top:5px; left:170px;">
		  <div class="alert alert-error">
			 <p>
				<strong><?php echo $d->getText('oops'); ?></strong><br><?php echo $d->getText('problemStartingConvo'); ?>
			 </p>
		  </div>
	   </div>
	   <?php } ?>
    </div>

    <div class="startContainer">
	   <form class="form-horizontal" id="startForm" method="post" action="">
		  <div id="unControlGroup" class="control-group">

			 <div class="controls">
				<div class="input-prepend">
				    <span class="add-on"><i class="icon-user"></i></span>
				    <input class="span2 inputWide" id="prependedInputName" type="text"
						 placeholder="<?php echo $d->getText('formEnterName'); ?>">
				</div>
				<span id="usernameErrorText" class="help-inline"
					 style="display:none;"><?php echo $d->getText('formEnterNameError'); ?></span>
			 </div>
		  </div>
		  <div class="control-group" id="emailControlGroup">
			 <div class="controls">
				<div class="input-prepend">
				    <span class="add-on"><i class="icon-envelope"></i></span>
				    <input class="span2 inputWide" id="prependedInputEmail" type="text"
						 placeholder="<?php echo $d->getText('formEnterEmail'); ?>">
				</div>
				<span id="emailErrorText" class="help-inline"
					 style="display:none;"><?php echo $d->getText('formEnterEmailError'); ?></span>
			 </div>
		  </div>
		  <?php if (count($departments) != 0) { ?>
		  <div id="deptControlGroup" class="control-group">
			 <div class="controls">
				<p><?php echo $d->getText('formPickDepartment'); ?></p>

				<div class="input-prepend">
				    <div class="btn-group">
					   <button class="btn dropdown-toggle" data-toggle="dropdown">
						  <i class="icon-home"></i>
						  <span class="caret"></span>
					   </button>
					   <ul class="dropdown-menu">
						  <?php for ($i = 0; $i < count($departments); $i++) { ?>
						  <li><a href="#"
							    onclick="$('#deptSelect').val('<?php echo $departments[$i]['departmentName']; ?>'); $('#departmentSelect').val('<?php echo $departments[$i]['departmentId']; ?>');"><?php echo $departments[$i]['departmentName']; ?></a>
						  </li>
						  <?php } ?>
					   </ul>
					   <input type="hidden" id="departmentSelect" value="">
				    </div>
				    <input class="span2 inputWide" id="deptSelect" type="text">
				</div>
				<span id="departmentErrorText" class="help-inline"
					 style="display:none;"><?php echo $d->getText('formPickDepartmentError'); ?></span>
			 </div>
		  </div>
		  <?php } ?>


		  <div class="control-group">
			 <div class="controls">

				<label class="checkbox">
				    <input id="storeMyDetails" type="checkbox"><?php echo $d->getText('formStoreMyDetails'); ?>
				</label>

			 </div>
		  </div>
		  <div class="control-group">
			 <div class="controls">
				<button type="submit"
					   class="btn btn-primary"><?php echo $d->getText('formSubmitStartChat'); ?></button>
			 </div>
		  </div>
	   </form>
    </div>
</div>
</body>

<script type="text/javascript">

    $(document).ready(function() {
	   $('#startForm').submit(function() {

		  $('#usernameErrorText').hide();
		  $('#emailErrorText').hide();
		  $('#departmentErrorText').hide();
		  $('#unControlGroup').removeClass('error');
		  $('#emailControlGroup').removeClass('error');
		  $('#deptControlGroup').removeClass('error');

		  // validate name, email address is optional
		  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		  var username = $('#prependedInputName').val();
		  var department = $('#deptSelect').val();
		  var departmentId = $('#departmentSelect').val();
		  var email = $('#prependedInputEmail').val();
		  var storeDetails = $('#storeMyDetails').is(":checked");
		  if (username.length < 2) {
			 $('#usernameErrorText').show();
			 $('#unControlGroup').addClass('error');

		  }
		  else if (storeDetails && !filter.test(email)) {
			 $('#emailErrorText').show();
			 $('#emailControlGroup').addClass('error');
		  }
		  else if (department.length == 0) {
			 $('#departmentErrorText').show();
			 $('#deptControlGroup').addClass('error');
		  }
		  else {
			 // start conversation
			 window.location = 'startConvo.php?name=' + username + '&email=' + email + '&department=' + departmentId + '&storeDetails=' + storeDetails;
		  }

		  return false;
	   });
    });
</script>
</html>