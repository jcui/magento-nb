<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 31/12/2012
 * Time: 10:17
  *
  *
 */

?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $c->getApplicationTitle(); ?></title>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/client.css" rel="stylesheet">

    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="/LiveSupport/ls_client/tracker/js/cookie.js"></script>
    <script type="text/javascript">

	   var vLeft = 0;

	   window.onbeforeunload = endConversationUserLeft;

	   function endConversationUserLeft() {

		  endConversation(true);
	   }

	   function endConversation(userLeft) {

		  var convoId = <?php echo $_SESSION['convoId']; ?>;
		  var time = (new Date).getTime();
		  var user = '<?php echo $_SESSION['name']; ?>';
		  var url = '';
		  url += 'action=endConversation&user=' + user + '&convoId=' + convoId + '&noCahe=' + time;
		  if (vLeft == 0) {
			 if (userLeft) {
				vLeft = 1;
				$.ajax({
				    async : false,
				    url: 'includes/handler.php?' + url

				});
			 }
			 else {
				vLeft = 1;
				$.ajax({
				    url: 'includes/handler.php?' + url,
				    success: function(data) {
					   window.location = 'index.php?action=feedback&convoEnded=true';
					   return true;
				    }
				});
			 }
		  }


		  $('#closeChatModal').modal('hide')
	   }

	   $(document).ready(function() {
		  var thisCookie = $.cookie("liveSupportChat_tracker");
		  var time;
		  var url;
		  var currPage = encodeURIComponent(window.location);
		  if (thisCookie != null) {
			 // update tracking
			 time = (new Date).getTime();
			 url = 'trackUser=true&trackAction=isChatting&nocache=' + time + '&userId=' + thisCookie;
			 $.ajax({
				url: '/LiveSupport/ls_client/tracker/tracker.php?' + url,
				success: function(data) {

				}
			 });
		  }
	   });
    </script>
</head>
<body>
<div class="row-fluid">
    <div class="span12">
	   <div id="clientContainer">
		  <div class="clientHeader">
			 <img class="chatLogo" src="<?php echo $c->getApplicationLogo(); ?>" alt="logo"
				 title="<?php echo $c->getApplicationTitle(); ?>">
			 <a href="#closeChatModal" role="button" class="btn btn-danger closeWindow"
			    data-toggle="modal"><?php echo $d->getText('endConversation');?></a>

		  </div>
		  <div id="displayContainer">
			 <div id="chatDisplay"></div>
			 <div id="typingStatus"><p id="typingStatusMessage"></p></div>
		  </div>

		  <form id="chatResponseForm" method="post" action="">
			 <div class="input-append">
				<input class="span2" id="clientResponse" type="text" style="width:630px;"
					  placeholder="<?php echo $d->getText('formTypeMessage');?>" onkeyup="iAmTyping(true);">
				<button class="btn btn-primary" id="clientResponseButton" type="button"
					   onclick="storeResponse();"><?php echo $d->getText('formSubmitSend');?></button>
			 </div>
		  </form>
	   </div>

	   <div id="closeChatModal" class="modal hide fade">
		  <div class="modal-header">
			 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			 <h3><?php echo $d->getText('formAreYouSure');?></h3>
		  </div>
		  <div class="modal-body">
			 <p><?php echo $d->getText('formEndConversationText');?></p>
		  </div>
		  <div class="modal-footer">
			 <a href="#" class="btn" data-dismiss="modal"><?php echo $d->getText('formCancel');?></a>
			 <a href="#" class="btn btn-danger"
			    onclick="endConversation(false)"><?php echo $d->getText('endConversation');?></a>
		  </div>
	   </div>

    </div>
</div>
</body>

<script type="text/javascript">


    var clientTyping = false;

    var updateTypingStatus = window.setTimeout(function() {
	   iAmTyping(false);
    }, 1000);
    var areYouTypingStatus = window.setTimeout(function() {
	   areYouTyping();
    }, 1000);

    var preventUpdateTyping = false;


    $(document).ready(function() {
	   getConversation();
	   $('#typingStatus').hide();
	   $("#chatDisplay").scrollTop($("#chatDisplay")[0].scrollHeight);


	   $('#chatResponseForm').submit(function() {
		  $('#clientResponseButton').click();
		  return false;
	   });
    });

    $('#chatDisplay').mouseover(function() {
	   window.hover = true;
    });

    $('#chatDisplay').mouseout(function() {
	   window.hover = false;
    });


    function getConversation() {
	   var convoId = <?php echo $_SESSION['convoId']; ?>;
	   var time = (new Date).getTime();
	   var url = '';
	   url += 'action=getConversation&convoId=' + convoId + '&noCahe=' + time;
	   $.ajax({
		  url: 'includes/handler.php?' + url,
		  success: function(data) {
			 $('#chatDisplay').empty().html(data);
			 if (window.hover == false) {
				$("#chatDisplay").scrollTop($("#chatDisplay")[0].scrollHeight);
			 }
			 setTimeout(function() {
				getConversation();
			 },<?php echo $settings['clientChatRefresh']; ?>);
			 return true;
		  }
	   });
    }

    function storeResponse() {
	   preventUpdateTyping = true;
	   var message = $('#clientResponse').val();
	   var user = '<?php echo $_SESSION['name']; ?>';
	   var time = (new Date).getTime();
	   var convoId = <?php echo $_SESSION['convoId']; ?>;
	   var url = '';
	   url += 'includes/handler.php?action=recordResponse&user=' + user + '&convoId=' + convoId + '&message=' + message + '&noCahe=' + time;
	   $.ajax({
		  url: url,
		  success: function(data) {
			 $('#clientResponse').val('');
			 getConversation();
			 return true;
		  }
	   });
    }

    function iAmTyping(amI) {
	   if (preventUpdateTyping == false) {
		  if (amI) {
			 clientTyping = true;
			 // clear false update, and reset
			 window.clearTimeout(updateTypingStatus);
			 updateTypingStatus = window.setTimeout(function() {
				iAmTyping(false);
			 }, 1000);
		  }
		  else {
			 clientTyping = false;
		  }
		  var convoId = <?php echo $_SESSION['convoId']; ?>;
		  var time = (new Date).getTime();
		  var url = '';
		  url += 'action=isTyping&convoId=' + convoId + '&isTyping=' + clientTyping + '&noCahe=' + time;
		  $.ajax({
			 url: 'includes/handler.php?' + url
		  });
	   }
	   preventUpdateTyping = false;
    }

    function areYouTyping() {
	   var convoId = <?php echo $_SESSION['convoId']; ?>;
	   var time = (new Date).getTime();
	   var url = '';
	   url += 'action=areYouTyping&convoId=' + convoId + '&noCahe=' + time;
	   $.ajax({
		  url: 'includes/handler.php?' + url,
		  success: function(data) {
			 if (data == "0") {
				$('#typingStatus').fadeOut(400);
				$('#typingStatusMessage').text('');
			 }
			 else {
				$('#typingStatus').fadeIn(400);
				$('#typingStatusMessage').text(data);
			 }
			 window.clearTimeout(areYouTypingStatus);
			 areYouTypingStatus = window.setTimeout(function() {
				areYouTyping();
			 }, 1000);
		  }
	   });
    }


</script>
</html>
