<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 18/01/2013
 * Time: 11:47
 */

$feedback = $c->getFeedback();
$_SESSION['conversationEnded'] = true;

?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $c->getApplicationTitle(); ?></title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/client.css" rel="stylesheet">

    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.raty.min.js"></script>
</head>
<body>

<div id="clientContainer" style="overflow-y:scroll;">
    <div class="clientHeader">
	   <img class="chatLogo" src="<?php echo $c->getApplicationLogo(); ?>" alt="logo" title="logo">
    </div>
    <div class="clientFeedbackInner">

	   <div id="leaveFeedback">
		  <legend><?php echo $d->getText('wouldYouLikeToLeaveFeedback');?></legend>
		  <p><?php echo $d->getText('wouldYouLikeToLeaveFeedbackText');?></p>


		  <div class="well">
			 <table class="table" id="ratingTable">
				<?php for ($i = 0; $i < count($feedback); $i++) { ?>
				<tr>
				    <td <?php if ($i == 0) {
				    echo 'style="border:none; width:80%;"';
				} ?> ">
				    <p><?php echo $feedback[$i]['question']; ?></p>
				    </td>
				    <td <?php if ($i == 0) {
					   echo 'style="border:none;"';
				    } ?>>
					   <div id="star_rating_<?php echo $feedback[$i]['id']; ?>" class="starRatingTarget"></div>
				    </td>
				</tr>
				<?php } ?>
			 </table>
		  </div>

		  <p>
			 <button class="btn btn-small btn-success pull-right" id="leaveFeedbackButton"
				    type="button"><?php echo $d->getText('formSubmitFeedback');?></button>
		  </p>


	   </div>
	   <div id="feedbackLeft">
		  <h1>
			 <legend><?php echo $d->getText('formThanks');?></legend>
		  </h1>
		  <p><?php echo $d->getText('feedbackLeft');?></p>
	   </div>


    </div>

</div>


</body>

<script type="text/javascript">

    $(document).ready(function() {

	   $('#feedbackLeft').hide();

	   $('#ratingTable tr').each(function() {
		  var starTarget = $(this).find(".starRatingTarget").attr('id');
		  $('#' + starTarget).raty();

	   });

	   $('#leaveFeedbackButton').click(function() {
		  var scoreString = '';
		  $('#ratingTable tr').each(function() {
			 var starTarget = $(this).find(".starRatingTarget").attr('id');
			 var thisScore = $('#' + starTarget).raty('score');
			 if (thisScore == undefined) {
				thisScore = 0;
			 }
			 var thisQuestionId = starTarget.replace('star_rating_', '');
			 scoreString += thisQuestionId + "$" + thisScore + ",";
		  });

		  var time = (new Date).getTime();
		  var url = '';
		  url += 'action=leaveFeedback&scores=' + scoreString + '&noCahe=' + time;
		  $.ajax({
			 url: 'includes/handler.php?' + url,
			 success: function(data) {
				$('#leaveFeedback').hide();
				$('#feedbackLeft').fadeIn(400);
				return true;
			 }
		  });
	   });
    });


</script>
</html>
