<?php
/**
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 30/12/2012
 * Time: 11:28
 */

class conversationFacade
{


    public function generateConvoOutput($data)
    {
	   $response = "";

	   if ($data == null) {
		  $response .= '<div class="alert alert-info"><p><i class="icon-info-sign"></i>&nbsp;Please select a conversation</p></div>';
	   }
	   else
	   {
		  $response .= '<ul id="chatouptut" class="unstyled">';
		  for ($i = 0; $i < count($data); $i++)
		  {
			 $class = ($i & 1) ? "odd" : "";
			 $temp = '
			 <li class="' . $class . '">
				<small>' . date('h:i', $data[$i]['time']) . ' <strong>' . $data[$i]['user'] . ':</strong></small>
				<p>' . $data[$i]['message'] . '</p>
			 </li>';
			 $response .= $temp;
		  }
		  $response .= "</ul>";
	   }
	   return $response;
    }

}
