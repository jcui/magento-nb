<?php
/**
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 31/12/2012
 * Time: 10:25
 */

class client extends database
{

    private $dt;
    private $mailer;
    private $appSettings;


    function __construct()
    {
	   $this->connect();
	   $this->setDT();
	   $this->mailer = new PHPMailer();
	   $this->checkEveryoneIsLoggedOut();
	   $this->appSettings = $this->getSettings();
	   $this->setTimeZone();

    }

    private function setDT()
    {
	   $this->dt = new displayText();
    }

    public function getSettings()
    {
	   $this->select('*', 'settings');
	   $res = $this->getResult();
	   $return = array();
	   foreach ($res as $r)
	   {
		  $return[$r['setting']] = $r['value'];
	   }
	   return $return;
    }

    private function setTimeZone()
    {
	   date_default_timezone_set($this->appSettings['timezone']);
    }

    public function checkEveryoneIsLoggedOut()
    {
	   $timespan = time() - 362;
	   $this->select("count(*) as'online'", 'users', "alive > {$timespan}");
	   $res = $this->getResult();
	   if ($res['online'] == 0) {
		  $where = array("setting = ", "online");
		  $rows = array(
			 "value" => "false"
		  );
		  $this->update('settings', $rows, $where);
	   }
    }

    public function getOnlineOffline()
    {
	   $this->select('value', 'settings', "setting = 'online'");
	   $res = $this->getResult();
	   return ($res['value'] == "true") ? 1 : 0;
    }

    public function getApplicationTitle()
    {
	   $this->select('value', 'settings', "setting = 'applicationTitle'");
	   $res = $this->getResult();
	   return $res['value'];
    }

    public function getApplicationLogo()
    {
	   $this->select('value', 'settings', "setting = 'applicationLogo'");
	   $res = $this->getResult();
	   return $res['value'];
    }

    public function isActiveUser($data)
    {
	   return ((isset($data['isClient']) && $data['isClient']) && (isset($_SESSION['conversationEnded']) && !$_SESSION['conversationEnded'])) ? true : false;
    }

    public function getDepartments()
    {
	   $this->select('*', 'departments');
	   $ret = $this->getResult();
	   $rows = $this->getRows();

	   if ($rows == 1) {
		  $t = $ret;
		  $ret = array();
		  $ret[0] = $t;
	   }
	   return $ret;

    }

    public function startInvitedConvo($operatorId, $name, $email, $department, $contactMe)
    {
	   $ret = array();
	   $ret['success'] = false;

	   if ($email == '') {
		  $email = "NONE";
	   }
	   // insert into conversations table
	   $rows = "started,departmentId,pickedUp,operatorId";
	   $time = time();
	   $values = array($time, $department, "1", $operatorId);
	   if ($this->insert('conversations', $values, $rows)) {

		  // get convo id
		  $where = "started = {$time}";
		  $this->select('conversationId', 'conversations', $where);
		  $res = $this->getResult();
		  $convoId = $res['conversationId'];


		  // insert into clients
		  $contact = ($contactMe) ? 1 : 0;
		  $rows = "name,email,date,conversationId,contactMe";
		  $values = array($name, $email, $time, $convoId, $contact);
		  if ($this->insert('clients', $values, $rows)) {
			 // add default message to conversation
			 $message = $this->dt->getText('welcomeMessage');
			 $rows = "user,message,time,conversationId,messageType";
			 $values = array('system', $message, $time, $convoId, 4);
			 if ($this->insert('conversations_responses', $values, $rows)) {
				$ret['success'] = true;
				$ret['convoId'] = $convoId;
			 }
			 else
			 {
				$ret['success'] = false;
			 }
		  }
		  else
		  {
			 $ret['success'] = false;
		  }
	   }
	   else
	   {
		  $ret['success'] = false;
	   }
	   return $ret;
    }

    public function startConvo($name, $email, $department, $contactMe)
    {
	   $ret = array();
	   $ret['success'] = false;

	   if ($email == '') {
		  $email = "NONE";
	   }
	   // insert into conversations table
	   $rows = "started,departmentId";
	   $time = time();
	   $values = array($time, $department);
	   if ($this->insert('conversations', $values, $rows)) {

		  // get convo id
		  $where = "started = {$time}";
		  $this->select('conversationId', 'conversations', $where);
		  $res = $this->getResult();
		  $convoId = $res['conversationId'];


		  // insert into clients
		  $contact = ($contactMe) ? 1 : 0;
		  $rows = "name,email,date,conversationId,contactMe";
		  $values = array($name, $email, $time, $convoId, $contact);
		  if ($this->insert('clients', $values, $rows)) {
			 // add default message to conversation
			 $message = $this->dt->getText('welcomeMessage');
			 $rows = "user,message,time,conversationId,messageType";
			 $values = array('system', $message, $time, $convoId, 4);
			 if ($this->insert('conversations_responses', $values, $rows)) {
				$ret['success'] = true;
				$ret['convoId'] = $convoId;
			 }
			 else
			 {
				$ret['success'] = false;
			 }
		  }
		  else
		  {
			 $ret['success'] = false;
		  }
	   }
	   else
	   {
		  $ret['success'] = false;
	   }
	   return $ret;
    }

    public function getFeedback()
    {
	   $this->select('*', 'feedbackQuestions', 'enabled = 1', 'displayOrder');
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows === 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }
	   return $res;
    }

    public function leaveFeedback($scores)
    {
	   // get each feedback question / score pair
	   $pairs = explode(",", $scores);
	   foreach ($pairs as $p)
	   {
		  if (strlen($p) != 0) {
			 $scorePair = explode('$', $p);
			 $rows = 'questionId,rating';
			 $values = array(
				$scorePair[0],
				$scorePair[1]
			 );
			 $this->insert('feedback', $values, $rows);
		  }
	   }

    }

    public function isTyping($convoId, $isTyping)
    {

	   $typing = ($isTyping == "true") ? 1 : 0;
	   $where = array("conversationId=", $convoId);
	   $rows = array(
		  "isTypingClient" => $typing
	   );
	   $this->update('conversations', $rows, $where);
    }

    public function areYouTyping($convoId)
    {
	   $this->select('operatorId,isTypingoperator', 'conversations', "conversationId = {$convoId}");
	   $res = $this->getResult();
	   if ($res['isTypingoperator']) {
		  $this->select('*', 'users', "id = {$res['operatorId']}");
		  $oo = $this->getResult();
		  $rows = $this->getRows();
		  if ($rows == 0) {
			 return 0;
		  }
		  $msg = $oo['username'] . " " . $this->dt->getText('isTyping');
		  return $msg;
	   }
	   return 0;

    }

    public function sendEmail($data)
    {

	   $body = "";
	   $body .= "<p><b>" . $this->dt->getText('emailFromName') . ":</b> " . $data['name'] . "</p>";
	   $body .= "<p><b>" . $this->dt->getText('emailFromEmail') . ":</b> " . $data['from'] . "</p>";
	   $body .= "<p><b>" . $this->dt->getText('emailChosenDepartment') . ":</b> " . $data['department'] . "</p>";
	   $body .= "<p><b>" . $this->dt->getText('emailTheirMessage') . ":</b></p>";
	   $body .= "<p>" . $data['message'] . "</p>";

	   $this->mailer->AddReplyTo($data['from'], $data['name']);

	   $this->mailer->SetFrom($data['from'], $data['name']);

	   $this->mailer->AddReplyTo($data['from'], $data['name']);

	   $this->select('value', 'settings', "setting = 'adminEmail'");

	   $res = $this->getResult();

	   $address = $res['value'];
	   $this->mailer->AddAddress($address, "Ryan Davies");

	   $this->mailer->Subject = $this->dt->getText('emailSubjectLine');

	   $this->mailer->MsgHTML($body);

	   if (!$this->mailer->Send()) {
		  return 0;
	   } else {
		  return 1;
	   }
    }

    public function getDisplayConfig()
    {
	   $where = "setting LIKE 'onOff_%'";
	   $this->select("setting,value", 'settings', $where);
	   $res = $this->getResult();
	   $return = Array();
	   foreach ($res as $r)
	   {
		  $return[$r['setting']] = $r['value'];
	   }
	   $this->select('value', 'settings', "setting = 'online'");
	   $res = $this->getResult();
	   $return['online'] = ($res['value'] == "true") ? 1 : 0;
	   return $return;
    }

}
