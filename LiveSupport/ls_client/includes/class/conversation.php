<?php
/**
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 30/12/2012
 * Time: 11:05
 */

class conversation extends database
{

    private $data = array();
    private $facade;
    private $displayText;

    function __construct()
    {
	   $this->connect();
	   $this->data = $_GET;
	   $this->facade = new conversationFacade();
	   $this->displayText = new displayText();
    }

    // create convo
    // store client details

    private function getConvoDetails($convoId)
    {
	   $this->select('*', 'conversations', "conversationId = {$convoId}");
	   $res = $this->getResult();
	   return $res;
    }


    public function getConversation()
    {
	   if ($this->data['convoId'] == "undefined") {
		  $ret = null;
	   }
	   else
	   {
		  $where = "conversationId = {$this->data['convoId']}";
		  $this->select('*', 'conversations_responses', $where, 'time asc');
		  $ret = $this->getResult();
		  $rows = $this->getRows();

		  if ($rows == 1) {
			 $t = $ret;
			 $ret = array();
			 $ret[0] = $t;
		  }
	   }
	   return $this->facade->generateConvoOutput($ret);
    }

    public function recordResponse()
    {
	   $rows = 'user,message,time,conversationId,messageType,isClientResponse';
	   $values = array(
		  $this->data['user'],
		  $this->data['message'],
		  time(),
		  $this->data['convoId'],
		  1,
		  1
	   );
	   $this->insert('conversations_responses', $values, $rows);
	   $this->isRead($this->data['convoId']);
    }

    public function isRead($convoId)
    {
	   $where = array("conversationId=", $convoId);
	   $rows = array(
		  "isReadClient" => "1"
	   );
	   $this->update('conversations', $rows, $where);
    }

    public function endConversation()
    {
	   // do not end conversation, as admins may still need it.  Instead, just record the fact that the user has left
	   // get conversation details
	   $convo = $this->getConvoDetails($this->data['convoId']);
	   // get operator details
	   $operator = $this->data['user'];

	   // insert "left chat" message
	   $rows = 'user,message,time,conversationId,messageType,isOperatorResponse';
	   $values = array(
		  $operator,
		  "{$operator} {$this->displayText->getText('leftConvo')}",
		  time(),
		  $this->data['convoId'],
		  5,
		  0
	   );
	   if ($this->insert('conversations_responses', $values, $rows))
		  return 0;
    }
}
