<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 30/12/2012
 * Time: 10:47
 */
header('Content-Type: text/html; charset=ISO-8859-15');
if (isset($_GET['action'])) {
    require 'class/database.php';
    require 'class/conversation.php';
    require 'class/conversationFacade.php';
    require 'class/class.phpmailer.php';
    require 'class/client.php';
    require 'class/displayText.php';
    $c = new conversation();
    $client = new client();

    switch ($_GET['action'])
    {
	   case "recordResponse":
		  {
		  $c->recordResponse();
		  break;
		  }
	   case "getConversation":
		  {
		  echo $convo = $c->getConversation();
		  break;
		  }
	   case "endConversation":
		  {
		  echo $c->endConversation();
		  break;
		  }
	   case "leaveFeedback":
		  {
		  echo $client->leaveFeedback($_GET['scores']);
		  break;
		  }
	   case "isTyping":
		  {
		  $client->isTyping($_GET['convoId'], $_GET['isTyping']);
		  break;
		  }
	   case "areYouTyping":
		  {
		  echo $client->areYouTyping($_GET['convoId']);
		  break;
		  }
	   case "sendEmail":
		  {
		  echo $client->sendEmail($_GET);
		  break;
		  }

    }
}






?>
