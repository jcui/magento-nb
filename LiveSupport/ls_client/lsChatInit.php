<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 17/02/2013
 * Time: 15:42
 */
require 'includes/class/database.php';
require 'includes/class/client.php';
require 'includes/class/displayText.php';
require 'includes/class/class.phpmailer.php';

$client = new client();
$displayConfig = $client->getDisplayConfig();
if ($displayConfig['online'] == "1") {
    ?>
<div id="lsChatInitContainer">
    <?php if ($displayConfig['onOff_onIsImage'] == "1") { ?>
    <a href="" id="liveSupportChatHyperLink" onclick="launchLiveSupport();">
	   <img src="<?php echo $displayConfig['onOff_onImage']; ?>"/></a>
    <?php } else { ?>
    <div id="lsChatInitContainerInner"
	    style="overflow:hidden; text-align:<?php echo $displayConfig['onOff_onTextAlign']; ?>; padding:<?php echo $displayConfig['onOff_onPadding']; ?>px; width:<?php echo $displayConfig['onOff_OnWidth']; ?>px; min-height:<?php echo $displayConfig['onOff_onHeight']; ?>px; background:<?php echo $displayConfig['onOff_onBackground']; ?>; color:<?php echo $displayConfig['onOff_onTextColor']; ?>; border:1px solid <?php echo $displayConfig['onOff_onBorderColor']; ?>;">
	   <p><?php echo $displayConfig['onOff_onDisplayText']; ?></p>
	   <?php if ($displayConfig['onOff_onIsLinkImage'] == "1") { ?>
	   <p><a href="" id="liveSupportChatHyperLink" onclick="launchLiveSupport();"
		    style="text-decoration:none; color:#000;">
		  <img src="<?php echo $displayConfig['onOff_onDisplayLinkImage']; ?>" style="max-width:100%;"/>
	   </a></p>
	   <?php } else { ?>
	   <p><a href="" id="liveSupportChatHyperLink" onclick="launchLiveSupport();"
		    style="text-decoration:none; color:#000;"><?php echo $displayConfig['onOff_onDisplayLinkText']; ?></a></p>
	   <?php } ?>
    </div>
    <?php } ?>
</div>
<?php } else { ?>
<div id="lsChatInitContainer">
    <?php if ($displayConfig['onOff_offIsImage'] == "1") { ?>
    <a href="" id="liveSupportChatHyperLink" onclick="launchLiveSupport();">
	   <img src="<?php echo $displayConfig['onOff_offImage']; ?>"/></a>
    <?php } else { ?>
    <div id="lsChatInitContainerInner"
	    style="overflow:hidden; text-align:<?php echo $displayConfig['onOff_offTextAlign']; ?>; padding:<?php echo $displayConfig['onOff_offPadding']; ?>px; width:<?php echo $displayConfig['onOff_OffWidth']; ?>px; min-height:<?php echo $displayConfig['onOff_offHeight']; ?>px; background:<?php echo $displayConfig['onOff_offBackground']; ?>; color:<?php echo $displayConfig['onOff_offTextColor']; ?>; border:1px solid <?php echo $displayConfig['onOff_offBorderColor']; ?>;">
	   <p><?php echo $displayConfig['onOff_offDisplayText']; ?></p>
	   <?php if ($displayConfig['onOff_offIsLinkImage'] == "1") { ?>
	   <p><a href="" id="liveSupportChatHyperLink" style="text-decoration:none; color:#000;">
		  <img src="<?php echo $displayConfig['onOff_offDisplayLinkImage']; ?>"/>
	   </a></p>
	   <?php } else { ?>
	   <p><a href="" id="liveSupportChatHyperLink"
		    style="text-decoration:<?php echo $displayConfig['onOff_offLinkUnderline']; ?>; color:#000;"><?php echo $displayConfig['onOff_offDisplayLinkText']; ?></a>
	   </p>
	   <?php } ?>
    </div>
    <?php } ?>
</div>




<?php } ?>

