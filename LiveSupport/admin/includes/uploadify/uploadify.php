<?php
/*
Uploadify
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/
require '../class/database.php';
require '../class/displayText.php';
require '../class/admin.php';

$admin = new admin();
$allowedFiles = $admin->getAllowedFileTypes();
// Define a destination
$targetFolder = '../../uploads'; // Relative to the root

$verifyToken = md5('unique_salt' . $_POST['timestamp']);

if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
    $tempFile = $_FILES['Filedata']['tmp_name'];
    $targetPath = $targetFolder;
    $targetFile = rtrim($targetPath, '/') . '/' . $_FILES['Filedata']['name'];

    // Validate the file type
    //$fileTypes = array('jpg','jpeg','gif','png'); // File extensions
    $fileTypes = explode(",", $allowedFiles);
    $fileParts = pathinfo($_FILES['Filedata']['name']);
    if (in_array($fileParts['extension'], $fileTypes)) {
	   if (move_uploaded_file($tempFile, $targetFile)) {
		  if ($admin->storeFile($_FILES['Filedata']['name'], $_POST['description'])) {
			 echo '1';
		  }
		  else
		  {
			 echo '3';
		  }
	   }
	   else
	   {
		  echo '2';
	   }
    } else {
	   echo '0';
    }
}
?>