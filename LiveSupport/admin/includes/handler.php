<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 30/12/2012
 * Time: 10:47
 */
if (isset($_GET['action'])) {
    require 'class/database.php';
    require 'class/conversation.php';
    require 'class/displayText.php';
    require 'class/conversationFacade.php';
    require 'class/clientTracker.php';
    require 'class/admin.php';
    require 'class/houseKeeping.php';
    $client = new clientTracker();
    $c = new conversation();
    $admin = new admin();
    $text = new displayText();
    switch ($_GET['action'])
    {
	   case "loadPage":
		  {
		  require 'templates/inc.' . $_GET['page'] . '.php';
		  break;
		  }
	   case "recordResponse":
		  {
		  $c->recordResponse();
		  break;
		  }
	   case "sendFileAttachment":
		  {
		  $c->insertFileAttachment();
		  break;
		  }
	   case "getConversation":
		  {
		  echo @$convo = $c->getConversation();
		  break;
		  }
	   case "getChatQueue":
		  {
		  echo $c->getChatQueue();
		  break;
		  }
	   case "updateFileVisibility" :
		  {
		  echo $admin->updateFileActive($_GET['fileId'], $_GET['active']);
		  break;
		  }
	   case "getChatTransferQueue":
		  {
		  echo $c->getChatTransferQueue();
		  break;
		  }
	   case "pickUpConversation":
		  {
		  echo $c->pickUpConversation($_GET['convoId'], $_GET['user']);
		  break;
		  }
	   case "getMyChats":
		  {
		  echo $c->getMyChats($_GET['user']);
		  break;
		  }
	   case "endConversation":
		  {
		  echo $c->endConversation($_GET['convoId']);
		  break;
		  }
	   case "getVisitorsOnline":
		  {
		  $clients = $client->getActiveTrackingSessions();
		  echo count($clients);
		  break;
		  }
	   case "getOperatorUsers":
		  {

		  break;
		  }
	   case "deleteFile":
		  {
		  echo $admin->deleteFile($_GET['fileId']);
		  break;
		  }
	   case "deleteUser":
		  {
		  echo $admin->deleteUser($_GET['userId']);
		  break;
		  }
	   case "updateUser":
		  {
		  echo $admin->updateUser($_GET);
		  break;
		  }
	   case "createUser":
		  {
		  echo $admin->createUser();
		  break;
		  }
	   case "addDepartment":
		  {
		  echo $admin->addDepartment($_GET['departmentName']);
		  break;
		  }
	   case "deleteDepartment":
		  {
		  echo $admin->deleteDepartment($_GET['departmentId']);
		  break;
		  }
	   case "deletePermissionGroup":
		  {
		  echo $admin->deletePermissionGroup($_GET['permissionGroupId']);
		  break;
		  }
	   case "updatePermissionGroup":
		  {
		  echo $admin->updatePermissionGroup($_GET['updatePermissionGroupId']);
		  break;
		  }
	   case "addPermissionGroup":
		  {
		  echo $admin->addPermissionGroup($_GET['permissionGroupName']);
		  break;
		  }
	   case "storeStandardResponse":
		  {
		  echo $admin->storeStandardResponse($_GET['responseText']);
		  break;
		  }
	   case "deleteStandardResponse":
		  {
		  echo $admin->deleteStandardResponse($_GET['responseId']);
		  break;
		  }
	   case "updateStandardResponse":
		  {
		  echo $admin->updateStandardResponse($_GET['responseId'], $_GET['responseText']);
		  break;
		  }
	   case "deleteFeedbackQuestion":
		  {
		  echo $admin->deleteFeedbackQuestion($_GET['responseId']);
		  break;
		  }
	   case "storeFeedbackQuestion":
		  {
		  echo $admin->storeFeedbackQuestion($_GET['questionText']);
		  break;
		  }
	   case "validation":
		  switch ($_GET['validationType'])
		  {
			 case "username":
				{
				echo $admin->isValidUsername($_GET['username']);
				break;
				}
			 case "password":
				{
				echo $admin->isValidPassword($_GET['password']);
				break;
				}
		  }
		  break;
	   case "getDepartmentsJSON":
		  {
		  echo $admin->getDepartmentsJSON();
		  break;
		  }
	   case "getPermissionsJSON":
		  {
		  echo $admin->getPermissionsJSON();
		  break;
		  }
	   case "isTyping":
		  {
		  $admin->isTyping($_GET['convoId'], $_GET['isTyping']);
		  break;
		  }
	   case "areYouTyping":
		  {
		  echo $admin->areYouTyping($_GET['convoId']);
		  break;
		  }
	   case "inviteUser":
		  {
		  echo $admin->inviteUserToChat($_GET['userId'], $_GET['operatorId'], $_GET['department'], $_GET['message']);
		  break;
		  }
	   case "getOnlineOffline":
		  {
		  echo $admin->getOnlineOffline();
		  break;
		  }
	   case "setOnlineOffline":
		  {
		  echo $admin->setOnlineOffline($_GET['onlineOffline']);
		  break;
		  }
	   case "getAudioAlertsOnOff":
		  {
		  echo $admin->getAudioAlertsOnOff();
		  break;
		  }
	   case "setAudioAlertsOnOff":
		  {
		  echo $admin->setAudioAlertsOnOff($_GET['audioOnOff']);
		  break;
		  }
	   case "getLanguageKeys":
		  {
		  echo $admin->getLanguageKeys($_GET['language']);
		  break;
		  }

	   case "setLanguageKeys":
		  {
		  echo $admin->setLanguageKeys($_GET);
		  break;
		  }
	   case "getOperatorsOnline":
		  {
		  echo $c->getoperatorsOnline();
		  break;
		  }

	   case "getOperatorChat" :
		  {
		  echo $c->getOperatorChat();
		  break;
		  }
	   case "submitOperatorMessage" :
		  {
		  $c->submitOperatorMessage($_GET['userId'], $_GET['message']);
		  break;
		  }
	   case "transferConversation":
		  {
		  echo $c->logConversationTransfer();
		  break;
		  }
	   case "keepAlive":
		  {
		  $admin->keepAliveUser($_GET['userId']);
		  break;
		  }
	   case "acceptTransfer":
		  {
		  echo $c->acceptTransfer($_GET['transferId']);
		  break;
		  }
	   case "rejectTransfer":
		  {
		  $c->rejectTransfer($_GET['transferId']);
		  break;
		  }
	   case "cancelTransfer":
		  {
		  $c->cancelTransfer($_GET['transferId']);
		  break;
		  }
	   case "updateSetting":
		  {
		  echo $admin->updateSetting($_GET['setting'], $_GET['value']);
		  break;
		  }
	   case "getGraphTotalConvos":
		  {
		  echo $admin->getGraphTotalConvos();
		  break;
		  }
	   case "getLanguagesForDropDown" :
		  {
		  echo $admin->getLanguagesForDropDown();
		  break;
		  }
	   case "getLanguageKey":
		  {
		  echo $admin->getLanguageKey($_GET['langNice']);
		  break;
		  }
	   case "createLanguage":
		  {
		  echo $admin->createLanguage($_GET['languageName']);
		  break;
		  }
    }
}






?>
