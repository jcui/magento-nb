<?php
/**
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 03/01/2013
 * Time: 20:27
 */

class clientTracker extends database
{


    function __construct()
    {
	   $this->connect();
    }


    public function registerUser()
    {
	   $time = time();
	   $rows = "registered";
	   $vals = array($time);
	   $this->insert('client_tracker', $vals, $rows);
	   $this->select('id', 'client_tracker', "registered = {$time}");
	   $res = $this->getResult();

	   return $res['id'];
    }

    public function updateUser($userId, $page)
    {
	   $rows = array("id = ", $userId);
	   $values = array("page" => $page, "lastSeen" => time());
	   $this->update('client_tracker', $values, $rows);


	   $this->select('*', 'client_tracker', "id = {$userId}");
	   $result = $this->getResult();
	   return json_encode($result);
    }

    private function getInvite($userId)
    {

    }

    private function cleanSessions()
    {
	   $diff = time() - 300;
	   $where = "lastSeen < {$diff}";
	   $this->delete('client_tracker', $where);
    }

    public function getActiveTrackingSessions()
    {
	   $this->cleanSessions();
	   $this->select('*', 'client_tracker', null, 'userInformed');
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }
	   return $res;
    }

}
