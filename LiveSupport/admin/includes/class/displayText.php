<?php
/**
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 28/12/2012
 * Time: 11:22
 */

class displayText extends database
{

    private $lang;

    function __construct()
    {
	   $this->connect();
	   $this->getLanguage();
    }

    private function getLanguage()
    {
	   $where = "setting = 'language'";
	   $this->select('value', 'settings', $where);
	   $result = $this->getResult();
	   $this->lang = $result['value'];
    }

    public function getText($key)
    {
	   $where = "controlKey = '{$key}' and languageKey='{$this->lang}'";
	   $this->select('*', 'display_text', $where);
	   $result = $this->getResult();
	   return $result['text'];
    }
}
