<?php
/**
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 06/01/2013
 * Time: 12:44
 */

class admin extends database
{


    private $dt;
    private $appSettings = Array();

    function __construct()
    {
	   $this->connect();
	   $this->dt = new displayText();
	   $this->appSettings = $this->getSettings();
	   $this->setTimeZone();
    }

    public function tzList()
    {
	   $timezones =
			 array(
				'(GMT-12:00) International Date Line West' => 'Pacific/Wake',
				'(GMT-11:00) Midway Island' => 'Pacific/Apia',
				'(GMT-11:00) Samoa' => 'Pacific/Apia',
				'(GMT-10:00) Hawaii' => 'Pacific/Honolulu',
				'(GMT-09:00) Alaska' => 'America/Anchorage',
				'(GMT-08:00) Pacific Time (US &amp; Canada); Tijuana' => 'America/Los_Angeles',
				'(GMT-07:00) Arizona' => 'America/Phoenix',
				'(GMT-07:00) Chihuahua' => 'America/Chihuahua',
				'(GMT-07:00) La Paz' => 'America/Chihuahua',
				'(GMT-07:00) Mazatlan' => 'America/Chihuahua',
				'(GMT-07:00) Mountain Time (US &amp; Canada)' => 'America/Denver',
				'(GMT-06:00) Central America' => 'America/Managua',
				'(GMT-06:00) Central Time (US &amp; Canada)' => 'America/Chicago',
				'(GMT-06:00) Guadalajara' => 'America/Mexico_City',
				'(GMT-06:00) Mexico City' => 'America/Mexico_City',
				'(GMT-06:00) Monterrey' => 'America/Mexico_City',
				'(GMT-06:00) Saskatchewan' => 'America/Regina',
				'(GMT-05:00) Bogota' => 'America/Bogota',
				'(GMT-05:00) Eastern Time (US &amp; Canada)' => 'America/New_York',
				'(GMT-05:00) Indiana (East)' => 'America/Indiana/Indianapolis',
				'(GMT-05:00) Lima' => 'America/Bogota',
				'(GMT-05:00) Quito' => 'America/Bogota',
				'(GMT-04:00) Atlantic Time (Canada)' => 'America/Halifax',
				'(GMT-04:00) Caracas' => 'America/Caracas',
				'(GMT-04:00) La Paz' => 'America/Caracas',
				'(GMT-04:00) Santiago' => 'America/Santiago',
				'(GMT-03:30) Newfoundland' => 'America/St_Johns',
				'(GMT-03:00) Brasilia' => 'America/Sao_Paulo',
				'(GMT-03:00) Buenos Aires' => 'America/Argentina/Buenos_Aires',
				'(GMT-03:00) Georgetown' => 'America/Argentina/Buenos_Aires',
				'(GMT-03:00) Greenland' => 'America/Godthab',
				'(GMT-02:00) Mid-Atlantic' => 'America/Noronha',
				'(GMT-01:00) Azores' => 'Atlantic/Azores',
				'(GMT-01:00) Cape Verde Is.' => 'Atlantic/Cape_Verde',
				'(GMT) Casablanca' => 'Africa/Casablanca',
				'(GMT) Edinburgh' => 'Europe/London',
				'(GMT) Greenwich Mean Time : Dublin' => 'Europe/London',
				'(GMT) Lisbon' => 'Europe/London',
				'(GMT) London' => 'Europe/London',
				'(GMT) Monrovia' => 'Africa/Casablanca',
				'(GMT+01:00) Amsterdam' => 'Europe/Berlin',
				'(GMT+01:00) Belgrade' => 'Europe/Belgrade',
				'(GMT+01:00) Berlin' => 'Europe/Berlin',
				'(GMT+01:00) Bern' => 'Europe/Berlin',
				'(GMT+01:00) Bratislava' => 'Europe/Belgrade',
				'(GMT+01:00) Brussels' => 'Europe/Paris',
				'(GMT+01:00) Budapest' => 'Europe/Belgrade',
				'(GMT+01:00) Copenhagen' => 'Europe/Paris',
				'(GMT+01:00) Ljubljana' => 'Europe/Belgrade',
				'(GMT+01:00) Madrid' => 'Europe/Paris',
				'(GMT+01:00) Paris' => 'Europe/Paris',
				'(GMT+01:00) Prague' => 'Europe/Belgrade',
				'(GMT+01:00) Rome' => 'Europe/Berlin',
				'(GMT+01:00) Sarajevo' => 'Europe/Sarajevo',
				'(GMT+01:00) Skopje' => 'Europe/Sarajevo',
				'(GMT+01:00) Stockholm' => 'Europe/Berlin',
				'(GMT+01:00) Vienna' => 'Europe/Berlin',
				'(GMT+01:00) Warsaw' => 'Europe/Sarajevo',
				'(GMT+01:00) West Central Africa' => 'Africa/Lagos',
				'(GMT+01:00) Zagreb' => 'Europe/Sarajevo',
				'(GMT+02:00) Athens' => 'Europe/Istanbul',
				'(GMT+02:00) Bucharest' => 'Europe/Bucharest',
				'(GMT+02:00) Cairo' => 'Africa/Cairo',
				'(GMT+02:00) Harare' => 'Africa/Johannesburg',
				'(GMT+02:00) Helsinki' => 'Europe/Helsinki',
				'(GMT+02:00) Istanbul' => 'Europe/Istanbul',
				'(GMT+02:00) Jerusalem' => 'Asia/Jerusalem',
				'(GMT+02:00) Kyiv' => 'Europe/Helsinki',
				'(GMT+02:00) Minsk' => 'Europe/Istanbul',
				'(GMT+02:00) Pretoria' => 'Africa/Johannesburg',
				'(GMT+02:00) Riga' => 'Europe/Helsinki',
				'(GMT+02:00) Sofia' => 'Europe/Helsinki',
				'(GMT+02:00) Tallinn' => 'Europe/Helsinki',
				'(GMT+02:00) Vilnius' => 'Europe/Helsinki',
				'(GMT+03:00) Baghdad' => 'Asia/Baghdad',
				'(GMT+03:00) Kuwait' => 'Asia/Riyadh',
				'(GMT+03:00) Moscow' => 'Europe/Moscow',
				'(GMT+03:00) Nairobi' => 'Africa/Nairobi',
				'(GMT+03:00) Riyadh' => 'Asia/Riyadh',
				'(GMT+03:00) St. Petersburg' => 'Europe/Moscow',
				'(GMT+03:00) Volgograd' => 'Europe/Moscow',
				'(GMT+03:30) Tehran' => 'Asia/Tehran',
				'(GMT+04:00) Abu Dhabi' => 'Asia/Muscat',
				'(GMT+04:00) Baku' => 'Asia/Tbilisi',
				'(GMT+04:00) Muscat' => 'Asia/Muscat',
				'(GMT+04:00) Tbilisi' => 'Asia/Tbilisi',
				'(GMT+04:00) Yerevan' => 'Asia/Tbilisi',
				'(GMT+04:30) Kabul' => 'Asia/Kabul',
				'(GMT+05:00) Ekaterinburg' => 'Asia/Yekaterinburg',
				'(GMT+05:00) Islamabad' => 'Asia/Karachi',
				'(GMT+05:00) Karachi' => 'Asia/Karachi',
				'(GMT+05:00) Tashkent' => 'Asia/Karachi',
				'(GMT+05:30) Chennai' => 'Asia/Calcutta',
				'(GMT+05:30) Kolkata' => 'Asia/Calcutta',
				'(GMT+05:30) Mumbai' => 'Asia/Calcutta',
				'(GMT+05:30) New Delhi' => 'Asia/Calcutta',
				'(GMT+05:45) Kathmandu' => 'Asia/Katmandu',
				'(GMT+06:00) Almaty' => 'Asia/Novosibirsk',
				'(GMT+06:00) Astana' => 'Asia/Dhaka',
				'(GMT+06:00) Dhaka' => 'Asia/Dhaka',
				'(GMT+06:00) Novosibirsk' => 'Asia/Novosibirsk',
				'(GMT+06:00) Sri Jayawardenepura' => 'Asia/Colombo',
				'(GMT+06:30) Rangoon' => 'Asia/Rangoon',
				'(GMT+07:00) Bangkok' => 'Asia/Bangkok',
				'(GMT+07:00) Hanoi' => 'Asia/Bangkok',
				'(GMT+07:00) Jakarta' => 'Asia/Bangkok',
				'(GMT+07:00) Krasnoyarsk' => 'Asia/Krasnoyarsk',
				'(GMT+08:00) Beijing' => 'Asia/Hong_Kong',
				'(GMT+08:00) Chongqing' => 'Asia/Hong_Kong',
				'(GMT+08:00) Hong Kong' => 'Asia/Hong_Kong',
				'(GMT+08:00) Irkutsk' => 'Asia/Irkutsk',
				'(GMT+08:00) Kuala Lumpur' => 'Asia/Singapore',
				'(GMT+08:00) Perth' => 'Australia/Perth',
				'(GMT+08:00) Singapore' => 'Asia/Singapore',
				'(GMT+08:00) Taipei' => 'Asia/Taipei',
				'(GMT+08:00) Ulaan Bataar' => 'Asia/Irkutsk',
				'(GMT+08:00) Urumqi' => 'Asia/Hong_Kong',
				'(GMT+09:00) Osaka' => 'Asia/Tokyo',
				'(GMT+09:00) Sapporo' => 'Asia/Tokyo',
				'(GMT+09:00) Seoul' => 'Asia/Seoul',
				'(GMT+09:00) Tokyo' => 'Asia/Tokyo',
				'(GMT+09:00) Yakutsk' => 'Asia/Yakutsk',
				'(GMT+09:30) Adelaide' => 'Australia/Adelaide',
				'(GMT+09:30) Darwin' => 'Australia/Darwin',
				'(GMT+10:00) Brisbane' => 'Australia/Brisbane',
				'(GMT+10:00) Canberra' => 'Australia/Sydney',
				'(GMT+10:00) Guam' => 'Pacific/Guam',
				'(GMT+10:00) Hobart' => 'Australia/Hobart',
				'(GMT+10:00) Melbourne' => 'Australia/Sydney',
				'(GMT+10:00) Port Moresby' => 'Pacific/Guam',
				'(GMT+10:00) Sydney' => 'Australia/Sydney',
				'(GMT+10:00) Vladivostok' => 'Asia/Vladivostok',
				'(GMT+11:00) Magadan' => 'Asia/Magadan',
				'(GMT+11:00) New Caledonia' => 'Asia/Magadan',
				'(GMT+11:00) Solomon Is.' => 'Asia/Magadan',
				'(GMT+12:00) Auckland' => 'Pacific/Auckland',
				'(GMT+12:00) Fiji' => 'Pacific/Fiji',
				'(GMT+12:00) Kamchatka' => 'Pacific/Fiji',
				'(GMT+12:00) Marshall Is.' => 'Pacific/Fiji',
				'(GMT+12:00) Wellington' => 'Pacific/Auckland',
				'(GMT+13:00) Nuku\'alofa' => 'Pacific/Tongatapu',
			 );
	   return $timezones;
    }

    public function getLanguageKey($input)
    {
	   $this->select('languageKey', 'display_text', "languageNice = '{$input}'", null, null, 1);
	   $res = $this->getResult();
	   return $res['languageKey'];
    }

    public function getSettings()
    {
	   $this->select('*', 'settings');
	   $res = $this->getResult();
	   $return = array();
	   foreach ($res as $r)
	   {
		  $return[$r['setting']] = $r['value'];
	   }
	   $this->select('languageNice', 'display_text', "languageKey = '{$return['language']}'", null, null, 1);
	   $res = $this->getResult();
	   $return['languageNice'] = $res['languageNice'];
	   return $return;
    }

    private function setTimeZone()
    {
	   date_default_timezone_set($this->appSettings['timezone']);
    }

    public function getOnlineOffline()
    {
	   $this->select('value', 'settings', "setting = 'online'");
	   $res = $this->getResult();
	   return ($res['value'] == "true") ? 1 : 0;
    }

    public function setOnlineOffline($onlineOffline)
    {

	   $where = array("setting=", "online");
	   $rows = array(
		  "value" => $onlineOffline
	   );
	   if ($this->update('settings', $rows, $where)) {
		  return 1;
	   }
	   return 0;
    }

    public function getAudioAlertsOnOff()
    {
	   $this->select('value', 'settings', "setting = 'audioAlerts'");
	   $res = $this->getResult();
	   return ($res['value'] == "true") ? 1 : 0;
    }

    public function setAudioAlertsOnOff($audioOnOff)
    {

	   $where = array("setting=", "audioAlerts");
	   $rows = array(
		  "value" => $audioOnOff
	   );
	   if ($this->update('settings', $rows, $where)) {
		  return 1;
	   }
	   return 0;
    }


    public function getOperatorUsersOnline($requester)
    {
	   $this->select('*', 'users', "isOnline = 1 and id != {$requester}");
	   $rows = $this->getRows();
	   $res = $this->getResult();
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }
	   return $res;
    }


    public function getOperatorUsers()
    {
	   $this->select('*', 'users');
	   $rows = $this->getRows();
	   $res = $this->getResult();
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }
	   return $res;
    }


    public function updateOperator()
    {

    }

    public function deleteOperator()
    {

    }

    public function getDepartments()
    {
	   $this->select('*', 'departments');
	   $rows = $this->getRows();
	   $res = $this->getResult();
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }
	   return $res;
    }

    public function getDepartmentName($dId)
    {
	   $this->select('departmentName', 'departments', "departmentId = {$dId}");
	   $res = $this->getResult();
	   return $res['departmentName'];
    }

    public function getPermissionGroups()
    {
	   $this->select('*', 'permissions_levels');
	   $rows = $this->getRows();
	   $res = $this->getResult();
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }
	   return $res;
    }


    public function isValidUsername($email)
    {
	   if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		  return 0;
	   }
	   $where = "email = '{$email}'";
	   $this->select('*', 'users', $where);
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows != 0) {
		  return 0;
	   }
	   return 1;
    }

    public function isValidPassword($password)
    {
	   $r1 = '/[A-Z]/'; //Uppercase
	   $r2 = '/[a-z]/'; //lowercase
	   $r3 = '/[!@#$%^&*()-_=+{};:,<.>]/'; // special chars
	   $r4 = '/[0-9]/'; //numbers

	   if (preg_match_all($r1, $password, $o) < 1) return 0;

	   if (preg_match_all($r2, $password, $o) < 1) return 0;

	   //if(preg_match_all($r3,$password, $o)<2) return FALSE;

	   if (preg_match_all($r4, $password, $o) < 1) return 0;

	   if (strlen($password) < 8) return 0;

	   return 1;
    }

    public function deleteUser($userId)
    {
	   // check that there is more than 1 user in the db ( cannot have no users )
	   $this->select('*', 'users');
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 0) {
		  return 0;
	   }

	   if ($this->delete('users', "id = {$userId}")) {
		  return 1;
	   }
	   return 0;
    }

    public function updateUser($data)
    {
	   $where = array("id=", $data['userId']);
	   $rows = array(
		  "username" => $data['displayName'],
		  "email" => $data['email'],
		  "department" => $data['department'],
		  "userLevel" => $data['permissions']
	   );
	   if($data['password'] != "")
	   {
		  $salt = rand(1000, 9999);
		  $hString = $salt . $data['password'];
		  $hash = sha1($hString);
		  $rows['salt'] = $salt;
		  $rows['password'] = $hash;
	   }
	   if ($this->update('users', $rows, $where)) {
		  return 1;
	   }
	   return 0;
    }

    public function createUser()
    {
	   // sanity
	   if ($this->isValidPassword($_GET['password']) == 0) {
		  return 0;
	   }
	   if ($this->isValidUsername($_GET['username']) == 0) {
		  return 0;
	   }

	   $salt = rand(1000, 9999);
	   $hString = $salt . $_GET['password'];
	   $hash = sha1($hString);

	   $rows = 'username,password,salt,email,userLevel,department';
	   $values = array(
		  $_GET['displayName'],
		  $hash,
		  $salt,
		  $_GET['username'],
		  $_GET['permissions'],
		  $_GET['department']

	   );
	   if ($this->insert('users', $values, $rows)) {
		  // get permissions

		  return 1;
	   }
	   else
	   {
		  return 0;
	   }
    }

    public function addDepartment($department)
    {

	   $rows = 'departmentName';
	   $values = array(
		  $department
	   );
	   if ($this->insert('departments', $values, $rows)) {
		  return 1;
	   }
	   else
	   {
		  return 0;
	   }
    }

    public function addPermissionGroup($group)
    {

	   $rows = 'permissionTitle';
	   $values = array(
		  $group
	   );
	   if ($this->insert('permissions_levels', $values, $rows)) {
		  $this->select('*', 'permissions_levels', "permissionTitle = '{$group}'");
		  $rr = $this->getResult();
		  $permTitle = $rr['id'];

		  $this->select('*', 'permissions_keys');
		  $res = $this->getResult();
		  foreach ($res as $r)
		  {
			 $rows = 'titleId,keyId,userRead,userWrite';
			 $values = array(
				$permTitle,
				$r['id'],
				0,
				0,
			 );
			 $this->insert('permissions', $values, $rows);
		  }

		  return 1;
	   }
	   else
	   {
		  return 0;
	   }
    }

    public function deletePermissionGroup($permId)
    {
	   $where = "userLevel != " . $permId;
	   // get lowest id user level not being deleted to transfer any users to
	   $this->select('userLevel', 'users', $where, 'userLevel asc', '', "1");
	   $res = $this->getResult();
	   $dept = $res['userLevel'];

	   $where = array("userLevel=", $permId);
	   $rows = array(
		  "userLevel" => $dept
	   );
	   if ($this->update('users', $rows, $where)) {
		  if ($this->delete('permissions', "titleId = {$permId}")) {
			 if ($this->delete('permissions_levels', "id = {$permId}")) {
				return 1;
			 }
			 return 0;
		  }
		  return 0;
	   }
	   return 0;

    }

    public function getFeedBackQuestions()
    {
	   $this->select('*', 'feedbackQuestions');
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }
	   return $res;
    }

    public function getFeedbackSummary()
    {
	   $sel = "question as 'question',round(avg(rating)) as 'rating', count(*) as 'total'";
	   $from = "ls_feedback f join ls_feedbackQuestions q on f.questionId = q.id";
	   $where = "enabled = 1";
	   $group = "questionId";

	   $this->select($sel, $from, $where, null, $group, null, null, false);
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }
	   return $res;
    }

    public function updatePermissionGroup($data)
    {
	   $thisErr = 0;
	   $where = "titleId = {$_GET['updatePermissionGroupId']}";
	   $this->select('*', 'permissions', $where);
	   $res = $this->getResult();
	   foreach ($res as $r)
	   {
		  $title = $this->convertPermisisonKeyId($r['keyId']);
		  $where = array("titleId=", $_GET['updatePermissionGroupId'], "AND keyId=", $r['keyId']);
		  $rows = array(
			 "userRead" => $_GET[$title . '_read'],
			 "userWrite" => $_GET[$title . '_write']
		  );

		  if (!$this->update('permissions', $rows, $where)) {
			 $thisErr++;
		  }
	   }

	   if ($thisErr == 0) {
		  return 1;
	   }
	   return 0;
    }

    public function deleteDepartment($deptId)
    {
	   $where = "department != " . $deptId;
	   // get lowest id department not being deleted to transfer any users to
	   $this->select('department', 'users', $where, 'department asc', '', "1");
	   $res = $this->getResult();
	   $dept = $res['department'];

	   $where = array("department=", $deptId);
	   $rows = array(
		  "department" => $dept
	   );
	   if ($this->update('users', $rows, $where)) {
		  $where = array("departmentId=", $deptId);
		  $rows = array(
			 "departmentId" => $dept
		  );
		  if ($this->update('conversations', $rows, $where)) {
			 if ($this->delete('departments', "departmentId = {$deptId}")) {
				return 1;
			 }
			 else
			 {
				return 0;
			 }
		  }
		  else
		  {
			 return 0;
		  }
	   }
	   return 0;

    }

    public function getFullPemrissions()
    {
	   $this->select('*', 'permissions_levels');
	   $res1 = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 1) {
		  $ret1 = array();
		  $ret1[0] = $res1;
		  $res1 = $ret1;
	   }
	   $ret = array();
	   foreach ($res1 as $p)
	   {

		  $this->select('*', 'permissions', "titleId = {$p['id']}");
		  $res = $this->getResult();

		  $ret[$p['id']] = array();

		  foreach ($res as $r)
		  {
			 $title = $this->convertPermisisonKeyId($r['keyId']);
			 $ret[$p['id']][$title]['read'] = $r['userRead'];
			 $ret[$p['id']][$title]['write'] = $r['userWrite'];
		  }
	   }
	   return $ret;
    }

    public function convertPermisisonId($id)
    {
	   $this->select('permissionTitle', 'permissions_levels', "id = {$id}");
	   $res = $this->getResult();
	   return $res['permissionTitle'];
    }

    public function convertPermisisonKeyId($id)
    {
	   $this->select('keyId', 'permissions_keys', "id = {$id}");
	   $res = $this->getResult();
	   return $res['keyId'];
    }

    public function convertDepartmentId($id)
    {
	   $this->select('departmentName', 'departments', "departmentId = {$id}");
	   $res = $this->getResult();
	   return $res['departmentName'];
    }

    public function getDepartmentsJSON()
    {
	   return json_encode($this->getDepartments());
    }

    public function getPermissionsJSON()
    {
	   return json_encode($this->getPermissionGroups());
    }

    public function getAllowedFileTypes()
    {
	   $this->select('*', 'settings', "setting = 'allowedFileTypes'");
	   $res = $this->getResult();
	   return $res['value'];
    }

    public function getStoredFiles()
    {
	   $this->select('*', 'files', '', 'added asc');
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }
	   return $res;
    }

    public function getStoredFilesPublic()
    {
	   $this->select('*', 'files', 'active = 1', 'added asc');
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }
	   return $res;
    }

    public function updateFileActive($fileId, $active)
    {
	   $tActive = ($active == 'true') ? 1 : 0;
	   $where = array("id=", $fileId);
	   $rows = array(
		  "active" => $tActive
	   );
	   $this->update('files', $rows, $where);

    }

    public function deleteFile($fileId)
    {
	   if ($this->delete('files', "id = {$fileId}")) {
		  return 1;
	   }
	   return 0;
    }

    public function storeFile($fileName, $fileDesc)
    {
	   if ($fileDesc == "") {
		  $fileDesc = $fileName;
	   }
	   $rows = 'url,added,description';
	   $values = array(
		  $fileName,
		  time(),
		  $fileDesc
	   );
	   if ($this->insert('files', $values, $rows)) {
		  return true;
	   }
	   return false;
    }

    public function getStandardResponses()
    {
	   $this->select('*', 'standard_responses');
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }
	   return $res;
    }

    public function storeFeedbackQuestion($text)
    {
	   $rows = 'question';
	   $values = array(
		  $text
	   );
	   if ($this->insert('feedbackQuestions', $values, $rows)) {
		  return 1;
	   }
	   return 0;
    }

    public function storeStandardResponse($text)
    {
	   $rows = 'added,text';
	   $values = array(
		  time(),
		  $text
	   );
	   if ($this->insert('standard_responses', $values, $rows)) {
		  return 1;
	   }
	   return 0;
    }

    public function updateStandardResponse($responseId, $text)
    {
	   $where = array("id=", $responseId);
	   $rows = array(
		  "text" => $text
	   );
	   if ($this->update('standard_responses', $rows, $where)) {
		  return 1;
	   }
	   return 0;
    }

    public function deleteFeedbackQuestion($responseId)
    {
	   if ($this->delete('feedbackQuestions', "id = {$responseId}")) {
		  return 1;
	   }
	   return 0;
    }

    public function deleteStandardResponse($responseId)
    {
	   if ($this->delete('standard_responses', "id = {$responseId}")) {
		  return 1;
	   }
	   return 0;
    }

    public function isTyping($convoId, $isTyping)
    {

	   $typing = ($isTyping == "true") ? 1 : 0;
	   $where = array("conversationId=", $convoId);
	   $rows = array(
		  "isTypingOperator" => $typing
	   );
	   $this->update('conversations', $rows, $where);
    }

    public function areYouTyping($convoId)
    {
	   if ($convoId != 'undefined') {
		  $this->select('isTypingClient', 'conversations', "conversationId = {$convoId}");
		  $res = $this->getResult();
		  if ($res['isTypingClient']) {
			 $this->select('*', 'clients', "conversationId = {$convoId}");
			 $oo = $this->getResult();
			 $rows = $this->getRows();
			 if ($rows == 0) {
				return 0;
			 }
			 $msg = $oo['name'] . " " . $this->dt->getText('isTyping');
			 return $msg;
		  }
	   }
	   return 0;

    }

    public function inviteUserToChat($user, $operator, $department, $message)
    {
	   $where = array("id=", $user);
	   $rows = array(
		  "convoInvite" => "1",
		  "operatorId" => $operator,
		  "message" => $message,
		  "departmentId" => $department
	   );
	   return ($this->update('client_tracker', $rows, $where)) ? 1 : 0;
    }

    public function getLanguageKeys($targetLanguage)
    {
	   $this->select('*', 'display_text', "languageKey = '{$targetLanguage}'");
	   $res = $this->getResult();
	   $rows = $this->getRows();

	   if ($rows == 0) {
		  $this->select('*', 'display_text', "languageNice = '{$targetLanguage}'");
		  $res = $this->getResult();
		  $rows = $this->getRows();
	   }
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }

	   $return = '<table class="table" id="languageKeyTable">';

	   foreach ($res as $r)
	   {
		  $return .= '<tr style="border:none;">';
		  $return .= '<td style="border:none;">';

		  if (strlen($r['text']) > 90) {
			 $return .= '<textarea onblur="updateControlKey(\'' . $r['controlKey'] . '\')" id="langKey_' . $r['controlKey'] . '" class="span7 langKeyTarget">';
			 $return .= $r['text'];
			 $return .= '</textarea>';
		  }
		  else
		  {
			 $return .= '<input onblur="updateControlKey(\'' . $r['controlKey'] . '\')" id="langKey_' . $r['controlKey'] . '" type="text" class="span7 langKeyTarget" value="' . $r['text'] . '">';
		  }

		  $return .= '&nbsp;&nbsp;<span id="span_' . $r['controlKey'] . '" class="label label-success" style="display:none;">' . $this->dt->getText('updated') . '</span>';

		  $return .= "</td>";
		  $return .= "</tr>";
	   }

	   $return .= '</table>';

	   return $return;
    }

    public function setLanguageKeys($data)
    {
	   // not always an md5 value sent in, so convert if required
	   $bl = $data['language'];
	   $this->select('languageKey', 'display_text', "languageKey = '{$bl}'", null, null, 1);
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 0) {
		  $this->select('languageKey', 'display_text', "languageNice = '{$bl}'", null, null, 1);
		  $res = $this->getResult();
		  $rows = $this->getRows();
		  $al = $res['languageKey'];
	   }
	   else
	   {
		  $al = $res['languageKey'];
	   }

	   $where = array("languageKey=", $al, "and controlKey=", $data['key']);
	   $rows = array(
		  "text" => $data['newText']
	   );
	   return ($this->update('display_text', $rows, $where)) ? 1 : 0;
    }


    public function getClientList()
    {
	   $this->select('name,email,date', 'clients', "contactMe = 1", "", "email,name");
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }
	   return $res;
    }

    public function getClientListNoDate()
    {
	   $this->select('name,email', 'clients', "contactMe = 1");
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }
	   return $res;
    }

    public function getClientCSV()
    {
	   return $this->array_to_scv($this->getClientListNoDate());
    }

    public function array_to_scv($array, $header_row = true, $col_sep = ",", $row_sep = "\n", $qut = '"')
    {
	   $output = "";
	   if (!is_array($array) or !is_array($array[0])) return false;

	   //Header row.
	   if ($header_row) {
		  foreach ($array[0] as $key => $val)
		  {
			 //Escaping quotes.
			 $key = str_replace($qut, "$qut$qut", $key);
			 $output .= "$col_sep$qut$key$qut";
		  }
		  $output = substr($output, 1) . "\n";
	   }
	   //Data rows.
	   foreach ($array as $key => $val)
	   {
		  $tmp = '';
		  foreach ($val as $cell_key => $cell_val)
		  {
			 //Escaping quotes.
			 $cell_val = str_replace($qut, "$qut$qut", $cell_val);
			 $tmp .= "$col_sep$qut$cell_val$qut";
		  }
		  $output .= substr($tmp, 1) . $row_sep;
	   }

	   return $output;
    }

    public function getConvoArchive()
    {
	   $this->select('ca.conversationId,ca.started,u.username,c.name', 'ls_conversations_archive ca JOIN ls_users u on ca.operatorId = u.id JOIN ls_clients c on ca.conversationId = c.conversationId', null, 'ca.started desc', '', '', '', false);
	   $res = $this->getResult();

	   $rows = $this->getRows();
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }
	   return $res;
    }

    public function keepAliveUser($user)
    {
	   $where = array("id=", $user);
	   $rows = array(
		  "alive" => time()
	   );
	   $this->update('users', $rows, $where);
	   $this->cleanActiveUsers();
    }

    public function cleanActiveUsers()
    {
	   $timespan = time() - 3600;
	   $where = array("alive <", $timespan);
	   $rows = array(
		  "isOnline" => 0
	   );
	   $this->update('users', $rows, $where);
    }

    public function hasWritePermission($key, $permissions)
    {
	   return ($permissions[$key]['write']) ? true : false;
    }

    public function updateSetting($setting, $value)
    {
	   $where = array("setting=", $setting);
	   $rows = array(
		  "value" => trim($value)
	   );
	   if ($this->update('settings', $rows, $where)) {
		  return 1;
	   }
	   return 0;
    }

    public function GetOnlineSettings()
    {
	   $where = "setting LIKE 'onOff_on%'";
	   $this->select('setting,value', 'settings', $where);
	   $res = $this->getResult();
	   $return = array();
	   foreach ($res as $r)
	   {
		  $return[$r['setting']] = $r['value'];
	   }
	   return $return;
    }

    public function GetOfflineSettings()
    {
	   $where = "setting LIKE 'onOff_off%'";
	   $this->select('setting,value', 'settings', $where);
	   $res = $this->getResult();
	   $return = array();
	   foreach ($res as $r)
	   {
		  $return[$r['setting']] = $r['value'];
	   }
	   return $return;
    }

    public function getDisplayConfig()
    {
	   $where = "setting LIKE 'onOff_%'";
	   $this->select("setting,value", 'settings', $where);
	   $res = $this->getResult();
	   $return = Array();
	   foreach ($res as $r)
	   {
		  $return[$r['setting']] = $r['value'];
	   }
	   $this->select('value', 'settings', "setting = 'online'");
	   $res = $this->getResult();
	   $return['online'] = ($res['value'] == "true") ? 1 : 0;
	   return $return;
    }

    public function getGraphTotalConvos()
    {
	   $pastTime = strtotime('-1 month', time());
	   $this->select("YEAR(FROM_UNIXTIME(started)) as 'year',MONTH(FROM_UNIXTIME(started)) as 'month',DAY(FROM_UNIXTIME(started)) as 'day', count(*) as 'number'", 'conversations_archive', "started >= {$pastTime}", null, 'YEAR(FROM_UNIXTIME(started)),MONTH(FROM_UNIXTIME(started)),DAY(FROM_UNIXTIME(started))');

	   $rows = $this->getRows();
	   $res = $this->getResult();

	   if ($rows != 0) {
		  $jRet = "[";
		  if ($rows == 1) {
			 $ret = array();
			 $ret[0] = $res;
			 $res = $ret;
		  }

		  $tCount = 1;
		  foreach ($res as $r)
		  {
			 $temp = "['";
			 $temp .= $r['year'] . "-" . $r['month'] . "-" . $r['day'];
			 $temp .= "',";
			 $temp .= $r['number'];
			 $temp .= "]";

			 if ($tCount < count($res)) {
				$temp .= ",";
				$tCount++;
			 }
			 $jRet .= $temp;
		  }
		  $jRet .= "]";
	   }
	   else
	   {
		  $jRet = "[null]";
	   }
	   return $jRet;

    }


    public function getTotalConvosByDept()
    {

	   $this->select('*', 'departments', "departmentId != 0");
	   $rows = $this->getRows();
	   $departments = $this->getResult();

	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $departments;
		  $departments = $ret;
	   }

	   $depts = array();

	   foreach ($departments as $d)
	   {
		  $pastTime = strtotime('-1 month', time());
		  $this->select("count(*) as 'number'", 'ls_conversations_archive ca join ls_departments d ON d.departmentId = ca.departmentId', "started >= {$pastTime} AND ca.departmentId = {$d['departmentId']}", null, null, null, null, false);
		  $res = $this->getResult();
		  $depts[$d['departmentName']] = $res['number'];
	   }


	   return $depts;

    }

    public function getGraphAverageChat()
    {
	   $pastTime = strtotime('-1 month', time());
	   $this->select("YEAR(FROM_UNIXTIME(started)) as 'year',MONTH(FROM_UNIXTIME(started)) as 'month',DAY(FROM_UNIXTIME(started)) as 'day', AVG(ended - started) as 'average'", 'conversations_archive', "started >= {$pastTime}", null, 'YEAR(FROM_UNIXTIME(started)),MONTH(FROM_UNIXTIME(started)),DAY(FROM_UNIXTIME(started))');

	   $rows = $this->getRows();
	   $res = $this->getResult();

	   if ($rows != 0) {
		  $jRet = "[";
		  if ($rows == 1) {
			 $ret = array();
			 $ret[0] = $res;
			 $res = $ret;
		  }
		  $jRet = "[";
		  $tCount = 1;
		  foreach ($res as $r)
		  {
			 $temp = "['";
			 $temp .= $r['year'] . "-" . $r['month'] . "-" . $r['day'];
			 $temp .= "',";
			 $temp .= round($r['average']) / 60;
			 $temp .= "]";

			 if ($tCount < count($res)) {
				$temp .= ",";
				$tCount++;
			 }
			 $jRet .= $temp;
		  }
		  $jRet .= "]";
	   }
	   else
	   {
		  $jRet = "[null]";
	   }

	   return $jRet;

    }

    public function getLanguagesForDropDown()
    {
	   $this->select("languageKey,languageNice", 'display_text', null, null, 'languageKey,languageNice');
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }

	   return json_encode($res);

    }

    public function createLanguage($languageName)
    {
	   $langaugeKey = md5($languageName);
	   $this->select('*', 'display_text', "languageNice = 'English'");
	   $res = $this->getResult();
	   foreach ($res as $r)
	   {
		  $rows = 'controlKey,languageKey,languageNice,text';
		  $values = array(
			 $r['controlKey'],
			 $langaugeKey,
			 $languageName,
			 $r['text']);
		  $this->insert('display_text', $values, $rows);
	   }
	   return 1;
    }

    public function getLanguages()
    {
	   $this->select("languageKey,languageNice", 'display_text', null, null, 'languageKey,languageNice');
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }
	   return $res;
    }


}


