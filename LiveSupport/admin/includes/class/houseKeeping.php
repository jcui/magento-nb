<?php
/**
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 19/01/2013
 * Time: 08:37
 */

class houseKeeping extends database
{

    private $dt;

    function __construct()
    {
	   $this->connect();
	   $this->dt = new displayText();
    }


    private function archiveConversation($conversationId)
    {
	   $where = "conversationId = {$conversationId} AND user != 'SYSTEM'";
	   $this->select("MAX(time) as 'time'", 'conversations_responses', $where);
	   $endedTime = $this->getResult();
	   $thisEndedTime = $endedTime['time'];

	   $this->select("conversationId,started,COALESCE(ended,{$thisEndedTime}) as 'ended',operatorId,departmentId", 'conversations', "conversationId = {$conversationId}");
	   $convo = $this->getResult();
	   $rows = $this->getRows();

	   if ($rows > 0) {
		  // get responses
		  $this->select('*', 'conversations_responses', "conversationId = {$conversationId}");
		  $resp = $this->getResult();
		  $rows = $this->getRows();
		  if ($rows > 0) {
			 if ($rows == 1) {
				$ret = array();
				$ret[0] = $resp;
				$resp = $ret;
			 }

			 $rows = 'conversationId,started,ended,operatorId,departmentId';
			 $values = array(
				$convo['conversationId'],
				$convo['started'],
				$convo['ended'],
				$convo['operatorId'],
				$convo['departmentId']
			 );
			 if ($this->insert('conversations_archive', $values, $rows)) {
				foreach ($resp as $r)
				{
				    $rows = 'user,message,time,conversationId,messageType';
				    $values = array(
					   $r['user'],
					   $r['message'],
					   $r['time'],
					   $r['conversationId'],
					   $r['messageType']
				    );
				    $this->insert('conversations_responses_archive', $values, $rows);
				}

				$where = "conversationId = {$conversationId}";
				$this->delete('conversations', $where);
				$this->delete('conversations_responses', $where);

				return true;
			 }
		  }
	   }
	   return false;
    }

    private function isPickedUp($convoId)
    {
	   $this->select('pickedUp', 'conversations', "conversationId = {$convoId}");
	   $result = $this->getResult();
	   return ($result['pickedUp'] == "1") ? true : false;
    }

    public function cleanOperatorChat()
    {
	   $timespan = time() - 3600;
	   $where = "time < {$timespan}";
	   $this->delete('shoutbox', $where);
    }

    public function cleanChats()
    {
	   $timespan = time() - 3600;
	   $where = "time < {$timespan}";
	   $group = "conversationId";
	   $this->select("conversationId,MAX(time) as 'updated'", 'conversations_responses', $where, '', $group);
	   $result = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows > 0) {
		  if ($rows == 1) {
			 $ret = array();
			 $ret[0] = $result;
			 $result = $ret;
		  }
		  foreach ($result as $r)
		  {
			 if ($this->isPickedUp($r['conversationId'])) {
				if ($r['updated'] < $timespan) {
				    $rows = 'user,message,time,conversationId,messageType,isOperatorResponse';
				    $values = array(
					   "SYSTEM",
					   $this->dt->getText('archivedConvo'),
					   time(),
					   $r['conversationId'],
					   4,
					   1
				    );
				    $this->insert('conversations_responses', $values, $rows);
				    $this->archiveConversation($r['conversationId']);
				}
			 }
		  }
	   }
    }


}
