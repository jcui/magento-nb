<?php
/**
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 30/12/2012
 * Time: 11:05
 */

class conversation extends database
{

    private $data = array();
    private $facade;
    private $displayText;
    private $houseKeeping;

    function __construct()
    {
	   $this->connect();
	   $this->data = $_GET;
	   $this->facade = new conversationFacade();
	   $this->displayText = new displayText();
	   $this->houseKeeping = new houseKeeping();
    }

    // create convo
    // store client details

    private function getConvoDetails($convoId)
    {
	   $this->select('*', 'conversations', "conversationId = {$convoId}");
	   $res = $this->getResult();
	   return $res;
    }

    private function getOperatorDetails($userId)
    {
	   $this->select('*', 'users', "id = {$userId}");
	   $res = $this->getResult();
	   return $res;
    }

    public function getDepartments()
    {
	   $this->select('*', 'departments');
	   $ret = $this->getResult();
	   $rows = $this->getRows();

	   if ($rows == 1) {
		  $t = $ret;
		  $ret = array();
		  $ret[0] = $t;
	   }
	   return $ret;

    }

    public function getConversation()
    {
	   if ($this->data['convoId'] == "undefined") {
		  $ret = null;
	   }
	   else
	   {


		  $where = "conversationId = {$this->data['convoId']}";
		  $this->select('*', 'conversations_responses', $where, 'time asc');
		  $ret = $this->getResult();
		  $rows = $this->getRows();
		  if ($rows == 1) {
			 $t = $ret;
			 $ret = array();
			 $ret[0] = $t;
		  }
		  $this->isActiveConvo($this->data['convoId'], $this->getOperatorFromConvoId($this->data['convoId']));
	   }

	   return $this->facade->generateConvoOutput($ret);
    }

    public function insertFileAttachment()
    {


	   $this->select('*', 'files', "id = {$this->data['fileId']}");
	   $res = $this->getResult();
	   $message = '<a href="./download.php?fileId=' . $this->data['fileId'] . '">' . $res['description'] . '</a>';

	   $rows = 'user,message,time,conversationId,messageType,isOperatorResponse';
	   $values = array(
		  $this->data['user'],
		  mysql_real_escape_string($message),
		  time(),
		  $this->data['convoId'],
		  1,
		  1
	   );
	   $this->insert('conversations_responses', $values, $rows);

	   // update conversation
	   $this->isRead($this->data['convoId']);
    }

    public function recordResponse()
    {
	   $rows = 'user,message,time,conversationId,messageType,isOperatorResponse';
	   $values = array(
		  $this->data['user'],
		  $this->data['message'],
		  time(),
		  $this->data['convoId'],
		  1,
		  1
	   );
	   $this->insert('conversations_responses', $values, $rows);

	   // update conversation
	   $this->isRead($this->data['convoId']);

    }

    public function getOperatorFromConvoId($convoId)
    {
	   $where = "conversationId = {$convoId}";
	   $this->select('operatorId', 'conversations', $where);
	   $res = $this->getResult();
	   return $res['operatorId'];
    }

    public function whoResponsedLast($convoId)
    {
	   $where = "conversationId = {$convoId}";
	   $this->select('*', 'conversations_responses', $where, 'time desc', '', 1);
	   $result = $this->getResult();
	   return ($result['isOperatorResponse']) ? "operator" : "client";
    }

    public function isRead($convoId)
    {
	   $where = array("conversationId=", $convoId);
	   $rows = array(
		  "isReadOperator" => "1"
	   );
	   $this->update('conversations', $rows, $where);
    }

    public function isActiveConvo($convoId, $user)
    {
	   $where = array("operatorId=", $user);
	   $rows = array(
		  "isActiveConvo" => "0"
	   );
	   $this->update('conversations', $rows, $where);

	   $where = array("conversationId=", $convoId);
	   $rows = array(
		  "isActiveConvo" => "1"
	   );
	   $this->update('conversations', $rows, $where);
    }

    public function getChatTransferQueue()
    {
	   $operator = $this->data['user'];
	   $transfersIn = $this->getMyChatTransfersIn($operator);
	   $transfersOut = $this->getMyChatTransfersOut($operator);
	   return $this->facade->generateChatTransferQueue($transfersIn, $transfersOut);
    }

    public function getChatQueue()
    {
	   $operator = $this->data['user'];
	   $this->houseKeeping->cleanChats();

	   $where = "ended is null";
	   $this->select('*', 'conversations', $where);
	   $ret = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 1) {
		  $t = $ret;
		  $ret = array();
		  $ret[0] = $t;
	   }
	   for ($i = 0; $i < count($ret); $i++)
	   {
		  $ret[$i]['clientName'] = $this->getClientForConvo($ret[$i]['conversationId']);
	   }

	   $departments = $this->getDepartments();

	   $return = array();

	   if ($rows != 0) {
		  foreach ($departments as $d)
		  {
			 $return[$d['departmentName']]['name'] = $d['departmentName'];
			 $return[$d['departmentName']]['id'] = $d['departmentId'];
			 $return[$d['departmentName']]['chats'] = array();
			 foreach ($ret as $r)
			 {
				if ($r['departmentId'] == $d['departmentId']) {
				    array_push($return[$d['departmentName']]['chats'], $r);
				}
			 }
		  }
	   }

	   return $this->facade->generateChatQueue($return);
    }

    private function getMyChatTransfersIn($user)
    {
	   $this->select("ct.*,c.name as 'clientName',u.username as 'operatorFromUN',u2.username as 'operatorToUN'", 'ls_chatTransfer ct JOIN ls_clients c on ct.conversationId = c.conversationId LEFT JOIN ls_users u on u.id = ct.operatorFrom LEFT JOIN ls_users u2 on u2.id = ct.operatorTo', "operatorTo = {$user} AND accepted != 2", '', '', '', '', false);
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 0) {
		  return array();
	   }
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
		  return $res;
	   }
	   return $res;
    }

    private function getMyChatTransfersOut($user)
    {
	   $this->select("ct.*,c.name as 'clientName',u.username as 'operatorFromUN',u2.username as 'operatorToUN'", 'ls_chatTransfer ct JOIN ls_clients c on ct.conversationId = c.conversationId LEFT JOIN ls_users u on u.id = ct.operatorFrom LEFT JOIN ls_users u2 on u2.id = ct.operatorTo', "operatorFrom = {$user}", '', '', '', '', false);
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 0) {
		  return array();
	   }
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
		  return $res;
	   }
	   return $res;

    }

    public function getMyChats($user)
    {
	   $where = "operatorId = {$user} and ended is null";
	   $this->select('*', 'conversations', $where);
	   $ret = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 1) {
		  $t = $ret;
		  $ret = array();
		  $ret[0] = $t;
	   }
	   for ($i = 0; $i < count($ret); $i++)
	   {
		  $ret[$i]['respondedLast'] = $this->whoResponsedLast($ret[$i]['conversationId']);
		  $ret[$i]['clientName'] = $this->getClientForConvo($ret[$i]['conversationId']);
	   }
	   return $this->facade->generateMyChatQueue($ret);
    }

    private function getClientForConvo($convoId)
    {
	   $where = "conversationId = {$convoId}";
	   $sel = "name";
	   $this->select($sel, 'clients', $where);
	   $ret = $this->getResult();
	   $rows = $this->getRows();

	   $rows = $this->getRows();
	   if ($rows == 1) {
		  $t = $ret;
		  $ret = array();
		  $ret[0] = $t;
	   }
	   return $ret[0]['name'];
    }

    public function pickUpConversation($convoId, $userId)
    {
	   $where = array("conversationId=", $convoId);
	   $rows = array(
		  "pickedUp" => "1",
		  "operatorId" => $userId
	   );
	   return ($this->update('conversations', $rows, $where)) ? true : false;
    }

    public function endConversation($convoId)
    {
	   // get conversation details
	   $convo = $this->getConvoDetails($convoId);
	   // get operator details
	   $operator = $this->getOperatorDetails($convo['operatorId']);

	   // insert "left chat" message
	   $rows = 'user,message,time,conversationId,messageType,isOperatorResponse';
	   $values = array(
		  $operator['username'],
		  "{$operator['username']} {$this->displayText->getText('leftConvo')}",
		  time(),
		  $convoId,
		  5,
		  1
	   );
	   if ($this->insert('conversations_responses', $values, $rows)) {
		  // end conversation
		  $where = array("conversationId=", $convoId);
		  $rows = array(
			 "ended" => time()
		  );
		  if ($this->update('conversations', $rows, $where)) {
			 return 1;
		  }
	   }
	   return 0;
    }

    public function getoperatorsOnline()
    {
	   $res = "";
	   $this->select('*', 'users', "isOnline = 1");
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }
	   return $this->facade->operatorsOnline($res);
    }

    public function getOperatorChat()
    {
	   $this->houseKeeping->cleanOperatorChat();
	   $res = "";
	   $this->select('u.username,sb.message', 'ls_shoutbox sb join ls_users u on u.id = sb.user ', "", "time asc", '', '', '', false);
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }
	   return $this->facade->operatorChat($res);
    }

    public function submitOperatorMessage()
    {
	   $rows = 'user,message,time';
	   $values = array(
		  $this->data['userId'],
		  $this->data['message'],
		  time()
	   );
	   $this->insert('shoutbox', $values, $rows);
    }

    public function logConversationTransfer()
    {

	   $this->select('*', 'chatTransfer', "conversationId = {$this->data['convoId']}");
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 0) {
		  $rows = 'operatorTo,operatorFrom,conversationId,message,accepted';
		  $values = array(
			 $this->data['to'],
			 $this->data['from'],
			 $this->data['convoId'],
			 $this->data['message'],
			 0
		  );
		  if ($this->insert('chatTransfer', $values, $rows)) {
			 return 1;
		  }
	   }
	   else
	   {
		  return 1;
	   }

	   return 0;
    }

    public function getTransferDetails($transferId)
    {
	   $this->select('*', 'chatTransfer', "id={$transferId}");
	   return $this->getResult();
    }

    public function acceptTransfer($transferId)
    {
	   $transfer = $this->getTransferDetails($transferId);
	   // delete chat transfer row
	   $this->cancelTransfer($transfer['id']);
	   // change operator id in conversation
	   $where = array("conversationId=", $transfer['conversationId']);
	   $rows = array(
		  "operatorId" => $transfer['operatorTo']
	   );
	   $message = $this->displayText->getText('conversationTransfered');
	   $this->update('conversations', $rows, $where);
	   // insert message into conversation
	   $rows = 'user,message,time,conversationId,messageType,isOperatorResponse';
	   $values = array(
		  "System",
		  $message,
		  time(),
		  $transfer['conversationId'],
		  2,
		  1
	   );
	   if ($this->insert('conversations_responses', $values, $rows)) {
		  return $transfer['conversationId'];
	   }

    }

    public function rejectTransfer($transferId)
    {
	   $where = array("id=", $transferId);
	   $rows = array(
		  "accepted" => "2"
	   );
	   $this->update('chatTransfer', $rows, $where);
    }

    public function cancelTransfer($transferId)
    {
	   $this->delete('chatTransfer', "id = {$transferId}");
    }

    public function getArchivedConversation($convoId)
    {
	   $this->select('*', 'ls_conversations_archive ca JOIN ls_conversations_responses_archive cra ON cra.conversationId = ca.conversationId', "ca.conversationId = {$convoId}", '', '', '', '', false);
	   $res = $this->getResult();
	   $rows = $this->getRows();
	   if ($rows == 1) {
		  $ret = array();
		  $ret[0] = $res;
		  $res = $ret;
	   }
	   return $this->facade->displayArchivedConversation($res);
    }
}
