<?php
/**
 * Created by PhpStorm.
 * User: Ryan
 * Date: Aug 12, 2010
 * Time: 5:13:06 PM
 * To change this template use File | Settings | File Templates.
 */


class database
{

    /* Database login details */

    private $db_host;
    private $db_user;
    private $db_pass;
    private $db_name;
    private $debugOn;
    private $prefix = "ls_";
    public $numResults;

    private $con = false; // Checks if connection is active
    private $result = array(); // Holds results

    private function getVars()
    {
	   include 'common/common.php';
	   $this->db_host = $this_db_host;
	   $this->db_user = $this_db_user;
	   $this->db_pass = $this_db_pass;
	   $this->db_name = $this_db_name;
    }

    function setDebug($debug)
    {
	   $this->debugOn = ($debug) ? true : false;
    }

    public function getDbVars()
    {
	   $ar['host'] = $this->db_host;
	   $ar['user'] = $this->db_user;
	   $ar['pass'] = $this->db_pass;
	   $ar['db'] = $this->db_name;
	   return $ar;
    }

    private function convertTableName($table)
    {
	   return $this->prefix . $table;
    }

    /* Connect to database, only one connection allowed */
    public function connect()
    {
	   $this->getVars();
	   if (!$this->con) {
		  $myconn = mysql_connect($this->db_host, $this->db_user, $this->db_pass);

		  if ($myconn) {
			 $seldb = mysql_select_db($this->db_name, $myconn);
			 if ($seldb) {
				$this->con = true;
				return true;
			 } else {
				return false;
			 }
		  } else {
			 if ($this->debugOn) {
				echo mysql_error();
			 }
			 return false;
		  }
	   } else {
		  return true;
	   }
    }

    /* End of connect function */

    /* Changes selected database */
    public function setDatabase($name)
    {
	   if ($this->con) {
		  if (mysql_close()) {
			 $this->con = false;
			 $this->result = null;
			 $this->db_name = $name;
			 $this->connect();
		  }
	   }
    }

    /* End of change database */

    /* Check table exists when performing queries */
    public function tableExists($table)
    {
	   $tablesInDb = mysql_query('SHOW TABLES FROM ' . $this->db_name . ' LIKE"' . $table . '"');
	   if ($tablesInDb) {
		  if (mysql_num_rows($tablesInDb) == 1) {
			 return true;
		  } else {
			 if ($this->debugOn) {
				echo mysql_error();
			 }
			 return false;
		  }
	   }
    }

    /* End of check table exists */

    /*
        * Selects information from the database.
        * Required: table (the name of the table)
        * Optional: rows (the columns requested, separated by commas)
        * where (column = value as a string)
        * order (column DIRECTION as a string)
    */

    /* SELECT */


    public function select($rows = '*', $table, $where = null, $order = null, $group = null, $limit = null, $join = null, $convertTable = true)
    {
	   if ($convertTable) {
		  $table = $this->convertTableName($table);
	   }
	   $this->result = null;
	   $q = 'SELECT ' . $rows . ' FROM ' . $table;
	   if ($join != null) $q .= ' ' . $join;

	   if ($where != null) $q .= ' WHERE ' . $where;
	   if ($group != null) $q .= ' GROUP BY ' . $group;
	   if ($order != null) $q .= ' ORDER BY ' . $order;
	   if ($limit != null) $q .= ' LIMIT ' . $limit;
	   if ($this->debugOn) {
		  echo $q;
	   }
	   $query = mysql_query($q);
	   if ($query) {
		  $this->numResults = mysql_num_rows($query);
		  for ($i = 0; $i < $this->numResults; $i++) {
			 $r = mysql_fetch_array($query);
			 $key = array_keys($r);
			 for ($x = 0; $x < count($key); $x++) {
				// sanitise keys, only alpha values allowed
				if (!is_int($key[$x])) {
				    if (mysql_num_rows($query) > 1)
					   $this->result[$i][$key[$x]] = utf8_decode($r[$key[$x]]);
				    else if (mysql_num_rows($query) < 1)
					   $this->result = null;
				    else
					   $this->result[$key[$x]] = utf8_decode($r[$key[$x]]);
				}
			 }
		  }
	   } else {
		  if ($this->debugOn) {
			 echo mysql_error();
		  }
		  echo mysql_error();
		  return false;
	   }
    }

    /* End of SELECT */

    /*
    * Insert values into the table
    * Required: table (the name of the table)
    * values (the values to be inserted)
    * Optional: rows (if values don't match the number of rows)
    */
    public function insert($table, $values, $rows = null)
    {
	   $table = $this->convertTableName($table);
	   if ($this->tableExists($table)) {
		  $insert = 'INSERT INTO ' . $table;
		  if ($rows != null)
			 $insert .= ' (' . $rows . ')';


		  for ($i = 0; $i < count($values); $i++) {
			 if (is_string($values[$i]))
				$values[$i] = '"' . utf8_encode($values[$i]) . '"';
		  }
		  $values = implode(',', $values);
		  $insert .= ' VALUES (' . $values . ')';
		  if ($this->debugOn) {
			 echo $insert;
		  }
		  $ins = mysql_query($insert);
		  if ($ins) {
			 return true;
		  } else {
			 if ($this->debugOn) {
				echo mysql_error();
			 }
			 ;
			 return false;
		  }
	   }
    }

    /* End of insert */

    /*
    * Deletes table or records where condition is true
    * Required: table (the name of the table)
    * Optional: where (condition [column = value])
    */
    public function delete($table, $where = null)
    {
	   $table = $this->convertTableName($table);
	   if ($this->tableExists($table)) {
		  if ($where == null) {
			 $delete = 'DELETE ' . $table;
		  } else {
			 $delete = 'DELETE FROM ' . $table . ' WHERE ' . $where;
		  }
		  $del = mysql_query($delete);

		  if ($del) {
			 return true;
		  } else {
			 if ($this->debugOn) {
				echo mysql_error();
			 }
			 return false;
		  }
	   } else {
		  if ($this->debugOn) {
			 echo mysql_error();
		  }
		  return false;
	   }
    }

    /* End of Delete */

    /*
    * Updates the database with the values sent
    * Required: table (the name of the table to be updated
    * rows (the rows/values in a key/value array
    * where (the row/condition in an array (row,condition) )
    */

    public function update($table, $rows, $where)
    {
	   $table = $this->convertTableName($table);
	   if ($this->tableExists($table)) {
		  // Parse the where values
		  // even values (including 0) contain the where rows
		  // odd values contain the clauses for the row
		  for ($i = 0; $i < count($where); $i++) {
			 if ($i % 2 != 0) {
				if (is_string($where[$i])) {
				    if (($i + 1) == null)
					   $where[$i] = '"' . $where[$i] . '" AND ';
				    else
					   $where[$i] = '"' . $where[$i] . '"';
				}
			 }
		  }
		  $where = implode('', $where);
		  $update = 'UPDATE ' . $table . ' SET ';
		  $keys = array_keys($rows);
		  for ($i = 0; $i < count($rows); $i++) {
			if (is_string($rows[$keys[$i]])) {
				$update .= $keys[$i] . '="' . utf8_encode($rows[$keys[$i]]) . '"';
			 }
			 else {
				$update .= $keys[$i] . '=' . utf8_encode($rows[$keys[$i]]);
			 }
			 // Parse to add commas
			 if ($i != count($rows) - 1) {
				$update .= ',';
			 }
		  }
		  $update .= ' WHERE ' . $where;
		  if ($this->debugOn) {
			 echo $update;
		  }
		  $query = mysql_query($update);
		  if ($query) {
			 return true;
		  }
		  else {
			 if ($this->debugOn) {
				echo mysql_error();
			 }
			 return false;
		  }
	   }
	   else {
		  if ($this->debugOn) {
			 echo mysql_error();
		  }
		  return false;
	   }
    }

    /* END of update */

    /* Return Results */
    public function getResult()
    {
	   return $this->result;
    }

    /* Return number of rows */
    public function getRows()
    {
	   return $this->numResults;
    }

}
