<?php
/**
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 28/12/2012
 * Time: 10:26
 */

class login extends database
{


    function __construct()
    {
	   $this->connect();
    }

    public function checkLogin($data)
    {

	   if (isset($data['loggedIn']) && $data['loggedIn'] == "true") {
		  return true;
	   }
	   else
	   {
		  header('location: login.php');
	   }

    }

    public function checkEveryoneIsLoggedOut()
    {
	   $this->select("count(*) as'online'", 'users', "isOnline = '1'");
	   $res = $this->getResult();
	   if ($res['online'] == 0) {
		  $where = array("setting = ", "online");
		  $rows = array(
			 "value" => "false"
		  );
		  $this->update('settings', $rows, $where);
	   }


    }

    public function logOut()
    {
	   $_SESSION = null;
	   session_destroy();
	   header('location: login.php');
    }

    public function handleLogin()
    {
	   if (isset($_POST['username']) && $_POST['username'] != '') {
		  // username submitted, check for password
		  if (isset($_POST['password']) && $_POST['password'] != '') {
			 // password submitted, check details against the database
			 $where = "email = '{$_POST['username']}'";
			 $this->select('id,password,salt,department,userLevel,username', 'users', $where);
			 $res = $this->getResult();
			 $rows = $this->getRows();
			 if ($rows == 1) {
				$salt = $res['salt'];
				$hashDB = $res['password'];
				$hash = sha1($salt . $_POST['password']);
				if ($hash == $hashDB) {
				    // details are good, generate session
				    $_SESSION['userId'] = $res['id'];
				    $_SESSION['loggedIn'] = "true";
				    $_SESSION['department'] = $res['department'];
				    $_SESSION['username'] = $res['username'];
				    $this->generatePermissionStructure($res['id']);
				    $this->updateLoginDb($res['id']);
				    // check for expired users
				    $timespan = time() - 3600;
				    $where = array("alive <", $timespan);
				    $rows = array(
					   "isOnline" => 1
				    );
				    $this->update('users', $rows, $where);
				    return true;
				}
				else
				{
				    return false;
				}
			 }
			 else
			 {
				return false;
			 }
		  }
		  else
		  {
			 return false;
		  }
	   }
	   else
	   {
		  return false;
	   }
    }

    private function updateLoginDb($user)
    {
	   $where = array("id=", $user);
	   $rows = array(
		  "isOnline" => "1",
		  "lastLogon" => time()
	   );
	   $this->update('users', $rows, $where);
    }

    public function logUserOut($user)
    {
	   $this->updateLogoutDb($user);
    }

    private function updateLogoutDb($user)
    {
	   $where = array("id=", $user);
	   $rows = array(
		  "isOnline" => "0"
	   );
	   $this->update('users', $rows, $where);
	   $this->checkEveryoneIsLoggedOut();
    }

    public function isAllowedFunction($functionKey, $functionPermission)
    {
	   return (isset($_SESSION['permissions'][$functionKey]) && $_SESSION['permissions'][$functionKey][$functionPermission]) ? true : false;
    }


    public function generatePermissionStructure($userId)
    {

	   $sel = 'pk.keyId,pl.permissionTitle,p.userRead,p.userWrite';
	   $from = 'permissions p JOIN ls_permissions_keys pk ON pk.id = p.keyId JOIN ls_permissions_levels pl ON pl.id = p.titleId JOIN ls_users u ON u.userLevel = p.titleId';
	   $where = "u.id = {$userId}";

	   $this->select($sel, $from, $where);
	   $result = $this->getResult();

	   $arrPerm = array();

	   foreach ($result as $r)
	   {
		  $arrPerm[$r['keyId']]['read'] = $r['userRead'];
		  $arrPerm[$r['keyId']]['write'] = $r['userWrite'];
	   }

	   $_SESSION['permissions'] = $arrPerm;
    }

}
