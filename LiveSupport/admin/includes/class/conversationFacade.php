<?php
/**
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 30/12/2012
 * Time: 11:28
 */

class conversationFacade
{

    private $displayTextInfo;


    function __construct()
    {
	   $this->displayTextInfo = new displayText();
    }


    public function generateConvoOutput($data)
    {
	   $response = "";

	   if ($data == null) {
		  $response .= '<div class="alert alert-info"><p><i class="icon-info-sign"></i>&nbsp;' . $this->displayTextInfo->getText('selectConvo') . '</p></div>';
	   }
	   else
	   {
		  $response .= '<ul id="chatouptutx" class="unstyled">';
		  for ($i = 0; $i < count($data); $i++)
		  {
			 $class = ($i & 1) ? "odd" : "";
			 $temp = '
			 <li class="' . $class . '">
				<small>' . date('h:i', $data[$i]['time']) . ' <strong>' . $data[$i]['user'] . ':</strong></small>
				<p>' . $data[$i]['message'] . '</p>
			 </li>';
			 $response .= $temp;
		  }
		  $response .= "</ul>";
		  $response .= '<input type="hidden" id="convoId" value="' . $data[0]['conversationId'] . '">';
	   }
	   return $response;
    }

    public function generateChatTransferQueue($transfersIn, $transfersOut)
    {
	   $response = "";
	   if (count($transfersIn) > 0 || count($transfersOut) > 0) {
		  if (count($transfersIn) > 0) {
			 $response .= '<legend><small>' . $this->displayTextInfo->getText('incomingTransfers') . '</small></legend>';
			 foreach ($transfersIn as $t)
			 {
				$response .= '
				    <div class="btn-group">
					 <a class="btn btn-info dropdown-toggle span3" style="width:335px; text-align:left;" data-toggle="dropdown" href="#">
					   <i class="icon-user"></i> ' . $t['clientName'] . ' <small>( ' . $t['operatorFromUN'] . ' <i class="icon-arrow-right"></i> ' . $t['operatorToUN'] . '  )</small>
					   <span class="caret pull-right"></span>
					 </a>
					 <ul class="dropdown-menu">
					   <li><a href="#" onClick="acceptTransfer(' . $t['id'] . ')">' . $this->displayTextInfo->getText('accept') . '</a></li>
					   <li><a href="#" onClick="rejectTransfer(' . $t['id'] . ')">' . $this->displayTextInfo->getText('reject') . '</a></li>
					   <li class="divider"></li>
					   <li class="transferMessageListItem"><p class="tinyTextDrop span1">' . $t['message'] . '</p></li>
					 </ul>
				    </div><div class="clear">&nbsp;</div>';
				$response .= '<input type="hidden" id="myChatsTransfersWaiting" value="' . count($transfersIn) . '">';
			 }
		  }

		  if (count($transfersOut) > 0) {


			 $response .= '<legend><small>' . $this->displayTextInfo->getText('outgoingTransfers') . '</small></legend>';
			 foreach ($transfersOut as $t)
			 {
				$class = ($t['accepted'] == "2") ? "btn-danger" : "";
				$response .= '
				    <div class="btn-group">
					 <a class="btn ' . $class . ' dropdown-toggle span3" style="width:335px; text-align:left;" data-toggle="dropdown" href="#">
					   <i class="icon-user"></i> ' . $t['clientName'] . ' <small>( ' . $t['operatorFromUN'] . ' <i class="icon-arrow-right"></i> ' . $t['operatorToUN'] . '  )</small>
					   <span class="caret pull-right"></span>';
				if ($t['accepted'] == "2") {
				    $response .= '<br><br><p>' . $this->displayTextInfo->getText('transferRejected') . '</p>';
				}

				$response .= '</a><ul class="dropdown-menu">';
				if ($t['accepted'] == "2") {
				    $response .= '<li><a href="#" onClick="cancelTransfer(' . $t['id'] . ')">' . $this->displayTextInfo->getText('formClear') . '</a></li>';
				}
				else
				{
				    $response .= '<li><a href="#" onClick="cancelTransfer(' . $t['id'] . ')">' . $this->displayTextInfo->getText('formCancel') . '</a></li>';
				}
				$response .= '</ul></div>';

				$response .= '<div class="clear">&nbsp;</div>';
			 }
		  }
	   }
	   return $response;
    }

    public function generateChatQueue($data)
    {
	   $pickedUp = 0;
	   $response = "";

	   $cCount = 0;
	   foreach ($data as $d)
	   {
		  if (count($d['chats']) > 0) {
			 $cCount++;
		  }
	   }


	   $response .= '<ul class="unstyled">';
	   if ($cCount == 0) {
		  $response .= '<li>' . $this->displayTextInfo->getText('noChatsWaiting') . '</li>';
	   }
	   else
	   {
		  foreach ($data as $d)
		  {
			 if (count($d['chats']) != 0) {
				$response .= '<legend><small>' . $d['name'] . '</small></legend>';
				foreach ($d['chats'] as $chat)
				{
				    if (!$chat['pickedUp']) {
					   $pickedUp++;
				    }
				    $class = ($chat['pickedUp']) ? "ls-chat-btn" : "ls-chat-btn btn-warning";
				    $text = ($chat['pickedUp']) ? $this->displayTextInfo->getText('chatAssigned') : $this->displayTextInfo->getText('chatNew');
				    $javascript = ($chat['pickedUp']) ? "disabled" : 'onclick="convoToPickUp = ' . $chat['conversationId'] . '; openModal(\'pickUpChat\');"';
				    $response .= '<li><button class="btn ' . $class . '" ' . $javascript . '>' . $chat['clientName'] . ' <span class="assigned">( ' . $text . ' )</span></button></li>';

				}
			 }
		  }
	   }
	   $response .= '</ul>';
	   $response .= '<input type="hidden" id="pickedUpConvos" value="' . $pickedUp . '">';
	   return $response;
    }

    public function generateMyChatQueue($data)
    {
	   $response = "";
	   $chatWaiting = 0;
	   if (count($data) == 0) {
		  $response .= '<p>' . $this->displayTextInfo->getText('noActiveConvos') . '</p><input type="hidden" id="mainTotalChats" value="0">';
	   }
	   else
	   {
		  $response .= '<ul class="unstyled">';

		  for ($i = 0; $i < count($data); $i++)
		  {
			 if (($data[$i]['respondedLast'] == "client") && (!$data[$i]['isActiveConvo'])) {
				$chatWaiting++;
			 }
			 $class = "";
			 if (!$data[$i]['isReadOperator'] && !$data[$i]['isReadClient']) {
				$class = "btn-warning";
			 }
			 if (($data[$i]['respondedLast'] == "client") && (!$data[$i]['isActiveConvo'])) {
				$class = "btn-warning";
			 }
			 if ($data[$i]['isActiveConvo']) {
				$class = "btn-success";
			 }
			 $response .= '<li><button class="btn ls-chat-btn ' . $class . '" onclick="getConversation(' . $data[$i]['conversationId'] . ',1);">' . $data[$i]['clientName'] . '</button></li>';
		  }

		  $response .= '</ul><input type="hidden" id="myChatsWaiting" value="' . $chatWaiting . '"><input type="hidden" id="mainTotalChats" value="' . count($data) . '">';
	   }
	   return $response;
    }

    public function operatorsOnline($data)
    {
	   $response = "";
	   $response = '<ul class="unstyled">';

	   foreach ($data as $d)
	   {
		  $response .= "<li>";
		  $response .= '<button disabled class="btn ls-chat-btn btn-primary">' . $d['username'] . '</button>';
		  $response .= "</li>";
	   }

	   $response .= "</ul>";
	   return $response;
    }

    public function operatorChat($data)
    {
	   if (count($data) != 0) {
		  $response = "";
		  $response = '<table class="table table-striped">';

		  foreach ($data as $d)
		  {
			 $response .= '<tr>';
			 $response .= '<td>';
			 $response .= '<strong>';
			 $response .= $d['username'];
			 $response .= '</strong>';
			 $response .= '<p>';
			 $response .= $d['message'];
			 $response .= '</p>';
			 $response .= '</td>';
			 $response .= '</tr>';
		  }


		  $response .= '</table>';
	   }
	   else
	   {
		  $response = "";
	   }
	   return $response;
    }

    public function displayArchivedConversation($data)
    {
	   $response = "";
	   if (count($data) == 0) {
		  $response = $this->displayTextInfo->getText('noConvoToDisplay');
	   }
	   else
	   {
		  $response .= '<ul id="chatouptut" class="unstyled">';
		  for ($i = 0; $i < count($data); $i++)
		  {
			 $class = ($i & 1) ? "odd" : "";
			 $temp = '
			 <li class="' . $class . '">
				<small>' . date('h:i', $data[$i]['time']) . ' <strong>' . $data[$i]['user'] . ':</strong></small>
				<p>' . $data[$i]['message'] . '</p>
			 </li>';
			 $response .= $temp;
		  }
		  $response .= "</ul>";
	   }
	   return $response;
    }

}
