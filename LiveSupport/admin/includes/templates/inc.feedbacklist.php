<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 07/01/2013
 * Time: 23:33
 */
session_start();
require '../class/database.php';
require '../class/displayText.php';
require '../class/admin.php';

$admin = new admin();
$responses = $admin->getFeedBackQuestions();
$text = new displayText();
?>


<?php if (count($responses) == 0) { ?>
<div class="alert alert-info">
    <p><?php echo $text->getText('noFeedbackQuestions'); ?></p>
</div>

<?php } else { ?>
<table class="table table-striped">
    <thead>
    <tr>
	   <th><?php echo $text->getText('question'); ?></th>
	   <th><?php echo $text->getText('delete'); ?></th>
    </tr>
    </thead>
    <tbody>


	   <?php foreach ($responses as $r) { ?>
    <tr id="row_<?php echo $r['id']; ?>">
	   <td class="thisStandardResponse"><?php echo $r['question']; ?></td>
	   <td class="feedback_delete_response">
		  <?php if ($admin->hasWritePermission('feedback', $_SESSION['permissions'])) { ?>
		  <a href="#"
			onclick="deleteFbQuestion(<?php echo $r['id']; ?>,'1','Are you sure?','Delete Feedback Question');"><img
				src="img/delete.png" width="25"></a>
		  <?php } ?>
	   </td>
    </tr>
	   <?php } ?>

    </tbody>
</table>
<?php } ?>