<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 01/01/2013
 * Time: 18:40
 */
session_start();
?>

<legend class="lead"><?php echo $text->getText('standardResponses'); ?>
    <button type="button" class="close" onclick="hideUtilityPage();"
		  aria-hidden="true">&times; <?php echo $text->getText('closeTab'); ?></button>
</legend>


<div class="alert alert-error" id="users_delete_response_error" style="display:none">
    <strong><?php echo $text->getText('error'); ?></strong>

    <p><?php echo $text->getText('errorDeletingResponse'); ?></p>
</div>

<div class="alert alert-success" id="users_delete_response_ok" style="display:none">
    <strong><?php echo $text->getText('allDone'); ?></strong>

    <p><?php echo $text->getText('responseDeleted'); ?></p>
</div>

<div class="alert alert-error" id="users_edit_response_error" style="display:none">
    <strong><?php echo $text->getText('error'); ?></strong>

    <p><?php echo $text->getText('errorEditingResponse'); ?></p>
</div>

<div class="alert alert-success" id="users_edit_response_ok" style="display:none">
    <strong><?php echo $text->getText('allDone'); ?></strong>

    <p><?php echo $text->getText('responseSaved'); ?></p>
</div>


<div id="ajaxResponsesContainer"></div>


<br/>
<?php if ($admin->hasWritePermission('standardresponses', $_SESSION['permissions'])) { ?>
<div class="input-append">
    <input class="span6" id="responseAddInput" type="text"
		 placeholder="<?php echo $text->getText('typeYourResponse'); ?>">
    <button class="btn btn-primary" type="button"
		  onclick="addResponse();"><?php echo $text->getText('addResponse'); ?></button>
</div>
<?php } ?>


<script type="text/javascript">
    $(document).ready(function() {
	   $('#ajaxResponsesContainer').load('includes/templates/inc.standardresponselist.php');
    });
</script>