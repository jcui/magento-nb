<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 06/01/2013
 * Time: 15:33
 */
session_start();
require '../class/database.php';
require '../class/displayText.php';
require '../class/admin.php';


$admin = new admin();
$text = new displayText();

$currentUsers = $admin->getOperatorUsers();

?>

<?php if (count($currentUsers) > 0) { ?>
<table class="table table-striped">
    <thead>
    <tr>
	   <th><?php echo $text->getText('formEmail'); ?></th>
	   <th><?php echo $text->getText('formDisplayName'); ?></th>
	   <th><?php echo $text->getText('formPassword'); ?></th>
	   <th><?php echo $text->getText('department'); ?></th>
	   <th><?php echo $text->getText('permissionLevel'); ?></th>
	   <th>&nbsp;</th>
	   <th>&nbsp;</th>
	   <th colspan="2" style="display:none;">&nbsp;</th>

    </tr>
    </thead>
    <tbody>
	   <?php foreach ($currentUsers as $c) { ?>
    <tr id="row_<?php echo $c['id']; ?>">
	   <td class="users_username"><?php echo $c['email']; ?></td>
	   <td class="users_displayname"><?php echo $c['username']; ?></td>
	   <td class="users_password">**********</td>
	   <td class="users_department"><?php echo $admin->convertDepartmentId($c['department']); ?></td>
	   <td class="users_permissions"><?php echo $admin->convertPermisisonId($c['userLevel']); ?></td>


	   <?php if ($admin->hasWritePermission('users', $_SESSION['permissions'])) { ?>
	   <td class="users_edit_user"><a href="#"
							    onclick="editUser(<?php echo $c['id']; ?>,<?php echo $c['userLevel']; ?>,<?php echo $c['department']; ?>);"><img
			 src="img/edit.png" width="25"></a></td>
	   <?php if ($c['id'] == $_SESSION['userId']) { ?>
		  <td class="users_delete_user">&nbsp;</td>
		  <? } else { ?>
		  <td class="users_delete_user"><a href="#"
									onclick="deleteUser(<?php echo $c['id']; ?>,'1','Are you sure?','Delete User');"><img
				src="img/delete.png" width="25"></a></td>
		  <?php } ?>
	   <td class="users_save_changes" colspan="2" style="display:none;">
		  <button class="btn btn-small btn-success"
				type="button"><?php echo $text->getText('saveChanges'); ?></button>
	   </td>
	   <?php } ?>
    </tr>
	   <?php } ?>
    </tbody>
</table>
<?php } else { ?>
<p>No users created</p>
<?php } ?>