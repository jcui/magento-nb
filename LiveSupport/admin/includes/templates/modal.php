<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 01/01/2013
 * Time: 12:30
 */

?>


<div id="LSChatModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	aria-hidden="true">
    <div class="modal-header">
	   <h3 id="LSChatModalLabel"></h3>
    </div>
    <div class="modal-body">
    </div>
    <div class="modal-footer">
	   <button class="btn" data-dismiss="modal" aria-hidden="true"
			 id="lsChatClose"><?php echo $text->getText('cancel'); ?></button>
	   <button class="btn btn-primary" id="LSChatModalButton"></button>
    </div>
</div>