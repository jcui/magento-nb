<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 01/01/2013
 * Time: 18:41
 */
session_start();
?>

<legend class="lead"><?php echo $text->getText('files'); ?>
    <button type="button" class="close" onclick="hideUtilityPage();"
		  aria-hidden="true">&times; <?php echo $text->getText('closeTab'); ?></button>
</legend>
<script src="includes/uploadify/jquery.uploadify.js" type="text/javascript"></script>


<div class="row">
    <div class="span3">
	   <?php if ($admin->hasWritePermission('files', $_SESSION['permissions'])) { ?>
	   <form>
		  <legend>
			 <small><?php echo $text->getText('chooseDescription'); ?></small>
		  </legend>
		  <div class="control-group" id="fileDescGroup">
			 <div class="controls">
				<input type="text" id="fileDescription" name="fileDescription">
			 </div>
		  </div>


		  <legend>
			 <small><?php echo $text->getText('selectFile'); ?></small>
		  </legend>

		  <div id="queue"></div>
		  <input disabled="true" id="file_upload" name="file_upload" type="file" multiple="true">
	   </form>
	   <legend>
		  <small><?php echo $text->getText('uploadFile'); ?></small>
	   </legend>
	   <button class="btn btn-primary" style="width:270px;"
			 onclick="validatefileupload()"><?php echo $text->getText('uploadFile'); ?></button>



	   <?php } ?>
    </div>
    <div class="span5">
	   <legend>
		  <small><?php echo $text->getText('existingFiles'); ?></small>
	   </legend>


	   <div class="alert alert-error" id="users_delete_file_error" style="display:none">
		  <strong><?php echo $text->getText('error'); ?></strong>

		  <p><?php echo $text->getText('errorDeletingFile'); ?></p>
	   </div>

	   <div class="alert alert-success" id="users_delete_file_ok" style="display:none">
		  <strong><?php echo $text->getText('allDone'); ?></strong>

		  <p><?php echo $text->getText('fileDeleted'); ?></p>
	   </div>


	   <div id="ajaxFilesHolder"></div>


    </div>

</div>

<script type="text/javascript">
    <?php $timestamp = time();?>

    $(document).ready(function() {
	   $('#ajaxFilesHolder').load('includes/templates/inc.fileslist.php');
    });

    function validatefileupload() {
	   if ($('#fileDescription').val() != "") {
		  $('#fileDescGroup').removeClass('error');
		  $('#file_upload').uploadify('upload', '*');
	   }
	   else {
		  $('#fileDescGroup').addClass('error');
	   }

    }

    $(function() {
	   $('#file_upload').uploadify({
		  'auto' : false,
		  'formData'	: {
			 'timestamp' : '<?php echo $timestamp;?>',
			 'token'	: '<?php echo md5('unique_salt' . $timestamp);?>'
		  },
		  'buttonText' : "<?php echo $text->getText('selectAFile'); ?>",
		  'queueSizeLimit' : 1,
		  'swf'	 : 'includes/uploadify/uploadify.swf',
		  'uploader' : 'includes/uploadify/uploadify.php',
		  'onUploadStart' : function(file) {
			 $('#file_upload').uploadify("settings", "formData", {"description": $("#fileDescription").val()})
		  },

		  'onUploadSuccess' : function(file, data, response) {
			 $('#fileDescription').val('');
			 if (data == "0") {
				alert('<?php echo $text->getText('invalidFileType'); ?> ')
			 }
			 if (data == "2") {
				alert('<?php echo $text->getText('fileMoveError'); ?> ')
			 }
			 if (data == "3") {
				alert('<?php echo $text->getText('fileDatabaseError'); ?> ')
			 }
			 $('#ajaxFilesHolder').load('includes/templates/inc.fileslist.php');
		  }
	   });
    });
</script>