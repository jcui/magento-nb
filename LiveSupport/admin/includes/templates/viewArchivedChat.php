<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 09/02/2013
 * Time: 19:36
 */

session_start();
require '../class/database.php';
require '../class/conversation.php';
require '../class/conversationFacade.php';
require '../class/houseKeeping.php';
require '../class/displayText.php';
$thisConvo = new conversation();
$convo = $thisConvo->getArchivedConversation($_GET['conversation']);
echo $convo;
?>



