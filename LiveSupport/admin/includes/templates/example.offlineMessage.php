<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 24/02/2013
 * Time: 18:12
 */
?>
<div id="lsChatInitContainer">
    <?php if ($displayConfig['onOff_offIsImage'] == "1") { ?>
    <a href="" id="liveSupportChatHyperLink" onclick="launchLiveSupport();">
	   <img src="<?php echo $displayConfig['onOff_offImage']; ?>"/></a>
    <?php } else { ?>
    <div id="lsChatInitContainerInner"
	    style="overflow:hidden; text-align:<?php echo $displayConfig['onOff_offTextAlign']; ?>; padding:<?php echo $displayConfig['onOff_offPadding']; ?>px; width:<?php echo $displayConfig['onOff_OffWidth']; ?>px; height:<?php echo $displayConfig['onOff_offHeight']; ?>px; background:<?php echo $displayConfig['onOff_offBackground']; ?>; color:<?php echo $displayConfig['onOff_offTextColor']; ?>; border:1px solid <?php echo $displayConfig['onOff_offBorderColor']; ?>;">
	   <p><?php echo $displayConfig['onOff_offDisplayText']; ?></p>
	   <?php if ($displayConfig['onOff_offIsLinkImage'] == "1") { ?>
	   <p><a href="" id="liveSupportChatHyperLink" style="text-decoration:none; color:#000;">
		  <img src="<?php echo $displayConfig['onOff_offDisplayLinkImage']; ?>"/>
	   </a></p>
	   <?php } else { ?>
	   <p><a href="" id="liveSupportChatHyperLink"
		    style="text-decoration:<?php echo $displayConfig['onOff_offLinkUnderline']; ?>; color:#000;"><?php echo $displayConfig['onOff_offDisplayLinkText']; ?></a>
	   </p>
	   <?php } ?>
    </div>
    <?php } ?>
</div>


