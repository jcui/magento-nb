<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 24/02/2013
 * Time: 18:16
 */

require '../class/database.php';
require '../class/admin.php';
require '../class/displayText.php';

$admin = new admin();
$displayConfig = $admin->getDisplayConfig();
$allsettings = $admin->getSettings();


if ($_GET['invite'] == 1) {
    require 'example.inviteMessage.php';
}
else if ($_GET['online'] == 1) {
    require 'example.onlineMessage.php';
}
else
{
    require 'example.offlineMessage.php';
}