<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 09/02/2013
 * Time: 12:13
 */
session_start();
require '../class/database.php';
require '../class/admin.php';
require '../class/displayText.php';
$admin = new admin();
$text = new displayText();
$operatorsOnline = $admin->getOperatorUsersOnline($_SESSION['userId']);

?>

<div id="operatorTransferDetails">

    <p><?php echo $text->getText('transferTarget'); ?></p>

    <p>
	   <select name="availableOperators" id="transferAvailableOperators">
		  <?php foreach ($operatorsOnline as $o) { ?>
		  <option value="<?php echo $o['id']; ?>"><?php echo $o['username']; ?></option>
		  <?php } ?>
	   </select>
    </p>

    <p><?php echo $text->getText('transferMessage'); ?></p>

    <p>
	   <TEXTAREA name="transferRequestMessage" id="transferRequestMessage" class="span5"></TEXTAREA>
    </p>
</div>

<div id="MTError" style="display:none;">
    <div class="alert alert-error">
	   <?php echo $text->getText('transferError'); ?>
    </div>
</div>

<script type="text/javascript">
    $('#operatorTransferReject').hide();
    $('#operatorTransferReject').hide();
    $('#operatorTransferWait').hide();
    $('#operatorTransferAccept').hide();
</script>