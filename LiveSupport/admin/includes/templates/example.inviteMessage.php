<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 18/03/2013
 * Time: 21:21
 */
//print_r($allsettings);
// set position
$x = 0;
$y = 0;
switch ($allsettings['invite_position'])
{
    case "topleft":
	   {
	   $x = "top";
	   $y = "left";
	   break;
	   }
    case "bottomleft":
	   {
	   $x = "bottom";
	   $y = "left";
	   break;
	   }
    case "topright":
	   {
	   $x = "top";
	   $y = "right";
	   break;
	   }
    case "bottomright":
	   {
	   $x = "bottom";
	   $y = "right";
	   break;
	   }
}

?>

<div id="inviteHolder" style="width:450px; height:500px; border:1px solid black; position:relative;">
    <div id="ls_ls_inviteContainer"
	    style="color:<?php echo $allsettings['invite_textcolor']; ?>; border:1px solid <?php echo $allsettings['invite_border']; ?>; padding:<?php echo $allsettings['invite_padding']; ?>px; width:<?php echo $allsettings['invite_width']; ?>px; min-height:<?php echo $allsettings['invite_height']; ?>px; background:<?php echo $allsettings['invite_background']; ?>; position:absolute; <?php echo $x; ?>:<?php echo $allsettings['invite_spacing']; ?>px; <?php echo $y; ?>:<?php echo $allsettings['invite_spacing']; ?>px;">
	   <strong>Operator:</strong>

	   <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt</p>
    </div>
</div>
