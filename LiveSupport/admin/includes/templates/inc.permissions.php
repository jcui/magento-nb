<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 06/01/2013
 * Time: 15:33
 */
session_start();
require '../class/database.php';
require '../class/admin.php';
require '../class/displayText.php';


$admin = new admin();
$text = new displayText();


$perms = $admin->getPermissionGroups();
$fullPerms = $admin->getFullPemrissions();
?>


<table class="table table-striped" id="userPermissionTable">
    <thead>
    <tr>
	   <td>&nbsp;</td>
	   <th colspan="2"><?php echo $text->getText('visitorsOnline'); ?></th>
	   <th colspan="2"><?php echo $text->getText('files'); ?></th>
	   <th colspan="2"><?php echo $text->getText('users'); ?></th>
	   <th colspan="2"><?php echo $text->getText('logs'); ?></th>
	   <th colspan="2"><?php echo $text->getText('settings'); ?></th>
	   <th colspan="2"><?php echo $text->getText('feedback'); ?></th>
	   <th colspan="2"><?php echo $text->getText('standardResponses'); ?></th>
	   <td colspan="1">&nbsp;</td>

    </tr>
    <tr>
	   <td>&nbsp;</td>
	   <td><?php echo $text->getText('read'); ?></td>
	   <td><?php echo $text->getText('write'); ?></td>
	   <td><?php echo $text->getText('read'); ?></td>
	   <td><?php echo $text->getText('write'); ?></td>
	   <td><?php echo $text->getText('read'); ?></td>
	   <td><?php echo $text->getText('write'); ?></td>
	   <td><?php echo $text->getText('read'); ?></td>
	   <td><?php echo $text->getText('write'); ?></td>
	   <td><?php echo $text->getText('read'); ?></td>
	   <td><?php echo $text->getText('write'); ?></td>
	   <td><?php echo $text->getText('read'); ?></td>
	   <td><?php echo $text->getText('write'); ?></td>
	   <td><?php echo $text->getText('read'); ?></td>
	   <td><?php echo $text->getText('write'); ?></td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($perms as $p) { ?>

    <tr id="permRow_<?php echo $p['id']; ?>">
	   <td><input type="hidden" class="permissionName"
			    value="<?php echo $p['id']; ?>"><?php echo $p['permissionTitle']; ?></td>
	   <td><input class="vo_read" type="checkbox" <?php if ($fullPerms[$p['id']]['visitorsonline']['read']) {
		  echo "checked";
	   } ?>></td>
	   <td><input class="vo_write" type="checkbox" <?php if ($fullPerms[$p['id']]['visitorsonline']['write']) {
		  echo "checked";
	   } ?>></td>
	   <td><input class="f_read" type="checkbox" <?php if ($fullPerms[$p['id']]['files']['read']) {
		  echo "checked";
	   } ?>></td>
	   <td><input class="f_write" type="checkbox" <?php if ($fullPerms[$p['id']]['files']['write']) {
		  echo "checked";
	   } ?>></td>
	   <td><input class="u_read" type="checkbox" <?php if ($fullPerms[$p['id']]['users']['read']) {
		  echo "checked";
	   } ?>></td>
	   <td><input class="u_write" type="checkbox" <?php if ($fullPerms[$p['id']]['users']['write']) {
		  echo "checked";
	   } ?>></td>
	   <td><input class="l_read" type="checkbox" <?php if ($fullPerms[$p['id']]['logs']['read']) {
		  echo "checked";
	   } ?>></td>
	   <td><input class="l_write" type="checkbox" <?php if ($fullPerms[$p['id']]['logs']['write']) {
		  echo "checked";
	   } ?>></td>
	   <td><input class="s_read" type="checkbox" <?php if ($fullPerms[$p['id']]['settings']['read']) {
		  echo "checked";
	   } ?>></td>
	   <td><input class="s_write" type="checkbox" <?php if ($fullPerms[$p['id']]['settings']['write']) {
		  echo "checked";
	   } ?>></td>
	   <td><input class="fb_read" type="checkbox" <?php if ($fullPerms[$p['id']]['feedback']['read']) {
		  echo "checked";
	   } ?>></td>
	   <td><input class="fb_write" type="checkbox" <?php if ($fullPerms[$p['id']]['feedback']['write']) {
		  echo "checked";
	   } ?>></td>
	   <td><input class="sr_read" type="checkbox" <?php if ($fullPerms[$p['id']]['standardresponses']['read']) {
		  echo "checked";
	   } ?>></td>
	   <td><input class="sr_write" type="checkbox" <?php if ($fullPerms[$p['id']]['standardresponses']['write']) {
		  echo "checked";
	   } ?>></td>
	   <?php if (count($perms) > 1) { ?>
	   <td style="width:5%;"><a href="#"
						   onclick="deletePermissionGroup(<?php echo $p['id']; ?>,'1','<?php echo $text->getText('areYouSure'); ?>','<?php echo $text->getText('deletePermGroup'); ?>');"><img
			 src="img/delete.png" width="30"></a></td>
	   <?php } ?>
    </tr>

	   <?php } ?>
    </tbody>
</table>

<form class="form-inline">
    <div class="input-append">
	   <input class="span4" id="permAddInput" type="text" placeholder="<?php echo $text->getText('permGroupName'); ?>">
	   <button class="btn btn-primary" type="button"
			 onclick="addPerm();"><?php echo $text->getText('addPermGroup'); ?></button>
    </div>
    <button class="btn btn-success pull-right" type="button"
		  onclick="savePerms();"><?php echo $text->getText('saveChanges'); ?></button>

</form>
