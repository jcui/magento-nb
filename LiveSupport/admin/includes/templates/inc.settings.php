<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 01/01/2013
 * Time: 18:41
 */
$timezones = $admin->tzList();
$onlineSettings = $admin->GetOnlineSettings();
$offlineSettings = $admin->GetOfflineSettings();
$allSettings = $admin->getSettings();
$languages = $admin->getLanguages();

?>

<legend class="lead"><?php echo $text->getText('settings'); ?>
    <button type="button" class="close" onclick="hideUtilityPage();"
		  aria-hidden="true">&times; <?php echo $text->getText('closeTab'); ?></button>
</legend>


<div class="tabbable"> <!-- Only required for left/right tabs -->
<ul class="nav nav-tabs">
    <li class="active"><a href="#perfCont" data-toggle="tab"><?php echo $text->getText('perfSettings'); ?></a></li>
    <li><a href="#applCont" data-toggle="tab"><?php echo $text->getText('applicationSettings'); ?></a></li>
    <li><a href="#langCont" data-toggle="tab"
		 onclick="getLanguagekeys();"><?php echo $text->getText('languageSettings'); ?></a></li>
    <li><a href="#brandCont" data-toggle="tab"><?php echo $text->getText('bradingSettings'); ?></a></li>
</ul>
<div class="tab-content">
<div class="tab-pane active" id="perfCont">

    <div class="row">
	   <div class="span4" style="padding:10px;">
		  <div class="sliderContainer">
			 <ul id="perfStatCont" class="unstyled">
				<li>
				    <legend>
					   <small><?php echo $text->getText('clientChatRefresh'); ?> <span
							 class="pull-right sliderStatLabel"></span>
					   </small>
				    </legend>
				    <div class="statSlider" id="clientChatRefresh"></div>
				    <input type="hidden" class="statMin" value="1000">
				    <input type="hidden" class="statMax" value="10000">
				    <input type="hidden" class="statVal" value="<?php echo $allSettings['clientChatRefresh']; ?>">

				    <input type="hidden" class="statValPerf" value="4000">
				    <input type="hidden" class="statValResp" value="2000">
				</li>
				<li>
				    <legend>
					   <small><?php echo $text->getText('adminChatRefresh'); ?> <span
							 class="pull-right sliderStatLabel">3 <?php echo $text->getText('seconds'); ?></span>
					   </small>
				    </legend>
				    <div class="statSlider" id="adminChatRefresh"></div>
				    <input type="hidden" class="statMin" value="1000">
				    <input type="hidden" class="statMax" value="10000">
				    <input type="hidden" class="statVal" value="<?php echo $allSettings['adminChatRefresh']; ?>">

				    <input type="hidden" class="statValPerf" value="4000">
				    <input type="hidden" class="statValResp" value="2000">
				</li>
				<li>
				    <legend>
					   <small><?php echo $text->getText('chatQRefresh'); ?><span
							 class="pull-right sliderStatLabel">3 <?php echo $text->getText('seconds'); ?></span>
					   </small>
				    </legend>
				    <div class="statSlider" id="chatQueueRefresh"></div>
				    <input type="hidden" class="statMin" value="1000">
				    <input type="hidden" class="statMax" value="10000">
				    <input type="hidden" class="statVal" value="<?php echo $allSettings['chatQueueRefresh']; ?>">

				    <input type="hidden" class="statValPerf" value="5000">
				    <input type="hidden" class="statValResp" value="3000">
				</li>
				<li>
				    <legend>
					   <small><?php echo $text->getText('myChatRefresh'); ?> <span
							 class="pull-right sliderStatLabel">3 <?php echo $text->getText('seconds'); ?></span>
					   </small>
				    </legend>
				    <div class="statSlider" id="myChatQueueRefresh"></div>
				    <input type="hidden" class="statMin" value="1000">
				    <input type="hidden" class="statMax" value="10000">
				    <input type="hidden" class="statVal" value="<?php echo $allSettings['myChatQueueRefresh']; ?>">

				    <input type="hidden" class="statValPerf" value="5000">
				    <input type="hidden" class="statValResp" value="3000">
				</li>
				<li>
				    <legend>
					   <small><?php echo $text->getText('operatorChatRefresh'); ?> <span
							 class="pull-right sliderStatLabel">3 <?php echo $text->getText('seconds'); ?></span>
					   </small>
				    </legend>
				    <div class="statSlider" id="operatorChatRefresh"></div>
				    <input type="hidden" class="statMin" value="1000">
				    <input type="hidden" class="statMax" value="10000">
				    <input type="hidden" class="statVal" value="<?php echo $allSettings['operatorChatRefresh']; ?>">

				    <input type="hidden" class="statValPerf" value="6000">
				    <input type="hidden" class="statValResp" value="2000">
				</li>
				<li>
				    <legend>
					   <small><?php echo $text->getText('visitorsOnlineRefresh'); ?> <span
							 class="pull-right sliderStatLabel">3 <?php echo $text->getText('seconds'); ?></span>
					   </small>
				    </legend>
				    <div class="statSlider" id="visitorsOnlineRefresh"></div>
				    <input type="hidden" class="statMin" value="1000">
				    <input type="hidden" class="statMax" value="10000">
				    <input type="hidden" class="statVal"
						 value="<?php echo $allSettings['visitorsOnlineRefresh']; ?>">

				    <input type="hidden" class="statValPerf" value="8000">
				    <input type="hidden" class="statValResp" value="4000">
				</li>
				<li>
				    <legend>
					   <small><?php echo $text->getText('operatorsOnlineRefresh'); ?> <span
							 class="pull-right sliderStatLabel">3 <?php echo $text->getText('seconds'); ?></span>
					   </small>
				    </legend>
				    <div class="statSlider" id="operatorsOnlineRefresh"></div>
				    <input type="hidden" class="statMin" value="1000">
				    <input type="hidden" class="statMax" value="10000">
				    <input type="hidden" class="statVal"
						 value="<?php echo $allSettings['operatorsOnlineRefresh']; ?>">

				    <input type="hidden" class="statValPerf" value="8000">
				    <input type="hidden" class="statValResp" value="4000">
				</li>


			 </ul>

		  </div>

	   </div>


	   <div class="span3" style="padding:10px;" id="perfControls">

		  <p>
			 <button id="statRespSwitch"
				    onclick="setBars('response'); updateSetting('performanceSetting','response');"
				    class="btn btn-block" type="button">
				<?php echo $text->getText('optimizeResponse'); ?>
			 </button>
		  </p>
		  <p>
			 <button id="statPerfSwitch"
				    onclick="setBars('performance'); updateSetting('performanceSetting','performance');"
				    class="btn btn-block"
				    type="button"><?php echo $text->getText('optimizePerformance'); ?>
			 </button>
		  </p>
		  <p>
			 <button id="statCustSwitch" onclick="updateSetting('performanceSetting','custom');"
				    class="btn btn-block" type="button"><?php echo $text->getText('optimizeCustom'); ?></button>
		  </p>

	   </div>


    </div>


</div>
<div class="tab-pane" id="applCont">
    <label><?php echo $text->getText('applicationTitle'); ?></label>
    <input type="text" name="applicationTitle" id="applicationTitle" class="span6"
		 value="<?php echo $allSettings['applicationTitle']; ?>"
		 onblur="updateSetting('applicationTitle',this.value);">
    <label><?php echo $text->getText('applicationLogo'); ?></label>
    <input type="text" class="span6" name="applicationLogo" id="applicationLogo"
		 value="<?php echo $allSettings['applicationLogo']; ?>" onblur="updateSetting('applicationLogo',this.value);">
    <label><?php echo $text->getText('applicationTimezone'); ?></label>
    <select id="timezone" class="span6" onblur="updateSetting('timezone',this.value);">
	   <?php foreach ($timezones as $key => $value) { ?>
	   <option value="<?php echo $value; ?>" <?php if ($value == $allSettings['timezone']) {
		  echo "selected";
	   } ?>><?php echo $key; ?></option>
	   <?php } ?>
    </select>
    <label><?php echo $text->getText('applicationLanguage'); ?></label>
    <select id="language" class="span6" onblur="updateSetting('language',this.value);">
	   <?php foreach ($languages as $k) { ?>
	   <option value="<?php echo $k['languageKey']; ?>" <?php if ($k['languageKey'] == $allSettings['language']) {
		  echo "selected";
	   } ?>><?php echo $k['languageNice']; ?></option>
	   <?php } ?>
    </select>
    <label><?php echo $text->getText('applicationAlowedFileTypes'); ?></label>
    <input name="allowedFileTypes" id="allowedFileTypes" type="text" class="span6"
		 value="<?php echo $allSettings['allowedFileTypes']; ?>"
		 onblur="updateSetting('allowedFileTypes',this.value);">
    <label><?php echo $text->getText('applicationEmailAddress'); ?></label>
    <input name="adminEmail" id="adminEmail" type="text" class="span6" value="<?php echo $allSettings['adminEmail']; ?>"
		 onblur="updateSetting('adminEmail',this.value);">

</div>

<div class="tab-pane" id="langCont">


    <form class="form-inline">
	   <div class="input-append">
		  <input class="span2" id="newLanguageName" type="text"
			    placeholder="<?php echo $text->getText('newLangName'); ?>">
		  <button class="btn btn-primary" type="button" id="operatorResponseButton" onclick="addLanguage()"
				>
			 <?php echo $text->getText('add'); ?>
		  </button>
	   </div>

	   <div class="btn-group">
		  <a class="btn dropdown-toggle btn-success" data-toggle="dropdown" href="#">
			 <span id="currentLanguageName"><?php echo $allSettings['languageNice']; ?></span>
			 <span class="caret"></span>
		  </a>
		  <ul class="dropdown-menu" id="langKeysList">
		  </ul>
	   </div>

	   <input class="span4" id="filterLanguageKeySearchField" type="text"
			placeholder="<?php echo $text->getText('quickSearch'); ?>">

    </form>


    <div id="langQueryResult"></div>


</div>


<div class="tab-pane" id="brandCont">
<div class="tabbable"> <!-- Only required for left/right tabs -->
<ul class="nav nav-tabs">
    <li class="active"><a href="#onCont" data-toggle="tab"><?php echo $text->getText('chatOnDisplay'); ?></a></li>
    <li><a href="#offCont" data-toggle="tab"><?php echo $text->getText('chatOffDisplay'); ?></a></li>
    <li><a href="#inviteCont" data-toggle="tab"><?php echo $text->getText('chatInviteDisplay'); ?></a></li>
</ul>
<div class="tab-content">
<div class="tab-pane active" id="onCont">


    <div class="wysywigContainer">
	   <div class="wysywigLeft">

		  <form>
			 <div class="control-group" id="controldgroup_onOff_onIsImage">
				<label><?php echo $text->getText('useImage'); ?></label>
				<select id="chooseImageOnline"
					   onchange="updateSetting('onOff_onIsImage',this.value); switchIsImageOnOff(this.value);">
				    <option value="1" <?php if ($onlineSettings['onOff_onIsImage'] == 1) {
					   echo 'selected="true"';
				    } ?> ><?php echo $text->getText('yes'); ?></option>
				    <option value="0" <?php if ($onlineSettings['onOff_onIsImage'] == 0) {
					   echo 'selected="true"';
				    } ?> ><?php echo $text->getText('no'); ?></option>
				</select>
			 </div>


			 <div id="onImageSpecific">
				<div class="control-group" id="controldgroup_onOff_onImage">
				    <label><?php echo $text->getText('formImageURL'); ?></label>
				    <input type="text" value="<?php echo $onlineSettings['onOff_onImage']; ?>"
						 onchange="updateSetting('onOff_onImage',this.value);">
				</div>
			 </div>
			 <div id="onTextSpecific">

				<div class="control-group" id="controldgroup_onOff_onIsLinkImage">
				    <label><?php echo $text->getText('useImageLink'); ?></label>
				    <select id="chooseLinkImageOnline"
						  onchange="updateSetting('onOff_onIsLinkImage',this.value); switchIsLinkImageOnOff(this.value);">
					   <option value="1" <?php if ($onlineSettings['onOff_onIsLinkImage'] == 1) {
						  echo 'selected="true"';
					   } ?> ><?php echo $text->getText('yes'); ?></option>
					   <option value="0" <?php if ($onlineSettings['onOff_onIsLinkImage'] == 0) {
						  echo 'selected="true"';
					   } ?> ><?php echo $text->getText('no'); ?></option>
				    </select>
				</div>

				<div id="onLinkImageSpecific">
				    <div class="control-group" id="controldgroup_onOff_onDisplayLinkImage">
					   <label><?php echo $text->getText('formImageURL'); ?></label>
					   <input type="text" value="<?php echo $onlineSettings['onOff_onDisplayLinkImage']; ?>"
							id="onDisplayLinkImage"
							onchange="updateSetting('onOff_onDisplayLinkImage',this.value);">
				    </div>
				</div>
				<div id="onLinkTextSpecific">

				    <div class="control-group" id="controldgroup_onOff_onDisplayLinkText">
					   <label><?php echo $text->getText('formLinkText'); ?></label>
					   <input type="text" value="<?php echo $onlineSettings['onOff_onDisplayLinkText']; ?>"
							id="onDisplayLinkText"
							onchange="updateSetting('onOff_onDisplayLinkText',this.value);">
				    </div>

				    <div class="control-group" id="controldgroup_onOff_onLinkUnderline">
					   <label><?php echo $text->getText('linkUnderlined'); ?></label>
					   <select id="LinkUnderline" onchange="updateSetting('onOff_onLinkUnderline',this.value);">
						  <option value="none" <?php if ($onlineSettings['onOff_onLinkUnderline'] == 'none') {
							 echo 'selected="true"';
						  } ?>><?php echo $text->getText('no'); ?></option>
						  <option value="underline" <?php if ($onlineSettings['onOff_onLinkUnderline'] == 'underline') {
							 echo 'selected="true"';
						  } ?>><?php echo $text->getText('yes'); ?></option>
					   </select>
				    </div>


				</div>


				<div class="control-group" id="controldgroup_onOff_onWidth">
				    <label><?php echo $text->getText('formWidth'); ?></label>
				    <input type="text" value="<?php echo $onlineSettings['onOff_OnWidth']; ?>" id="onWidth"
						 onchange="updateSetting('onOff_onWidth',this.value);">
				</div>
				<div class="control-group" id="controldgroup_onOff_onHeight">
				    <label><?php echo $text->getText('formHeight'); ?></label>
				    <input type="text" value="<?php echo $onlineSettings['onOff_onHeight']; ?>" id="onHeight"
						 onchange="updateSetting('onOff_OnHeight',this.value);">
				</div>
				<div class="control-group" id="controldgroup_onOff_onBackground">
				    <label><?php echo $text->getText('formBackgroundColor'); ?></label>

				    <div class="input-append">
					   <input type="text" value="<?php echo $onlineSettings['onOff_onBackground']; ?>""
					   id="onBackground" onchange="updateSetting('onOff_onBackground',this.value);">
					   <span class="add-on" id="onBackgroundPicker" style="cursor:pointer;"><img
							 src="img/color_wheel.png"/></span>
				    </div>
				</div>
				<div class="control-group" id="controldgroup_onOff_onBorderColor">
				    <label><?php echo $text->getText('formBorderColor'); ?></label>

				    <div class="input-append">
					   <input type="text" value="<?php echo $onlineSettings['onOff_onBorderColor']; ?>"
							id="onBorder" onchange="updateSetting('onOff_onBorderColor',this.value);">
					   <span class="add-on" id="onBorderPicker" style="cursor:pointer;"><img
							 src="img/color_wheel.png"/></span>
				    </div>
				</div>
				<div class="control-group" id="controldgroup_onOff_onTextColor">
				    <label><?php echo $text->getText('formTextColor'); ?></label>

				    <div class="input-append">
					   <input type="text" value="<?php echo $onlineSettings['onOff_onTextColor']; ?>"
							id="onTextColor" onchange="updateSetting('onOff_onTextColor',this.value);">
					   <span class="add-on" id="onTextColorPicker" style="cursor:pointer;"><img
							 src="img/color_wheel.png"/></span>
				    </div>
				</div>
				<div class="control-group" id="controldgroup_onOff_onTextAlign">
				    <label><?php echo $text->getText('formTextAlign'); ?></label>
				    <select id="onTextAlign" onchange="updateSetting('onOff_onTextAlign',this.value);">
					   <option value="center" <?php if ($onlineSettings['onOff_onTextAlign'] == 'center') {
						  echo 'selected="true"';
					   } ?>><?php echo $text->getText('center'); ?></option>
					   <option value="left" <?php if ($onlineSettings['onOff_onTextAlign'] == 'left') {
						  echo 'selected="true"';
					   } ?>><?php echo $text->getText('left'); ?></option>
					   <option value="right" <?php if ($onlineSettings['onOff_onTextAlign'] == 'right') {
						  echo 'selected="true"';
					   } ?>><?php echo $text->getText('right'); ?></option>
				    </select>
				</div>
				<div class="control-group" id="controldgroup_onOff_onPadding">
				    <label><?php echo $text->getText('formPadding'); ?></label>
				    <input type="text" value="<?php echo $onlineSettings['onOff_onPadding']; ?>" id="onPadding"
						 onchange="updateSetting('onOff_onPadding',this.value);">
				</div>
				<div class="control-group" id="controldgroup_onDisplayText">
				    <label><?php echo $text->getText('formDisplayText'); ?></label>
				    <textarea rows="10" id="onDisplayText"
						    onkeyup="updateSetting('onOff_onDisplayText',this.value);"><?php echo $onlineSettings['onOff_onDisplayText']; ?></textarea>
				</div>

			 </div>
		  </form>

	   </div>
	   <div class="wysywigRight">
		  <div id="thisOnlinePreview"></div>
	   </div>
    </div>

</div>
<div class="tab-pane" id="offCont">
    <div class="wysywigContainer">
	   <div class="wysywigLeft">

		  <form>
			 <div class="control-group" id="controldgroup_onOff_offIsImage">
				<label><?php echo $text->getText('useImage'); ?></label>
				<select id="chooseImageOffline"
					   onchange="updateSetting('onOff_offIsImage',this.value); switchIsImageOnOffOff(this.value);">
				    <option value="1" <?php if ($offlineSettings['onOff_offIsImage'] == 1) {
					   echo 'selected="true"';
				    } ?> ><?php echo $text->getText('yes'); ?></option>
				    <option value="0" <?php if ($offlineSettings['onOff_offIsImage'] == 0) {
					   echo 'selected="true"';
				    } ?> ><?php echo $text->getText('no'); ?></option>
				</select>
			 </div>


			 <div id="offImageSpecific">
				<div class="control-group" id="controldgroup_onOff_offImage">
				    <label><?php echo $text->getText('formImageURL'); ?></label>
				    <input type="text" value="<?php echo $offlineSettings['onOff_offImage']; ?>"
						 onchange="updateSetting('onOff_offImage',this.value);">
				</div>
			 </div>
			 <div id="offTextSpecific">

				<div class="control-group" id="controldgroup_onOff_offIsLinkImage">
				    <label><?php echo $text->getText('useImageLink'); ?></label>
				    <select id="chooseLinkImageOffline"
						  onchange="updateSetting('onOff_offIsLinkImage',this.value); switchIsLinkImageOnOffOff(this.value);">
					   <option value="1" <?php if ($offlineSettings['onOff_offIsLinkImage'] == 1) {
						  echo 'selected="true"';
					   } ?> ><?php echo $text->getText('yes'); ?></option>
					   <option value="0" <?php if ($offlineSettings['onOff_offIsLinkImage'] == 0) {
						  echo 'selected="true"';
					   } ?> ><?php echo $text->getText('no'); ?></option>
				    </select>
				</div>

				<div id="offLinkImageSpecific">
				    <div class="control-group" id="controldgroup_onOff_offDisplayLinkImage">
					   <label><?php echo $text->getText('formImageURL'); ?></label>
					   <input type="text" value="<?php echo $offlineSettings['onOff_offDisplayLinkImage']; ?>"
							id="offDisplayLinkImage"
							onchange="updateSetting('onOff_offDisplayLinkImage',this.value);">
				    </div>
				</div>
				<div id="offLinkTextSpecific">

				    <div class="control-group" id="controldgroup_onOff_offDisplayLinkText">
					   <label><?php echo $text->getText('formLinkText'); ?></label>
					   <input type="text" value="<?php echo $offlineSettings['onOff_offDisplayLinkText']; ?>"
							id="offDisplayLinkText"
							onchange="updateSetting('onOff_offDisplayLinkText',this.value);">
				    </div>

				    <div class="control-group" id="controldgroup_onOff_offLinkUnderline">
					   <label><?php echo $text->getText('linkUnderlined'); ?></label>
					   <select id="LinkUnderlineoff"
							 onchange="updateSetting('onOff_offLinkUnderline',this.value);">
						  <option value="none" <?php if ($offlineSettings['onOff_offLinkUnderline'] == 'none') {
							 echo 'selected="true"';
						  } ?>><?php echo $text->getText('no'); ?></option>
						  <option value="underline" <?php if ($offlineSettings['onOff_offLinkUnderline'] == 'underline') {
							 echo 'selected="true"';
						  } ?>><?php echo $text->getText('yes'); ?></option>
					   </select>
				    </div>


				</div>


				<div class="control-group" id="controldgroup_onOff_OffWidth">
				    <label><?php echo $text->getText('formWidth'); ?></label>
				    <input type="text" value="<?php echo $offlineSettings['onOff_OffWidth']; ?>" id="offWidth"
						 onchange="updateSetting('onOff_OffWidth',this.value);">
				</div>
				<div class="control-group" id="controldgroup_onOff_offHeight">
				    <label><?php echo $text->getText('formHeight'); ?></label>
				    <input type="text" value="<?php echo $offlineSettings['onOff_offHeight']; ?>" id="offHeight"
						 onchange="updateSetting('onOff_offHeight',this.value);">
				</div>
				<div class="control-group" id="controldgroup_onOff_offBackground">
				    <label><?php echo $text->getText('formbackgroundColor'); ?></label>

				    <div class="input-append">
					   <input type="text" value="<?php echo $offlineSettings['onOff_offBackground']; ?>"
							id="offBackground" onchange="updateSetting('onOff_offBackground',this.value);">
					   <span class="add-on" id="offBackgroundPicker" style="cursor:pointer;"><img
							 src="img/color_wheel.png"/></span>
				    </div>
				</div>
				<div class="control-group" id="controldgroup_onOff_offBorderColor">
				    <label><?php echo $text->getText('formBorderColor'); ?></label>

				    <div class="input-append">
					   <input type="text" value="<?php echo $offlineSettings['onOff_offBorderColor']; ?>"
							id="offBorder" onchange="updateSetting('onOff_offBorderColor',this.value);">
					   <span class="add-on" id="offBorderPicker" style="cursor:pointer;"><img
							 src="img/color_wheel.png"/></span>
				    </div>
				</div>
				<div class="control-group" id="controldgroup_onOff_offTextColor">
				    <label><?php echo $text->getText('formTextColor'); ?></label>

				    <div class="input-append">
					   <input type="text" value="<?php echo $offlineSettings['onOff_offTextColor']; ?>"
							id="offTextColor" onchange="updateSetting('onOff_offTextColor',this.value);">
					   <span class="add-on" id="offTextColorPicker" style="cursor:pointer;"><img
							 src="img/color_wheel.png"/></span>
				    </div>
				</div>
				<div class="control-group" id="controldgroup_onOff_offTextAlign">
				    <label><?php echo $text->getText('formTextAlign'); ?></label>
				    <select id="offTextAlign" onchange="updateSetting('onOff_offTextAlign',this.value);">
					   <option value="center" <?php if ($offlineSettings['onOff_offTextAlign'] == 'center') {
						  echo 'selected="true"';
					   } ?>><?php echo $text->getText('center'); ?></option>
					   <option value="left" <?php if ($offlineSettings['onOff_offTextAlign'] == 'left') {
						  echo 'selected="true"';
					   } ?>><?php echo $text->getText('left'); ?></option>
					   <option value="right" <?php if ($offlineSettings['onOff_offTextAlign'] == 'right') {
						  echo 'selected="true"';
					   } ?>><?php echo $text->getText('right'); ?></option>
				    </select>
				</div>
				<div class="control-group" id="controldgroup_onOff_offPadding">
				    <label><?php echo $text->getText('formPadding'); ?></label>
				    <input type="text" value="<?php echo $offlineSettings['onOff_offPadding']; ?>" id="offPadding"
						 onchange="updateSetting('onOff_offPadding',this.value);">
				</div>
				<div class="control-group" id="controldgroup_offDisplayText">
				    <label><?php echo $text->getText('formDisplayText'); ?></label>
				    <textarea rows="10" id="offDisplayText"
						    onkeyup="updateSetting('onOff_offDisplayText',this.value);"><?php echo $offlineSettings['onOff_offDisplayText']; ?></textarea>
				</div>

			 </div>
		  </form>

	   </div>
	   <div class="wysywigRight">
		  <div id="thisOfflinePreview"></div>
	   </div>
    </div>
</div>
<div class="tab-pane" id="inviteCont">
    <div class="wysywigContainer">
	   <div class="wysywigLeft">
		  <div class="control-group" id="controldgroup_invite_position">
			 <label><?php echo $text->getText('formPosition'); ?></label>
			 <select id="invitePosition" onchange="updateSetting('invite_position',this.value);">
				<option value="topleft" <?php if ($allSettings['invite_position'] == "topleft") {
				    echo "selected";
				} ?>><?php echo $text->getText('topLeft'); ?></option>
				<option value="topright"  <?php if ($allSettings['invite_position'] == "topright") {
				    echo "selected";
				} ?>><?php echo $text->getText('topRight'); ?></option>
				<option value="bottomleft"  <?php if ($allSettings['invite_position'] == "bottomleft") {
				    echo "selected";
				} ?>><?php echo $text->getText('bottomLeft'); ?></option>
				<option value="bottomright"  <?php if ($allSettings['invite_position'] == "bottomright") {
				    echo "selected";
				} ?>><?php echo $text->getText('bottomRight'); ?></option>
			 </select>
		  </div>

		  <div class="control-group" id="controldgroup_invite_height">
			 <label><?php echo $text->getText('formHeight'); ?></label>
			 <input type="text" value="<?php echo $allSettings['invite_height']; ?>" id="inviteHeight"
				   onchange="updateSetting('invite_height',this.value);">
		  </div>

		  <div class="control-group" id="controldgroup_invite_width">
			 <label><?php echo $text->getText('formWidth'); ?></label>
			 <input type="text" value="<?php echo $allSettings['invite_width']; ?>" id="inviteWidth"
				   onchange="updateSetting('invite_width',this.value);">
		  </div>

		  <div class="control-group" id="controldgroup_invite_spacing">
			 <label><?php echo $text->getText('formSpacing'); ?></label>
			 <input type="text" value="<?php echo $allSettings['invite_spacing']; ?>" id="inviteSpacing"
				   onchange="updateSetting('invite_spacing',this.value);">
		  </div>

		  <div class="control-group" id="controldgroup_invite_padding">
			 <label><?php echo $text->getText('formPadding'); ?></label>
			 <input type="text" value="<?php echo $allSettings['invite_padding']; ?>" id="invitePadding"
				   onchange="updateSetting('invite_padding',this.value);">
		  </div>

		  <div class="control-group" id="controldgroup_invite_textcolor">
			 <label><?php echo $text->getText('formTextColor'); ?></label>

			 <div class="input-append">
				<input type="text" value="<?php echo $allSettings['invite_textcolor']; ?>" id="inviteTextcolor"
					  onchange="updateSetting('invite_textcolor',this.value);">
				<span class="add-on" id="inviteTextcolorPicker" style="cursor:pointer;"><img
					   src="img/color_wheel.png"/></span>
			 </div>
		  </div>

		  <div class="control-group" id="controldgroup_invite_border">
			 <label><?php echo $text->getText('formBorderColor'); ?></label>

			 <div class="input-append">
				<input type="text" value="<?php echo $allSettings['invite_border']; ?>" id="inviteBorder"
					  onchange="updateSetting('invite_border',this.value);">
				<span class="add-on" id="inviteBorderPicker" style="cursor:pointer;"><img
					   src="img/color_wheel.png"/></span>
			 </div>
		  </div>

		  <div class="control-group" id="controldgroup_invite_background">
			 <label><?php echo $text->getText('formBackgroundColor'); ?></label>

			 <div class="input-append">
				<input type="text" value="<?php echo $allSettings['invite_background']; ?>" id="inviteBackground"
					  onchange="updateSetting('invite_background',this.value);">
				<span class="add-on" id="inviteBackgroundPicker" style="cursor:pointer;"><img
					   src="img/color_wheel.png"/></span>
			 </div>
		  </div>
	   </div>
	   <div class="wysywigRight">
		  <div id="thisInvitePreview"></div>
	   </div>
    </div>

</div>


</div>


</div>
</div>
</div>


<script type="text/javascript">

$(document).ready(function() {
    makeSliders('<?php echo $allSettings['performanceSetting']; ?>');
    getOnlinePreview();
    getOfflinePreview();
    getInvitePreview();
    setupLanguageDropDown();

    $('#filterLanguageKeySearchField').keyup(function() {
	   filterLanguageTable();
    });

    $("#inviteTextcolorPicker").spectrum({
	   showInput: true,
	   change: function(color) {
		  $('#inviteTextcolor').val(color.toHexString());
		  updateSetting('invite_textcolor', color.toHexString());
	   }
    });

    $("#inviteBorderPicker").spectrum({
	   showInput: true,
	   change: function(color) {
		  $('#inviteBorder').val(color.toHexString());
		  updateSetting('invite_border', color.toHexString());
	   }
    });

    $("#inviteBackgroundPicker").spectrum({
	   showInput: true,
	   change: function(color) {
		  $('#inviteBackground').val(color.toHexString());
		  updateSetting('invite_background', color.toHexString());
	   }
    });

    $("#onTextColorPicker").spectrum({
	   showInput: true,
	   change: function(color) {
		  $('#onTextColor').val(color.toHexString());
		  updateSetting('onOff_onTextColor', color.toHexString());
	   }
    });

    $("#onBorderPicker").spectrum({
	   showInput: true,
	   change: function(color) {
		  $('#onBorder').val(color.toHexString());
		  updateSetting('onOff_onBorderColor', color.toHexString());
	   }
    });
    $("#onBackgroundPicker").spectrum({
	   showInput: true,
	   change: function(color) {
		  $('#onBackground').val(color.toHexString());
		  updateSetting('onOff_onBackground', color.toHexString());
	   }
    });

    $("#offTextColorPicker").spectrum({
	   showInput: true,
	   change: function(color) {
		  $('#offTextColor').val(color.toHexString());
		  updateSetting('onOff_offTextColor', color.toHexString());
	   }
    });

    $("#offBorderPicker").spectrum({
	   showInput: true,
	   change: function(color) {
		  $('#offBorder').val(color.toHexString());
		  updateSetting('onOff_offBorderColor', color.toHexString());
	   }
    });
    $("#offBackgroundPicker").spectrum({
	   showInput: true,
	   change: function(color) {
		  $('#offBackground').val(color.toHexString());
		  updateSetting('onOff_offBackground', color.toHexString());
	   }
    });


    switchIsImageOnOff($('#chooseImageOnline').val());
    switchIsLinkImageOnOff($('#chooseLinkImageOnline').val());
    switchIsImageOnOffOff($('#chooseImageOffline').val());
    switchIsLinkImageOnOffOff($('#chooseLinkImageOffline').val());

});

function makeSliders(presetName) {
    $('#perfStatCont li').each(function() {
	   var thisId = $(this).find('.statSlider').attr('id');
	   var thisSpan = $(this).find('.sliderStatLabel');
	   var min = $(this).find('.statMin').val();
	   var max = $(this).find('.statMax').val();
	   var thisValue;
	   if (presetName != undefined) {
		  if (presetName == "performance") {
			 thisValue = $(this).find('.statValPerf').val();
		  }
		  if (presetName == "response") {
			 thisValue = $(this).find('.statValResp').val();
		  }
		  if (presetName == "custom") {

			 thisValue = $(this).find('.statVal').val();
		  }
	   }
	   else {
		  thisValue = $(this).find('.statVal').val();
	   }
	   $("#" + thisId).slider(
	   {
		  animate : true,
		  range : 'min',
		  value : thisValue,
		  min: parseInt(min),
		  max: parseInt(max),
		  step: 1000,
		  stop: function(event, ui) {
			 updateSetting(thisId, $("#" + thisId).slider("value"));
			 setBars();
		  }
	   });
    });
    setBars(presetName);
}


function setBars(tSetting) {
    $('#perfStatCont li').each(function() {

	   var thisId = $(this).find('.statSlider').attr('id');
	   var thisSpan = $(this).find('.sliderStatLabel');
	   var thisValue;
	   $(this).find('.statVal').val($("#" + thisId).slider("value"));
	   if (tSetting != undefined) {
		  if (tSetting == "performance") {
			 thisValue = $(this).find('.statValPerf').val();
		  }
		  if (tSetting == "response") {
			 thisValue = $(this).find('.statValResp').val();
		  }
		  if (tSetting == "custom") {
			 thisValue = $(this).find('.statVal').val();
		  }
	   }
	   else {
		  thisValue = $(this).find('.statVal').val();
	   }
	   $("#" + thisId).slider('value', thisValue);
	   $(thisSpan).text(($("#" + thisId).slider("value") / 1000) + " <?php echo $text->getText('seconds'); ?>");
	   var thisTValue = $("#" + thisId).slider("value");

	   updateSetting(thisId, thisTValue);
    });


    $('#perfControls .btn').each(function() {
	   $(this).removeClass('btn-primary');
    });

    if (tSetting == "performance") {
	   $('#statPerfSwitch').addClass('btn-primary');
	   updateSetting('performanceSetting', 'performance');
    }
    else if (tSetting == "response") {
	   $('#statRespSwitch').addClass('btn-primary');
	   updateSetting('performanceSetting', 'response');
    }
    else {
	   $('#statCustSwitch').addClass('btn-primary');
	   updateSetting('performanceSetting', 'custom');
    }
}


function filterLanguageTable() {
    var searchKey = $('#filterLanguageKeySearchField').val().toLowerCase();

    if (searchKey.length >= 3) {
	   $('#languageKeyTable tr').each(function() {
		  var thisTarget = $(this).find('.langKeyTarget').attr('id');
		  var searchVal = $('#' + thisTarget).val().toLowerCase();
		  var has_string = strpos(searchVal, searchKey, 0);
		  if (!has_string) {
			 $(this).hide();
		  }
		  else {
			 $(this).show();
		  }
	   });
    }

    if (searchKey.length == 0) {
	   $('#languageKeyTable tr').each(function() {
		  $(this).show();
	   });
    }

}

function strpos(haystack, needle, offset) {
    var i = (haystack + '').indexOf(needle, (offset || 0));
    return i === -1 ? false : true;
}

function updateControlKey(cKey) {
    var lang = getSelectedLanguage('updateTrue');
    var newVal = $('#langKey_' + cKey).val();
    var time = (new Date).getTime();
    var url = '';
    url += 'action=setLanguageKeys&key=' + cKey + '&newText=' + newVal + '&language=' + lang + '&noCahe=' + time;

    $.ajax({
	   url: 'includes/handler.php?' + url,
	   success: function(data) {
		  if (data == "1") {
			 $('#span_' + cKey).show();

		  }
	   }
    });
}


function addLanguage() {
    var time = (new Date).getTime();
    var url = '';
    url += 'action=createLanguage&languageName=' + $('#newLanguageName').val() + '&noCahe=' + time;
    $.ajax({
	   url: 'includes/handler.php?' + url,
	   success: function(data) {
		  updateLanguageByNice($('#newLanguageName').val());
		  setupLanguageDropDown();
	   }
    });
}

function setupLanguageDropDown() {
    var time = (new Date).getTime();
    var url = '';
    url += 'action=getLanguagesForDropDown&noCahe=' + time;
    $.ajax({
	   url: 'includes/handler.php?' + url,
	   success: function(data) {
		  var responseObj = JSON.parse(data);
		  $("#langKeysList").empty();
		  $.each(responseObj, function(i, item) {
			 var liStruct = ' <li><a href="#" onclick="updateLanguageByNice(\'' + item.languageNice + '\');">' + item.languageNice + '</a></li>';
			 $("#langKeysList").append(liStruct);
		  });
	   }
    });
}

function getLanguagekeys(keySetting) {
    var lang = getSelectedLanguage(keySetting);
    var time = (new Date).getTime();
    var url = '';
    url += 'action=getLanguageKeys&language=' + lang + '&noCahe=' + time;
    $.ajax({
	   url: 'includes/handler.php?' + url,
	   success: function(data) {
		  $('#langQueryResult').empty().append(data);
	   }
    });

}

function updateLanguageByNice(niceName) {
    $('#currentLanguageName').text(niceName);
    getLanguagekeys(niceName);
}

function getSelectedLanguage(getUpdatedKey) {
    if (getUpdatedKey != undefined) {
	   return $('#currentLanguageName').text();
    }
    else {
	   return '<?php echo $allSettings['language']; ?>';
    }
}


function updateSetting(thisSetting, thisValue, cKey) {
    var time = (new Date).getTime();
    var url = '';
    url += 'action=updateSetting&setting=' + encodeURIComponent(thisSetting) + '&value=' + encodeURIComponent(thisValue) + '&noCahe=' + time;
    if (cKey == undefined) {
	   cKey = thisSetting;
    }
    $.ajax({
	   url: 'includes/handler.php?' + url,
	   success: function(data) {
		  if (data == "1") {
			 $('#controldgroup_' + cKey).addClass('success');
			 getOnlinePreview();
			 getOfflinePreview();
			 getInvitePreview();
		  }
	   }
    });
}

function switchIsImageOnOff(thisSwitch) {
    if (thisSwitch == "1") {
	   $('#onImageSpecific').show();
	   $('#onTextSpecific').hide();
    }
    else {
	   $('#onImageSpecific').hide();
	   $('#onTextSpecific').show();
    }

}

function switchIsLinkImageOnOff(thisSwitch) {
    if (thisSwitch == "1") {
	   $('#onLinkImageSpecific').show();
	   $('#onLinkTextSpecific').hide();
    }
    else {
	   $('#onLinkImageSpecific').hide();
	   $('#onLinkTextSpecific').show();
    }

}

function switchIsImageOnOffOff(thisSwitch) {
    if (thisSwitch == "1") {
	   $('#offImageSpecific').show();
	   $('#offTextSpecific').hide();
    }
    else {
	   $('#offImageSpecific').hide();
	   $('#offTextSpecific').show();
    }
}

function switchIsLinkImageOnOffOff(thisSwitch) {
    if (thisSwitch == "1") {
	   $('#offLinkImageSpecific').show();
	   $('#offLinkTextSpecific').hide();
    }
    else {
	   $('#offLinkImageSpecific').hide();
	   $('#offLinkTextSpecific').show();
    }
}


function getOnlinePreview() {
    $('#thisOnlinePreview').empty().load('includes/templates/example.displayOnOff.php?online=1');
}

function getOfflinePreview() {
    $('#thisOfflinePreview').empty().load('includes/templates/example.displayOnOff.php?online=0');
}

function getInvitePreview() {
    $('#thisInvitePreview').empty().load('includes/templates/example.displayOnOff.php?invite=1');
}


</script>