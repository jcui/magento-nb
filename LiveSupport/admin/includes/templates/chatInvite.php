<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 18/01/2013
 * Time: 15:28
 */

session_start();
require '../class/database.php';
require '../class/admin.php';
require '../class/displayText.php';
$admin = new admin();
$text = new displayText();

?>

<div id="MTstandard">
    <p><?php echo $text->getText('chatinviteMessage'); ?></p>
    <textarea id="inviteUserMessage" style="width:90%;"></textarea>
</div>
<div id="MTError" style="display:none;">
    <div class="alert alert-error">
	   <?php echo $text->getText('chatInviteError'); ?>
    </div>
</div>