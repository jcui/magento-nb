<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 24/02/2013
 * Time: 18:12
 */
?>
<div id="lsChatInitContainer">
    <?php if ($displayConfig['onOff_onIsImage'] == "1") { ?>
    <a href="" id="liveSupportChatHyperLink" onclick="launchLiveSupport();">
	   <img src="<?php echo $displayConfig['onOff_onImage']; ?>"/></a>
    <?php } else { ?>
    <div id="lsChatInitContainerInner"
	    style="overflow:hidden; text-align:<?php echo $displayConfig['onOff_onTextAlign']; ?>; padding:<?php echo $displayConfig['onOff_onPadding']; ?>px; width:<?php echo $displayConfig['onOff_OnWidth']; ?>px; height:<?php echo $displayConfig['onOff_onHeight']; ?>px; background:<?php echo $displayConfig['onOff_onBackground']; ?>; color:<?php echo $displayConfig['onOff_onTextColor']; ?>; border:1px solid <?php echo $displayConfig['onOff_onBorderColor']; ?>;">
	   <p><?php echo $displayConfig['onOff_onDisplayText']; ?></p>
	   <?php if ($displayConfig['onOff_onIsLinkImage'] == "1") { ?>
	   <p><a href="" id="liveSupportChatHyperLink" style="text-decoration:none; color:#000;">
		  <img src="<?php echo $displayConfig['onOff_onDisplayLinkImage']; ?>"/>
	   </a></p>
	   <?php } else { ?>
	   <p><a href="" id="liveSupportChatHyperLink"
		    style="text-decoration:<?php echo $displayConfig['onOff_onLinkUnderline']; ?>; color:#000;"><?php echo $displayConfig['onOff_onDisplayLinkText']; ?></a>
	   </p>
	   <?php } ?>
    </div>
    <?php } ?>
</div>


