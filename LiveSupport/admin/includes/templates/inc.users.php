<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 01/01/2013
 * Time: 18:14
 */
session_start();
$currentUsers = $admin->getOperatorUsers();
$departments = $admin->getDepartments();
$permissionsLocal = $admin->getPermissionGroups();

?>

<legend class="lead"><?php echo $text->getText('users'); ?>
    <button type="button" class="close" onclick="hideUtilityPage();"
		  aria-hidden="true">&times; <?php echo $text->getText('closeTab'); ?></button>
</legend>

<div class="tabbable"> <!-- Only required for left/right tabs -->
<ul class="nav nav-tabs">
    <li class="active"><a href="#users_CurrentUsers" data-toggle="tab"><?php echo $text->getText('currentUsers'); ?></a>
    </li>
    <?php if ($admin->hasWritePermission('users', $_SESSION['permissions'])) { ?>
    <li><a href="#users_AddUser" data-toggle="tab"><?php echo $text->getText('addUser'); ?></a></li>
    <li><a href="#users_Departments" data-toggle="tab"><?php echo $text->getText('manageDepartments'); ?></a></li>
    <li><a href="#users_Permissions" data-toggle="tab"><?php echo $text->getText('managePermissionGroups'); ?></a></li>
    <?php } ?>

</ul>
<div class="tab-content">
<div class="tab-pane active" id="users_CurrentUsers">

    <div class="alert alert-error" id="users_delete_user_error" style="display:none">
	   <strong><?php echo $text->getText('error'); ?></strong>

	   <p><?php echo $text->getText('errorDeletingUser'); ?></p>
    </div>

    <div class="alert alert-success" id="users_delete_user_ok" style="display:none">
	   <strong><?php echo $text->getText('allDone'); ?></strong>

	   <p><?php echo $text->getText('userDeleted'); ?></p>
    </div>

    <div class="alert alert-error" id="users_edit_user_error" style="display:none">
	   <strong><?php echo $text->getText('error'); ?></strong>

	   <p><?php echo $text->getText('errorEditingUser'); ?></p>
    </div>

    <div class="alert alert-success" id="users_edit_user_ok" style="display:none">
	   <strong><?php echo $text->getText('allDone'); ?></strong>

	   <p><?php echo $text->getText('userEdited'); ?></p>
    </div>

    <div id="ajaxUserListHolder"></div>
</div>
<div class="tab-pane" id="users_AddUser">


    <div class="alert alert-success" id="users_add_user_ok" style="display:none">
	   <strong><?php echo $text->getText('allDone'); ?></strong>

	   <p><?php echo $text->getText('userAdded'); ?></p>
    </div>

    <div class="alert alert-error" id="users_add_user_error" style="display:none">
	   <strong><?php echo $text->getText('error'); ?></strong>

	   <p><?php echo $text->getText('addUserError'); ?></p>
    </div>


    <form class="form-horizontal" id="add_user_form">
	   <div class="control-group" id="user_email">
		  <label class="control-label" for="inputEmail"><?php echo $text->getText('formEmail'); ?></label>

		  <div class="controls">
			 <input class="span3" type="text" id="inputEmail"
				   placeholder="<?php echo $text->getText('formEmail'); ?>">
			 <span class="help-inline" style="display:none;"
				  id="user_email_error"><?php echo $text->getText('validationErrorEmail'); ?></span>
		  </div>
	   </div>
	   <div class="control-group" id="user_display_name">
		  <label class="control-label" for="inputDisplayName"><?php echo $text->getText('formDisplayName'); ?></label>

		  <div class="controls">
			 <input class="span3" type="text" id="inputDisplayName"
				   placeholder="<?php echo $text->getText('formDisplayName'); ?>">
			 <span class="help-inline" id="user_display_name_error"
				  style="display:none;"><?php echo $text->getText('validationDisplayNameError'); ?></span>
		  </div>
	   </div>
	   <div class="control-group" id="user_password1">
		  <label class="control-label" for="inputPassword"><?php echo $text->getText('formPassword'); ?></label>

		  <div class="controls">
			 <input class="span3" type="password" id="inputPassword"
				   placeholder="<?php echo $text->getText('formPassword'); ?>">
		  </div>
	   </div>
	   <div class="control-group" id="user_password2">
		  <label class="control-label"
			    for="inputPasswordRepeat"><?php echo $text->getText('formRepeatPassword'); ?></label>

		  <div class="controls">
			 <input class="span3" type="password" id="inputPasswordRepeat"
				   placeholder="<?php echo $text->getText('formRepeatPassword'); ?>">
			 <span class="help-inline" id="user_password1_error_noMatch"
				  style="display:none;"><?php echo $text->getText('passwordsDontMatch'); ?></span>
			 <span class="help-inline" id="user_password1_error_inValid"
				  style="display:none;"><?php echo $text->getText('validationPasswordType'); ?></span>
		  </div>
	   </div>

	   <div id="deptControlGroup" class="control-group">
		  <label class="control-label" for="selectDepartment"><?php echo $text->getText('department'); ?></label>

		  <div class="controls">
			 <div class="input-prepend">
				<div class="btn-group">
				    <button class="btn dropdown-toggle" data-toggle="dropdown">
					   <i class="icon-home"></i>
					   <span class="caret"></span>
				    </button>
				    <ul class="dropdown-menu" id="selectDepartment">
					   <?php for ($i = 0; $i < count($departments); $i++) { ?>
					   <li><a href="#"
							onclick="$('#deptSelect').val('<?php echo $departments[$i]['departmentName']; ?>'); $('#departmentSelect').val('<?php echo $departments[$i]['departmentId']; ?>');"><?php echo $departments[$i]['departmentName']; ?></a>
					   </li>
					   <?php } ?>
				    </ul>
				    <input type="hidden" id="departmentSelect" value="">
				</div>
				<input class="span2 inputWideAppend" id="deptSelect" type="text" disabled>
			 </div>
			 <span id="departmentErrorText" class="help-inline"
				  style="display:none;"><?php echo $text->getText('pleasePickDepartment'); ?></span>
		  </div>
	   </div>


	   <div id="permControlGroup" class="control-group">
		  <label class="control-label"
			    for="selectPermissions"><?php echo $text->getText('permissionLevel'); ?></label>

		  <div class="controls">
			 <div class="input-prepend">
				<div class="btn-group">
				    <button class="btn dropdown-toggle" data-toggle="dropdown">
					   <i class="icon-user"></i>
					   <span class="caret"></span>
				    </button>
				    <ul class="dropdown-menu" id="selectPermissions">
					   <?php for ($i = 0; $i < count($permissionsLocal); $i++) { ?>
					   <li><a href="#"
							onclick="$('#permissionSelect').val('<?php echo $permissionsLocal[$i]['permissionTitle']; ?>'); $('#permissionSelected').val('<?php echo $permissionsLocal[$i]['id']; ?>');"><?php echo $permissionsLocal[$i]['permissionTitle']; ?></a>
					   </li>
					   <?php } ?>
				    </ul>
				    <input type="hidden" id="permissionSelected" value="">
				</div>
				<input class="span2 inputWideAppend" id="permissionSelect" type="text" disabled>
			 </div>
			 <span id="permissionErrorText" class="help-inline"
				  style="display:none;"><?php echo $text->getText('pleasePickPermission'); ?></span>
		  </div>
	   </div>


	   <div class="control-group">
		  <div class="controls">
			 <button class="btn btn-primary" id="createUserSubmit"
				    type="button"><?php echo $text->getText('createUserSubmit'); ?></button>
		  </div>
	   </div>
    </form>


</div>
<div class="tab-pane" id="users_Departments">

    <div class="alert alert-error" id="users_edit_department_error" style="display:none">
	   <strong><?php echo $text->getText('error'); ?></strong>

	   <p><?php echo $text->getText('errorEditingDepartment'); ?></p>
    </div>

    <div class="alert alert-success" id="users_edit_department_ok" style="display:none">
	   <strong><?php echo $text->getText('allDone'); ?></strong>

	   <p><?php echo $text->getText('departmentChangesSaved'); ?></p>
    </div>

    <div class="alert alert-error" id="users_delete_department_error" style="display:none">
	   <strong><?php echo $text->getText('error'); ?></strong>

	   <p><?php echo $text->getText('errorDeletingDepartment'); ?></p>
    </div>

    <div class="alert alert-success" id="users_delete_department_ok" style="display:none">
	   <strong><?php echo $text->getText('allDone'); ?></strong>

	   <p><?php echo $text->getText('departmentDeleted'); ?></p>
    </div>


    <div class="alert alert-error" id="users_add_department_error" style="display:none">
	   <strong><?php echo $text->getText('error'); ?></strong>

	   <p><?php echo $text->getText('errorAddingDepartment'); ?></p>
    </div>

    <div class="alert alert-success" id="users_add_department_ok" style="display:none">
	   <strong><?php echo $text->getText('allDone'); ?></strong>

	   <p><?php echo $text->getText('departmentAdded'); ?></p>
    </div>


    <div id="ajaxDeptListHolder"></div>
</div>
<div class="tab-pane" id="users_Permissions">

    <div class="alert alert-error" id="users_edit_permissions_error" style="display:none">
	   <strong><?php echo $text->getText('error'); ?></strong>

	   <p><?php echo $text->getText('errorEditingPermissionLevel'); ?></p>
    </div>

    <div class="alert alert-success" id="users_edit_permissions_ok" style="display:none">
	   <strong><?php echo $text->getText('allDone'); ?></strong>

	   <p><?php echo $text->getText('permissionChangesSaved'); ?></p>
    </div>

    <div class="alert alert-error" id="users_delete_permissions_error" style="display:none">
	   <strong><?php echo $text->getText('error'); ?></strong>

	   <p><?php echo $text->getText('errorDeletingPermissionGroup'); ?></p>
    </div>

    <div class="alert alert-success" id="users_delete_permissions_ok" style="display:none">
	   <strong><?php echo $text->getText('allDone'); ?></strong>

	   <p><?php echo $text->getText('permissionGroupDeleted'); ?></p>
    </div>


    <div class="alert alert-error" id="users_add_permissions_error" style="display:none">
	   <strong><?php echo $text->getText('error'); ?></strong>

	   <p><?php echo $text->getText('errorAddingPermissionGroup'); ?></p>
    </div>

    <div class="alert alert-success" id="users_add_permissions_ok" style="display:none">
	   <strong><?php echo $text->getText('allDone'); ?></strong>

	   <p><?php echo $text->getText('permissionGroupAdded'); ?></p>
    </div>


    <div id="ajaxPermListHolder"></div>


</div>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

	   $('#ajaxUserListHolder').empty().load('includes/templates/inc.userlist.php');
	   $('#ajaxDeptListHolder').empty().load('includes/templates/inc.departmentlist.php');
	   $('#ajaxPermListHolder').empty().load('includes/templates/inc.permissions.php');

	   $('#createUserSubmit').click(function() {

		  $('#users_add_user_error').hide();
		  $('#users_add_user_ok').hide();

		  var time = (new Date).getTime();
		  var errors = false;
		  var emailAddress = $('#inputEmail').val();
		  var displayName = $('#inputDisplayName').val();
		  var password1 = $('#inputPassword').val();
		  var password2 = $('#inputPasswordRepeat').val();
		  var department = $('#departmentSelect').val();
		  var permissions = $('#permissionSelected').val();

		  // validate display name
		  if (displayName.length >= 3) {
			 $('#user_display_name').addClass('success');
			 $('#user_display_name_error').hide();
		  }
		  else {
			 $('#user_display_name').removeClass('success');
			 $('#user_display_name').addClass('error');
			 $('#user_display_name_error').show();
			 errors = true;

		  }


		  // validate email address
		  $.ajax({
			 url: 'includes/handler.php?action=validation&validationType=username&username=' + emailAddress + '&nocache' + time,
			 success: function(data) {
				if (data == "1") {
				    $('#user_email').addClass('success');
				    $('#user_email_error').hide();
				}
				else {
				    $('#user_email').removeClass('success');
				    $('#user_email').addClass('error');
				    $('#user_email_error').show();
				    errors = true;
				}
			 }
		  });

		  // validate password
		  $('#user_password1_error_noMatch').hide();
		  $('#user_password1_error_inValid').hide();
		  if (password1 != password2) {
			 $('#user_password1').removeClass('success');
			 $('#user_password2').removeClass('success');
			 $('#user_password1').addClass('error');
			 $('#user_password2').addClass('error');
			 $('#user_password1_error_noMatch').show();
			 errors = true;
		  }
		  else {
			 $.ajax({
				url: 'includes/handler.php?action=validation&validationType=password&password=' + password1 + '&nocache' + time,
				success: function(data) {
				    if (data == "1") {
					   $('#user_password1').addClass('success');
					   $('#user_password2').addClass('success');
					   $('#user_password1_error_noMacth').hide();
					   $('#user_password1_error_inValid').hide();
				    }
				    else {
					   $('#user_password1').removeClass('success');
					   $('#user_password2').removeClass('success');
					   $('#user_password1').addClass('error');
					   $('#user_password2').addClass('error');
					   $('#user_password1_error_inValid').show();
					   errors = true;
				    }
				}
			 });
		  }

		  // validate department
		  if (department.length != 0) {
			 $('#deptControlGroup').addClass('success');
			 $('#departmentErrorText').hide();
		  }
		  else {
			 $('#deptControlGroup').removeClass('success');
			 $('#deptControlGroup').addClass('error');
			 $('#departmentErrorText').show();
			 errors = true;
		  }

		  // validate permissions
		  if (permissions.length != 0) {
			 $('#permControlGroup').addClass('success');
			 $('#permissionErrorText').hide();
		  }
		  else {
			 $('#permControlGroup').removeClass('success');
			 $('#permControlGroup').addClass('error');
			 $('#permissionErrorText').show();
			 errors = true;
		  }

		  if (!errors) {
			 $.ajax({
				url: 'includes/handler.php?action=createUser&username=' + emailAddress + '&displayName=' + displayName + '&password=' + password1 + '&department=' + department + '&permissions=' + permissions + '&nocache' + time,
				success: function(data) {
				    if (data == "1") {
					   $('#users_add_user_error').hide();
					   $('#users_add_user_ok').show();
					   $('#add_user_form :input').each(function() {
						  $(this).val('');
					   });
					   $('#add_user_form').each(function() {
						  $(this).removeClass('success');
						  $(this).removeClass('error');
					   });
					   $('#ajaxUserListHolder').empty().load('includes/templates/inc.userlist.php');
				    }
				    else {
					   $('#users_add_user_error').show();
					   $('#users_add_user_ok').hide();
				    }
				}
			 });
		  }

	   });

    });
</script>