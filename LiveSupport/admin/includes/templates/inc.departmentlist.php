<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 06/01/2013
 * Time: 15:33
 */
session_start();
require '../class/database.php';
require '../class/displayText.php';
require '../class/admin.php';


$admin = new admin();
$text = new displayText();

$departments = $admin->getDepartments();
?>

<?php if (count($departments) > 0) { ?>
<table class="table table-striped">
    <thead>
    <tr>
	   <th><?php echo $text->getText('departmentName'); ?></th>
	   <th>&nbsp;</th>

    </tr>
    </thead>
    <tbody>
	   <?php foreach ($departments as $d) { ?>
    <tr id="row_<?php echo $d['departmentId']; ?>">
	   <td style="width:95%"><?php echo $d['departmentName']; ?></td>
	   <td class="users_delete_user">
		  <?php if (count($departments) > 1) { ?>
		  <a href="#"
			onclick="deleteDepartment(<?php echo $d['departmentId']; ?>,'1','<?php echo $text->getText('areYouSure'); ?>','<?php echo $text->getText('deleteDepartment'); ?>');"><img
				src="img/delete.png" width="25"></a>
		  <?php } ?>
	   </td>
    </tr>
	   <?php } ?>
    </tbody>
</table>
<?php } else { ?>
<p><?php echo $text->getText('noDepartments'); ?></p>
<?php } ?>



<div class="input-append">
    <input class="span4" id="departmentAddInput" type="text"
		 placeholder="<?php echo $text->getText('departmentName'); ?>">
    <button class="btn btn-primary" type="button"
		  onclick="addDepartment();"><?php echo $text->getText('addDepartment'); ?></button>
</div>