<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 01/01/2013
 * Time: 18:41
 */
session_start();
$admin = new admin();
$feedbackSummary = $admin->getFeedbackSummary();
?>

<legend class="lead"><?php echo $text->getText('feedback'); ?>
    <button type="button" class="close" onclick="hideUtilityPage();"
		  aria-hidden="true">&times; <?php echo $text->getText('closeTab'); ?></button>
</legend>


<div class="tabbable"> <!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs">
	   <li class="active"><a href="#feedbackoverview"
						data-toggle="tab"><?php echo $text->getText('feedbackOverview'); ?></a></li>
	   <li><a href="#feedbackquestions" data-toggle="tab"><?php echo $text->getText('questions'); ?></a></li>
    </ul>
    <div class="tab-content">
	   <div class="tab-pane active" id="feedbackoverview">

		  <table class="table table-striped" id="archivedConvoTable">
			 <thead>
			 <th width="60%"><?php echo $text->getText('question'); ?></th>
			 <th width="20%"><?php echo $text->getText('responses'); ?></th>
			 <th width="20%"><?php echo $text->getText('averageScore'); ?></th>
			 </thead>
			 <?php if (count($feedbackSummary) != 0) {
			 foreach ($feedbackSummary as $f) { ?>
				<tr>
				    <td><?php echo $f['question']; ?></td>
				    <td><?php echo $f['total']; ?></td>
				    <td><?php echo $f['rating']; ?></td>
				</tr>
				<?php }
		  } else { ?>
			 <div class="alert alert-info">
				<p><?php echo $text->getText('noFeedbackRecieved'); ?></p>
			 </div>
			 <?php } ?>
		  </table>


	   </div>
	   <div class="tab-pane" id="feedbackquestions">


		  <div class="alert alert-error" id="question_delete_response_error" style="display:none">
			 <strong><?php echo $text->getText('error'); ?></strong>

			 <p><?php echo $text->getText('errorDeletingQuestion'); ?></p>
		  </div>

		  <div class="alert alert-success" id="question_delete_response_ok" style="display:none">
			 <strong><?php echo $text->getText('allDone'); ?></strong>

			 <p><?php echo $text->getText('questionDeleted'); ?></p>
		  </div>

		  <div class="alert alert-error" id="question_edit_response_error" style="display:none">
			 <strong><?php echo $text->getText('error'); ?></strong>

			 <p><?php echo $text->getText('errorEditingQuestion'); ?></p>
		  </div>

		  <div class="alert alert-success" id="question_edit_response_ok" style="display:none">
			 <strong><?php echo $text->getText('allDone'); ?></strong>

			 <p><?php echo $text->getText('questionSaved'); ?></p>
		  </div>


		  <div id="ajaxResponsesContainer"></div>


		  <br/>
		  <?php if ($admin->hasWritePermission('feedback', $_SESSION['permissions'])) { ?>
		  <div class="input-append">
			 <input class="span6" id="questionAddInput" type="text"
				   placeholder="<?php echo $text->getText('typeQuestion'); ?>">
			 <button class="btn btn-primary" type="button"
				    onclick="addFbQuestion();"><?php echo $text->getText('addFeedBackQuestion'); ?></button>
		  </div>
		  <?php } ?>


		  <script type="text/javascript">
			 $(document).ready(function() {
				$('#ajaxResponsesContainer').load('includes/templates/inc.feedbacklist.php');
			 });
		  </script>

	   </div>
    </div>
</div>
