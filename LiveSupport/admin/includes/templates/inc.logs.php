<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 01/01/2013
 * Time: 18:41
 */

session_start();
$admin = new admin();
$clients = $admin->getClientList();
$convos = $admin->getConvoArchive();

?>

<legend class="lead"><?php echo $text->getText('logs'); ?>
    <button type="button" class="close" onclick="hideUtilityPage();"
		  aria-hidden="true">&times; <?php echo $text->getText('closeTab'); ?></button>
</legend>


<div class="tabbable"> <!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs">
	   <li class="active"><a href="#logsConvoList" data-toggle="tab"><?php echo $text->getText('convoLogs'); ?></a>
	   </li>
	   <li><a href="#logsClientList" data-toggle="tab"><?php echo $text->getText('clients'); ?></a></li>
    </ul>
    <div class="tab-content">
	   <div class="tab-pane active" id="logsConvoList">

		  <table class="table table-striped" id="archivedConvoTable">
			 <thead>
			 <th width="25%"><?php echo $text->getText('date'); ?></th>
			 <th width="25%"><?php echo $text->getText('client'); ?></th>
			 <th width="25%"><?php echo $text->getText('operator'); ?></th>
			 <th>&nbsp</th>
			 </thead>
			 <?php if (count($convos) != 0) {
			 foreach ($convos as $c) {
				?>
				<tr>
				    <td><?php echo date("F j, Y", $c['started']); ?></td>
				    <td><?php echo $c['name']; ?></td>
				    <td><?php echo $c['username']; ?></td>
				    <td style="text-align:right;">
					   <button class="btn"
							 onclick="viewArchivedConvo(<?php echo $c['conversationId']; ?>)"><?php echo $text->getText('viewConvo'); ?></button>
				    </td>
				</tr>
				<?php }
		  } ?>
		  </table>


	   </div>
	   <div class="tab-pane" id="logsClientList">

		  <?php if ($admin->hasWritePermission('logs', $_SESSION['permissions'])) { ?>
		  <p><a class="btn btn-success"
			   href="./assets/clientsCsv.php"><?php echo $text->getText('downloadCSV'); ?></a></p>
		  <?php } ?>

		  <table class="table table-striped">
			 <thead>
			 <th><?php echo $text->getText('clientName'); ?></th>
			 <th><?php echo $text->getText('emailAddress'); ?></th>
			 <th><?php echo $text->getText('date'); ?></th>
			 </thead>
			 <?php if(count($clients) != 0) { foreach ($clients as $c) { ?>
			 <tr>
			<td><?php echo $c['name']; ?></td>
			 <td><?php echo $c['email']; ?></td>
			 <td><?php echo date("F j, Y", $c['date']); ?></td>
			 <?php }} ?>
		  </tr>
		  </table>


	   </div>
    </div>
</div>
