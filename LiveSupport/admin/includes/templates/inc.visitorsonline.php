<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 01/01/2013
 * Time: 18:34
 */
session_start();
$client = new clientTracker();
$tSessions = $client->getActiveTrackingSessions();
$text = new displayText();
?>

<legend class="lead"><?php echo $text->getText('visitorsOnline'); ?>
    <button type="button" class="close" onclick="hideUtilityPage();"
		  aria-hidden="true">&times; <?php echo $text->getText('closeTab'); ?></button>
</legend>


<?php if (count($tSessions) != 0) { ?>
<table class="table table-striped">
    <thead>
    <tr>
	   <th><?php echo $text->getText('firstSeen'); ?></th>
	   <th><?php echo $text->getText('lastSeen'); ?></th>
	   <th><?php echo $text->getText('currentPage'); ?></th>
	   <th>&nbsp;</th>
    </tr>
    </thead>
    <tbody>
	   <?php foreach ($tSessions as $s) { ?>
    <tr>
	   <td><?php echo date("g:i a", $s['registered']); ?></td>
	   <td><?php echo date("g:i a", $s['lastSeen']); ?></td>
	   <td><?php echo $s['page']; ?></td>
	   <td>
		  <?php if ($admin->hasWritePermission('visitorsonline', $_SESSION['permissions'])) { ?>
		  <?php if ($s['userInformed'] == "0") { ?>
			 <button class="btn btn-success span1"
				    onclick="inviteUserToChat(<?php echo $s['id']; ?>);"><?php echo $text->getText('invite'); ?></button>
			 <?php } else { ?>
			 <p><?php echo $text->getText('alreadyApproached'); ?></p>
			 <?php } ?>
		  <?php } ?>
	   </td>
    </tr>
	   <?php } ?>
    </tbody>
</table>
<?php } else { ?>

<p><?php echo $text->getText('noUsersOnline'); ?></p>

<?php } ?>