<?php
session_start();
require '../class/database.php';
require '../class/admin.php';
require '../class/displayText.php';
$admin = new admin();
$text = new displayText();
?>
<div id="MTstandard">

    <p><?php echo $text->getText('pickUpConvoMain'); ?></p>
    <ul>
	   <li><?php echo $text->getText('userLeavesChat'); ?></li>
	   <li><?php echo $text->getText('pickUpYouTrans'); ?></li>
    </ul>
</div>
<div id="MTError" style="display:none;">
    <div class="alert alert-error">
	   <?php echo $text->getText('pickUpError'); ?>
    </div>
</div>