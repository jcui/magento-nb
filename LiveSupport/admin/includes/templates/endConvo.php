<?php

session_start();
require '../class/database.php';
require '../class/admin.php';
require '../class/displayText.php';
$admin = new admin();
$text = new displayText();

?>
<div id="MTstandard">
    <p><?php echo $text->getText('closeConvoConfirm'); ?></p>
</div>
<div id="MTError" style="display:none;">
    <div class="alert alert-error">
	   <?php echo $text->getText('closeConvoError'); ?>
    </div>
</div>