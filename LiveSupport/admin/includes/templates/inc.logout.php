<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 01/01/2013
 * Time: 18:42
 */

?>

<legend class="lead"><?php echo $text->getText('confirmLogOut'); ?>
    <button type="button" class="close" onclick="hideUtilityPage();"
		  aria-hidden="true">&times; <?php echo $text->getText('closeTab'); ?></button>
</legend>
<form id="logOutForm" action="logout.php">
    <p>
	   <button class="btn btn-danger" type="button"
			 onclick="switchActiveClass('home'); $('#mainChatLink').tab('show');"><?php echo $text->getText('cancel'); ?></button>
	   <button class="btn btn-primary" type="button"
			 onclick="$('#logOutForm').submit();"><?php echo $text->getText('yes'); ?></button>
    </p>
</form>