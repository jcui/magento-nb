<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 07/01/2013
 * Time: 20:45
 */
session_start();
require '../class/database.php';
require '../class/admin.php';
require '../class/displayText.php';
$admin = new admin();
$files = $admin->getStoredFiles();
$text = new displayText();
?>

<table class="table table-striped">
    <thead>
    <tr>
	   <th><?php echo $text->getText('fileName'); ?></th>
	   <th><?php echo $text->getText('added'); ?></th>
	   <th><?php echo $text->getText('active'); ?></th>
	   <td>&nbsp;</td>
    </tr>
    </thead>
    <tbody>
<?php
if (count($files) != 0) {
    foreach ($files as $f) {
	   ?>
    <tr id="row_<?php echo $f['id']; ?>">
	   <td><?php echo $f['description']; ?></td>
	   <td><?php echo date("Y-m-d : g:i", $f['added']); ?></td>
	   <td>
		  <?php if ($admin->hasWritePermission('files', $_SESSION['permissions'])) { ?>
		  <input id="fileVisBox_<?php echo $f['id']; ?>" type="checkbox" <?php if ($f['active']) {
			 echo "checked";
		  } ?>  onchange="updateFileVisibility(<?php echo $f['id']; ?>);">
		  <?php } ?>
	   </td>
	   <td class="users_delete_user">
		  <?php if ($admin->hasWritePermission('files', $_SESSION['permissions'])) { ?>
		  <a href="#" onclick="deleteFile(<?php echo $f['id']; ?>,'1','Are you sure?','Delete File');"><img
				src="img/delete.png" width="25"></a>
		  <?php } ?>
	   </td>
    </tr>
	   <?php }
} ?>
    </tbody>
</table>
