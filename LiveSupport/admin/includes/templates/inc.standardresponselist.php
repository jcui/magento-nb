<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 07/01/2013
 * Time: 23:33
 */
session_start();
require '../class/database.php';
require '../class/displayText.php';
require '../class/admin.php';

$admin = new admin();
$responses = $admin->getStandardResponses();
$text = new displayText();

?>


<?php if (count($responses) == 0) { ?>
<div class="alert alert-info">
    <p>No Standard responses have been created yet.</p>
</div>

<?php } else { ?>
<table class="table table-striped">
    <thead>
    <tr>
	   <th><?php echo $text->getText('standardResponseList'); ?></th>
	   <th width="150"><?php echo $text->getText('addedOn'); ?></th>
	   <th><?php echo $text->getText('edit'); ?></th>
	   <th><?php echo $text->getText('delete'); ?></th>
	   <th colspan="2" style="display:none;">&nbsp;</th>
    </tr>
    </thead>
    <tbody>


	   <?php foreach ($responses as $r) { ?>
    <tr id="row_<?php echo $r['id']; ?>">
	   <td class="thisStandardResponse"><?php echo $r['text']; ?></td>
	   <td><?php echo date("Y-m-d : g:i", $r['added']); ?></td>
	   <td class="response_edit_response">
		  <?php if ($admin->hasWritePermission('standardresponses', $_SESSION['permissions'])) { ?>
		  <a href="#" onclick="enableResponseEdit(<?php echo $r['id']; ?>);"><img src="img/edit.png" width="25"></a>
		  <?php } ?>
	   </td>
	   <td class="response_delete_response">
		  <?php if ($admin->hasWritePermission('standardresponses', $_SESSION['permissions'])) { ?>
		  <a href="#" onclick="deleteResponse(<?php echo $r['id']; ?>,'1','Are you sure?','Delete Response');"><img
				src="img/delete.png" width="25"></a>
		  <?php } ?>
	   </td>
	   <td class="response_save_response" colspan="2" style="display:none;">
		  <?php if ($admin->hasWritePermission('standardresponses', $_SESSION['permissions'])) { ?>
		  <button onclick="editResponse(<?php echo $r['id']; ?>);" class="btn btn-small btn-success" type="button">
			 Save Changes
		  </button>
		  <?php } ?>
	   </td>
    </tr>
	   <?php } ?>

    </tbody>
</table>
<?php } ?>