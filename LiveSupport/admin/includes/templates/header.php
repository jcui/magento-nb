<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 01/01/2013
 * Time: 12:21
 */
session_start();
require 'includes/class/database.php';
require 'includes/class/login.php';
require 'includes/class/displayText.php';
require 'includes/class/admin.php';


$login = new login();
$login->checkLogin($_SESSION);
$text = new displayText();
$admin = new admin();
header('Content-type: text/html; charset=UTF-8');
$settings = $admin->getSettings();
$sResponses = $admin->getStandardResponses();
$sFiles = $admin->getStoredFilesPublic();

?>

<!DOCTYPE html>
<html>
<head>
    <title><?php echo $settings['applicationTitle']; ?></title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/smoothness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" media="screen">
    <link href="css/override.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="includes/uploadify/uploadify.css">
    <link rel="stylesheet" type="text/css" href="css/colorpicker.css">
    <link rel="stylesheet" type="text/css" href="css/jqplot.css"/>


    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="js/colorpicker.js"></script>
    <script src="js/functions.js"></script>
    <!--[if lt IE 9]>
    <script language="javascript" type="text/javascript" src="js/excanvas.js"></script>
    <![endif]-->
    <script language="javascript" type="text/javascript" src="js/jqplot.min.js"></script>
    <script type="text/javascript" src="js/jqplot.dateAxisRenderer.min.js"></script>
    <script type="text/javascript" src="js/jqplot.barRenderer.min.js"></script>
    <script type="text/javascript" src="js/jqplot.categoryAxisRenderer.min.js"></script>
    <script type="text/javascript">


	   // misc messages
	   viewArchivedConvoMessage = "<?php echo $text->getText('viewArchivedConvo'); ?>";
	   pickUpConvoMessage = "<?php echo $text->getText('pickUpConvo'); ?>";
	   endConvoMessage = "<?php echo $text->getText('endConvo'); ?>";
	   inviteConvoMessage = "<?php echo $text->getText('convoInvite'); ?>";


	   $(document).ready(function() {
		  operatorId = <?php echo $_SESSION['userId']; ?>;
		  window.hover = false;

		  getOnlineOffline();
		  getAudioAlertsOnOff();
		  getChatQueue();
		  getMyChats();
		  getConversation();
		  getVisitorsOnline();
		  $("#currChatWindow").scrollTop($("#currChatWindow")[0].scrollHeight);
		  hideUtilityPage();
		  getOperatorChat();
		  getOperatorsOnline();
		  keepAlive();
		  getChatTransferQueue();

		  var p = $('#mainSideBar');
		  var position = p.position();

		  $('#mainSideBar').height($(window).height() - (position.top + 22));

		  $('#chatResponseForm').submit(function() {
			 $('#operatorResponseButton').click();
			 return false;
		  });


		  setOperatorID(<?php echo $_SESSION['userId']; ?>);
		  setMyDepartment(<?php echo $_SESSION['department']; ?>);

		  varClientChatRefresh = <?php echo $settings['clientChatRefresh']; ?>;
		  varAdminChatRefresh = <?php echo $settings['adminChatRefresh']; ?>;
		  varChatQueueRefresh = <?php echo $settings['chatQueueRefresh']; ?>;
		  varMyChatQueueRefresh = <?php echo $settings['myChatQueueRefresh']; ?>;
		  varOperatorChatRefresh = <?php echo $settings['operatorChatRefresh']; ?>;
		  varVisitorsOnlineRefresh = <?php echo $settings['visitorsOnlineRefresh']; ?>;
		  varOperatorsOnlineRefresh = <?php echo $settings['operatorsOnlineRefresh']; ?>;


		  plotInteractions = $.jqplot('interactionChart', [<?php echo $admin->getGraphTotalConvos(); ?>], {
			 title: '<?php echo $text->getText('custChats30'); ?>',

			 seriesDefaults: {
				rendererOptions: {
				    smooth: true
				}
			 },
			 axes: {
				xaxis: {
				    pad:0,
				    renderer:$.jqplot.DateAxisRenderer,
				    tickOptions:{formatString:'%b %#d'},
				    tickInterval:'1 day'


				},
				yaxis: {
				    min: 0,
				    pad:2,
				    label: "<?php echo $text->getText('noOfChats'); ?>"
				}
			 },
			 series:[
				{lineWidth:4, markerOptions:{style:'square'}}
			 ]
		  });

		  plotChatLength = $.jqplot('chatLengthChart', [<?php echo $admin->getGraphAverageChat(); ?>], {
			 title: '<?php echo $text->getText('averageConvoLength'); ?>',
			 axesDefaults: {
				labelRenderer: $.jqplot.CanvasAxisLabelRenderer
			 },
			 axes: {
				xaxis: {
				    renderer:$.jqplot.DateAxisRenderer,
				    tickOptions:{formatString:'%b %#d'},
				    tickInterval:'1 day'
				},
				yaxis: {
				    label: "<?php echo $text->getText('convoLength'); ?>",
				    min: 0,
				    pad:2
				}
			 }
		  });


		  $('a[data-toggle="tab"]').on('shown', function (e) {
			 if (e.target.id == "statLink") {
				plotInteractions.replot();
				plotChatLength.replot();
			 }
		  })

	   });


    </script>
</head>
<body style="height:100%;">
<div class="navbar">
    <div class="navbar-inner">
	   <div class="btn-group pull-left" style="margin-right:20px;">
		  <a class="btn dropdown-toggle btn-danger" data-toggle="dropdown" href="#"
			id="chatOnlineOfflineSettingsButton">
			 <span><?php echo  $settings['applicationTitle']; ?></span>&nbsp;&nbsp;&nbsp;<i class="icon-music"
																			id="audioOnDisplayIcon"></i>&nbsp;
			 <span class="caret"></span>
		  </a>
		  <ul class="dropdown-menu">

			 <div id="chatAvailableDiv" onclick="setOnlineOffline(true)">
				<li><a href="#"><?php echo $text->getText('chatIsAvailable'); ?></a></li>
			 </div>
			 <div id="chatUnAvailableDiv" onclick="setOnlineOffline(false)">
				<li><a href="#"><?php echo $text->getText('chatIsNotAvailable'); ?></a></li>
			 </div>
			 <div id="audioOnDiv" onclick="setAudioAlertsOnOff(true)">
				<li><a href="#"><?php echo $text->getText('audioOn'); ?></a></li>
			 </div>
			 <div id="audioOffDiv" onclick="setAudioAlertsOnOff(false)">
				<li><a href="#"><?php echo $text->getText('audioOff'); ?></a></li>
			 </div>


		  </ul>
	   </div>

	   <ul class="nav" id="mainChatNavigationMenu">
		  <li id="navTabhome" class="active"><a href="#"
										onclick="switchActiveClass('home'); $('#mainChatLink').tab('show');"><?php echo $text->getText('Conversations'); ?></a>
		  </li>
		  <?php if ($login->isAllowedFunction('visitorsonline', 'read')) { ?>
		  <li id="navTabvisitorsonline"><a href="#"
									onclick="loadUtilityPage('visitorsonline','<?php echo $text->getText('visitorsOnline'); ?>')"><?php echo $text->getText('visitorsOnline'); ?>
			 <span id="visitorsOnlineBadge" class="badge badge-success">0</span></a></li>
		  <?php } ?>
		  <?php if ($login->isAllowedFunction('standardresponses', 'read')) { ?>
		  <li id="navTabstandardresponses"><a href="#"
									   onclick="loadUtilityPage('standardresponses','<?php echo $text->getText('standardResponses'); ?>')"><?php echo $text->getText('standardResponses'); ?></a>
		  </li>
		  <?php } ?>
		  <?php if ($login->isAllowedFunction('files', 'read')) { ?>
		  <li id="navTabfiles"><a href="#"
							 onclick="loadUtilityPage('files','<?php echo $text->getText('files'); ?>')"><?php echo $text->getText('files'); ?></a>
		  </li>
		  <?php } ?>
		  <?php if ($login->isAllowedFunction('users', 'read')) { ?>
		  <li id="navTabusers"><a href="#"
							 onclick="loadUtilityPage('users','<?php echo $text->getText('users'); ?>')"><?php echo $text->getText('users'); ?></a>
		  </li>
		  <?php } ?>
		  <?php if ($login->isAllowedFunction('feedback', 'read')) { ?>
		  <li id="navTabfeedback"><a href="#"
							    onclick="loadUtilityPage('feedback','<?php echo $text->getText('feedback'); ?>')"><?php echo $text->getText('feedback'); ?></a>
		  </li>
		  <?php } ?>
		  <?php if ($login->isAllowedFunction('logs', 'read')) { ?>
		  <li id="navTablogs"><a href="#"
							onclick="loadUtilityPage('logs','<?php echo $text->getText('logs'); ?>')"><?php echo $text->getText('logs'); ?></a>
		  </li>
		  <?php } ?>
		  <?php if ($login->isAllowedFunction('settings', 'read')) { ?>
		  <li id="navTabsettings"><a href="#"
							    onclick="loadUtilityPage('settings','<?php echo $text->getText('settings'); ?>')"><?php echo $text->getText('settings'); ?></a>
		  </li>
		  <?php } ?>
		  <li id="navTablogout"><a href="#"
							  onclick="loadUtilityPage('logout','<?php echo $text->getText('logOut'); ?>')"><?php echo $text->getText('logOut'); ?></a>
		  </li>
	   </ul>
    </div>
</div>
<div class="row">
    <div class="span4 bg" id="mainSideBar">
	   <div class="b-pad">
		  <div class="tabbable" id="chatQueueContainer">
			 <ul class="nav nav-tabs">
				<li class="active"><a href="#tab1" data-toggle="tab"><?php echo $text->getText('chatQueue'); ?>
				    <span id="newConvosBadge" class="badge badge-warning">!</span></a></li>
				<li><a href="#tab2" data-toggle="tab"><?php echo $text->getText('myChats'); ?> <span
					   id="MyNewConvosBadge" class="badge badge-info"></span></a></li>
				<li><a href="#tab3" data-toggle="tab"><?php echo $text->getText('operators'); ?> <span
					   class="badge badge-info"></span></a></li>
			 </ul>
			 <div class="tab-content">
				<div class="tab-pane tab-pane-taller active" id="tab1">
				    <div id="chatQueueContainerInner"></div>
				    <div id="chatTransferQueueContainerInner"></div>
				</div>
				<div class="tab-pane tab-pane-taller" id="tab2"></div>
				<div class="tab-pane tab-pane-taller" id="tab3">

				    <div class="tabbable">


					   <ul class="nav nav-tabs">
						  <li class="active"><a href="#operatorsOnlineDiv"
										    data-toggle="tab"><?php echo $text->getText('operatorsOnline'); ?></a>
						  </li>
						  <li><a href="#operatorsChatDiv"
							    data-toggle="tab"><?php echo $text->getText('operatorChat'); ?></a></li>
					   </ul>


					   <div class="tab-content">
						  <div class="tab-pane active" id="operatorsOnlineDiv"></div>

						  <div class="tab-pane" id="operatorsChatDiv">
							 <div id="operatorChatContainer"></div>
							 <form class="form-inline" id="operatorChatForm" onsubmit="submitOperatorMessage()">
								<div class="input-append">
								    <input class="span2" id="operatorChatDiv" type="text" style="width:290px;"
										 placeholder="">
								    <input type="submit" class="btn btn-primary" type="button"
										 value="<?php echo $text->getText('formSubmitSend'); ?>"
										 id="operatorChatSubmit">
								</div>
							 </form>
						  </div>
					   </div>
				    </div>
				</div>
			 </div>
		  </div>
	   </div>
    </div>
    
    <?php require 'modal.php'; ?>