<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 06/01/2013
 * Time: 17:26
 */
require 'includes/class/database.php';
require 'includes/class/login.php';
session_start();
$l = new login();
$l->logUserOut($_SESSION['userId']);
session_destroy();
$_SESSION = null;

header('location: login.php');