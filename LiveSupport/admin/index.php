<?php require 'includes/templates/header.php'; ?>

<div class="span8" style="position:relative;">
    <div class="tabbable">
	   <ul class="nav nav-tabs">
		  <li class="active">
			 <a href="#conversations" data-toggle="tab" id="mainChatLink"
			    onclick="switchActiveClass('home');"><?php echo $text->getText('Conversations'); ?></a>
		  </li>
		  <li id="utilityTab">
			 <a href="#utilityData" data-toggle="tab" id="utilityLink"></a>
		  </li>
		  <li id="statTab" class="pull-right">
			 <a href="#statData" data-toggle="tab" id="statLink"><?php echo $text->getText('stats'); ?></a>
		  </li>
	   </ul>
	   <div class="tab-content" style="overflow-x:hidden;">
		  <div class="tab-pane active" id="conversations">
			 <ul class="nav nav-tabs" id="mainConvoOptions">
				<li class="dropdown">
				    <a class="dropdown-toggle"
					  data-toggle="dropdown"
					  href="#">
					   <?php echo $text->getText('convoOptions'); ?>
					   <b class="caret"></b>

				    </a>
				    <ul class="dropdown-menu">
					   <li><a href="#"
							onclick="endConversation()"><?php echo $text->getText('endConversation'); ?></a></li>
					   <li><a href="#"
							onclick="transferConversation()"><?php echo $text->getText('transferConversation'); ?></a>
					   </li>
				    </ul>
				</li>
			 </ul>


			 <div id="mainConversationDisplay">
				<div id="displayContainer">
				    <div class="chatoutput" id="currChatWindow"></div>
				    <div id="typingStatus"><p id="typingStatusMessage"></p></div>
				</div>
				<form id="chatResponseForm" method="post" action="">
				    <div class="input-append">
					   <input class="span2" id="operatorResponse" type="text" style="width:700px;"
							onkeyup="iAmTyping(true);">
					   <button class="btn btn-primary" type="button" id="operatorResponseButton"
							 onclick="storeResponse('<?php echo $_SESSION['username']; ?>')">
						  <?php echo $text->getText('formSubmitSend'); ?>
					   </button>
				    </div>
				</form>
				<form class="form-inline">
				    <br>

				    <select id="userStandardResponses"
						  onchange="sendStandardResponse(this.value,'<?php echo $_SESSION['username']; ?>');">
					   <option><?php echo $text->getText('selectStandardResponse'); ?></option>
					   <?php if(count($sResponses) != 0) {foreach ($sResponses as $r) { ?>
					   <option value="<?php echo $r['text']; ?>"><?php echo $r['text']; ?></option>
					   <?php }} ?>
				    </select>
				    <button class="btn btn-primary"
						  type="button"><?php echo $text->getText('insertResponse'); ?></button>
				    <br>
				    <br>
				    <select id="userFiles"
						  onchange="sendFileResponse(this.value,'<?php echo $_SESSION['username']; ?>')">
					   <option><?php echo $text->getText('selectFileAttach'); ?></option>
					   <?php if(count($sFiles) != 0) {foreach ($sFiles as $r) { ?>
					   <option value="<?php echo $r['id']; ?>"><?php echo $r['description']; ?></option>
					   <?php }} ?>
				    </select>
				    <button class="btn btn-primary"
						  type="button"><?php echo $text->getText('attachFile'); ?></button>
				</form>
			 </div>
			 <div id="mainChatEmptyDisplay" style="display:none;">
				<div class="alert alert-info">
				    <p><?php echo $text->getText('noChatsYet'); ?></p>
				</div>
			 </div>
		  </div>
		  <div class="tab-pane" id="utilityData"></div>
		  <div class="tab-pane" id="statData">

			 <div id="interactionChart" style="height:250px;width:700px; margin:0 0 15px 0;"></div>
			 <div id="chatLengthChart" style="height:250px;width:700px; margin:0 0 15px 0;"></div>

		  </div>
	   </div>
    </div>
</div>





</div>


<?php require 'includes/templates/footer.php'; ?>



