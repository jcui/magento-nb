$(document).ready(function() {

    $('.chatoutput').mouseover(function() {
        window.hover = true;
    });

    $('.chatoutput').mouseout(function() {
        window.hover = false;
    });

    $('#operatorChatContainer').mouseover(function() {
        window.operatorHover = true;
    });

    $('#operatorChatContainer').mouseout(function() {
        window.operatorHover = false;
    });

});


var varClientChatRefresh = 2000;
var varAdminChatRefresh = 2000;
var varChatQueueRefresh = 2000; //
var varMyChatQueueRefresh = 2000; //
var varOperatorChatRefresh = 2000;//
var varVisitorsOnlineRefresh = 2000; //
var varOperatorsOnlineRefresh = 2000; //



// state variables

var defaultPageTitle = document.title;

// audioAlertsOn
var audioAlertsEnabled;

// new message sound
var newMessageSnd
var a = document.createElement('audio');
// first try mp3
if(!!(a.canPlayType && a.canPlayType('audio/mpeg;').replace(/no/, '')))
{
    newMessageSnd = new Audio("includes/audio/newMessage.mp3");
}
else
{
    // use wav
    newMessageSnd = new Audio("includes/audio/newMessage.wav");
}

// new chat state
var newChats = 0;
var myNewChats = 0;
var myNewChatsLast = 0;
var convoToTransfer;

var myDepartment;
var chatInviteId;

// pick up chat
var convoToPickUp = null;

// operator id
var operatorId = null;

// newChatSoundPlayed
var newChatSoundPlayed = 0;

var clientTyping = false;

var preventUpdateTyping = false;

// repeated functions
var getOnlineOfflineTimer = window.setTimeout(function() {
    getOnlineOffline();
}, 10000);
var getMyChatsTimer = window.setTimeout(function() {
    getMyChats();
}, 5000);
var getChatQueueTimer = window.setTimeout(function() {
    getChatQueue();
}, 5000);
var chatTimer = window.setTimeout(function() {
    getConversation();
}, 2000);
var visitorsOnlineTimer = window.setTimeout(function() {
    getVisitorsOnline();
}, 5000);
var updateTypingStatus = window.setTimeout(function() {
    iAmTyping(false);
}, 1000);
var areYouTypingStatus = window.setTimeout(function() {
    areYouTyping();
}, 1000);
var getOperatorsOnlineTimer = window.setTimeout(function() {
    getOperatorsOnline();
}, 5000);
var getOperatorChatTimer = window.setTimeout(function() {
    getOperatorChat();
}, 5000);
var keepAliveTimer = window.setTimeout(function() {
    keepAlive();
}, 120000);
var getChatTransferQueueTimer = window.setTimeout(function() {
    getChatTransferQueue();
}, 10000);

function setOperatorID(opId) {
    operatorId = opId;
}

function setMyDepartment(dept) {
    myDepartment = dept;
}

function hideModal() {
    $('#loadModal').hide();
}

function storeResponse(userName) {
    var message = $('#operatorResponse').val();
    var user = userName;
    var time = (new Date).getTime();
    var convoId = $('#convoId').val();
    var url = '';
    url += 'action=recordResponse&user=' + user + '&convoId=' + convoId + '&message=' + message + '&noCahe=' + time;

    $.ajax({
        url: 'includes/handler.php?' + encodeURI(url),
        success: function(data) {
            $('#operatorResponse').val('');
            getConversation();
            return true;
        }
    });
}

function getConversation(convoIdInp, switchTab) {
    var convoId
    if (convoIdInp != undefined) {
        convoId = convoIdInp;
    }
    else {
        convoId = $('#convoId').val();
    }
    var time = (new Date).getTime();
    var url = '';
    url += 'action=getConversation&convoId=' + convoId + '&noCahe=' + time;

    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            if (switchTab == "1") {
                $('#mainChatLink').tab('show');
            }
            $('#currChatWindow').empty().html(data);
            if (window.hover == false) {
                $("#currChatWindow").scrollTop($("#currChatWindow")[0].scrollHeight);
            }
            window.clearTimeout(chatTimer);
            chatTimer = window.setTimeout(function() {
                getConversation(convoId);
            }, varAdminChatRefresh);
            getMyChats();
            return true;
        }
    });
}

function getMyChats() {
    var time = (new Date).getTime();
    var url = 'action=getMyChats&noCahe=' + time + '&user=' + operatorId;
    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            $('#tab2').empty().html(data);

            if ($('#mainTotalChats').val() == "0") {
                $('#mainChatEmptyDisplay').show();
                $('#mainConversationDisplay').hide();
                $('#mainConvoOptions').hide();
            }
            else {
                $('#mainChatEmptyDisplay').hide();
                $('#mainConversationDisplay').show();
                $('#mainConvoOptions').show();
            }

            if ($('#myChatsWaiting').val() != undefined && $('#myChatsWaiting').val() != "0") {
                $('#MyNewConvosBadge').show().html($('#myChatsWaiting').val());
                if (myNewChats == 0 || parseInt($('#myChatsWaiting').val()) != myNewChatsLast) {
                    if (audioAlertsEnabled) {
                        newMessageSnd.play();
                    }
                    myNewChats = 1;
                }
                myNewChatsLast = parseInt($('#myChatsWaiting').val());
            }
            else {
                myNewChats = 0;
                $('#MyNewConvosBadge').hide();
            }
            if (typeof(getMyChatsTimer) !== undefined) {
                window.clearTimeout(getMyChatsTimer);
            }
            getMyChatsTimer = window.setTimeout(function() {
                getMyChats();
            }, varMyChatQueueRefresh);
            return true;
        }
    });
}

function getChatQueue() {
    var time = (new Date).getTime();
    var url = 'action=getChatQueue&noCahe=' + time + '&user=' + operatorId;
    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            $('#chatQueueContainerInner').empty().html(data);

            if ($('#pickedUpConvos').val() != "0") {
                $('#newConvosBadge').show().html($('#pickedUpConvos').val());
                if (newChats == 0) {
                    if (audioAlertsEnabled) {
                        newMessageSnd.play();
                    }
                    newChats = 1;
                    document.title = "New Message!";
                }
            }
            else if ($('#myChatsTransfersWaiting').val() != undefined && $('#myChatsTransfersWaiting').val() != "0") {
                $('#newConvosBadge').show().html($('#myChatsTransfersWaiting').val());
            }
            else {
                newChats = 0;
                $('#newConvosBadge').hide();
                document.title = defaultPageTitle;
            }
            window.clearTimeout(getChatQueueTimer);
            getChatQueueTimer = window.setTimeout(function() {
                getChatQueue();
            }, varChatQueueRefresh);
            return true;
        }
    });
}

function getChatTransferQueue() {
    var time = (new Date).getTime();
    var url = 'action=getChatTransferQueue&noCahe=' + time + '&user=' + operatorId;
    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            $('#chatTransferQueueContainerInner').empty().html(data);
            if ($('#myChatsTransfersWaiting').val() != undefined && $('#myChatsTransfersWaiting').val() != "0") {
                $('#newConvosBadge').show().html($('#myChatsTransfersWaiting').val());
            }


            window.clearTimeout(getChatTransferQueueTimer);
            getChatTransferQueueTimer = window.setTimeout(function() {
                getChatTransferQueue();
            }, 10000);
            return true;
        }
    });
}

function getVisitorsOnline() {
    var time = (new Date).getTime();
    var url = 'action=getVisitorsOnline&noCahe=' + time;

    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            $('#visitorsOnlineBadge').html(data)
            window.clearTimeout(visitorsOnlineTimer);
            visitorsOnlineTimer = window.setTimeout(function() {
                getVisitorsOnline();
            }, varVisitorsOnlineRefresh);
            return true;
        }
    });
}

function inviteUserToChat(userId, message) {

    chatInviteId = userId;

    if (message == undefined) {
        openModal('chatInvite');
    }
    else {
        var time = (new Date).getTime();
        var url = 'action=inviteUser&operatorId=' + operatorId + '&userId=' + userId + '&department=' + myDepartment + '&message=' + message + '&noCahe=' + time;

        $.ajax({
            url: 'includes/handler.php?' + url,
            success: function(data) {
                if (data == "1") {
                    closeModal();
                }
                else {
                    showModalError();
                }
                return true;
            }
        });
    }

}

function viewArchivedConvo(conversationId) {
    openModal('viewArchivedConvo', conversationId);
}

function openModal(windowType, extraVar) {
    closeModal();

    switch (windowType) {
        case "viewArchivedConvo":
        {
            var time = (new Date).getTime();
            typeText = viewArchivedConvoMessage;
            dataPath = 'includes/templates/viewArchivedChat.php?conversation=' + extraVar + '&nocache=' + time;
            $('#LSChatModalButton').hide();
            $('#lsChatClose').click(function() {
                closeModal()
            });
            break;
        }
        case "transferConvo":
        {
            typeText = "Transfer conversation";
            dataPath = 'includes/templates/transferChat.php';
            $('#LSChatModalButton').text('Transfer conversation');
            $('#LSChatModalButton').click(function() {
                initiateTransfer();
            });
            break;
        }
        case "pickUpChat":
        {
            typeText = pickUpConvoMessage;
            dataPath = 'includes/templates/pickUpChat.php';
            $('#LSChatModalButton').click(function() {
                pickUpConversation(convoToPickUp)
            });
            $('#LSChatModalButton').text(pickUpConvoMessage);
            break
        }
        case "endConvo":
        {
            typeText = endConvoMessage;
            dataPath = 'includes/templates/endConvo.php';
            $('#LSChatModalButton').click(function() {
                endConversation(getConvoId())
            });
            $('#LSChatModalButton').text(endConvoMessage);
            $('#LSChatModalButton').addClass('btn-danger');


            break
        }
        case "chatInvite":
        {
            typeText = inviteConvoMessage;
            dataPath = 'includes/templates/chatInvite.php';
            $('#LSChatModalButton').click(function() {
                inviteUserToChat(chatInviteId, $('#inviteUserMessage').val());
            });
            $('#LSChatModalButton').text('Invite User');
            $('#LSChatModalButton').addClass('btn-success');


            break
        }
    }
    $('#LSChatModal').modal({
        keyboard: false,
        backdrop: true,
        remote: dataPath
    });
    $('#LSChatModalLabel').text(typeText);
}

function getConvoId() {
    return $('#convoId').val();
}

function closeModal() {
    $('#LSChatModalButton').show();
    $('#LSChatModal').modal('hide');
    $('#LSChatModal').data('modal', null);
    $('#LSChatModalButton').off('click');
    $('#LSChatModalButton').removeClass('btn-danger');
    $('#LSChatModalButton').removeClass('btn-success');
}

function showModalError() {
    $('#MTstandard').hide();
    $('#MTError').show();
}

function endConversation(convoId) {

    if (getConvoId() != undefined) {
        if (convoId == undefined) {
            openModal('endConvo');
        }
        else {
            var time = (new Date).getTime();
            var url = '';
            url += 'action=endConversation&user=' + operatorId + '&convoId=' + convoId + '&noCahe=' + time;
            $.ajax({
                url: 'includes/handler.php?' + url,
                success: function(data) {
                    if (data == "1") {
                        getConversation('clear');
                        closeModal();
                        getChatQueue();
                        getMyChats();
                    }
                    else {
                        showModalError();
                    }

                }
            });
        }
    }
}

function initiateTransfer() {
    $('#LSChatModalButton').show();
    $('#LSChatModal').modal('hide');
    $('#LSChatModal').data('modal', null);
    $('#LSChatModalButton').off('click');
    $('#LSChatModalButton').removeClass('btn-danger');
    $('#LSChatModalButton').removeClass('btn-success');

    $('#operatorTransferDetails').hide();
    $('#operatorTransferWait').show();

    var time = (new Date).getTime();
    var url = '';
    var messagetotransfer = $('#transferRequestMessage').val();
    var operatorTarget = $('#transferAvailableOperators').val();
    url += 'action=transferConversation&from=' + operatorId + '&convoId=' + getConvoId() + '&noCahe=' + time + '&to=' + operatorTarget + '&message=' + messagetotransfer;

    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            if (data == "1") {
                closeModal();
            }
            else {
                showModalError();
            }

        }
    });
}

function transferConversation(convoId) {
    if (getConvoId() != undefined) {
        openModal('transferConvo');
    }
}

function pickUpConversation(convoId) {
    var time = (new Date).getTime();
    var url = '';
    url += 'action=pickUpConversation&user=' + operatorId + '&convoId=' + convoId + '&noCahe=' + time;

    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            if (data == "1") {

                closeModal();
                getChatQueue();
                getMyChats();
            }
            else {
                showModalError();
            }

        }
    });
}

function switchActiveClass(pageId) {
    $('#mainChatNavigationMenu li').removeClass('active');
    $('#navTab' + pageId).addClass('active');
}

function hideUtilityPage() {
    $('#mainChatLink').tab('show');
    $('#utilityData').empty();
    $('#utilityTab').hide();
    switchActiveClass('home');
}

function loadUtilityPage(pageId, pageKey) {
    // determine file path
    // load data
    var time = (new Date).getTime();
    var url = '';
    url += 'action=loadPage&page=' + pageId + '&noCahe=' + time;

    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            // insert data into utility div
            $('#utilityData').html(data);
            // set tab title
            $('#utilityLink').text(pageKey);
            $('#utilityLink').click(function() {
                switchActiveClass(pageId);
            });
            //show tab
            $('#utilityTab').show();
            // activate tab
            $('#utilityLink').tab('show');

            $('#mainChatNavigationMenu li').removeClass('active');
            $('#navTab' + pageId).addClass('active');

        }
    });
}

/* Users Page */

function addPerm() {
    $('#users_add_permissions_ok').hide();
    $('#users_add_permissions_error').hide();
    var permName = $('#permAddInput').val();
    if (permName != '') {
        var time = (new Date).getTime();
        var url = '';
        url += 'action=addPermissionGroup&permissionGroupName=' + permName + '&noCahe=' + time;
        $.ajax({
            url: 'includes/handler.php?' + url,
            success: function(data) {

                if (data == "1") {
                    $('#users_add_permissions_ok').show().delay(3000).fadeOut(1000);
                    $('#ajaxPermListHolder').empty().load('includes/templates/inc.permissions.php');

                }
                else {
                    $('#users_add_permissions_error').show().delay(3000).fadeOut(1000);
                }
            }
        });
    }
}

function addFbQuestion() {
    $('#question_add_response_ok').hide();
    $('#question_add_response_error').hide();

    var responseText = $('#questionAddInput').val();
    if (responseText != '') {
        var time = (new Date).getTime();
        var url = '';
        url += 'action=storeFeedbackQuestion&questionText=' + responseText + '&noCahe=' + time;
        $.ajax({
            url: 'includes/handler.php?' + url,
            success: function(data) {
                if (data == "1") {
                    $('#question_add_response_ok').show().delay(3000).fadeOut(1000);
                    $('#ajaxResponsesContainer').load('includes/templates/inc.feedbacklist.php');
                    $('#questionAddInput').val('');

                }
                else {
                    $('#question_add_response_error').show().delay(3000).fadeOut(1000);
                }
            }
        });
    }
}

function addResponse() {
    $('#users_add_response_ok').hide();
    $('#users_add_response_error').hide();

    var responseText = $('#responseAddInput').val();
    if (responseText != '') {
        var time = (new Date).getTime();
        var url = '';
        url += 'action=storeStandardResponse&responseText=' + responseText + '&noCahe=' + time;
        $.ajax({
            url: 'includes/handler.php?' + url,
            success: function(data) {
                if (data == "1") {
                    $('#users_add_response_ok').show().delay(3000).fadeOut(1000);
                    $('#ajaxResponsesContainer').load('includes/templates/inc.standardresponselist.php');
                    $('#responseAddInput').val('');

                }
                else {
                    $('#users_add_response_error').show().delay(3000).fadeOut(1000);
                }
            }
        });
    }
}

function addDepartment() {
    $('#users_add_department_ok').hide();
    $('#users_add_department_error').hide();

    var deptName = $('#departmentAddInput').val();
    if (deptName != '') {
        var time = (new Date).getTime();
        var url = '';
        url += 'action=addDepartment&departmentName=' + deptName + '&noCahe=' + time;
        $.ajax({
            url: 'includes/handler.php?' + url,
            success: function(data) {
                if (data == "1") {
                    $('#users_add_department_ok').show().delay(3000).fadeOut(1000);
                    $('#ajaxDeptListHolder').empty().load('includes/templates/inc.departmentlist.php');

                }
                else {
                    $('#users_add_department_error').show().delay(3000).fadeOut(1000);
                }
            }
        });
    }
}

function savePerms() {
    $('#users_edit_permissions_error').hide();
    $('#users_edit_permissions_ok').hide();

    $('#userPermissionTable tr').each(function() {
        var pid = $(this).find(".permissionName").val();
        if (pid != undefined) {

            var vo_read = ($(this).find('.vo_read').is(':checked')) ? 1 : 0;
            var vo_write = ($(this).find('.vo_write').is(':checked') ) ? 1 : 0;
            var sr_read = ($(this).find('.sr_read').is(':checked')) ? 1 : 0;
            var sr_write = ($(this).find('.sr_write').is(':checked') ) ? 1 : 0;
            var f_read = ($(this).find('.f_read').is(':checked')) ? 1 : 0;
            var f_write = ($(this).find('.f_write').is(':checked') ) ? 1 : 0;
            var u_read = ($(this).find('.u_read').is(':checked')) ? 1 : 0;
            var u_write = ($(this).find('.u_write').is(':checked')) ? 1 : 0;
            var l_read = ($(this).find('.l_read').is(':checked')) ? 1 : 0;
            var l_write = ($(this).find('.l_write').is(':checked') ) ? 1 : 0;
            var s_read = ($(this).find('.s_read').is(':checked')) ? 1 : 0;
            var s_write = ($(this).find('.s_write').is(':checked')) ? 1 : 0;
            var fb_read = ($(this).find('.fb_read').is(':checked')) ? 1 : 0;
            var fb_write = ($(this).find('.fb_write').is(':checked')) ? 1 : 0;
            var tPs;
            var time = (new Date).getTime();
            var url = '';
            url += 'action=updatePermissionGroup&updatePermissionGroupId=' + pid + '&noCahe=' + time;
            tPs = '&visitorsonline_read=' + vo_read;
            url += tPs;
            tPs = '&visitorsonline_write=' + vo_write;
            url += tPs;
            tPs = '&standardresponses_read=' + sr_read;
            url += tPs;
            tPs = '&standardresponses_write=' + sr_write;
            url += tPs;
            tPs = '&files_read=' + f_read;
            url += tPs;
            tPs = '&files_write=' + f_write;
            url += tPs;
            tPs = '&users_read=' + u_read;
            url += tPs;
            tPs = '&users_write=' + u_write;
            url += tPs;
            tPs = '&logs_read=' + l_read;
            url += tPs;
            tPs = '&logs_write=' + l_write;
            url += tPs;
            tPs = '&settings_read=' + s_read;
            url += tPs;
            tPs = '&settings_write=' + s_write;
            url += tPs;
            tPs = '&feedback_read=' + fb_read;
            url += tPs;
            tPs = '&feedback_write=' + fb_write;
            url += tPs;
            $.ajax({
                url: 'includes/handler.php?' + url,
                success: function(data) {
                    if (data == "1") {
                        $('#users_edit_permissions_ok').show().delay(3000).fadeOut(1000);
                        $('#ajaxDeptListHolder').empty().load('includes/templates/inc.departmentlist.php');
                        $('#ajaxPermListHolder').empty().load('includes/templates/inc.permissions.php');

                    }
                    else {
                        $('#users_edit_permissions_error').show().delay(3000).fadeOut(1000);
                    }
                }
            });


        }

    });
}

function deletePermissionGroup(permId, showModal, utilityText, utilityLinkText) {
    $('#users_delete_permissions_error').hide();
    $('#users_delete_permissions_ok').hide();
    if (showModal == "1") {
        $('#LSChatModal').modal({
            keyboard: false,
            backdrop: true
        });

        $('#LSChatModalLabel').text(utilityText);
        $('#LSChatModalButton').click(function() {
            deletePermissionGroup(permId, false);
        });
        $('#LSChatModalButton').text(utilityLinkText);
    }
    else {
        var time = (new Date).getTime();
        var url = '';
        url += 'action=deletePermissionGroup&permissionGroupId=' + permId + '&noCahe=' + time;

        $.ajax({
            url: 'includes/handler.php?' + url,
            success: function(data) {
                if (data == "1") {
                    $('#permRow_' + permId).remove();
                    $('#users_delete_permissions_ok').show().delay(3000).fadeOut(1000);
                    $('#ajaxDeptListHolder').empty().load('includes/templates/inc.departmentlist.php');
                    $('#ajaxUserListHolder').empty().load('includes/templates/inc.userlist.php');
                    $('#ajaxPermListHolder').empty().load('includes/templates/inc.permissions.php');

                }
                else {
                    $('#users_delete_permissions_error').show().delay(3000).fadeOut(1000);
                }
                closeModal();
            }
        });
    }
}

function deleteDepartment(deptId, showModal, utilityText, utilityLinkText) {
    $('#users_delete_department_error').hide();
    $('#users_delete_department_ok').hide();
    if (showModal == "1") {
        $('#LSChatModal').modal({
            keyboard: false,
            backdrop: true
        });

        $('#LSChatModalLabel').text(utilityText);
        $('#LSChatModalButton').click(function() {
            deleteDepartment(deptId, false);
        });
        $('#LSChatModalButton').text(utilityLinkText);
    }
    else {
        var time = (new Date).getTime();
        var url = '';
        url += 'action=deleteDepartment&departmentId=' + deptId + '&noCahe=' + time;

        $.ajax({
            url: 'includes/handler.php?' + url,
            success: function(data) {
                if (data == "1") {
                    $('#row_' + deptId).remove();
                    $('#users_delete_department_ok').show().delay(3000).fadeOut(1000);
                    $('#ajaxDeptListHolder').empty().load('includes/templates/inc.departmentlist.php');
                    $('#ajaxUserListHolder').empty().load('includes/templates/inc.userlist.php');

                }
                else {
                    $('#users_delete_department_error').show().delay(3000).fadeOut(1000);
                }
                closeModal();
            }
        });
    }
}

function deleteFile(fileId, showModal, utilityText, utilityLinkText) {
    $('#users_delete_file_error').hide();
    $('#users_delete_file_ok').hide();
    if (showModal == "1") {
        $('#LSChatModal').modal({
            keyboard: false,
            backdrop: true
        });

        $('#LSChatModalLabel').text(utilityText);
        $('#LSChatModalButton').click(function() {
            deleteFile(fileId, false);
        });
        $('#LSChatModalButton').text(utilityLinkText);
    }
    else {
        // delete user

        var time = (new Date).getTime();
        var url = '';
        url += 'action=deleteFile&fileId=' + fileId + '&noCahe=' + time;

        $.ajax({
            url: 'includes/handler.php?' + url,
            success: function(data) {
                if (data == "1") {
                    $('#row_' + fileId).remove();
                    $('#users_delete_file_ok').show().delay(3000).fadeOut(1000);

                }
                else {
                    $('#users_delete_file_error').show().delay(3000).fadeOut(1000);
                }
                closeModal();
            }
        });

    }
}

function deleteUser(userId, showModal, utilityText, utilityLinkText) {
    $('#users_delete_user_error').hide();
    $('#users_delete_user_ok').hide();
    if (showModal == "1") {
        $('#LSChatModal').modal({
            keyboard: false,
            backdrop: true
        });

        $('#LSChatModalLabel').text(utilityText);
        $('#LSChatModalButton').click(function() {
            deleteUser(userId, false);
        });
        $('#LSChatModalButton').text(utilityLinkText);
    }
    else {
        // delete user

        var time = (new Date).getTime();
        var url = '';
        url += 'action=deleteUser&userId=' + userId + '&noCahe=' + time;

        $.ajax({
            url: 'includes/handler.php?' + url,
            success: function(data) {
                if (data == "1") {
                    $('#row_' + userId).remove();
                    $('#users_delete_user_ok').show().delay(3000).fadeOut(1000);

                }
                else {
                    $('#users_delete_user_error').show().delay(3000).fadeOut(1000);
                }
                closeModal();
            }
        });

    }
}


function deleteFbQuestion(responseId, showModal, utilityText, utilityLinkText) {
    $('#users_delete_response_error').hide();
    $('#users_delete_response_ok').hide();
    if (showModal == "1") {
        $('#LSChatModal').modal({
            keyboard: false,
            backdrop: true
        });

        $('#LSChatModalLabel').text(utilityText);
        $('#LSChatModalButton').click(function() {
            deleteFbQuestion(responseId, false);
        });
        $('#LSChatModalButton').text(utilityLinkText);
    }
    else {
        // delete user

        var time = (new Date).getTime();
        var url = '';
        url += 'action=deleteFeedbackQuestion&responseId=' + responseId + '&noCahe=' + time;

        $.ajax({
            url: 'includes/handler.php?' + url,
            success: function(data) {
                if (data == "1") {
                    $('#row_' + responseId).remove();
                    $('#question_delete_response_ok').show().delay(3000).fadeOut(1000);

                }
                else {
                    $('#question_delete_response_error').show().delay(3000).fadeOut(1000);
                }
                closeModal();
            }
        });

    }
}

function deleteResponse(responseId, showModal, utilityText, utilityLinkText) {
    $('#users_delete_response_error').hide();
    $('#users_delete_response_ok').hide();
    if (showModal == "1") {
        $('#LSChatModal').modal({
            keyboard: false,
            backdrop: true
        });

        $('#LSChatModalLabel').text(utilityText);
        $('#LSChatModalButton').click(function() {
            deleteResponse(responseId, false);
        });
        $('#LSChatModalButton').text(utilityLinkText);
    }
    else {
        // delete user

        var time = (new Date).getTime();
        var url = '';
        url += 'action=deleteStandardResponse&responseId=' + responseId + '&noCahe=' + time;

        $.ajax({
            url: 'includes/handler.php?' + url,
            success: function(data) {
                if (data == "1") {
                    $('#row_' + responseId).remove();
                    $('#users_delete_response_ok').show().delay(3000).fadeOut(1000);

                }
                else {
                    $('#users_delete_response_error').show().delay(3000).fadeOut(1000);
                }
                closeModal();
            }
        });

    }
}

function updateUser(userIdUpdate) {
    var email;
    var dName;
    var dept;
    var perm;
    var pass;

    $('#users_edit_user_error').hide();
    $('#users_edit_user_ok').hide();

    $('#row_' + userIdUpdate).each(function() {
        email = $(this).find("#edit_userName").val();
    });
    $('#row_' + userIdUpdate).each(function() {
        dName = $(this).find("#edit_displayName").val();
    });
    $('#row_' + userIdUpdate).each(function() {
        dept = $(this).find("#edit_department").val();
    });
    $('#row_' + userIdUpdate).each(function() {
        perm = $(this).find("#edit_permissions").val();
    });
    $('#row_' + userIdUpdate).each(function() {
        pass = $(this).find("#edit_password").val();
    });

    var time = (new Date).getTime();
    var url = '';
    url += 'action=updateUser&userId=' + userIdUpdate + '&password=' + encodeURIComponent(pass) + '&displayName=' + dName + '&email=' + email + '&department=' + dept + '&permissions=' + perm + '&noCahe=' + time;
    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            if (data == "1") {
                $('#users_edit_user_error').hide();
                $('#users_edit_user_ok').show().delay(3000).fadeOut(1000);
                $('#ajaxUserListHolder').empty().load('includes/templates/inc.userlist.php');
            }
            else {
                $('#users_edit_user_error').show().delay(3000).fadeOut(1000);
                $('#users_edit_user_ok').hide();
            }

        }
    });

}

function getOperatorChat() {
    if (window.operatorHover == undefined) {
        window.operatorHover = false;
    }
    var time = (new Date).getTime();
    var url = '';
    url += 'action=getOperatorChat&noCahe=' + time;

    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data1) {
            $('#operatorChatContainer').empty().html(data1);


            if (window.operatorHover == false) {
                $("#operatorChatContainer").scrollTop($("#operatorChatContainer")[0].scrollHeight);
            }


            if (typeof(getOperatorChatTimer) !== undefined) {
                window.clearTimeout(getOperatorChatTimer);
            }
            getOperatorChatTimer = window.setTimeout(function() {
                getOperatorChat();
            }, varOperatorChatRefresh);
        }
    });
}

function submitOperatorMessage() {
    event.preventDefault();
    var thisMessage = $('#operatorChatDiv').val();
    var time = (new Date).getTime();
    var url = '';
    url += 'action=submitOperatorMessage&noCahe=' + time + '&message=' + encodeURIComponent(thisMessage) + '&userId=' + operatorId;

    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            $('#operatorChatDiv').val('');
            getOperatorChat();
            return false;
        }
    });
    return false;
}

function getOperatorsOnline() {
    var time = (new Date).getTime();
    var url = '';
    url += 'action=getOperatorsOnline&noCahe=' + time;

    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data2) {
            $('#operatorsOnlineDiv').empty().html(data2);


            if (typeof(getOperatorsOnlineTimer) !== undefined) {
                window.clearTimeout(getOperatorsOnlineTimer);
            }
            getOperatorsOnlineTimer = window.setTimeout(function() {
                getOperatorsOnline();
            }, varOperatorsOnlineRefresh);
        }
    });
}

function enableResponseEdit(responseId) {
    var responseText = $('#row_' + responseId).find('.thisStandardResponse').text();
    var tInput = '<textarea id="thisStandardResponseEdit"></textarea>';
    var tCell = $('#row_' + responseId).find('.thisStandardResponse');
    $(tCell).empty().html(tInput);
    $('#row_' + responseId).find('#thisStandardResponseEdit').val(responseText);

    $('#row_' + responseId).find(".response_save_response").show();
    $('#row_' + responseId).find(".response_delete_response").hide();
    $('#row_' + responseId).find(".response_edit_response").hide();

}

function editResponse(responseId) {
    $('#users_edit_response_error').hide();
    $('#users_edit_response_ok').hide();

    var responseText = $('#row_' + responseId).find('#thisStandardResponseEdit').val();
    var time = (new Date).getTime();
    var url = '';
    url += 'action=updateStandardResponse&responseId=' + responseId + '&responseText=' + responseText + '&noCahe=' + time;

    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            if (data == "1") {
                $('#users_edit_response_ok').show().delay(3000).fadeOut(1000);
                $('#ajaxResponsesContainer').load('includes/templates/inc.standardresponselist.php');
            }
            else {
                $('#users_edit_response_error').show().delay(3000).fadeOut(1000);
            }
        }
    });
}

function editUser(userId, currentPermissions, currentDepartment, update) {
    var username = '';
    var DisplayName = '';
    var Department = '';
    var permissionLevel = '';
    var password = '';

    //$('#row_' + userId).hide();
    $('#row_' + userId).each(function() {
        username = $(this).find(".users_username").html();
    });
    $('#row_' + userId).each(function() {
        DisplayName = $(this).find(".users_displayname").html();
    });
    $('#row_' + userId).each(function() {
        Department = $(this).find(".users_department").html();
    });
    $('#row_' + userId).each(function() {
        permissionLevel = $(this).find(".users_permissions").html();
    });
    $('#row_' + userId).each(function() {
        password = $(this).find(".users_password").html();
    });

    // create input field for email address
    var un_input = '<input type="text" id="edit_userName" value="' + username + '" >';
    // create input field for password
    var pw_input = '<input type="text" id="edit_password" value="" class="input-small">';
    // create input field for display Name
    var dn_input = '<input class="input-small" type="text" id="edit_displayName" value="' + DisplayName + '" >';
    // create Select for department
    // get departments
    var dep_input = '<select class="input-small" id="edit_department">';
    var time = (new Date).getTime();
    var url = '';
    url += 'action=getDepartmentsJSON&noCahe=' + time;
    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            data = JSON.parse(data);
            $.each(data, function(i, item) {
                selected = (data[i].departmentId == currentDepartment) ? "selected" : "";
                dep_input += '<option value="' + data[i].departmentId + '" ' + selected + '>' + data[i].departmentName + '</option>';
            });
            dep_input += '</select>';
            $('#row_' + userId).find(".users_department").empty().html(dep_input);
        }
    });
    // create select for permission level
    // get permission levels
    var perm_input = '<select class="input-small" id="edit_permissions">';
    url = '';
    url += 'action=getPermissionsJSON&noCahe=' + time;
    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            data = JSON.parse(data);
            $.each(data, function(i, item) {
                selected = (data[i].id == currentPermissions) ? "selected" : "";
                perm_input += '<option value="' + data[i].id + '" ' + selected + '>' + data[i].permissionTitle + '</option>';
            });
            perm_input += '</select>';
            $('#row_' + userId).find(".users_permissions").empty().html(perm_input);
        }
    });

    $('#row_' + userId).find(".users_save_changes").show();
    $('#row_' + userId).find(".users_delete_user").hide();
    $('#row_' + userId).find(".users_edit_user").hide();

    $('#row_' + userId).find(".users_username").empty().html(un_input);
    $('#row_' + userId).find(".users_displayname").empty().html(dn_input);
    $('#row_' + userId).find(".users_password").empty().html(pw_input);

    $('#row_' + userId).find(".users_save_changes").click(function() {
        updateUser(userId);
    });

}

function iAmTyping(amI) {
    if (preventUpdateTyping == false) {
        if (amI) {
            clientTyping = true;
            // clear false update, and reset
            window.clearTimeout(updateTypingStatus);
            updateTypingStatus = window.setTimeout(function() {
                iAmTyping(false);
            }, 1000);
        }
        else {
            clientTyping = false;
        }
        var convoId = getConvoId();
        var time = (new Date).getTime();
        var url = '';
        url += 'action=isTyping&convoId=' + convoId + '&isTyping=' + clientTyping + '&noCahe=' + time;
        $.ajax({
            url: 'includes/handler.php?' + url
        });
    }
    preventUpdateTyping = false;
}

function areYouTyping() {
    var convoId = getConvoId();
    var time = (new Date).getTime();
    var url = '';
    url += 'action=areYouTyping&convoId=' + convoId + '&noCahe=' + time;
    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            if (data == "0") {
                $('#typingStatus').fadeOut(400);
                $('#typingStatusMessage').text('');
            }
            else {
                $('#typingStatus').fadeIn(400);
                $('#typingStatusMessage').text(data);
            }
            window.clearTimeout(areYouTypingStatus);
            areYouTypingStatus = window.setTimeout(function() {
                areYouTyping();
            }, 1000);
        }
    });
}

function getOnlineOffline() {
    var time = (new Date).getTime();
    var url = '';
    url += 'action=getOnlineOffline&noCahe=' + time;
    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            if (data == "0") {
                $('#chatOnlineOfflineSettingsButton').removeClass('btn-success');
                $('#chatOnlineOfflineSettingsButton').addClass('btn-danger');

                $('#chatAvailableDiv').show();
                $('#chatUnAvailableDiv').hide();
            }
            else {
                $('#chatOnlineOfflineSettingsButton').removeClass('btn-danger');
                $('#chatOnlineOfflineSettingsButton').addClass('btn-success');

                $('#chatAvailableDiv').hide();
                $('#chatUnAvailableDiv').show();
            }
            window.clearTimeout(getOnlineOfflineTimer);
            getOnlineOfflineTimer = window.setTimeout(function() {
                getOnlineOffline();
            }, 10000);
        }
    });
}

function setOnlineOffline(onlineOffline) {
    var time = (new Date).getTime();
    var url = '';
    url += 'action=setOnlineOffline&noCahe=' + time + '&onlineOffline=' + onlineOffline;
    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            if (data == "1") {
                getOnlineOffline();
            }
        }
    });
}

function getAudioAlertsOnOff() {
    var time = (new Date).getTime();
    var url = '';
    url += 'action=getAudioAlertsOnOff&noCahe=' + time;
    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            if (data == "0") {
                audioAlertsEnabled = false;
                $('#audioOnDisplayIcon').hide();
                $('#audioOnDiv').show();
                $('#audioOffDiv').hide();
            }
            else {
                audioAlertsEnabled = true;
                $('#audioOnDisplayIcon').show();
                $('#audioOnDiv').hide();
                $('#audioOffDiv').show();
            }
        }
    });
}

function setAudioAlertsOnOff(audioOnOff) {

    var time = (new Date).getTime();
    var url = '';
    url += 'action=setAudioAlertsOnOff&noCahe=' + time + '&audioOnOff=' + audioOnOff;
    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            if (data == "1") {
                getAudioAlertsOnOff();
            }
        }
    });
}

function keepAlive() {
    var time = (new Date).getTime();
    var url = '';
    url += 'action=keepAlive&noCahe=' + time + '&userId=' + operatorId;
    $.ajax({ url: 'includes/handler.php?' + url});
    window.clearTimeout(keepAliveTimer);
    keepAliveTimer = window.setTimeout(function() {
        keepAlive();
    }, 120000);
}

function cancelTransfer(transferId) {
    var time = (new Date).getTime();
    var url = '';
    url += 'action=cancelTransfer&noCahe=' + time + '&transferId=' + transferId;
    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            getChatTransferQueue();
        }
    });

}

function rejectTransfer(transferId) {
    var time = (new Date).getTime();
    var url = '';
    url += 'action=rejectTransfer&noCahe=' + time + '&transferId=' + transferId;
    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            getChatTransferQueue();
        }
    });

}

function acceptTransfer(transferId) {
    var time = (new Date).getTime();
    var url = '';
    url += 'action=acceptTransfer&noCahe=' + time + '&transferId=' + transferId;
    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {
            getChatTransferQueue();
            getConversation(data, 1)
        }
    });
}

function sendStandardResponse(respValue, username) {
    $('#operatorResponse').val('').val(respValue);
    storeResponse(username);
    $('#userStandardResponses').get(0).selectedIndex = 0;
}

function sendFileResponse(fileId, userName) {
    var user = userName;
    var time = (new Date).getTime();
    var convoId = $('#convoId').val();
    var url = '';
    url += 'action=sendFileAttachment&user=' + user + '&convoId=' + convoId + '&fileId=' + fileId + '&noCahe=' + time;

    $.ajax({
        url: 'includes/handler.php?' + encodeURI(url),
        success: function(data) {
            $('#operatorResponse').val('');
            getConversation();
            return true;
        }
    });

    $('#userFiles').get(0).selectedIndex = 0;
}

function updateFileVisibility(tVal) {
    var visSet = ($('#fileVisBox_' + tVal).is(':checked')) ? true : false;
    var time = (new Date).getTime();
    var url = '';
    url += 'action=updateFileVisibility&noCahe=' + time + '&fileId=' + tVal + '&active=' + visSet;
    $.ajax({
        url: 'includes/handler.php?' + url,
        success: function(data) {

        }
    });
}

function getGraphTotalConvos() {
    var time = (new Date).getTime();
    var url = '';
    url += 'action=getGraphTotalConvos&noCahe=' + time;
    $.ajax({
        async: false,
        url: 'includes/handler.php?' + url,
        success: function(data) {
            alert(data);
            return data;
        }
    });
}
