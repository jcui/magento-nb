<?php
/*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 28/12/2012
 * Time: 10:46
 */
session_start();
$loginError = false;
require 'includes/class/database.php';
require 'includes/class/login.php';
require 'includes/class/displayText.php';
$login = new login();
if (isset($_POST['loginSubmit'])) {
    if (!$login->handleLogin()) {
	   $loginError = true;
    }
    else
    {
	   header('location: index.php');
    }
}
$text = new displayText();

?>

<!DOCTYPE html>
<html>
<head>
    <title>Live Support Chat - Login</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/override.css" rel="stylesheet" media="screen">
</head>
<body>


<div class="container">
    <form class="form-signin" method="post" action="">
	   <?php if (isset($loginError) && $loginError) { ?>
	   <div class="alert alert-error">
		  <strong><?php echo $text->getText('Error'); ?><br/></strong><?php echo $text->getText('loginError'); ?>
	   </div>
	   <?php } ?>
	   <h2 class="form-signin-heading">Please sign in</h2>
	   <input type="text" class="input-block-level" placeholder="Email address" name="username">
	   <input type="password" class="input-block-level" placeholder="Password" name="password">
	   <button class="btn btn-large btn-primary" type="submit" name="loginSubmit">Sign in</button>
    </form>

</div>
</body>
</html>