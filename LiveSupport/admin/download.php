<?php
if (isset($_GET['fileId'])) {
    require 'includes/class/database.php';
    $db = new database();
    $db->connect();
    $db->select('*', 'files', "id = {$_GET['fileId']}");
    $res = $db->getResult();
    $fileURL = $res['url'];


    $file = 'uploads/' . $fileURL;

    if (file_exists($file)) {
	   header('Content-Description: File Transfer');
	   header('Content-Type: application/octet-stream');
	   header('Content-Disposition: attachment; filename=' . basename($file));
	   header('Content-Transfer-Encoding: binary');
	   header('Expires: 0');
	   header('Cache-Control: must-revalidate');
	   header('Pragma: public');
	   header('Content-Length: ' . filesize($file));
	   ob_clean();
	   flush();
	   readfile($file);
	   exit;
    }
}