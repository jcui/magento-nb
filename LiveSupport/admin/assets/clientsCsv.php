<?php
 /*
 * Created by Ryan Davies, All rights reserved
 * http://www.fireboxdesign.co.uk
 * Date: 09/02/2013
 * Time: 18:58
 */
header('Content-Type: application/csv');
header('Content-Disposition: attachment; filename=clients.csv');
header('Pragma: no-cache');
require '../includes/class/database.php';
require '../includes/class/admin.php';
require '../includes/class/displayText.php';
$admin = new admin();
echo $admin->getClientCSV();
?>