var SocialSuite = Class.create();
SocialSuite.prototype = {
    initialize: function(config) {
        this.useGoogle = parseInt(config.google);
        this.useFacebook = parseInt(config.facebook);
        this.googleFire = parseInt(config.googleFire);
        this.facebookFire = parseInt(config.facebookFire);
        this.googleImage = config.googleImage;
        this.facebookImage = config.facebookImage;
        this.googleUrl = config.googleUrl;
        this.facebookUrl = config.facebookUrl;
        this.text = config.loginText;

        this.prepareMarkup();
    },

    prepareMarkup: function() {
        var facebookButton = '';
        var googleButton = '';
        if (this.useGoogle || this.useFacebook) {
            if (this.useFacebook && this.facebookFire) {
                facebookButton = '<a class="socialsuite-firecheckout-auth" href="' + this.facebookUrl +'"><img src="' + this.facebookImage +'" alt=""/></a>';
            }
            if (this.useGoogle && this.googleFire) {
                googleButton = '<a class="socialsuite-firecheckout-auth" href="' + this.googleUrl +'"><img src="' + this.googleImage +'" alt=""/></a>';
            }

            if (facebookButton !== '' || googleButton !== '') {
                var wrapper = $$('p.firecheckout-login-wrapper')[0];
                var wrapperHtml = wrapper.innerHTML;
                wrapper.update(this.text + facebookButton + googleButton + wrapperHtml);
            }
        }

    }
};