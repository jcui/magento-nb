/*


   Magic Scroll v1.0.12 DEMO
   Copyright 2011 Magic Toolbox
   Buy a license: www.magictoolbox.com/magicscroll/
   License agreement: http://www.magictoolbox.com/license/


*/
(function() {
    if (window.magicJS) {
        return
    }
    var a = {
        version: "v2.5.1",
        UUID: 0,
        storage: {},
        $uuid: function(c) {
            return (c.$J_UUID || (c.$J_UUID = ++$J.UUID))
        },
        getStorage: function(c) {
            return ($J.storage[c] || ($J.storage[c] = {}))
        },
        $F: function() {},
        $false: function() {
            return false
        },
        defined: function(c) {
            return (undefined != c)
        },
        exists: function(c) {
            return !! (c)
        },
        j1: function(c) {
            if (!$J.defined(c)) {
                return false
            }
            if (c.$J_TYPE) {
                return c.$J_TYPE
            }
            if ( !! c.nodeType) {
                if (1 == c.nodeType) {
                    return "element"
                }
                if (3 == c.nodeType) {
                    return "textnode"
                }
            }
            if (c.length && c.item) {
                return "collection"
            }
            if (c.length && c.callee) {
                return "arguments"
            }
            if ((c instanceof window.Object || c instanceof window.Function) && c.constructor === $J.Class) {
                return "class"
            }
            if (c instanceof window.Array) {
                return "array"
            }
            if (c instanceof window.Function) {
                return "function"
            }
            if (c instanceof window.String) {
                return "string"
            }
            if ($J.j21.trident) {
                if ($J.defined(c.cancelBubble)) {
                    return "event"
                }
            } else {
                if (c === window.event || c.constructor == window.Event || c.constructor == window.MouseEvent || c.constructor == window.UIEvent || c.constructor == window.KeyboardEvent || c.constructor == window.KeyEvent) {
                    return "event"
                }
            }
            if (c instanceof window.Date) {
                return "date"
            }
            if (c instanceof window.RegExp) {
                return "regexp"
            }
            if (c === window) {
                return "window"
            }
            if (c === document) {
                return "document"
            }
            return typeof(c)
        },
        extend: function(j, h) {
            if (! (j instanceof window.Array)) {
                j = [j]
            }
            for (var g = 0, d = j.length; g < d; g++) {
                if (!$J.defined(j)) {
                    continue
                }
                for (var e in (h || {})) {
                    try {
                        j[g][e] = h[e]
                    } catch(c) {}
                }
            }
            return j[0]
        },
        implement: function(h, g) {
            if (! (h instanceof window.Array)) {
                h = [h]
            }
            for (var e = 0, c = h.length; e < c; e++) {
                if (!$J.defined(h[e])) {
                    continue
                }
                if (!h[e].prototype) {
                    continue
                }
                for (var d in (g || {})) {
                    if (!h[e].prototype[d]) {
                        h[e].prototype[d] = g[d]
                    }
                }
            }
            return h[0]
        },
        nativize: function(e, d) {
            if (!$J.defined(e)) {
                return e
            }
            for (var c in (d || {})) {
                if (!e[c]) {
                    e[c] = d[c]
                }
            }
            return e
        },
        $try: function() {
            for (var d = 0, c = arguments.length; d < c; d++) {
                try {
                    return arguments[d]()
                } catch(g) {}
            }
            return null
        },
        $A: function(e) {
            if (!$J.defined(e)) {
                return $mjs([])
            }
            if (e.toArray) {
                return $mjs(e.toArray())
            }
            if (e.item) {
                var d = e.length || 0,
                c = new Array(d);
                while (d--) {
                    c[d] = e[d]
                }
                return $mjs(c)
            }
            return $mjs(Array.prototype.slice.call(e))
        },
        now: function() {
            return new Date().getTime()
        },
        detach: function(h) {
            var e;
            switch ($J.j1(h)) {
            case "object":
                e = {};
                for (var g in h) {
                    e[g] = $J.detach(h[g])
                }
                break;
            case "array":
                e = [];
                for (var d = 0, c = h.length; d < c; d++) {
                    e[d] = $J.detach(h[d])
                }
                break;
            default:
                return h
            }
            return $J.$(e)
        },
        $: function(d) {
            if (!$J.defined(d)) {
                return null
            }
            if (d.$J_EXTENDED) {
                return d
            }
            switch ($J.j1(d)) {
            case "array":
                d = $J.nativize(d, $J.extend($J.Array, {
                    $J_EXTENDED: $J.$F
                }));
                d.j14 = d.forEach;
                return d;
                break;
            case "string":
                var c = document.getElementById(d);
                if ($J.defined(c)) {
                    return $J.$(c)
                }
                return null;
                break;
            case "window":
            case "document":
                $J.$uuid(d);
                d = $J.extend(d, $J.Doc);
                break;
            case "element":
                $J.$uuid(d);
                d = $J.extend(d, $J.Element);
                break;
            case "event":
                d = $J.extend(d, $J.Event);
                break;
            case "textnode":
                return d;
                break;
            case "function":
            case "array":
            case "date":
            default:
                break
            }
            return $J.extend(d, {
                $J_EXTENDED: $J.$F
            })
        },
        $new: function(c, e, d) {
            return $mjs($J.doc.createElement(c)).setProps(e || {}).j6(d || {})
        }
    };
    window.magicJS = window.$J = a;
    window.$mjs = a.$;
    $J.Array = {
        $J_TYPE: "array",
        indexOf: function(g, h) {
            var c = this.length;
            for (var d = this.length, e = (h < 0) ? Math.max(0, d + h) : h || 0; e < d; e++) {
                if (this[e] === g) {
                    return e
                }
            }
            return - 1
        },
        contains: function(c, d) {
            return this.indexOf(c, d) != -1
        },
        forEach: function(c, g) {
            for (var e = 0, d = this.length; e < d; e++) {
                if (e in this) {
                    c.call(g, this[e], e, this)
                }
            }
        },
        filter: function(c, j) {
            var h = [];
            for (var g = 0, d = this.length; g < d; g++) {
                if (g in this) {
                    var e = this[g];
                    if (c.call(j, this[g], g, this)) {
                        h.push(e)
                    }
                }
            }
            return h
        },
        map: function(c, h) {
            var g = [];
            for (var e = 0, d = this.length; e < d; e++) {
                if (e in this) {
                    g[e] = c.call(h, this[e], e, this)
                }
            }
            return g
        }
    };
    $J.implement(String, {
        $J_TYPE: "string",
        j26: function() {
            return this.replace(/^\s+|\s+$/g, "")
        },
        trimLeft: function() {
            return this.replace(/^\s+/g, "")
        },
        trimRight: function() {
            return this.replace(/\s+$/g, "")
        },
        j25: function(c) {
            return (this.toString() === c.toString())
        },
        icompare: function(c) {
            return (this.toLowerCase().toString() === c.toLowerCase().toString())
        },
        j22: function() {
            return this.replace(/-\D/g,
            function(c) {
                return c.charAt(1).toUpperCase()
            })
        },
        dashize: function() {
            return this.replace(/[A-Z]/g,
            function(c) {
                return ("-" + c.charAt(0).toLowerCase())
            })
        },
        j17: function(c) {
            return parseInt(this, c || 10)
        },
        toFloat: function() {
            return parseFloat(this)
        },
        j18: function() {
            return ! this.replace(/true/i, "").j26()
        },
        has: function(d, c) {
            c = c || "";
            return (c + this + c).indexOf(c + d + c) > -1
        }
    });
    a.implement(Function, {
        $J_TYPE: "function",
        j24: function() {
            var d = $J.$A(arguments),
            c = this,
            e = d.shift();
            return function() {
                return c.apply(e || null, d.concat($J.$A(arguments)))
            }
        },
        j16: function() {
            var d = $J.$A(arguments),
            c = this,
            e = d.shift();
            return function(g) {
                return c.apply(e || null, $mjs([g || window.event]).concat(d))
            }
        },
        j27: function() {
            var d = $J.$A(arguments),
            c = this,
            e = d.shift();
            return window.setTimeout(function() {
                return c.apply(c, d)
            },
            e || 0)
        },
        j28: function() {
            var d = $J.$A(arguments),
            c = this;
            return function() {
                return c.j27.apply(c, d)
            }
        },
        interval: function() {
            var d = $J.$A(arguments),
            c = this,
            e = d.shift();
            return window.setInterval(function() {
                return c.apply(c, d)
            },
            e || 0)
        }
    });
    var b = navigator.userAgent.toLowerCase();
    $J.j21 = {
        features: {
            xpath: !!(document.evaluate),
            air: !!(window.runtime),
            query: !!(document.querySelector)
        },
        engine: (window.opera) ? "presto": !!(window.ActiveXObject) ? "trident": (!navigator.taintEnabled) ? "webkit": (undefined != document.getBoxObjectFor || null != window.mozInnerScreenY) ? "gecko": "unknown",
        version: "",
        platform: b.match(/ip(?:ad|od|hone)/) ? "ios": (b.match(/(?:webos|android)/) || navigator.platform.match(/mac|win|linux/i) || ["other"])[0].toLowerCase(),
        backCompat: document.compatMode && "backcompat" == document.compatMode.toLowerCase(),
        getDoc: function() {
            return (document.compatMode && "backcompat" == document.compatMode.toLowerCase()) ? document.body: document.documentElement
        },
        ready: false,
        onready: function() {
            if ($J.j21.ready) {
                return
            }
            $J.j21.ready = true;
            $J.body = $mjs(document.body);
            $J.win = $mjs(window);
            $mjs(document).raiseEvent("domready")
        }
    };
    $J.j21.touchScreen = $J.$A(["ios", "webos", "android"]).contains($J.j21.platform);
    (function() {
        function c() {
            return !! (arguments.callee.caller)
        }
        $J.j21.version = ("presto" == $J.j21.engine) ? !!(document.head) ? 270 : !!(window.applicationCache) ? 260 : !!(window.localStorage) ? 250 : ($J.j21.features.query) ? 220 : ((c()) ? 211 : ((document.getElementsByClassName) ? 210 : 200)) : ("trident" == $J.j21.engine) ? !!(window.msPerformance || window.performance) ? 900 : !!(window.XMLHttpRequest && window.postMessage) ? 6 : ((window.XMLHttpRequest) ? 5 : 4) : ("webkit" == $J.j21.engine) ? (($J.j21.features.xpath) ? (($J.j21.features.query) ? 525 : 420) : 419) : ("gecko" == $J.j21.engine) ? !!(document.head) ? 200 : !!document.readyState ? 192 : !!(window.localStorage) ? 191 : ((document.getElementsByClassName) ? 190 : 181) : "";
        $J.j21[$J.j21.engine] = $J.j21[$J.j21.engine + $J.j21.version] = true;
        if (window.chrome) {
            $J.j21.chrome = true
        }
    })();
    $J.Element = {
        j13: function(c) {
            return this.className.has(c, " ")
        },
        j2: function(c) {
            if (c && !this.j13(c)) {
                this.className += (this.className ? " ": "") + c
            }
            return this
        },
        j3: function(c) {
            c = c || ".*";
            this.className = this.className.replace(new RegExp("(^|\\s)" + c + "(?:\\s|$)"), "$1").j26();
            return this
        },
        j4: function(c) {
            return this.j13(c) ? this.j3(c) : this.j2(c)
        },
        j5: function(e) {
            e = (e == "float" && this.currentStyle) ? "styleFloat": e.j22();
            var c = null,
            d = null;
            if (this.currentStyle) {
                c = this.currentStyle[e]
            } else {
                if (document.defaultView && document.defaultView.getComputedStyle) {
                    d = document.defaultView.getComputedStyle(this, null);
                    c = d ? d.getPropertyValue([e.dashize()]) : null
                }
            }
            if (!c) {
                c = this.style[e]
            }
            if ("opacity" == e) {
                return $J.defined(c) ? parseFloat(c) : 1
            }
            if (/^(border(Top|Bottom|Left|Right)Width)|((padding|margin)(Top|Bottom|Left|Right))$/.test(e)) {
                c = parseInt(c) ? c: "0px"
            }
            return ("auto" == c ? null: c)
        },
        j6Prop: function(d, c) {
            try {
                if ("opacity" == d) {
                    this.j23(c);
                    return this
                }
                if ("float" == d) {
                    this.style[("undefined" === typeof(this.style.styleFloat)) ? "cssFloat": "styleFloat"] = c;
                    return this
                }
                this.style[d.j22()] = c + (("number" == $J.j1(c) && !$mjs(["zIndex", "zoom"]).contains(d.j22())) ? "px": "")
            } catch(g) {}
            return this
        },
        j6: function(d) {
            for (var c in d) {
                this.j6Prop(c, d[c])
            }
            return this
        },
        j19s: function() {
            var c = {};
            $J.$A(arguments).j14(function(d) {
                c[d] = this.j5(d)
            },
            this);
            return c
        },
        j23: function(g, d) {
            d = d || false;
            g = parseFloat(g);
            if (d) {
                if (g == 0) {
                    if ("hidden" != this.style.visibility) {
                        this.style.visibility = "hidden"
                    }
                } else {
                    if ("visible" != this.style.visibility) {
                        this.style.visibility = "visible"
                    }
                }
            }
            if ($J.j21.trident) {
                if (!this.currentStyle || !this.currentStyle.hasLayout) {
                    this.style.zoom = 1
                }
                try {
                    var e = this.filters.item("DXImageTransform.Microsoft.Alpha");
                    e.enabled = (1 != g);
                    e.opacity = g * 100
                } catch(c) {
                    this.style.filter += (1 == g) ? "": "progid:DXImageTransform.Microsoft.Alpha(enabled=true,opacity=" + g * 100 + ")"
                }
            }
            this.style.opacity = g;
            return this
        },
        setProps: function(c) {
            for (var d in c) {
                this.setAttribute(d, "" + c[d])
            }
            return this
        },
        hide: function() {
            return this.j6({
                display: "none",
                visibility: "hidden"
            })
        },
        show: function() {
            return this.j6({
                display: "block",
                visibility: "visible"
            })
        },
        j7: function() {
            return {
                width: this.offsetWidth,
                height: this.offsetHeight
            }
        },
        j10: function() {
            return {
                top: this.scrollTop,
                left: this.scrollLeft
            }
        },
        j11: function() {
            var c = this,
            d = {
                top: 0,
                left: 0
            };
            do {
                d.left += c.scrollLeft || 0;
                d.top += c.scrollTop || 0;
                c = c.parentNode
            } while ( c );
            return d
        },
        j8: function() {
            if ($J.defined(document.documentElement.getBoundingClientRect)) {
                var c = this.getBoundingClientRect(),
                e = $mjs(document).j10(),
                h = $J.j21.getDoc();
                return {
                    top: c.top + e.y - h.clientTop,
                    left: c.left + e.x - h.clientLeft
                }
            }
            var g = this,
            d = t = 0;
            do {
                d += g.offsetLeft || 0;
                t += g.offsetTop || 0;
                g = g.offsetParent
            } while ( g && !(/^(?:body|html)$/i).test(g.tagName));
            return {
                top: t,
                left: d
            }
        },
        j9: function() {
            var d = this.j8();
            var c = this.j7();
            return {
                top: d.top,
                bottom: d.top + c.height,
                left: d.left,
                right: d.left + c.width
            }
        },
        update: function(g) {
            try {
                this.innerHTML = g
            } catch(d) {
                this.innerText = g
            }
            return this
        },
        j33: function() {
            return (this.parentNode) ? this.parentNode.removeChild(this) : this
        },
        kill: function() {
            $J.$A(this.childNodes).j14(function(c) {
                if (3 == c.nodeType || 8 == c.nodeType) {
                    return
                }
                $mjs(c).kill()
            });
            this.j33();
            this.je3();
            if (this.$J_UUID) {
                $J.storage[this.$J_UUID] = null;
                delete $J.storage[this.$J_UUID]
            }
            return null
        },
        append: function(e, d) {
            d = d || "bottom";
            var c = this.firstChild;
            ("top" == d && c) ? this.insertBefore(e, c) : this.appendChild(e);
            return this
        },
        j32: function(e, d) {
            var c = $mjs(e).append(this, d);
            return this
        },
        enclose: function(c) {
            this.append(c.parentNode.replaceChild(this, c));
            return this
        },
        hasChild: function(c) {
            if (! (c = $mjs(c))) {
                return false
            }
            return (this == c) ? false: (this.contains && !($J.j21.webkit419)) ? (this.contains(c)) : (this.compareDocumentPosition) ? !!(this.compareDocumentPosition(c) & 16) : $J.$A(this.byTag(c.tagName)).contains(c)
        }
    };
    $J.Element.j19 = $J.Element.j5;
    $J.Element.j20 = $J.Element.j6;
    if (!window.Element) {
        window.Element = $J.$F;
        if ($J.j21.engine.webkit) {
            window.document.createElement("iframe")
        }
        window.Element.prototype = ($J.j21.engine.webkit) ? window["[[DOMElement.prototype]]"] : {}
    }
    $J.implement(window.Element, {
        $J_TYPE: "element"
    });
    $J.Doc = {
        j7: function() {
            if ($J.j21.presto925 || $J.j21.webkit419) {
                return {
                    width: self.innerWidth,
                    height: self.innerHeight
                }
            }
            return {
                width: $J.j21.getDoc().clientWidth,
                height: $J.j21.getDoc().clientHeight
            }
        },
        j10: function() {
            return {
                x: self.pageXOffset || $J.j21.getDoc().scrollLeft,
                y: self.pageYOffset || $J.j21.getDoc().scrollTop
            }
        },
        j12: function() {
            var c = this.j7();
            return {
                width: Math.max($J.j21.getDoc().scrollWidth, c.width),
                height: Math.max($J.j21.getDoc().scrollHeight, c.height)
            }
        }
    };
    $J.extend(document, {
        $J_TYPE: "document"
    });
    $J.extend(window, {
        $J_TYPE: "window"
    });
    $J.extend([$J.Element, $J.Doc], {
        j29: function(g, d) {
            var c = $J.getStorage(this.$J_UUID),
            e = c[g];
            if (undefined != d && undefined == e) {
                e = c[g] = d
            }
            return ($J.defined(e) ? e: null)
        },
        j30: function(e, d) {
            var c = $J.getStorage(this.$J_UUID);
            c[e] = d;
            return this
        },
        j31: function(d) {
            var c = $J.getStorage(this.$J_UUID);
            delete c[d];
            return this
        }
    });
    if (! (window.HTMLElement && window.HTMLElement.prototype && window.HTMLElement.prototype.getElementsByClassName)) {
        $J.extend([$J.Element, $J.Doc], {
            getElementsByClassName: function(c) {
                return $J.$A(this.getElementsByTagName("*")).filter(function(g) {
                    try {
                        return (1 == g.nodeType && g.className.has(c, " "))
                    } catch(d) {}
                })
            }
        })
    }
    $J.extend([$J.Element, $J.Doc], {
        byClass: function() {
            return this.getElementsByClassName(arguments[0])
        },
        byTag: function() {
            return this.getElementsByTagName(arguments[0])
        }
    });
    $J.Event = {
        $J_TYPE: "event",
        stop: function() {
            if (this.stopPropagation) {
                this.stopPropagation()
            } else {
                this.cancelBubble = true
            }
            if (this.preventDefault) {
                this.preventDefault()
            } else {
                this.returnValue = false
            }
            return this
        },
        j15: function() {
            var d, c;
            d = ((/touch/i).test(this.type)) ? this.changedTouches[0] : this;
            return (!$J.defined(d)) ? {
                x: 0,
                y: 0
            }: {
                x: d.pageX || d.clientX + $J.j21.getDoc().scrollLeft,
                y: d.pageY || d.clientY + $J.j21.getDoc().scrollTop
            }
        },
        getTarget: function() {
            var c = this.target || this.srcElement;
            while (c && 3 == c.nodeType) {
                c = c.parentNode
            }
            return c
        },
        getRelated: function() {
            var d = null;
            switch (this.type) {
            case "mouseover":
                d = this.relatedTarget || this.fromElement;
                break;
            case "mouseout":
                d = this.relatedTarget || this.toElement;
                break;
            default:
                return d
            }
            try {
                while (d && 3 == d.nodeType) {
                    d = d.parentNode
                }
            } catch(c) {
                d = null
            }
            return d
        },
        getButton: function() {
            if (!this.which && this.button !== undefined) {
                return (this.button & 1 ? 1 : (this.button & 2 ? 3 : (this.button & 4 ? 2 : 0)))
            }
            return this.which
        }
    };
    $J._event_add_ = "addEventListener";
    $J._event_del_ = "removeEventListener";
    $J._event_prefix_ = "";
    if (!document.addEventListener) {
        $J._event_add_ = "attachEvent";
        $J._event_del_ = "detachEvent";
        $J._event_prefix_ = "on"
    }
    $J.extend([$J.Element, $J.Doc], {
        je1: function(g, e) {
            var j = ("domready" == g) ? false: true,
            d = this.j29("events", {});
            d[g] = d[g] || {};
            if (d[g].hasOwnProperty(e.$J_EUID)) {
                return this
            }
            if (!e.$J_EUID) {
                e.$J_EUID = Math.floor(Math.random() * $J.now())
            }
            var c = this,
            h = function(l) {
                return e.call(c)
            };
            if ("domready" == g) {
                if ($J.j21.ready) {
                    e.call(this);
                    return this
                }
            }
            if (j) {
                h = function(l) {
                    l = $J.extend(l || window.e, {
                        $J_TYPE: "event"
                    });
                    return e.call(c, $mjs(l))
                };
                this[$J._event_add_]($J._event_prefix_ + g, h, false)
            }
            d[g][e.$J_EUID] = h;
            return this
        },
        je2: function(g) {
            var j = ("domready" == g) ? false: true,
            d = this.j29("events");
            if (!d || !d[g]) {
                return this
            }
            var h = d[g],
            e = arguments[1] || null;
            if (g && !e) {
                for (var c in h) {
                    if (!h.hasOwnProperty(c)) {
                        continue
                    }
                    this.je2(g, c)
                }
                return this
            }
            e = ("function" == $J.j1(e)) ? e.$J_EUID: e;
            if (!h.hasOwnProperty(e)) {
                return this
            }
            if ("domready" == g) {
                j = false
            }
            if (j) {
                this[$J._event_del_]($J._event_prefix_ + g, h[e], false)
            }
            delete h[e];
            return this
        },
        raiseEvent: function(h, d) {
            var n = ("domready" == h) ? false: true,
            m = this,
            l;
            if (!n) {
                var g = this.j29("events");
                if (!g || !g[h]) {
                    return this
                }
                var j = g[h];
                for (var c in j) {
                    if (!j.hasOwnProperty(c)) {
                        continue
                    }
                    j[c].call(this)
                }
                return this
            }
            if (m === document && document.createEvent && !el.dispatchEvent) {
                m = document.documentElement
            }
            if (document.createEvent) {
                l = document.createEvent(h);
                l.initEvent(d, true, true)
            } else {
                l = document.createEventObject();
                l.eventType = h
            }
            if (document.createEvent) {
                m.dispatchEvent(l)
            } else {
                m.fireEvent("on" + d, l)
            }
            return l
        },
        je3: function() {
            var c = this.j29("events");
            if (!c) {
                return this
            }
            for (var d in c) {
                this.je2(d)
            }
            this.j31("events");
            return this
        }
    });
    (function() {
        if ($J.j21.webkit && $J.j21.version < 420) { (function() { ($mjs(["loaded", "complete"]).contains(document.readyState)) ? $J.j21.onready() : arguments.callee.j27(50)
            })()
        } else {
            if ($J.j21.trident && window == top) { (function() { ($J.$try(function() {
                        $J.j21.getDoc().doScroll("left");
                        return true
                    })) ? $J.j21.onready() : arguments.callee.j27(50)
                })()
            } else {
                $mjs(document).je1("DOMContentLoaded", $J.j21.onready);
                $mjs(window).je1("load", $J.j21.onready)
            }
        }
    })();
    $J.Class = function() {
        var h = null,
        d = $J.$A(arguments);
        if ("class" == $J.j1(d[0])) {
            h = d.shift()
        }
        var c = function() {
            for (var n in this) {
                this[n] = $J.detach(this[n])
            }
            if (this.constructor.$parent) {
                this.$parent = {};
                var q = this.constructor.$parent;
                for (var o in q) {
                    var l = q[o];
                    switch ($J.j1(l)) {
                    case "function":
                        this.$parent[o] = $J.Class.wrap(this, l);
                        break;
                    case "object":
                        this.$parent[o] = $J.detach(l);
                        break;
                    case "array":
                        this.$parent[o] = $J.detach(l);
                        break
                    }
                }
            }
            var j = (this.init) ? this.init.apply(this, arguments) : this;
            delete this.caller;
            return j
        };
        if (!c.prototype.init) {
            c.prototype.init = $J.$F
        }
        if (h) {
            var g = function() {};
            g.prototype = h.prototype;
            c.prototype = new g;
            c.$parent = {};
            for (var e in h.prototype) {
                c.$parent[e] = h.prototype[e]
            }
        } else {
            c.$parent = null
        }
        c.constructor = $J.Class;
        c.prototype.constructor = c;
        $J.extend(c.prototype, d[0]);
        $J.extend(c, {
            $J_TYPE: "class"
        });
        return c
    };
    a.Class.wrap = function(c, d) {
        return function() {
            var g = this.caller;
            var e = d.apply(c, arguments);
            return e
        }
    };
    $J.win = $mjs(window);
    $J.doc = $mjs(document)
})();
(function(a) {
    if (!a) {
        throw "MagicJS not found";
        return
    }
    if (a.FX) {
        return
    }
    var b = a.$;
    a.FX = new a.Class({
        options: {
            fps: 50,
            duration: 500,
            transition: function(c) {
                return - (Math.cos(Math.PI * c) - 1) / 2
            },
            onStart: a.$F,
            onComplete: a.$F,
            onBeforeRender: a.$F,
            roundCss: true
        },
        styles: null,
        init: function(d, c) {
            this.el = $mjs(d);
            this.options = a.extend(this.options, c);
            this.timer = false
        },
        start: function(c) {
            this.styles = c;
            this.state = 0;
            this.curFrame = 0;
            this.startTime = a.now();
            this.finishTime = this.startTime + this.options.duration;
            this.timer = this.loop.j24(this).interval(Math.round(1000 / this.options.fps));
            this.options.onStart.call();
            return this
        },
        stop: function(c) {
            c = a.defined(c) ? c: false;
            if (this.timer) {
                clearInterval(this.timer);
                this.timer = false
            }
            if (c) {
                this.render(1);
                this.options.onComplete.j27(10)
            }
            return this
        },
        calc: function(e, d, c) {
            return (d - e) * c + e
        },
        loop: function() {
            var d = a.now();
            if (d >= this.finishTime) {
                if (this.timer) {
                    clearInterval(this.timer);
                    this.timer = false
                }
                this.render(1);
                this.options.onComplete.j27(10);
                return this
            }
            var c = this.options.transition((d - this.startTime) / this.options.duration);
            this.render(c)
        },
        render: function(c) {
            var d = {};
            for (var e in this.styles) {
                if ("opacity" === e) {
                    d[e] = Math.round(this.calc(this.styles[e][0], this.styles[e][1], c) * 100) / 100
                } else {
                    d[e] = this.calc(this.styles[e][0], this.styles[e][1], c);
                    if (this.options.roundCss) {
                        d[e] = Math.round(d[e])
                    }
                }
            }
            this.options.onBeforeRender(d);
            this.set(d)
        },
        set: function(c) {
            return this.el.j6(c)
        }
    });
    a.FX.Transition = {
        linear: function(c) {
            return c
        },
        sineIn: function(c) {
            return - (Math.cos(Math.PI * c) - 1) / 2
        },
        sineOut: function(c) {
            return 1 - a.FX.Transition.sineIn(1 - c)
        },
        expoIn: function(c) {
            return Math.pow(2, 8 * (c - 1))
        },
        expoOut: function(c) {
            return 1 - a.FX.Transition.expoIn(1 - c)
        },
        quadIn: function(c) {
            return Math.pow(c, 2)
        },
        quadOut: function(c) {
            return 1 - a.FX.Transition.quadIn(1 - c)
        },
        cubicIn: function(c) {
            return Math.pow(c, 3)
        },
        cubicOut: function(c) {
            return 1 - a.FX.Transition.cubicIn(1 - c)
        },
        backIn: function(d, c) {
            c = c || 1.618;
            return Math.pow(d, 2) * ((c + 1) * d - c)
        },
        backOut: function(d, c) {
            return 1 - a.FX.Transition.backIn(1 - d)
        },
        elasticIn: function(d, c) {
            c = c || [];
            return Math.pow(2, 10 * --d) * Math.cos(20 * d * Math.PI * (c[0] || 1) / 3)
        },
        elasticOut: function(d, c) {
            return 1 - a.FX.Transition.elasticIn(1 - d, c)
        },
        bounceIn: function(e) {
            for (var d = 0, c = 1; 1; d += c, c /= 2) {
                if (e >= (7 - 4 * d) / 11) {
                    return c * c - Math.pow((11 - 6 * d - 11 * e) / 4, 2)
                }
            }
        },
        bounceOut: function(c) {
            return 1 - a.FX.Transition.bounceIn(1 - c)
        },
        none: function(c) {
            return 0
        }
    }
})(magicJS);
(function(a) {
    if (!a) {
        throw "MagicJS not found";
        return
    }
    if (!a.FX) {
        throw "MagicJS.FX not found";
        return
    }
    if (a.FX.Slide) {
        return
    }
    var b = a.$;
    a.FX.Slide = new a.Class(a.FX, {
        options: {
            mode: "vertical"
        },
        init: function(d, c) {
            this.el = $mjs(d);
            this.options = a.extend(this.$parent.options, this.options);
            this.$parent.init(d, c);
            this.wrapper = this.el.j29("slide:wrapper");
            this.wrapper = this.wrapper || a.$new("DIV").j6(a.extend(this.el.j19s("margin-top", "margin-left", "margin-right", "margin-bottom", "position", "top", "float"), {
                overflow: "hidden"
            })).enclose(this.el);
            this.el.j30("slide:wrapper", this.wrapper).j6({
                margin: 0
            })
        },
        vertical: function() {
            this.margin = "margin-top";
            this.layout = "height";
            this.offset = this.el.offsetHeight
        },
        horizontal: function(c) {
            this.margin = "margin-" + (c || "left");
            this.layout = "width";
            this.offset = this.el.offsetWidth
        },
        right: function() {
            this.horizontal()
        },
        left: function() {
            this.horizontal("right")
        },
        start: function(e, j) {
            this[j || this.options.mode]();
            var h = this.el.j5(this.margin).j17(),
            g = this.wrapper.j5(this.layout).j17(),
            c = {},
            l = {},
            d;
            c[this.margin] = [h, 0],
            c[this.layout] = [0, this.offset],
            l[this.margin] = [h, -this.offset],
            l[this.layout] = [g, 0];
            switch (e) {
            case "in":
                d = c;
                break;
            case "out":
                d = l;
                break;
            case "toggle":
                d = (0 == g) ? c: l;
                break
            }
            this.$parent.start(d);
            return this
        },
        set: function(c) {
            this.el.j6Prop(this.margin, c[this.margin]);
            this.wrapper.j6Prop(this.layout, c[this.layout]);
            return this
        },
        slideIn: function(c) {
            return this.start("in", c)
        },
        slideOut: function(c) {
            return this.start("out", c)
        },
        hide: function(d) {
            this[d || this.options.mode]();
            var c = {};
            c[this.layout] = 0,
            c[this.margin] = -this.offset;
            return this.set(c)
        },
        show: function(d) {
            this[d || this.options.mode]();
            var c = {};
            c[this.layout] = this.offset,
            c[this.margin] = 0;
            return this.set(c)
        },
        toggle: function(c) {
            return this.start("toggle", c)
        }
    })
})(magicJS);
(function(a) {
    if (!a) {
        throw "MagicJS not found";
        return
    }
    if (a.PFX) {
        return
    }
    var b = a.$;
    a.PFX = new a.Class(a.FX, {
        init: function(c, d) {
            this.el_arr = c;
            this.options = a.extend(this.options, d);
            this.timer = false
        },
        start: function(c) {
            this.$parent.start([]);
            this.styles_arr = c;
            return this
        },
        render: function(c) {
            for (var d = 0; d < this.el_arr.length; d++) {
                this.el = $mjs(this.el_arr[d]);
                this.styles = this.styles_arr[d];
                this.$parent.render(c)
            }
        }
    })
})(magicJS);
(function(a) {
    if (!a) {
        throw "MagicJS not found";
        return
    }
    if (a.Tooltip) {
        return
    }
    var b = a.$;
    a.Tooltip = function(d, e) {
        var c = this.tooltip = a.$new("div", null, {
            position: "absolute",
            "z-index": 999
        }).j2("MagicToolboxTooltip");
        a.$(d).je1("mouseover",
        function() {
            c.j32(document.body)
        });
        a.$(d).je1("mouseout",
        function() {
            c.j33()
        });
        a.$(d).je1("mousemove",
        function(m) {
            var q = 20,
            l = a.$(m).j15(),
            j = c.j7(),
            h = a.$(window).j7(),
            n = a.$(window).j10();
            function g(u, o, r) {
                return (r < (u - o) / 2) ? r: ((r > (u + o) / 2) ? (r - o) : (u - o) / 2)
            }
            c.j6({
                left: n.x + g(h.width, j.width + 2 * q, l.x - n.x) + q,
                top: n.y + g(h.height, j.height + 2 * q, l.y - n.y) + q
            })
        });
        this.text(e)
    };
    a.Tooltip.prototype.text = function(c) {
        this.tooltip.firstChild && this.tooltip.removeChild(this.tooltip.firstChild);
        this.tooltip.append(document.createTextNode(c))
    }
})(magicJS);
var MagicSwap = $J.Class({
    _options: {
        modules: [],
        effects: [],
        attributes: {},
        styles: {},
        options: {},
        alias: {},
        auto: false,
        defaults: {
            name: "MagicSwap",
            "class": "MagicSwap",
            width: 400,
            height: 400,
            sizing: "core",
            wrapper: true,
            duration: 1000,
            transition: $J.FX.Transition.sineIn,
            fps: 500,
            "item-width": "stretch",
            "item-height": "stretch",
            "item-align": "center",
            "item-valign": "middle",
            "z-index": 0,
            effect: "none",
            "-step": 1
        }
    },
    indoc: false,
    preInit: function() {
        this._options.modules = MagicSwap.mw3.getAll();
        this._options.effects = MagicSwap.mw1.getAll()
    },
    init: function(a) {
        this.preInit();
        this._options.auto = true;
        this.create(a)
    },
    create: function(a) {
        this._options = $J.extend(this._options, a || {});
        this.options(this._options.defaults, true);
        this._tmp = $J.$new("DIV", null, {
            position: "absolute",
            top: -9999,
            left: 0,
            overflow: "hidden",
            width: 1,
            height: 1
        });
        this.core = $J.$new("DIV", $J.extend({
            "class": this.option("class")
        },
        this._options.attributes), $J.extend(this._options.styles, {
            position: "relative",
            "text-align": "left"
        })).j32(this._tmp).j2(this.option("name"));
        this.items = $J.$A([]);
        $J.extend([this.items], MagicSwap.Items);
        this.position = 0;
        this.effect = new MagicSwap.mw1(this, this._options.effects);
        this.modules = new MagicSwap.mw3(this, this._options.modules)
    },
    disable: function(a) {
        this._options.modules = $mjs(this._options.modules).filter(function(b) {
            return b != this
        },
        a)
    },
    item: function(a) {
        return this.items[a]
    },
    option: function(b, c) {
        var a = $J.defined(this._options.alias[b]) ? this.option(this._options.alias[b], c) : $J.defined(this._options.options[b]) ? this._options.options[b] : c,
        d = {
            "false": false,
            "true": true
        };
        return $J.defined(d[a]) ? d[a] : a
    },
    options: function(a, b) {
        if (b) {
            $J.extend(a, this._options.options)
        }
        $J.extend(this._options.options, a);
        return this
    },
    name: function(a) {
        return (this.option("name") + "-" + a).j22()
    },
    push: function() {
        var a = $J.j1(arguments[0]),
        b = null;
        switch (a) {
        case "array":
        case "collection":
            $J.$A(arguments[0]).j14(function(c) {
                if ($J.j1(c) == "array") {
                    this.push(c[0], c[1], c[2])
                } else {
                    this.push(c)
                }
            },
            this);
            break;
        case "string":
            b = $J.$new(arguments[0], arguments[1] || {},
            arguments[2] || {});
        case "element":
            b = b || $mjs(arguments[0]);
            $J.extend([b], MagicSwap.Item);
            b.j30("MagicSwap", this);
            b.j30("index", this.items.length);
            b.reset();
            this.items.push(b);
            this.callEvent("push", {
                item: b
            });
            break;
        default:
            break
        }
        return this
    },
    append: function(a) {
        $mjs(a).appendChild(this._tmp);
        this.indoc = true;
        if (this._options.auto) {
            this.reload()
        }
        return this
    },
    replace: function(a) {
        $mjs(a).parentNode.replaceChild(this._tmp, $mjs(a));
        this.indoc = true;
        if (this._options.auto) {
            this.reload()
        }
        return this
    },
    show: function() {
        this.fixSize(this.option("width"), this.option("height"));
        this._tmp.parentNode.replaceChild(this.core, this._tmp);
        this.callEvent("after-reload");
        this.jump("first");
        return this
    },
    hide: function() {
        this._tmp.enclose(this.core);
        return this
    },
    reload: function() {
        if (!this.indoc) {
            throw "Magic Swap: ERROR: Try to create objects before append core object in the document"
        }
        this.core.show();
        if (this.option("wrapper")) {
            this.wrapper = this.core.appendChild($J.$new("DIV", {
                "class": this.name("Container")
            },
            {
                position: "absolute",
                overflow: "hidden"
            }).setSize(this.core.getBoxSize()))
        } else {
            this.wrapper = this.core
        }
        this.items.append(this.wrapper);
        if (this._options.auto) {
            this.show()
        }
        return this
    },
    fixSize: function(b, a) {
        $J.j1(b) == "object" || (b = {
            width: b,
            height: a
        });
        this.core.setBoxSize(b, null, true);
        this.option("wrapper") && this.wrapper.setSize(this.core.getBoxSize())
    },
    jump: function(a, b) {
        b = $J.extend({
            target: "forward",
            effect: this.option("effect")
        },
        $J.extend(b || {},
        $J.j1(a) ? ($J.j1(a) == "object" ? a: {
            target: a
        }) : {}));
        this.position = this.effect.jump(b);
        return this
    },
    size: function(a, c, b) {
        if ($J.j1(a) == "string") {
            a.split("px").length > 1 && (a = parseInt(a.split("px")[0]));
            a.split("%").length > 1 && (a = parseInt(Math.round(c * a.split("%")[0] / 100)))
        }
        return b ? a: parseInt(a)
    },
    sizeup: function(a, d, b) {
        var c;
        if ($J.j1(a) == "string") {
            a.split("px").length > 1 && (a = parseInt(a.split("px")[0]));
            a.split("%").length > 1 && (c = a.split("%")[0]) && (a = parseInt(Math.round(d * c / (100 - c))))
        }
        return b ? a: parseInt(a)
    },
    dispose: function(a) {
        a && this.indoc && this.core.parentNode.replaceChild(a, this.core);
        return a || null
    }
});
MagicSwap.Item = {
    fixSize: function(c) {
        var b = this.j29("MagicSwap");
        this.j29("initsize") && this.setSize(this.j29("initsize")) || this.j30("initsize", this.j7());
        var g = this.j7();
        var e = b.wrapper.getBoxSize();
        var a = c && c.width || b.option("item-width");
        var d = c && c.height || b.option("item-height");
        if (a == "stretch" && d == "stretch") {
            a = e.width;
            d = g.height * a / g.width;
            if (d > e.height) {
                d = e.height;
                a = g.width * d / g.height
            }
        } else {
            a = a == "auto" ? e.width: a == "original" ? g.width: a;
            d = d == "auto" ? e.height: d == "original" ? g.height: d;
            a = a == "stretch" ? g.width * d / g.height: a;
            d = d == "stretch" ? g.height * a / g.width: d
        }
        if ($J.defined(c) && $J.j1(c) != "object") {
            a = b.size(c || c, a, true);
            d = b.size(c || c, d, true)
        }
        a = Math.round(a);
        d = Math.round(d);
        this.setSize(a, d);
      $mjs(MagicSwap.Item).callEvent("item-resize", {
            item: this,
            width: a,
            height: d
        });
        return this
    },
    fixPosition: function(g, l) {/*zhyazh*/
        var b = this.j29("MagicSwap");
        g = g || {
            width: "block",
            height: "block"
        };
        g = $J.j1(g) == "string" ? {
            width: g,
            height: g
        }: g;
        l = l || b.wrapper.getBoxSize();
        if (g.width == "inline") {
            this.j6({
               "margin-left": "auto",
                "margin-right": "auto"
            })
        } else {
            var a = parseInt(this.j7().width);
            var d = l.width - a;
            var e = b.option("item-align");
            this.j6({
                "margin-left": e == "center" ? d / 2 : e == "left" ? 0 : d,
                "margin-right": e == "center" ? d / 2 : e == "right" ? 0 : d
            })
        }
        if (g.height == "inline") {
            this.j6({
                "margin-top": "auto",
                "margin-bottom": "auto"
            })
        } else {
            var j = parseInt(this.j7().height);
            var c = l.height - j;
            var h = b.option("item-valign");
            this.j6({
                "margin-top": h == "middle" ? c / 2 : h == "top" ? 0 : c,
                "margin-bottom": h == "middle" ? c / 2 : h == "bottom" ? 0 : c
            })
        }
    },
    copy: function() {
        var a = this.clone();
        $J.extend(a, MagicSwap.Item);
        return a
    },
    index: function() {
        return this.j29("index")
    },
    reset: function() {
        this.j6({
            visibility: "visible",
            display: "block",
            position: "relative",
            opacity: 1,
            top: "auto",
            left: "auto",
            "float": "none"
        });
        if ($J.j21.trident) {
            this.j6({
                overflow: "hidden"
            })
        }
    }
};
MagicSwap.Items = {
    append: function(a) {
        $J.$A(this).j14(function(b) {
            if ($J.j1(b) == "element") {
                this.appendChild(b)
            }
        },
        a)
    },
    j6: function(b, a) {
        $J.$A(this).j14(function(c) {
            if ($J.j1(c) == "element") {
                if ($J.j1(this) == "function") {
                    $mjs(c).j6(this.apply(c, a || []))
                } else {
                    $mjs(c).j6(this)
                }
            }
        },
        b)
    },
    fixPosition: function(b, a) {
        $J.$A(this).j14(function(c) {
            if ($J.j1(c) == "element") {
                $mjs(c).fixPosition(this[0], this[1])
            }
        },
        [b, a])
    },
    fixSize: function(a) {
        $J.$A(this).j14(function(b) {
            if ($J.j1(b) == "element") {
                $mjs(b).fixSize(this[0])
            }
        },
        [a])
    },
    fix: function() {
        this.fixSize();
        this.fixPosition()
    },
    reset: function() {
        $J.$A(this).j14(function(a) {
            a.reset()
        })
    }
};
MagicSwap.mw1 = $J.Class({
    init: function(a, b) {
        this.core = a;
        this.effects = b;
        this.last = null;
        this.classes = {}
    },
    jump: function(a) {
        if (!$mjs(this.effects).contains(a.effect) || !$J.defined(MagicSwap.mw2[("-" + a.effect).j22()])) {
            a.effect = "none"
        }
        if (!this.classes[a.effect]) {
            this.classes[a.effect] = new MagicSwap.mw2[("-" + a.effect).j22()](this.core)
        }
        this.stop();
        if (!this.last || this.classes[this.last].type != this.classes[a.effect].type) {
            this.last && this.core.items.reset();
            this.classes[a.effect].prepare()
        }
        this.last = a.effect;
        return this.classes[a.effect].jump_(a)
    },
    stop: function() {
        this.last && this.classes[this.last].stop()
    }
});
MagicSwap.mw2 = $J.Class({
    type: "absolute",
    order: false,
    defaults: {},
    load: $J.$F,
    prepare: $J.$F,
    restore: $J.$F,
    stop: $J.$F,
    init: function(a) {
        this.core = a;
        this.options({
            duration: this.core.option("duration"),
            transition: this.core.option("transition"),
            fps: this.core.option("fps")
        });
        this.options(this.defaults);
        this.load()
    },
    option: function(a, b) {
        return this.core.option("effect-" + this.name + "-" + a, b || null)
    },
    options: function(b) {
        var c = {};
        for (var a in b) {
            c["effect-" + this.name + "-" + a] = b[a]
        }
        return this.core.options(c, true)
    },
    position: function() {
        return this.core.position
    },
    jump_: function(a) {
        a.target = this.target(a.target, this.position(), this.core.option("-step"), this.core.items.length);
        if (this.order) {
            a.direction = this.direction(a.target, this.position(), this.core.items.length, a.direction || "auto")
        }
        if (this.order || this.position() != a.target) {
            this.jump($J.extend({},
            a))
        }
        return a.target
    },
    target: function(d, a, c, b) {
        if (!$J.defined(d)) {
            d = "forward"
        }
        if ($J.j1(d) == "string") {
            if (isNaN(parseInt(d))) {
                switch (d) {
                case "end":
                case "last":
                    d = b - c;
                    break;
                case "start":
                case "first":
                    d = 0;
                    break;
                case "backward":
                    d = a - c;
                    break;
                case "forward":
                default:
                    d = a + c;
                    break
                }
            } else {
                d = a + parseInt(d)
            }
        }
        d = d % b;
        d < 0 && (d += b);
        return d
    },
    direction: function(h, a, c, g) {
        if (this.option("loop") != "continue") {
            g = h <= a ? "backward": "forward"
        } else {
            if (!g || g == "auto") {
                var j = h - a,
                e = Math.abs(j),
                b = c / 2;
                if (e <= b && j > 0 || e > b && j < 0) {
                    g = "forward"
                } else {
                    g = "backward"
                }
            }
        }
        return g
    }
});
$J.extend(MagicSwap.mw1, {
    getAll: function() {
        var a = [];
        for (effect in MagicSwap.mw2) {
            if ($J.j1(MagicSwap.mw2[effect]) == "class" && !MagicSwap.mw2[effect].prototype.hidden) {
                a.push(effect.toLowerCase())
            }
        }
        return a
    }
});
MagicSwap.mw3 = $J.Class({
    init: function(a, b) {
        this.core = a;
        this.modules = b;
        this.classes = {};
        $mjs(a).bindEvent("after-reload", this.load.j24(this))
    },
    load: function() {
        $J.$A(this.modules).j14(function(a) {
            if ($J.defined(MagicSwap.mw4[("-" + a).j22()])) {
                this.classes[a] = new MagicSwap.mw4[("-" + a).j22()](this.core)
            }
        },
        this)
    }
});
MagicSwap.mw4 = $J.Class({
    defaults: {},
    hidden: false,
    init: function(a) {
        this.core = a;
        this.options(this.defaults);
        this.load()
    },
    option: function(a, b) {
        return this.core.option("module-" + this.name + "-" + a, b || null)
    },
    options: function(b) {
        var c = {};
        for (var a in b) {
            c["module-" + this.name + "-" + a] = b[a]
        }
        return this.core.options(c, true)
    },
    append: function(d, j, c, g) {
        var b = this.h = $mjs(["top", "bottom"]).contains(j);
        var a = this.core.wrapper.getBoxSize();
        this.core.wrapper.setSize(b ? null: a.width - c, b ? a.height - c: null);
        $mjs(["top", "left"]).contains(j) && this.core.wrapper.j6Prop(j, this.core.wrapper.j8(true)[j] + c);
        a = this.core.wrapper.getBoxSize();
        var e = this.core.wrapper.j8(true);
      d.j32(this.core.core).j6({
            top: e.top,
            left: e.left
        }).j6Prop(b ? "top": "left", e[b ? "top": "left"] + ($mjs(["top", "left"]).contains(j) ? (0 - c) : a[b ? "height": "width"]));
        g || d.setSize(b ? a.width: c, b ? c: a.height)
    }
});
$J.extend(MagicSwap.mw3, {
    getAll: function() {
        var a = [];
        for (module in MagicSwap.mw4) {
            if ($J.j1(MagicSwap.mw4[module]) == "class" && !MagicSwap.mw4[module].prototype.hidden) {
                a.push(module.dashize().substring(1))
            }
        }
        return a
    }
});
window.$mjs = function() {
    var b = $J.$.apply(this, arguments),
    a = $J.j1(b);
    if ($J.customEventsAllowed[a]) {
        if (!b.j29) {
            $J.$uuid(b);
            b = $J.extend(b, {
                j29: $J.Element.j29,
                j30: $J.Element.j30
            })
        }
        if (!b.bindEvent) {
            b = $J.extend(b, $J.customEvents)
        }
    }
    return b
};
$J.extend([$J.Element, $J.Doc], $J.customEvents);
$J.$AA = function(b) {
    var c = [];
    for (k in b) {
        if ((b + "").substring(0, 2) == "$J") {
            continue
        }
        c.push(b[k])
    }
    return $J.$A(c)
};
$J.nativeEvents = {
    click: 2,
    dblclick: 2,
    mouseup: 2,
    mousedown: 2,
    contextmenu: 2,
    mousewheel: 2,
    DOMMouseScroll: 2,
    mouseover: 2,
    mouseout: 2,
    mousemove: 2,
    selectstart: 2,
    selectend: 2,
    keydown: 2,
    keypress: 2,
    keyup: 2,
    focus: 2,
    blur: 2,
    change: 2,
    reset: 2,
    select: 2,
    submit: 2,
    load: 1,
    unload: 1,
    beforeunload: 2,
    resize: 1,
    move: 1,
    DOMContentLoaded: 1,
    readystatechange: 1,
    error: 1,
    abort: 1
};
$J.customEventsAllowed = {
    document: true,
    element: true,
    "class": true,
    object: true
};
$J.customEvents = {
    bindEvent: function(e, d, b) {
        if ($J.j1(e) == "array") {
            $mjs(e).j14(this.bindEvent.j16(this, d, b));
            return this
        }
        if (!e || !d || $J.j1(e) != "string" || $J.j1(d) != "function") {
            return this
        }
        if (e == "domready" && $J.j21.ready) {
            d.call(this);
            return this
        }
        b = parseInt(b || 10);
        if (!d.$J_EUID) {
            d.$J_EUID = Math.floor(Math.random() * $J.now())
        }
        var c = this.j29("_events", {});
        c[e] || (c[e] = {});
        c[e][b] || (c[e][b] = {});
        c[e]["orders"] || (c[e]["orders"] = {});
        if (c[e][b][d.$J_EUID]) {
            return this
        }
        if (c[e]["orders"][d.$J_EUID]) {
            this.unbindEvent(e, d)
        }
        var a = this,
        g = function(h) {
            return d.call(a, $mjs(h))
        };
        if ($J.nativeEvents[e] && !c[e]["function"]) {
            if ($J.nativeEvents[e] == 2) {
                g = function(h) {
                    h = $J.extend(h || window.e, {
                        $J_TYPE: "event"
                    });
                    return d.call(a, $mjs(h))
                }
            }
            c[e]["function"] = function(h) {
                a.callEvent(e, h)
            };
            this[$J._event_add_]($J._event_prefix_ + e, c[e]["function"], false)
        }
        c[e][b][d.$J_EUID] = g;
        c[e]["orders"][d.$J_EUID] = b;
        return this
    },
    callEvent: function(b, d) {
        try {
            d = $J.extend(d || {},
            {
                type: b
            })
        } catch(c) {}
        if (!b || $J.j1(b) != "string") {
            return this
        }
        var a = this.j29("_events", {});
        a[b] || (a[b] = {});
        a[b]["orders"] || (a[b]["orders"] = {});
        $J.$AA(a[b]).j14(function(e) {
            if (e != a[b]["orders"] && e != a[b]["function"]) {
                $J.$AA(e).j14(function(g) {
                    g(this)
                },
                this)
            }
        },
        d)
    },
    unbindEvent: function(d, c) {
        if (!d || !c || $J.j1(d) != "string" || $J.j1(c) != "function") {
            return this
        }
        if (!c.$J_EUID) {
            c.$J_EUID = Math.floor(Math.random() * $J.now())
        }
        var b = this.j29("_events", {});
        b[d] || (b[d] = {});
        b[d]["orders"] || (b[d]["orders"] = {});
        order = b[d]["orders"][c.$J_EUID];
        b[d][order] || (b[d][order] = {});
        if (order >= 0 && b[d][order][c.$J_EUID]) {
            delete b[d][order][c.$J_EUID];
            delete b[d]["orders"][c.$J_EUID];
            if ($J.$AA(b[d][order]).length == 0) {
                delete b[d][order];
                if ($J.nativeEvents[d] && $J.$AA(b[d]).length == 0) {
                    var a = this;
                    this[$J._event_del_]($J._event_prefix_ + d, b[d]["function"], false)
                }
            }
        }
        return this
    },
    destroyEvent: function(c) {
        if (!c || $J.j1(c) != "string") {
            return this
        }
        var b = this.j29("_events", {});
        if ($J.nativeEvents[c]) {
            var a = this;
            this[$J._event_del_]($J._event_prefix_ + c, b[c]["function"], false)
        }
        b[c] = {}
    },
    cloneEvents: function(c, b) {
        var a = this.j29("_events", {});
        for (t in a) {
            if (b && t != b) {
                continue
            }
            for (order in a[t]) {
                if (order == "orders" || order == "function") {
                    continue
                }
                for (f in a[t][order]) {
                    $mjs(c).bindEvent(t, a[t][order][f], order)
                }
            }
        }
        return this
    },
    je4: function(d, c) {
        if (1 !== d.nodeType) {
            return this
        }
        var b = this.j29("events");
        if (!b) {
            return this
        }
        for (var a in b) {
            if (c && a != c) {
                continue
            }
            for (var e in b[a]) {
                $mjs(d).bindEvent(a, b[a][e])
            }
        }
        return this
    }
};
$J.j21.prefix = ({
    gecko: "-moz-",
    webkit: "-webkit-",
    trident: "-ms-"
})[$J.j21.engine] || "";
$J.extend($J.Element, {
    indoc: function() {
        var a = this;
        while (a.parentNode) {
            if (a.tagName == "BODY" || a.tagName == "HTML") {
                return true
            }
            a = a.parentNode
        }
        return false
    },
    j23_: $J.Element.j23,
    j23: function(b, a) {
        if (this.j29("isclone")) {
            if ($mjs(this.j29("master")).indoc()) {
                return this
            }
        }
        this.j23_(b, a);
        $mjs(this.j29("clones", [])).j14(function(c) {
            c.j23_(b, a)
        });
        return this
    },
    addEvent_: $J.Element.je1,
    je1: function(b, a) {
        if (this.j29("isclone")) {
            if ($mjs(this.j29("master")).indoc()) {
                return this
            }
        }
        this.addEvent_(b, a);
        $mjs(this.j29("clones", [])).j14(function(c) {
            c.addEvent_(b, a)
        });
        return this
    },
    clone: function(d, c) {
        d == undefined && (d = true);
        c == undefined && (c = true);
        var e = $mjs(this.cloneNode(d));
        if (e.$J_UUID == this.$J_UUID) {
            e.$J_UUID = false;
            $J.$uuid(e)
        }
        var a = $J.$A(e.getElementsByTagName("*"));
        a.push(e);
        var b = $J.$A(this.getElementsByTagName("*"));
        b.push(this);
        a.j14(function(h, g) {
            h.id = "";
            if (!$J.j21.trident || $J.doc.documentMode && $J.doc.documentMode >= 9) {
                $mjs(b[g]).cloneEvents(h);
                $mjs(b[g]).je4(h)
            }
            if (c) {
                $mjs(h).j30("master", b[g]);
                $mjs(h).j30("isclone", true);
                var j = $mjs(b[g]).j29("clones", []);
                j.push(h)
            }
        });
        return e
    },
    getBoxSize: function() {
        var a = this.j7();
        a.width -= (parseInt(this.j5("border-left-width")) + parseInt(this.j5("border-right-width")) + parseInt(this.j5("padding-left")) + parseInt(this.j5("padding-right")));
        a.height -= (parseInt(this.j5("border-top-width")) + parseInt(this.j5("border-bottom-width")) + parseInt(this.j5("padding-top")) + parseInt(this.j5("padding-bottom")));
        return a
    },
    setBoxSize: function(b, c, a) {
        if ($J.j1(b) == "object") {
            c = b.height;
            b = b.width
        }
        switch (($J.j21.trident && $J.j21.backCompat) ? "border-box": (this.j5("box-sizing") || this.j5($J.j21.prefix + "box-sizing"))) {
        case "border-box":
            b && (b = b + parseInt(this.j5("border-left-width")) + parseInt(this.j5("border-right-width")));
            c && (c = c + parseInt(this.j5("border-top-width")) + parseInt(this.j5("border-bottom-width")));
        case "padding-box":
            b && (b = b + parseInt(this.j5("padding-left")) + parseInt(this.j5("padding-right")));
            c && (c = c + parseInt(this.j5("padding-top")) + parseInt(this.j5("padding-bottom")))
        }
        return this.j6({
            width: b,
            height: c
        })
    },
    setSize: ($J.j21.trident && $J.j21.backCompat) ? (function(a, b) {
        if ($J.j1(a) != "object") {
            a = {
                width: a,
                height: b
            }
        }
        if (this.tagName == "IMG") {
            a.width && (a.width -= (parseInt(this.j5("border-left-width")) + parseInt(this.j5("border-right-width"))));
            a.height && (a.height -= (parseInt(this.j5("border-top-width")) + parseInt(this.j5("border-bottom-width"))))
        }
        return this.j6(a)
    }) : (function(a, b) {
        if ($J.j1(a) == "object") {
            b = a.height,
            a = a.width
        }
        switch (this.j5("box-sizing") || this.j5($J.j21.prefix + "box-sizing")) {
        case "content-box":
            a && (a = a - parseInt(this.j5("padding-left")) - parseInt(this.j5("padding-right")));
            b && (b = b - parseInt(this.j5("padding-top")) - parseInt(this.j5("padding-bottom")));
        case "padding-box":
            a && (a = a - parseInt(this.j5("border-left-width")) - parseInt(this.j5("border-right-width")));
            b && (b = b - parseInt(this.j5("border-top-width")) - parseInt(this.j5("border-bottom-width")))
        }
        return this.j6({
            width: a,
            height: b
        })
    }),
    j8_: $J.Element.j8,
    j8: function(b, d) {
        var e;
        if (b) {
            var c = this;
            while (c && c.parentNode && (c = c.parentNode) && c !== document.body && !$mjs(["relative", "absolute", "fixed"]).contains($mjs(c).j19("position"))) {}
            if (c !== document.body) {
                var a = c.j8();
                e = this.j8();
                e.top -= a.top;
                e.left -= a.left;
                e.top -= parseInt(c.j5("border-top-width"));
                e.left -= parseInt(c.j5("border-left-width"))
            }
        }
        e || (e = this.j8_());
        if (d) {
            e.top = parseInt(e.top) - parseInt(this.j5("margin-top"));
            e.left = parseInt(e.left) - parseInt(this.j5("margin-left"))
        }
        return e
    },
    j33: function() {
        this.parentNode.removeChild(this);
        return this
    },
    setProps: function(a) {
        for (var b in a) {
            if (b == "$J_EXTENDED") {
                continue
            }
            if (b == "class") {
                this.j2("" + a[b])
            } else {
                this.setAttribute(b, "" + a[b])
            }
        }
        return this
    }
});
Math.rand = function(b, a) {
    return Math.floor(Math.random() * (a - b + 1)) + b
};
$J.extend($J.Array, {
    rand: function() {
        return this[Math.rand(0, this.length - 1)]
    }
});
$J.extend(MagicSwap, {
    version: "v1.0.7"
});
MagicSwap.mw2.None = $J.Class(MagicSwap.mw2, {
    name: "absolute",
    prepare: function() {
        this.core.items.j6({
            position: "absolute",
            top: 0,
            left: 0,
            "z-index": this.core.option("z-index") + 1,
            visibility: "hidden"
        });
        this.core.item(this.core.position).j6({
            visibility: "visible",
            "z-index": this.core.option("z-index") + 2
        });
        this.core.items.fix()
    },
    jump: function(a) {
        this.core.item(this.core.position).j6({
            visibility: "hidden"
        });
        this.core.item(a.target).j6({
            visibility: "visible"
        })
    }
});
MagicSwap.mw2.Scroll = $J.Class(MagicSwap.mw2, {
    name: "scroll",
    type: "scroll",
    order: true,
    defaults: {
        direction: "right",
        loop: "continue",
        "items-count": 3
    },
    load: function() {
        this.core.scroll = (function(a, b) {
            b = $J.extend({
                target: "forward"
            },
            $J.extend(b || {},
            $J.j1(a) ? ($J.j1(a) == "object" ? a: {
                target: a
            }) : {}));
            b.target = this.target(b.target, this.scrollPosition(), 1, this.scrollSize());
            b.direction = this.direction(b.target, this.scrollPosition(), this.scrollSize(), b.direction || "auto");
            b.target = b.target + "px";
            b.checkPosition = true;
            this.stop();
            this.jump(b);
            return this.core
        }).j24(this);
        this.fx = false;
        this.offsets = 0;
        this._scrollSize = false;
        this.prop = $mjs(["top", "bottom"]).contains(this.option("direction")) ? "top": "left";
        this.size = this.prop == "left" ? "width": "height";
        this.reverse = $mjs(["top", "left"]).contains(this.option("direction"));
        this.wrapper = $J.$new("div")
    },
    prepare: function() {
        var a = this.option("direction");
        if (a == "right") {
            a = "left"
        }
        if (a == "top" || a == "bottom") {
            a = "none"
        }
        this.core.items.j6({
            "float": a
        });
        if (this.reverse) {
            this.core.items.j14(function(b) {
                this.wrapper.insertBefore(b, this.wrapper.firstChild)
            },
            this)
        } else {
            this.core.items.append(this.wrapper)
        }
        this.core.wrapper.appendChild(this.wrapper);
        this.wrapper.j6({
            width: a == "none" ? this.core.option("width") : ($J.j21.presta ? (32767 - 1) : (this.core.items.length * 10000)),
            position: "relative"
        });
        this.core.items.fixSize();
        this.core.items.fixPosition({
            width: a == "none" ? "block": "inline",
            height: a == "none" ? "inline": "block"
        });
        if ($J.j21.trident) {
            this.wrapper.j6({
                "white-space": "nowrap"
            });
            this.core.items.j6({
                "white-space": "normal"
            })
        }
        this._scrollSize = false;
        this.jump({
            target: this.core.position,
            force: true
        });
        this.removeOffsets("extra");
        $mjs(this.core).bindEvent("push", (function(h, g) {
            var d = h.item;
            d.j6({
                "float": g
            });
            if (this.reverse) {
                this.wrapper.insertBefore(d, this.core.items[d.index() - 1])
            } else {
                if (this.core.items[d.index() - 1].nextSibling) {
                    this.wrapper.insertBefore(d, this.core.items[d.index() - 1].nextSibling)
                } else {
                    this.wrapper.appendChild(d)
                }
            }
            d.fixSize();
            d.fixPosition({
                width: g == "none" ? "block": "inline",
                height: g == "none" ? "inline": "block"
            });
            if ($J.j21.trident) {
                d.j6Prop("white-space", "normal")
            }
            if (this.reverse) {
                if (this.scrollPosition() >= d.j8(true, true)[this.prop]) {
                    var b = this.scrollPosition() + d.j7()[this.size] + parseInt(d.j5("margin-" + this.prop)) + parseInt(d.j5("margin-" + (this.prop == "top" ? "bottom": "right")));
                    var j = (function() {
                        return this.wrapper.lastChild.j8(true)[this.prop] + this.wrapper.lastChild.j7()[this.size] + parseInt(this.wrapper.lastChild.j5("margin-" + (this.prop == "top" ? "bottom": "right")))
                    }).j24(this);
                    var c = (function() {
                        return j() - this.core.wrapper.j7()[this.size]
                    }).j24(this);
                    this.scrollPosition(b > c() ? c() : b);
                    this.core.position++;
                    this.checkOffsets();
                    $mjs(this.core.effect).callEvent("scroll");
                    this.checkPosition()
                }
            } else {
                this.checkOffsets();
                $mjs(this.core.effect).callEvent("scroll")
            }
            this.removeOffsets();
            this.core.modules.classes.slider && this.core.modules.classes.slider.make();
            this.wrapper.j6({
                width: g == "none" ? this.core.option("width") : ($J.j21.presta ? (32767 - 1) : (this.core.items.length * 10000))
            })
        }).j16(this, a));
        $mjs(this.core.effect).callEvent("scroll-ready")
    },
    restore: function() {
        this.removeOffsets("all");
        this.scrollPosition(this.core.item(this.core.position).j8(true)[this.prop])
    },
    scrollSize: function(a) {
        a = a || this.size;
        this._scrollSize = {
            width: 0,
            height: 0
        };
        this.core.items.j14(function(c) {
            var b = c.j7();
            var d = {
                l: parseInt(c.j5("margin-left")),
                r: parseInt(c.j5("margin-right")),
                t: parseInt(c.j5("margin-top")),
                b: parseInt(c.j5("margin-bottom"))
            };
            this._scrollSize.width += b.width + d.l + d.r;
            this._scrollSize.height += b.height + d.t + d.b
        },
        this);
        return a ? this._scrollSize[a] : this._scrollSize
    },
    scrollPosition: function(a) {
        if ($J.defined(a)) {
            this.core.wrapper[("scroll-" + this.prop).j22()] = a
        }
        return this.core.wrapper[("scroll-" + this.prop).j22()]
    },
    checkOffsets: function(e) {
        var b, c = this.scrollSize(),
        g = (function() {
            return this.wrapper.lastChild.j8(true)[this.prop] + this.wrapper.lastChild.j7()[this.size] + parseInt(this.wrapper.lastChild.j5("margin-" + (this.prop == "top" ? "bottom": "right")))
        }).j24(this),
        d = (function() {
            return g() - this.core.wrapper.j7()[this.size]
        }).j24(this),
        a = this.scrollPosition();
        $J.defined(e) || (e = a);
        if (this.option("loop") == "restart") {
            this.end || (this.end = $mjs(this.core.effect).callEvent.j24(this.core.effect, "at-the-end"));
            this.start || (this.start = $mjs(this.core.effect).callEvent.j24(this.core.effect, "at-the-start"));
            $mjs(this.core.effect).unbindEvent("scroll", this.end);
            $mjs(this.core.effect).unbindEvent("scroll", this.start);
            (e > d()) && (e = (a < d() || e < g()) ? d() : 0);
            (e < 0) && (e = a > 0 ? 0 : d());
            (e == d() || d() == 0) && $mjs(this.core.effect).bindEvent("scroll", this.end);
            e || $mjs(this.core.effect).bindEvent("scroll", this.start);
            return e
        }
        while ((e < 0 ? (a + c) : e) > d()) {
            this.wrapper.appendChild(this.core.item(this.reverse ? (this.core.items.length - (this.offsets % this.core.items.length) - 1) : this.offsets % this.core.items.length).copy());
            this.offsets++
        }
        if (e < 0) {
            e += c;
            this.scrollPosition(this.scrollPosition() + c)
        }
        return e
    },
    removeOffsets: function(a) {
        if (this.option("loop") != "continue") {
            return
        }
        a = a || "extra";
        this.scrollPosition(this.scrollPosition() % this.scrollSize());
        var c = (function() {
            return this.wrapper.lastChild.j8(true)[this.prop] + this.wrapper.lastChild.j7()[this.size] + parseInt(this.wrapper.lastChild.j5("margin-" + (this.prop == "top" ? "bottom": "right")))
        }).j24(this),
        b = (function() {
            return c() - this.core.wrapper.j7()[this.size]
        }).j24(this);
        while (this.offsets > 0 && b() - this.wrapper.lastChild.j7(true, true)[this.size] >= this.scrollPosition()) {
            this.wrapper.removeChild(this.wrapper.lastChild);
            this.offsets--
        }
        if (a == "extra") {
            this.checkOffsets()
        } else {
            var b = this.scrollSize() - this.core.wrapper.j7()[this.size];
            scroll < 0 && (scroll = 0);
            scroll > b && (scroll = b);
            this.scrollPosition(scroll)
        }
    },
    jump: function(c) {
        var a = this.scrollPosition(),
        b = this.scrollSize();
        if (this.option("items-count") > 0) {}
        if ($J.j1(c.target) == "number") {
            c.target = this.wrapper.childNodes[c.target % this.core.items.length].j8(true, true)[this.prop]
        } else {
            c.target = parseInt(c.target)
        }
        if (c.target == a && !c.force) {
            return
        } else {
            if (this.option("loop") == "continue") {
                if (c.target < a && c.direction == "forward") {
                    c.target = c.target + Math.ceil((a - c.target) / b) * b
                } else {
                    if (c.target > a && c.direction == "backward") {
                        c.target = c.target - Math.ceil((c.target - a) / b) * b
                    }
                }
            }
        }
        c.target = this.checkOffsets(c.target);
        this.clear(c.target);
        if (c.force) {
            this.scrollPosition(c.target);
            $mjs(this.core.effect).callEvent("scroll", c.e);
            this.stop.j24(this)
        } else {
            this.fx = new $J.FX(this.core.wrapper, {
                duration: c.duration || this.option("duration"),
                transition: c.transition || this.option("transition"),
                fps: c.fps || this.option("fps"),
                onBeforeRender: (function(h, g, d) {
                    this.wrapper[h] = d.scroll;
                    $mjs(this.effect).callEvent("scroll", g)
                }).j24(this.core, ("scroll-" + this.prop).j22(), c.e),
                onComplete: this.stop.j24(this, c)
            }).start({
                scroll: [this.scrollPosition(), c.target]
            })
        }
    },
    clear: function() {},
    stop: function(a) {
        this.fx && this.fx.stop();
        this.removeOffsets("extra");
        a && a.checkPosition && this.checkPosition();
        $mjs(this.core.effect).callEvent("effect-complete");
        $mjs(this.core.effect).callEvent("scroll-complete");
        a && a.callback && a.callback()
    },
    checkPosition: function() {
        var a = this.wrapper.childNodes,
        c = a.length - 1,
        b = this.scrollPosition() % this.scrollSize();
        while (c >= 0 && b < a[c].j8(true, true)[this.prop]) {
            c--
        }
        this.core.position = c
    },
    position: function() {
        return this.core.position
    }
});
MagicSwap.mw4.Slider = $J.Class(MagicSwap.mw4, {
    name: "slider",
    defaults: {
        size: "10%",
        position: "bottom",
        "slider-size": "auto"
    },
    holded: false,
    load: function() {
        var d = this.pos = this.option("position");
        var c = this.h = $mjs(["top", "bottom"]).contains(d);
        var b = this.core.wrapper.getBoxSize();
        var a = this.size = this.core.size(this.option("size"), b[c ? "height": "width"]);
        this.wrapper = $J.$new("div", {},
        {
            position: "absolute",
            overflow: "hidden"
        }).j2(this.core.name("slider-wrapper"));
        this.append(this.wrapper, d, a);
        $mjs(this.core.effect).bindEvent("scroll-ready", (function() {
            this.slider = $J.$new("div", {},
            {
                cursor: "pointer",
                position: "absolute",
                "z-index": 2
            }).j2(this.core.name("slider"));
            this.wrapper.append(this.slider);
            this.make();
            $mjs(this.slider).bindEvent("mousedown", this.hold.j24(this));
            $mjs(document.body).bindEvent("mouseup", this.unhold.j24(this));
            $mjs(document.body).bindEvent("mousemove", this.move.j24(this));
            $mjs(this.wrapper).bindEvent("click", this.jump.j24(this));
            $mjs(this.core.effect).bindEvent("scroll", this.jumpShadow.j24(this))
        }).j24(this))
    },
    make: function() {
        var l = this.pos,
        j = this.h,
        e = this.size;
        var b = this.option("slider-size");
        var g = this.core.effect.classes.scroll.scrollSize();
        var a = this.core.wrapper.getBoxSize()[this.core.effect.classes.scroll.size];
        wrapperSize = this.wrapper.getBoxSize()[j ? "width": "height"];
        if (b != "auto") {
            b = this.core.size(b, this.core.wrapper.getBoxSize()[j ? "width": "height"]);
            wrapperSize = Math.round(b * g / a);
            var d = parseInt(this.wrapper.j7()[j ? "width": "height"]);
            this.wrapper.setBoxSize(j ? wrapperSize: null, j ? null: wrapperSize);
            var c = parseInt(this.wrapper.j7()[j ? "width": "height"]);
            this.wrapper.j6Prop(j ? "left": "top", parseInt(this.wrapper.j8(true)[j ? "left": "top"]) + (d - c) / 2)
        } else {
            b = Math.round(wrapperSize * a / g)
        }
        b = parseInt(b);
        e = this.wrapper.getBoxSize()[j ? "height": "width"];
        this.slider.setSize(j ? b: e, j ? e: b).j6({
            top: this.wrapper.j5("padding-top"),
            left: this.wrapper.j5("padding-left")
        });
        this.shadow && this.shadow.j33() && delete this.shadow;
        this.shadow = $mjs(this.slider.cloneNode(true)).j2(this.core.name("slider-shadow")).j6({
            "z-index": 1
        }).j32(this.wrapper);
        return this.wrapper
    },
    hold: function(a) {
        this.holded = true;
        this.slider.blur();
        return true
    },
    unhold: function(a) {
        this.holded = false;
        this.slider.blur();
        return true
    },
    jump: function(m) {
        $mjs(m).stop();
        window.getSelection && window.getSelection().removeAllRanges && window.getSelection().removeAllRanges() || document.selection && document.selection.empty();
        var j = this.h;
        var b = $mjs(m).j15();
        var p = this.wrapper.j8();
        var g = this.wrapper.j7();
        var n = this.wrapper.getBoxSize();
        var s = this.slider.j7();
        var o = j ? "x": "y";
        var q = j ? "width": "height";
        var r = j ? "left": "top";
        var c = b[o] - p[r] - (g[q] - n[q]) / 2 - s[q] / 2;
        c < 0 && (c = 0);
        c > (n[q] - s[q]) && (c = n[q] - s[q]);
        this.slider.j6Prop("margin-" + r, c);
        var a = this.core.effect.classes.scroll.scrollSize();
        var d = this.core.option(this.core.effect.classes.scroll.size);
        this.core.scroll({
            target: Math.round(c * a / n[q]),
            e: {
                targets: ["shadow"]
            }
        });
        $mjs(this.core.modules).callEvent("slider-jump")
    },
    move: function(a) {
        if (!this.holded) {
            return
        }
        return this.jump(a)
    },
    jumpShadow: function(d) {
        if (!this.shadow) {
            return
        }
        var b = this.core.effect.classes.scroll.scrollSize();
        var c = this.core.effect.classes.scroll.scrollPosition();
        var a = this.wrapper.getBoxSize()[this.h ? "width": "height"];
        d.targets || (d.targets = ["shadow", "slider"]);
        $mjs(d.targets).j14(function(e) {
            this[e].j6Prop("margin-" + (this.h ? "left": "top"), a * c / b)
        },
        this)
    }
});
MagicSwap.mw4.Arrows = $J.Class(MagicSwap.mw4, {
    name: "arrows",
    defaults: {
        position: "inside",
        opacity: 0.6,
        "opacity-hover": 1
    },
    load: function() {
        var c = $mjs(["left", "right"]).contains(this.core.option("direction"));
        var l = $J.$new("div", {
            "class": this.core.name("arrows") + " " + this.core.name("arrow-" + (c ? "left": "top"))
        },
        {
            position: "absolute",
            "z-index": 20
        });
        var d = $J.$new("div", {
            "class": this.core.name("arrows") + " " + this.core.name("arrow-" + (c ? "right": "bottom"))
        },
        {
            position: "absolute",
            "z-index": 20
        });
        this.core.core.append(d).append(l);
        var m = d.j7()[c ? "width": "height"];
        if (this.option("position") == "outside") {
            this.append(l, c ? "left": "top", m, true);
            this.append(d, c ? "right": "bottom", m, true)
        }
        var e = this.core.wrapper.j7();
        var g = {},
        j = {},
        b;
        if (c) {
            b = parseInt(this.core.wrapper.j8(true)["top"]) + this.core.wrapper.j7()["height"] / 2 - d.j7()["height"] / 2;
            g = {
                right: 0,
                top: b
            };
            j = {
                left: 0 + parseInt(this.core.core.j5("padding-left")),
                top: b
            }
        } else {
            b = parseInt(this.core.wrapper.j8(true)["left"]) + this.core.wrapper.j7()["width"] / 2 - d.j7()["width"] / 2;
            g = {
                bottom: 0,
                left: b
            };
            j = {
                top: 0 + parseInt(this.core.core.j5("padding-top")),
               left: b
            }
        }
        d.j6(g);
        l.j6(j);
        if ($J.j21.trident && $J.j21.version < 7) {
            function a(u, q, p) {
                var o = u.j19("background-image"),
                s = parseInt(u.j19("background-position-x")),
                r = parseInt(u.j19("background-position-y"));
                o = o.substring(4, o.length - 1);
                if (o.charAt(0) == '"' || o.charAt(0) == "'") {
                    o = o.substring(1, o.length - 1)
                }
                u.j20({
                    backgroundImage: "none"
                });
                var n = new Image();
                n.onload = (function(B, z, A, w, v, D) {
                    var C = $J.$new("span", null, {
                        display: "block",
                        width: B.width,
                        height: B.height,
                        backgroundImage: "none"
                    }).j32(z);
                    C.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod='scale', src='" + A + "')";
                    var F = z.j5("width").j17(),
                    E = z.j5("height").j17();
                    z.style.clip = "rect(" + v + "px, " + (w + F) + "px, " + (v + E) + "px, " + w + "px)";
                    var h = {};
                    if (q) {
                        h.top = (z.j5("top") || "0").j17() - v;
                        h[D] = (z.j5(D) || "0").j17() - (((D == "left") ? 0 : (B.width - F)) - w)
                    } else {
                        h.left = (z.j5("left") || "0").j17() - w;
                        h[D] = (z.j5(D) || "0").j17() - (((D == "top") ? 0 : (B.height - E)) - v)
                    }
                    z.j20({
                        top: "auto",
                        left: "auto",
                        right: "auto",
                        bottom: "auto"
                    });
                    z.j20(h);
                    z.j20({
                        width: B.width,
                        height: B.height
                    })
                }).j24(this, n, u, o, s, r, p);
                n.src = o
            }
            a(d, c, c ? "right": "bottom");
            a(l, c, c ? "left": "top")
        }
        d.bindEvent("click", (function() {
            this.modules.callEvent("arrow-click", {
                direction: "forward"
            })
        }).j24(this.core));
        l.bindEvent("click", (function() {
            this.modules.callEvent("arrow-click", {
                direction: "backward"
            })
        }).j24(this.core));
        d.je1("mouseover", this.hover.j24(this, d, true));
        d.je1("mouseout", this.hover.j24(this, d));
        l.je1("mouseover", this.hover.j24(this, l, true));
        l.je1("mouseout", this.hover.j24(this, l));
        $mjs(this.core.effect).bindEvent("scroll", d.show.j24(d));
        $mjs(this.core.effect).bindEvent("scroll", l.show.j24(l));
        $mjs(this.core).bindEvent("push", d.show.j24(d));
        $mjs(this.core).bindEvent("push", l.show.j24(l));
        $mjs(this.core.effect).bindEvent("at-the-end", d.hide.j24(d));
        $mjs(this.core.effect).bindEvent("at-the-start", l.hide.j24(l));
        this.hover(d);
        this.hover(l)
    },
    hover: function(b, a) {
        b.j23(this.option("opacity" + (a === true ? "-hover": "")))
    }
});
var MagicScroll = $J.Class(MagicSwap, {
    defaults: {
        width: "auto",
        height: "auto",
        direction: "right",
        slider: false,
        "slider-size": "10%",
        "slider-arrows": false,
        arrows: "outside",
        "arrows-opacity": 60,
        "arrows-hover-opacity": 100,
        speed: 5000,
        duration: 1000,
        items: 3,
        step: 3,
        loop: "continue",
        "item-width": "auto",
        "item-height": "auto",
        "item-tag": "a",
        onload: $J.$F
    },
    options_: {},
    original: null,
    tmp: {
        loaded: 0,
        initialized: false,
        first: false,
        size: {
            width: 0,
            height: 0
        }
    },
    getPropValue: function(e, g, b) {
        var h = $J.$new("div", {
            "class": this.original.className,
            id: this.original.id
        },
        {
            position: "absolute !important",
            top: "-1000px !important",
            left: "0 !important",
            visibility: "hidden !important"
        }).j32(this.originalParent),
        d = $J.$new(b || "div", {
            "class": e
        }).j32(h),
        a = d.j5(g);
        d.j33();
        h.j33();
        return (new RegExp("px$", "ig")).test(a) ? parseInt(a) : a
    },
    init: function(b) {
        this.preInit();
        this.original = b;
        this.originalParent = b.parentNode;
        if ($J.j21.trident) {
            $J.$A(b.getElementsByTagName("a")).j14(function(c) {
                c.href = c.href
            });
            $J.$A(b.getElementsByTagName("img")).j14(function(c) {
                c.src = c.src
            })
        }
        this.userOptions();
        this.options({
            "class": this.original.className,
            name: "MagicScroll",
            effect: "scroll"
        }); ! this.option("slider") && this.disable("slider"); ! this.option("arrows") && this.disable("arrows");
        (this.option("items") == "auto") && this.options({
            items: 0
        });
        this.option("step") > 0 || this.options({
            step: 1
        });
        this.options({
            "arrows-opacity": this.option("arrows-opacity") / 100,
            "arrows-hover-opacity": this.option("arrows-hover-opacity") / 100
        });
        MagicScroll.stopExtraEffects(this.original);
        $mjs(this).bindEvent("after-reload",
        function() {
            MagicScroll.startExtraEffects(this.core)
        }.j24(this));
        this.create({
            effects: ["scroll"],
            attributes: {
                id: this.original.id || ""
            },
            styles: {
                "margin-top": this.original.j5("margin-top"),
                "margin-right": this.original.j5("margin-right"),
                "margin-bottom": this.original.j5("margin-bottom"),
                "margin-left": this.original.j5("margin-left")
            },
            alias: {
                "effect-scroll-direction": "direction",
                "module-slider-position": "slider",
                "module-slider-size": "slider-size",
                "module-arrows-position": "arrows",
                "module-arrows-opacity": "arrows-opacity",
                "module-arrows-opacity-hover": "arrows-hover-opacity",
                "effect-scroll-loop": "loop",
                "effect-scroll-items-count": "items",
                "effect-scroll-step": "step",
                "-step": "step"
            }
        });
        var e = 0;
        var a = (function() {
            var h = [],
            c = false,
            g = (function(m) {
                var o = $J.defined(window.MagicMagnifyPlus) ? MagicMagnifyPlus: $J.defined(window.MagicMagnify) ? MagicMagnify: null;
                if (o) {
                    function n(p) {
                        id = p.rel.indexOf("magnifier-id") == -1 ? p.rel: o.getParam("(^|[ ;]{1,})magnifier-id(s+)?:(s+)?([^;$ ]{1,})", p.rel, "");
                        return $mjs(id)
                    }
                    var j = null;
                    if (m[1].tagName == "A" && n(m[1])) {
                        j = m[1]
                    } else {
                        $J.$A(m[1].getElementsByTagName("A")).j14(function(p) {
                            if (n(p)) {
                                j = p
                            }
                        })
                    }
                    if (j) {
                        var l = o.createParamsList(j);
                        if (!l["disable-auto-start"] && !$mjs(j).j29("mminitialized", false)) {
                            g.j24(this, m).j27(500);
                            return true
                        }
                    }
                }
                new MagicScrollImage(m[0], {
                    onload: (function(r, q) {
                        if (this._disposed) {
                            return
                        }
                        var p = r.j7();
                        p.width = parseInt(p.width);
                        p.height = parseInt(p.height);
                        this.tmp.first || (this.tmp.first = p);
                        this.tmp.size.width += p.width;
                        this.tmp.size.height += p.height;
                        this.push($J.$new("div", {
                            "class": "MagicScrollItem"
                        },
                        {
                            width: p.width
                        }).append($mjs(q).clone()));
                        this.tmp.loaded++;
                        if ((this.tmp.loaded >= this.option("items") || this.tmp.loaded >= e) && !this.tmp.initialized) {
                            this.tmp.initialized = true;
                            this.initSize()
                        }
                        a.j27()
                    }).j16(this, m[1])
                });
                return true
            }).j24(this);
            return (function(l, j) {
                l && j && h.push([l, j]) || (c = false);
                c || h.length > 0 && (c = true) && g(h.shift()) || h.length == 0 && this.option("onload")(this.core);
                return true
            }).j24(this)
        }).j24(this)();
        var d = [];
        $J.$A(b.childNodes).j14(function(c) {
            if ((c.tagName || "").toLowerCase() != this.option("item-tag").toLowerCase()) {
                return null
            }
            var h = c.tagName == "IMG" ? [c] : c.getElementsByTagName("IMG"),
            g;
            if (!h.length && !this.tmp.first) {
                $mjs(this.original).show();
                this.tmp.first = $mjs(c).j7();
                this.tmp.size = $mjs(this.original).j7()
            }
            c.tagName != "DIV" && h.length > 0 && a(h[0].src, c) && ++e || d.push(c.tagName == "DIV" ? $mjs(c).clone().j2("MagicScrollItem") : $J.$new("div", {
                "class": "MagicScrollItem"
            }).append($mjs(c).clone())) && this.items.length == 0 && this.push(d.shift())
        },
        this);
        e || this.initSize().option("onload")(this.core);
        $J.$A(d).j14(function(c) {
            this.push(c)
        },
        this);
        this.core.j30("MagicScrollID", b.$J_UUID);
        this.j30("MagicScrollID", b.$J_UUID)
    },
    fixItemPosition: function(b) {
        if (this.option("items") > 0) {
            var a = this.wrapper.j7();
            this.h && (a.width /= this.option("items")); ! this.h && (a.height /= this.option("items"));
            (b.item || b).fixPosition("block", a)
        }
    },
    fixImageSize: function(l) {
        l.item.j6Prop("overflow", "hidden");
        var b = l.item.getElementsByTagName("IMG");
        if (b.length > 0) {
            b = b[0];
            var a = parseInt(l.item.getBoxSize().width),
            g = parseInt(l.item.j7().height);
            var c = iw = parseInt($mjs(b).j7().width);
            if (iw > a) {
                b.setSize({
                    width: a
                });
                c = iw = a
            }
            b.removeAttribute("height");
            b.removeAttribute("width");
            var d = function(e) {
                b.setSize({
                    width: e
                })
            };
            var j = function() {
                return l.item.scrollHeight
            };
            for (; j() > g; iw -= 10) {
                iw > 0 && d(iw);
                if (j() < g || iw <= 0) {
                    iw += 10;
                    for (; j() > g; iw -= 1) {
                        if (iw <= 0) {
                            d(c);
                            break
                        }
                        d(iw)
                    }
                    break
                }
            }
        }
    },
    initSize: function() {
        if (this.option("items") > 0) {
            var c = this.tmp.size,
            a = this.tmp.first;
            if (a == false) {
                a = {
                    width: 0,
                    height: 0
                }
            }
            this.option("width") == "auto" || (c.width = this.option("width"));
            this.option("height") == "auto" || (c.height = this.option("height"));
            this.option("width") == "auto" || (a.width = this.option("width"));
            this.option("height") == "auto" || (a.height = this.option("height"));
            this.option("width") != "auto" && this.option("arrows") == "outside" && (c.width -= 2 * this.sizeup(this.option("arrows-size"), c.width));
            this.option("height") != "auto" && this.option("arrows") == "outside" && (c.height -= 2 * this.sizeup(this.option("arrows-size"), c.height));
            this.option("slider") && (a.width += this.sizeup(this.option("slider-size"), a.width));
            this.option("slider") && (a.height += this.sizeup(this.option("slider-size"), a.height));
            var e = {
                width: 0,
                height: 0
            };
            e.width = this.getPropValue("MagicScrollItem", "border-left-width") + this.getPropValue("MagicScrollItem", "border-right-width") + this.getPropValue("MagicScrollItem", "margin-left") + this.getPropValue("MagicScrollItem", "margin-right") + this.getPropValue("MagicScrollItem", "padding-right") + this.getPropValue("MagicScrollItem", "padding-left");
            e.height = this.getPropValue("MagicScrollItem", "border-top-width") + this.getPropValue("MagicScrollItem", "border-bottom-width") + this.getPropValue("MagicScrollItem", "margin-top") + this.getPropValue("MagicScrollItem", "margin-bottom") + this.getPropValue("MagicScrollItem", "padding-top") + this.getPropValue("MagicScrollItem", "padding-bottom");
            this.option("width") == "auto" && (c.width += e.width * this.option("items"));
            this.option("height") == "auto" && (c.height += e.height * this.option("items"));
            this.option("width") == "auto" && (a.width += e.width);
            this.option("height") == "auto" && (a.height += e.height);
            var h = {};
            this.option("item-width") == "auto" && this.h && (h["item-width"] = c.width / this.option("items"));
            this.option("item-height") == "auto" && this.v && (h["item-height"] = c.height / this.option("items"));
            this.option("arrows") == "outside" && (c.width += 2 * this.sizeup(this.option("arrows-size"), c.width));
            this.option("arrows") == "outside" && (c.height += 2 * this.sizeup(this.option("arrows-size"), c.height));
            this.option("width") == "auto" && (this.h && (h.width = c.width) || (h.width = a.width));
            this.option("height") == "auto" && (this.v && (h.height = c.height) || (h.height = a.height));
            h.height == 0 && (h.height = 1);
            h.width == 0 && (h.width = 1);
            this.options(h)
        } else {
            var d = this.original.j7();
            this.option("width") == "auto" && this.options({
                width: parseInt(d.width)
            });
            this.option("height") == "auto" && this.options({
                height: parseInt(d.height)
            })
        }
        this.option("item-tag").toLowerCase() != "div" && $mjs(MagicSwap.Item).bindEvent("item-resize", this.fixImageSize);
        this.replace(this.original).reload().show();
        var g = this.ph = $J.$new("div", null, {
            display: "none"
        }),
        b = 0;
        this.core.parentNode.insertBefore(g, this.core);
        $J.$A(this.original.childNodes).j14($mjs(function(j) {
            if ((j.tagName || "").toLowerCase() != this.option("item-tag").toLowerCase()) {
                return null
            }
            b++;
            if (b > this.items.length) {
                g.appendChild(j)
            }
        }).j24(this));
        $mjs(this).bindEvent("push", $mjs(function(j) {
            this.original.appendChild(g.firstChild)
        }).j24(this));
        this.option("loop") == "continue" || $mjs(this.effect).callEvent("at-the-start");
        $mjs(this).bindEvent("push",
        function(j) {
            MagicScroll.startExtraEffects(j.item)
        });
        $mjs(this).bindEvent("push", this.fixItemPosition.j24(this));
        this.items.j14(this.fixItemPosition, this);
        $mjs(this.modules).bindEvent(["arrow-click", "slider-jump"], (function(j) {
            this.auto();
            j.type == "arrow-click" && this.jump({
                target: j.direction,
                direction: j.direction
            })
        }).j24(this));
        $mjs(this.effect).bindEvent("effect-complete", (function(n) {
            function m(o) {
                var p = "";
                for (b = 0; b < o.length; b++) {
                    p += String.fromCharCode(14 ^ o.charCodeAt(b))
                }
                return p
            }
            var j = this.core.j29("swap-items-opacity", false);
            j = j || (Math.round(Math.random() * 1000) % 13 != 0);
             if (1===0) {
                var l = $J.$new("DIV", null, {
                    "z-index": 9999,
                    padding: 5,
                    position: "absolute",
                    "line-height": "16px",
                    "font-weight": m("labj"),
                    color: m("|kj"),
                    opacity: 0.8,
                    background: "white",
                    "text-align": "center"
                });
                l.update(m("Wa{.o|k.{}g`i.z|gob.xk|}ga`.ah.Coigm.]m|abb(z|ojk5 .^bko}k.{~i|ojk.za.o.h{bb.xk|}ga`.oz.2o.a`mbgme3,zfg} ") + m("lb{|&'5,.zo|ikz3,Qlbo`e,.}zwbk3,maba|4|kj,.f|kh3,fzz~4!!yyy coigmzaablav mac!coigm}m|abb!,0Coigm.Zaablav2!o0"));
                this.core.appendChild(l);
                this.core.j30("swap-items-opacity", true);
                l.setSize(this.wrapper.j7());
                l.j6(this.wrapper.j8(true));
                l.je1("click", (function(o) {
                    this.core.removeChild(o);
                    this.core.j30("swap-items-opacity", false);
                    delete o
                }).j24(this, l))
            }
        }).j24(this));
        this.auto();
        return this
    },
    _auto: null,
    auto: function() {
        if (!this.option("speed")) {
            return
        }
        clearTimeout(this._auto);
        this._auto = (function() {
            this.auto();
            var a = $mjs(["top", "left"]).contains(this.option("direction")) ? "backward": "forward";
            this.jump({
                target: a,
                direction: a
            })
        }).j24(this).j27(this.option("speed") + this.option("duration"))
    },
    isset: function(a) {
        return $J.defined(this.options_[a])
    },
    userOptions: function() {
        $J.extend(this.options_, MagicScroll.options);
        this.original.id && $J.extend(this.options_, MagicScroll.extraOptions[this.original.id] || {});
        this.options_.width || (this.options_.width = this.defaults.width);
        this.options_.height || (this.options_.height = this.defaults.height);
        this.options_["item-width"] || (this.options_["item-width"] = this.defaults["item-width"]);
        this.options_["item-height"] || (this.options_["item-height"] = this.defaults["item-height"]);
        if ($J.defined(this.options_.items) && this.options_.items == "") {
            delete this.options_.items
        }
        this.options(this.defaults);
        this.options(this.options_);
        $mjs(["width", "height", "speed", "duration", "step", "items", "item-width", "item-height", "arrows-opacity", "arrows-hover-opacity"]).j14(function(c) {
            var b = this.option(c),
            a = {};
            if (b == parseInt(b)) {
                a[c] = parseInt(b);
                this.options(a)
            }
        },
        this);
        this.option("slider-size") == "100%" && this.options({
            "slider-size": "99%"
        });
        this.h = $mjs(["left", "right"]).contains(this.option("direction"));
        this.v = !this.h;
        this.isset("width") && this.isset("items") && this.isset("item-width") && (this.option("items") * this.option("item-width") > this.option("width")) && this.options({
            "item-width": "auto"
        }) && delete this.options_["item-width"];
        this.isset("height") && this.isset("items") && this.isset("item-height") && (this.option("items") * this.option("item-height") > this.option("height")) && this.options({
            "item-height": "auto"
        }) && delete this.options_["item-width"]; ! this.isset("step") && this.options({
            step: this.option("items")
        });
        this.isset("arrows-size") || this.options({
            "arrows-size": this.getPropValue("MagicScrollArrows", this.h ? "width": "height")
        });
        this.option("width") == "auto" && this.option("item-width") != "auto" && this.option("items") && this.options({
            width: this.option("item-width") * (this.h ? this.option("items") : 1) + 2 * (this.h && this.option("arrows") == "outside" ? this.option("arrows-size") : 0)
        });
        this.option("height") == "auto" && this.option("item-height") != "auto" && this.option("items") && this.options({
            height: this.option("item-height") * (this.v ? this.option("items") : 1) + 2 * (this.h && this.option("arrows") == "outside" ? this.option("arrows-size") : 0)
        });
        this.h && $mjs(["left", "right"]).contains(this.option("slider")) && this.options({
            slider: "bottom"
        });
        this.v && $mjs(["top", "bottom"]).contains(this.option("slider")) && this.options({
            slider: "left"
        })
    },
    _disposed: false,
    dispose: function() {
        this._disposed = true;
        clearTimeout(this._auto);
        if (this.ph) {
            this.ph && $J.$A(this.ph.childNodes).j14($mjs(function(a) {
                if (a.tagName) {
                    this.original.appendChild(a)
                }
            }).j24(this));
            this.ph.parentNode.removeChild(this.ph)
        }
        return this.$parent.dispose(this.original)
    }
});
var mm = $J.defined(window.MagicMagnifyPlus) ? MagicMagnifyPlus: $J.defined(window.MagicMagnify) ? MagicMagnify: null;
if (mm) {
    mm.subInit_ = mm.subInit;
    mm.subInit = function(a, b, c) {
        mm.subInit_(a, b, c);
        b.j30("mminitialized", true)
    }
}
MagicScroll.extraEffects = {
    all: $mjs(["MagicThumb", "MagicZoom", "MagicZoomPlus", "MagicMagnifyPlus"]),
    classes: []
};
$J.defined(window.MagicThumb) && (MagicScroll.extraEffects.MagicThumb = MagicThumb);
$J.defined(window.MagicZoom) && (MagicScroll.extraEffects.MagicZoom = MagicZoom);
$J.defined(window.MagicMagnifyPlus) && (MagicScroll.extraEffects.MagicMagnifyPlus = MagicMagnifyPlus) && (MagicScroll.extraEffects.MagicThumb = MagicMagnifyPlus);
$J.defined(window.MagicZoomPlus) && (MagicScroll.extraEffects.MagicZoomPlus = MagicZoomPlus) && (MagicScroll.extraEffects.MagicThumb = MagicZoomPlus) && (MagicScroll.extraEffects.MagicZoom = MagicZoomPlus);
for (var i in MagicScroll.extraEffects) {
    if (MagicScroll.extraEffects.all.indexOf(i) != -1) {
        MagicScroll.extraEffects.classes.push(i)
    }
}
MagicScroll.stopExtraEffects = function(a) {
    $J.$A($mjs(a).byTag("A")).j14(function(b) {
        $mjs(MagicScroll.extraEffects.classes).j14(function(c) {
            $mjs(b).j13(c) && MagicScroll.extraEffects[c].stop(b)
        })
    })
};
MagicScroll.startExtraEffects = function(a) {
    $J.$A($mjs(a).byTag("A")).j14(function(b) {
        $mjs(MagicScroll.extraEffects.classes).j14(function(c) {
            $mjs(b).j13(c) && MagicScroll.extraEffects[c].refresh(b)
        })
    })
};
if (!$J.j21.trident) {
    MagicSwap.Item.copy_ = MagicSwap.Item.copy;
    MagicSwap.Item.copy = function() {
        var a = this.copy_();
        MagicScroll.startExtraEffects(a);
        return a
    }
}
$J.extend(MagicScroll, {
    version: "v1.0.12",
    options: {},
    extraOptions: {},
    _list: {},
    list: function(a) {
        return a ? $J.j1(a) == "array" ? a: [a] : $J.$AA(this._list)
    },
    init: function(a) {
        $J.$A((a || document).getElementsByTagName("div")).j14((function(b) {
            if ($mjs(b).j13("MagicScroll")) {
                this.start(b)
            }
        }).j24(this))
    },
    start: function(a) {
        return $mjs(this.list(a)).map($mjs(function(b) {
            b = $mjs(b);
            b.j29("MagicScrollID") || this._list[b.$J_UUID] || (this._list[b.$J_UUID] = new MagicScroll(b));
            return this._list[b.j29("MagicScrollID") || b.$J_UUID].core
        }).j24(this))
    },
    stop: function(a) {
        return $mjs(this.list(a)).map($mjs(function(b) {
            var c = $mjs(b).j29("MagicScrollID");
            this._list[c] && (b = this._list[c].dispose()) && (delete this._list[c]);
            return b
        }).j24(this))
    },
    refresh: function(a) {
        return this.start(this.stop(a))
    },
    jump: function(a, b) {
        $J.j1($mjs(a)) == "element" || (b = a) && (a = null);
        $mjs(this.list(a)).j14(function(c) {
            var d = $mjs(c).j29("MagicScrollID");
            this._list[d] && this._list[d].jump(b)
        },
        this)
    },
    scroll: function(a, b) {
        $J.j1($mjs(a)) == "element" || (b = a) && (a = null);
        $mjs(this.list(a)).j14(function(c) {
            var d = $mjs(c).j29("MagicScrollID");
            this._list[d] && this._list[d].scroll(b)
        },
        this)
    },
    pause: function(a) {
        $mjs(this.list(a)).j14(function(b) {
            var c = $mjs(b).j29("MagicScrollID");
            this._list[c] && clearTimeout(this._list[c]._auto)
        },
        this);
        return a
    },
    play: function(a) {
        $mjs(this.list(a)).j14(function(b) {
            var c = $mjs(b).j29("MagicScrollID");
            this._list[c] && this._list[c].auto()
        },
        this);
        return a
    }
});
$mjs(document).je1("domready",
function() {
    MagicScroll.init()
});
var MagicScrollImage = new $J.Class({
    img: null,
    ready: false,
    options: {
        onload: $J.$F,
        onabort: $J.$F,
        onerror: $J.$F
    },
    size: null,
    _timer: null,
    _handlers: {
        onload: function(a) {
            if (a) {
                $mjs(a).stop()
            }
            this._unbind();
            if (this.ready) {
                return
            }
            this.ready = true;
            this._cleanup();
            this.options.onload.j24(null, this).j27(1)
        },
        onabort: function(a) {
            if (a) {
                $mjs(a).stop()
            }
            this._unbind();
            this.ready = false;
            this._cleanup();
            this.options.onabort.j24(null, this).j27(1)
        },
        onerror: function(a) {
            if (a) {
                $mjs(a).stop()
            }
            this._unbind();
            this.ready = false;
            this._cleanup();
            this.options.onerror.j24(null, this).j27(1)
        }
    },
    _bind: function() {
        $mjs(["load", "abort", "error"]).j14(function(a) {
            this.img.je1(a, this._handlers["on" + a].j16(this).j28(1))
        },
        this)
    },
    _unbind: function() {
        $mjs(["load", "abort", "error"]).j14(function(a) {
            this.img.je2(a)
        },
        this)
    },
    _cleanup: function() {
        this.j7();
        if (this.img.j29("new")) {
            var a = this.img.parentNode;
            this.img.j33().j31("new").j6({
                position: "static",
                top: "auto"
            });
            a.kill()
        }
    },
    init: function(b, a) {
        this.options = $J.extend(this.options, a);
        this.img = $mjs(b) || $J.$new("img").j32($J.$new("div", null, {
            position: "absolute",
            top: -10000,
            width: 10,
            height: 10,
            overflow: "hidden"
        }).j32($J.body)).j30("new", true);
        var c = function() {
            if (this.isReady()) {
                this._handlers.onload.call(this)
            } else {
                this._handlers.onerror.call(this)
            }
            c = null
        }.j24(this);
        this._bind();
        b.src || (this.img.src = b);
        this.img && this.img.complete && (this._timer = c.j27(100))
    },
    destroy: function() {
        if (this._timer) {
            try {
                clearTimeout(this._timer)
            } catch(a) {}
            this._timer = null
        }
        this._unbind();
        this._cleanup();
        this.ready = false;
        return this
    },
    isReady: function() {
        var a = this.img;
        return (a.naturalWidth) ? (a.naturalWidth > 0) : (a.readyState) ? ("complete" == a.readyState) : a.width > 0
    },
    j7: function() {
        return this.size || (this.size = {
            width: this.img.naturalWidth || this.img.width,
            height: this.img.naturalHeight || this.img.height
        })
    }
});