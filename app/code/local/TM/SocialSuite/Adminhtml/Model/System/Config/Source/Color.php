<?php
class TM_SocialSuite_Adminhtml_Model_System_Config_Source_Color
{
     public function toOptionArray()
    {
        return array(
            array('value'=>'light', 'label'=>Mage::helper('socialsuite')->__('Light')),
            array('value'=>'dark', 'label'=>Mage::helper('socialsuite')->__('Dark'))
        );
    }
}
