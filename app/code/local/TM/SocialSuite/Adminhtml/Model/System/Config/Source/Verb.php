<?php
class TM_SocialSuite_Adminhtml_Model_System_Config_Source_Verb
{
     public function toOptionArray()
    {
        return array(
            array('value'=>'like', 'label'=>Mage::helper('socialsuite')->__('Like')),
            array('value'=>'recommend', 'label'=>Mage::helper('socialsuite')->__('Recommend'))
        );
    }
}