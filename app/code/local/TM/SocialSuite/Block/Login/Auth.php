<?php

class TM_SocialSuite_Block_Login_Auth extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getFacebookLoginUrl()
    {
        $url = Mage::getUrl('socialsuite/auth/index');
        $module = $this->getRequest()->getModuleName();
        $controller = $this->getRequest()->getControllerName();
        $action = $this->getRequest()->getActionName();
        if ($module === 'checkout' && $controller === 'onepage' && $action === 'index' ) {
            $url .= '?checkout=1';
        }
        return $url;
    }

    public function getGoogleLoginUrl()
    {
        $url = Mage::getUrl('socialsuite/auth/google');
        $module = $this->getRequest()->getModuleName();
        $controller = $this->getRequest()->getControllerName();
        $action = $this->getRequest()->getActionName();
        if ($module === 'checkout' && $controller === 'onepage' && $action === 'index' ) {
            $url .= '?checkout=1';
        }
        return $url;
    }
}