<?php
class Magestore_Groupdeal_Block_Deallist extends Mage_Catalog_Block_Product_Abstract
{	
  	const WAITING 	= 6;
  	const OPENING 	= 5;
  	const REACHED 	= 4;
  	const UNREACHED	= 3;
  	const END 		  = 2;
  	const ENABLED 	= 1;
  	const DISABLED	= 0;
  	
  	public function _prepareLayout(){
		$headBlock = $this->getLayout()->getBlock('head');
		$headBlock->setTitle($this->getTitle());
		return parent::_prepareLayout();
    }
	
    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getLoadedProductCollection()
    {
        return $this->_getProductCollection();
    }	
	
	  protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {
            $layer = Mage::getSingleton('catalog/layer');
			$store_id = Mage::app()->getStore()->getId();
			$categoryId = $this->getRequest()->getParam('category');
            $productIds = Mage::helper('groupdeal')->getActiveGroupdealProductIds($categoryId);
			
			$this->_productCollection = Mage::getResourceModel('catalog/product_collection')
											->setStoreId($this->getStoreId())
											->addFieldToFilter('entity_id',array('in'=>$productIds))
											->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
									//		->addMinimalPrice()
									//		->addTaxPercents()
											->addStoreFilter()
											->setOrder('groupdeal_featured', 'DESC')
											->setOrder('groupdeal_endtime', 'ASC');
																			
		}
		
        return $this->_productCollection;
    }
	
	public function getColumnCount(){
		return 3;
	}
	
	public function getDealUrl($productId){
		$deal = $this->getDeal($productId);
		return $deal->getDealUrl();
	}
	
	public function getDeal($productId){
		$deal = Mage::getModel('groupdeal/deal')->loadDealByProduct($productId);
		return $deal;
	}
	
	public function getOneLineTitle($title, $len){
		if(strlen($title) <= $len)
			return $title;
		else
			return substr($title, 0, $len) . '...';
	}
	
	public function getCategoryName(){
		$categoryId = $this->getRequest()->getParam('category');
		return Mage::getModel('catalog/category')->load($categoryId)->getName();
	}
	
	public function getTitle(){
		$categoryId = $this->getRequest()->getParam('category');
		$categoryName = Mage::getModel('catalog/category')->load($categoryId)->getName();
		if($categoryId)
			return Mage::helper('groupdeal')->__('Deals in ') . $categoryName;
		else
			return Mage::helper('groupdeal')->__('All Deals');
	}
    
    protected $_defaultToolbarBlock = 'catalog/product_list_toolbar';
      public function getMode()
    {
        return $this->getChild('toolbar')->getCurrentMode();
    }
    protected function _beforeToHtml()
    {
        $toolbar = $this->getToolbarBlock();

        // called prepare sortable parameters
        $collection = $this->_getProductCollection();

        // use sortable parameters
        if ($orders = $this->getAvailableOrders()) {
            $toolbar->setAvailableOrders($orders);
        }
        if ($sort = $this->getSortBy()) {
            $toolbar->setDefaultOrder($sort);
        }
        if ($dir = $this->getDefaultDirection()) {
            $toolbar->setDefaultDirection($dir);
        }
        if ($modes = $this->getModes()) {
            $toolbar->setModes($modes);
        }

        // set collection to toolbar and apply sort
        $toolbar->setCollection($collection);

        $this->setChild('toolbar', $toolbar);
        Mage::dispatchEvent('catalog_block_product_list_collection', array(
            'collection' => $this->_getProductCollection()
        ));

        $this->_getProductCollection()->load();

        return parent::_beforeToHtml();
    }

    /**
     * Retrieve Toolbar block
     *
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function getToolbarBlock()
    {
        if ($blockName = $this->getToolbarBlockName()) {
            if ($block = $this->getLayout()->getBlock($blockName)) {
                return $block;
            }
        }
        $block = $this->getLayout()->createBlock($this->_defaultToolbarBlock, microtime());
        return $block;
    }

    /**
     * Retrieve additional blocks html
     *
     * @return string
     */
    public function getAdditionalHtml()
    {
        return $this->getChildHtml('additional');
    }

    /**
     * Retrieve list toolbar HTML
     *
     * @return string
     */
    public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }
    
}