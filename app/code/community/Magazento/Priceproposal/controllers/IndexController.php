<?php

class Magazento_Priceproposal_IndexController extends Mage_Core_Controller_Front_Action {

   public function formAction() {
       $this->loadLayout();
       $this->renderLayout();
   }

   public function resultAction() {
       if ($data = $this->getRequest()->getParams()) {
           Mage::getModel('priceproposal/item')->createProposal($data);
       }
       $this->loadLayout();
       $this->renderLayout();
   }

}
?>
