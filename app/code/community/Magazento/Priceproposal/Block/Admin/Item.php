<?php
/*
 *  Created on May 3, 2011
 *  Author Ivan Proskuryakov - volgodark@gmail.com - Magazento.com
 *  Copyright Proskuryakov Ivan. Magazento.com © 2011. All Rights Reserved.
 *  Single Use, Limited Licence and Single Use No Resale Licence ["Single Use"]
 */
?>
<?php

class Magazento_Priceproposal_Block_Admin_Item extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = 'admin_item';
        $this->_blockGroup = 'priceproposal';
        $this->_headerText = Mage::helper('priceproposal')->__('Priceproposal grid');
        $this->setTemplate('widget/grid/container.phtml');
    }

}
