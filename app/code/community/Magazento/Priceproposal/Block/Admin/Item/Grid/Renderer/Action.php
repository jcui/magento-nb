<?php
/*
 *  Created on May 3, 2011
 *  Author Ivan Proskuryakov - volgodark@gmail.com - Magazento.com
 *  Copyright Proskuryakov Ivan. Magazento.com © 2011. All Rights Reserved.
 *  Single Use, Limited Licence and Single Use No Resale Licence ["Single Use"]
 */
?>
<?php


class Magazento_Priceproposal_Block_Admin_Item_Grid_Renderer_Action extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Action
{
		    public function render(Varien_Object $row)
	    {

	        $actions[] = array(
	        	'url' => $this->getUrl('*/*/seen', array('item_id' => $row->getId())),
	        	'caption' => Mage::helper('priceproposal')->__('Seen')
	         );

	         $actions[] = array(
	        	'url' => $this->getUrl('*/*/delete', array('item_id' => $row->getId())),
	        	'caption' => Mage::helper('priceproposal')->__('Delete'),
	        	'confirm' => Mage::helper('priceproposal')->__('Are you sure you want to delete this item ?')
	         );

	        $this->getColumn()->setActions($actions);

	        return parent::render($row);
	    }
}
