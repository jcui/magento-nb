<?php
/*
 *  Created on May 3, 2011
 *  Author Ivan Proskuryakov - volgodark@gmail.com - Magazento.com
 *  Copyright Proskuryakov Ivan. Magazento.com © 2011. All Rights Reserved.
 *  Single Use, Limited Licence and Single Use No Resale Licence ["Single Use"]
 */
?>
<?php

class Magazento_Priceproposal_Block_Admin_Item_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId('priceproposal_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('priceproposal')->__('Item Information'));
    }

    protected function _beforeToHtml() {
        $this->addTab('form_section_item', array(
            'label' => Mage::helper('priceproposal')->__('Item Information'),
            'title' => Mage::helper('priceproposal')->__('Item Information'),
            'content' => $this->getLayout()->createBlock('priceproposal/admin_item_edit_tab_form')->toHtml(),
        ));
//        $this->addTab('form_section_other', array(
//            'label' => Mage::helper('priceproposal')->__('Item Information additional'),
//            'title' => Mage::helper('priceproposal')->__('Item Information additional'),
//            'content' => $this->getLayout()->createBlock('priceproposal/admin_item_edit_tab_other')->toHtml(),
//        ));

        return parent::_beforeToHtml();
    }

}