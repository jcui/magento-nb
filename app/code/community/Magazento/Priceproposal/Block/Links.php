<?php

class Magazento_Priceproposal_Block_Links extends Mage_Customer_Block_Account_Navigation
{

    public function addTopLink()
    {
        if (Mage::getStoreConfig('priceproposal/topmenu/link')) {
            $storeId = Mage::app()->getStore()->getId();
            $storeUrl = Mage::getModel('core/store')->load($storeId)->getUrl();
            $route = 'javascript:aopenForm();';
            $title = Mage::getStoreConfig('priceproposal/topmenu/link_title');
            $this->getParentBlock()->addLink($title, $route, $title, false, array(), 15, null, 'class="price-feedback"');
        }

        
   }



	
}
