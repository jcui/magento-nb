<?php
/*
* Created on May 20, 2011
*  Author Ivan Proskuryakov - volgodark@gmail.com - Magazento.com
*  Copyright Proskuryakov Ivan. Magazento.com © 2011. All Rights Reserved.
*  Single Use, Limited Licence and Single Use No Resale Licence ["Single Use"]
*/
?>
<?php

$installer = $this;
$installer->startSetup();
$installer->run("
-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS `{$this->getTable('magazento_priceproposal_item')}` (
 `item_id` smallint(6) NOT NULL AUTO_INCREMENT,
 `store_id` int(11) NOT NULL DEFAULT '0',
 `product_id` int(11) NOT NULL,
 `product_name` varchar(250) NOT NULL,
 `competitior_name` varchar(100) NOT NULL,
 `competitior_price` varchar(50) NOT NULL,
 `competitior_link` tinytext NOT NULL,
 `user_email` varchar(250) NOT NULL,
 `user_skype` varchar(250) NOT NULL,
 `user_phone` varchar(250) NOT NULL,
 `comment` text NOT NULL,
 `from_time` datetime DEFAULT NULL,
 `status` tinyint(4) NOT NULL DEFAULT '0',
 PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=58 ;

INSERT INTO `{$this->getTable('magazento_priceproposal_item')}` (`item_id`, `store_id`, `product_id`, `product_name`, `competitior_name`, `competitior_price`, `competitior_link`, `user_email`, `user_skype`, `user_phone`, `comment`, `from_time`, `status`) VALUES
(57, 1, 19, 'AT&T 8525 PDA', 'Sony PS3', '500 USD', 'http://us.playstation.com/', 'magazento@gmail.com', 'VOLGODARK', ' 7 909389 2222', 'The official PlayStation resource for PlayStation 3 PS3. ... Sony makedotbelieve. PlayStation', '2011-05-03 00:41:16', 0);


-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS  `{$this->getTable('magazento_priceproposal_item_store')}` (
 `item_id` smallint(6) unsigned DEFAULT NULL,
 `store_id` smallint(6) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `{$this->getTable('magazento_priceproposal_item_store')}` (`item_id`, `store_id`) VALUES
(5, 0),
(57, 1);

");

$installer->endSetup();
?>