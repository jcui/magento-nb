<?php
/**

 */
$this->startSetup();
$this->addAttribute('catalog_product', 'sales', array(
                        'type'              => 'varchar',
                        'backend'           => '',
                        'frontend'          => '',
                        'label'             => 'Sales',
                        'input'             => 'text',
                        'frontend_class'    => 'validate-digits',
                        'source'            => '',
                        'global'            => true,
                        'unique'            => false,
                        'visible'           => true,
                        'required'          => false,
                        'user_defined'      => false,
                        'default'           => '0',
                        'searchable'        => false,
                        'filterable'        => false,
                        'comparable'        => false,                       
                        'is_html_allowed_on_front'=>true,
                        'visible_on_front'  => false,
                        'used_in_product_listing'=>true,
                        'used_for_sort_by'  =>true,
                    ));

$this->endSetup();


// EOF